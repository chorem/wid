/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

let assert = require("assert");
let helper = require("./TestHelper");
let sign = helper.sign;

let timeApi = require("../src/TimeApi");
let reportApi = require("../src/ReportApi");
let eventApi = require("../src/EventApi");

describe("Report", function() {

    it("Create", function(done) {
        let token;

        sign("JackCreateReport")
        .then((t) => {
            token = t;
            return reportApi.add({
                token,
                name: "test",
                tags: "js",
                period: 1
            });
        })
        .then((v) => {
            assert(v);
            return reportApi.getAll({token});
        })
        .then((r) => {
            assert(r.rows.length);
            done();
        });
    });

    it("Remove", function(done) {
        let token;

        sign("JackRemoveReport")
        .then((t) => {
            token = t;
            return reportApi.add({
                token,
                name: "test",
                tags: "js",
                period: 1
            });
        })
        .then(({id}) => {
            assert(id);
            return reportApi.remove({token, id});
        })
        .then((r) => {
            assert(r);
            return reportApi.getAll({token});
        })
        .then((r) => {
            assert(!r.rows.length);
            done();
        });
    });

    it("Get", function(done) {
        let token;

        sign("JackGetReport")
        .then((t) => {
            token = t;
            return reportApi.add({
                token,
                name: "test",
                tags: "wid",
                period: 1
            });
        })
        .then(({id: v}) => {
            assert(v);
            return reportApi.get({token, id: v});
        })
        .then((r) => {
            assert(r);
            done();
        });
    });

    it("Total", function(done) {
        let now = Date.now();
        let begin = now;
        let end = now + 20000;
        let token;
        let id;

        sign("JackTotalReport")
        .then((t) => {
            token = t;
            return reportApi.add({
                token,
                name: "test",
                tags: "wid",
                period: 0
            });
        })
        .then(({id: r}) => {
            assert(r);
            id = r;
            return timeApi.add({
                token,
                beginDateTime: begin,
                endDateTime: end,
                tags: ["dev", "wid"]
            });
        })
        .then((r) => {
            assert(r);
            return reportApi.getTotalTime({token, id, beginDateTime: begin, endDateTime: end});
        })
        .then((r) => {
            assert.equal(20000, r.time);
            done();
        });
    });

    it("Widget", function(done) {
        let now = Date.now();
        let begin = now;
        let end = now + 20000;
        let token;

        sign("JackWidgetReport")
        .then((t) => {
            token = t;
            return reportApi.add({
                token,
                name: "test",
                tags: "wid",
                period: 0,
                widget: 1
            });
        })
        .then(({id: r}) => {
            assert(r);
            return timeApi.add({
                token,
                beginDateTime: begin,
                endDateTime: end,
                tags: ["dev", "wid"]
            });
        })
        .then((r) => {
            assert(r);
            return reportApi.getWidgetsTime({token, beginDateTime: begin, endDateTime: end});
        })
        .then((r) => {
            assert.equal(20000, r.widgets[0].time);
            done();
        });
    });

    it("Details", function(done) {
        let now = Date.now();
        let begin = now;
        let end = now + 20000;
        let token;
        let id;

        sign("JackDetailsReport")
        .then((t) => {
            token = t;
            return reportApi.add({
                token,
                name: "test",
                tags: "wid",
                period: 0,
                widget: 0
            });
        })
        .then(({id: r}) => {
            assert(r);
            id = r;
            return timeApi.add({
                token,
                beginDateTime: begin,
                endDateTime: end,
                tags: ["dev", "wid"]
            });
        })
        .then((r) => {
            assert(r);
            return reportApi.getDetailsTime({token, id, beginDateTime: begin, endDateTime: end});
        })
        .then((r) => {
            assert.equal(20000, r.total);
            assert(r.details.length);
            done();
        });
    });

    it("ChartDuration", function(done) {
        let now = Date.now();
        let begin = now;
        let end = now + 20000;
        let token;

        sign("JackChartDuration")
        .then((t) => {
            token = t;
            return timeApi.add({
                token,
                beginDateTime: begin,
                endDateTime: end,
                tags: ["dev", "wid"]
            });
        })
        .then((r) => {
            assert(r);
            return reportApi.getChartDuration({token, period: 3, query: "wid", beginDateTime: begin, endDateTime: end});
        })
        .then((r) => {
            assert.equal(20000, r.rows[0].time);
            done();
        });
    });

    it("ChartProject", function(done) {
        let now = Date.now();
        let begin = now;
        let end = now + 20000;
        let token;

        sign("JackChartCodeProject")
        .then((t) => {
            token = t;
            return timeApi.add({
                token,
                beginDateTime: begin,
                endDateTime: end,
                tags: ["dev", "wid"]
            });
        })
        .then((r) => {
            assert(r);
            return eventApi.add({
                token,
                event: {
                    beginDateTime: begin,
                    endDateTime: begin,
                    type: "code",
                    source: "test",
                    data: {
                        project: "wid"
                    }
                }
            });
        })
        .then((r) => {
            assert(r);
            return reportApi.getChartCodeProject({token, period: 3, query: "wid", beginDateTime: begin, endDateTime: end});
        })
        .then((r) => {
            assert.equal(0, r.rows[0].duration);
            assert.equal("wid", r.rows[0].project);
            done();
        });
    });

    it("ChartCodeLanguage", function(done) {
        let now = Date.now();
        let begin = now;
        let end = now + 20000;
        let token;

        sign("JackChartCodeLanguage")
        .then((t) => {
            token = t;
            return timeApi.add({
                token,
                beginDateTime: begin,
                endDateTime: end,
                tags: ["dev", "wid"]
            });
        })
        .then((r) => {
            assert(r);
            return eventApi.add({
                token,
                event: {
                    beginDateTime: begin,
                    endDateTime: begin,
                    type: "code",
                    source: "test",
                    data: {
                        language: "js"
                    }
                }
            });
        })
        .then((r) => {
            assert(r);
            return reportApi.getChartCodeLanguage({token, period: 3, query: "wid", beginDateTime: begin, endDateTime: end});
        })
        .then((r) => {
            assert.equal(1, r.rows[0].total);
            assert.equal("js", r.rows[0].language);
            done();
        });
    });

});

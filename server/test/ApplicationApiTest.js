/* eslint strict: 0 */
/* eslint camelcase: 0 */
// Needed in Node in order to use let
"use strict";

let assert = require("assert");
let helper = require("./TestHelper");
let sign = helper.sign;

let eventApi = require("../src/EventApi");
let applicationApi = require("../src/ApplicationApi");

describe("Application", function() {

    it("Create identity", function(done) {
        let token;

        sign("JackCreateIdentity")
        .then((t) => {
            token = t;
            return applicationApi.createIdentity({token, identifier: "JackCreateIdentity"});
        })
        .then((v) => {
            assert(v);
            return applicationApi.getIdentities({token});
        })
        .then((r) => {
            assert(r.rows.length);
            done();
        });
    });

    it("Remove identity", function(done) {
        let token;

        sign("JackRemoveIdentity")
        .then((t) => {
            token = t;
            return applicationApi.createIdentity({token, identifier: "JackRemoveIdentity"});
        })
        .then((v) => {
            assert(v);
            return applicationApi.removeIdentity({token, id: v.id});
        })
        .then((v) => {
            assert(v);
            return applicationApi.getIdentities({token});
        })
        .then((r) => {
            assert(r.rows.length);
            done();
        });
    });

    it("Create key", function(done) {
        let token;

        sign("JackCreateKey")
        .then((t) => {
            token = t;
            return applicationApi.createKey({token, name: "calendarCreate"});
        })
        .then((v) => {
            assert(v);
            return applicationApi.getKeys({token});
        })
        .then((r) => {
            assert(r.rows.length);
            done();
        });
    });

    it("Remove key", function(done) {
        let token;

        sign("JackRemoveKey")
        .then((t) => {
            token = t;
            return applicationApi.createKey({token, name: "calendarRemove"});
        })
        .then((v) => {
            assert(v);
            return applicationApi.removeKey({token, id: v.id});
        })
        .then((v) => {
            assert(v);
            return applicationApi.getKeys({token});
        })
        .then((r) => {
            assert(!r.rows.length);
            done();
        });
    });

    it("Authorize", function(done) {
        let token;

        sign("JackAuthorizeKey")
        .then((t) => {
            token = t;
            return applicationApi.createKey({token, name: "calendarAuthorize"});
        })
        .then((v) => {
            assert(v);
            return applicationApi.isAutorize({key: v.key});
        })
        .then(() => {
            done();
        });
    });

    it("No authorize", function(done) {
        applicationApi.isAutorize({key: "invalid"}).catch(done);
    });

    it("GitLab WebHook", function(done) {
        let token;

        sign("JackGitLab", true)
        .then((t) => {
            token = t;
            return applicationApi.createKey({token, name: "gitlab-test"});
        })
        .then((r) => {
            assert(r);

            return applicationApi.onGitLabWebHook({
                key: r.key,
                data: {
                    user_email: "JackGitLab@",
                    project: {
                        name: "projet-name"
                    },
                    commits: [
                        {
                            id: 1,
                            timestamp: "2011-12-12T14:27:31+02:00",
                            author: {email: "JackGitLab@"},
                            message: "test #3435"
                        },
                        {
                            id: 1,
                            timestamp: "2011-12-12T14:27:31+02:00",
                            author: {email: "JackGitLab@"},
                            message: "other test"
                        }
                    ]
                }
            });
        })
        .then((v) => {
            assert(v);
            return eventApi.getAll({token});
        })
        .then((r) => {
            assert(r.rows.length);
            done();
        });
    });

    it("GitHub WebHook", function(done) {
        let token;

        sign("JackGitHub", true)
        .then((t) => {
            token = t;
            return applicationApi.createKey({token, name: "github-test"});
        })
        .then((r) => {
            assert(r);

            return applicationApi.onGitHubWebHook({
                key: r.key,
                data: {
                    commits: [
                        {
                            author: {
                                email: "JackGitHub@"
                            },
                            message: "test #3435"
                        },
                        {
                            author: {
                                email: "JackGitHub@"
                            },
                            message: "other test"
                        }
                    ],
                    repository: {
                        name: "projet-name"
                    }
                }
            });
        })
        .then((v) => {
            assert(v);
            return eventApi.getAll({token});
        })
        .then((r) => {
            assert(r.rows.length);
            done();
        });
    });

    it("Events WebHook", function(done) {
        let token;

        sign("JackEvents")
        .then((t) => {
            token = t;
            return applicationApi.createKey({token, name: "events-test"});
        })
        .then((r) => {
            assert(r);

            return applicationApi.onEventsWebHook({
                key: r.key,
                data: [{
                    beginDateTime: Date.now(),
                    endDateTime: Date.now(),
                    type: "test",
                    source: "test",
                    data: "test"
                },
                {
                    beginDateTime: Date.now(),
                    endDateTime: Date.now(),
                    type: "test2",
                    source: "test",
                    data: "test2"
                }]
            });
        })
        .then((v) => {
            assert(v);
            return eventApi.getAll({token});
        })
        .then((r) => {
            assert(r.rows.length);
            done();
        });
    });

    it("Wakatime WebHook", function(done) {
        let token;

        sign("JackWakatime")
        .then((t) => {
            token = t;
            return applicationApi.createKey({token, name: "wakatime-test"});
        })
        .then((r) => {
            assert(r);

            return applicationApi.onWakatimeWebHook({
                headers: {
                    "user-agent": "test test",
                    "x-machine-name": "mocha"
                },
                key: r.key,
                data: {
                    project: "wid",
                    is_write: true,
                    language: "JavaScript",
                    branch: "master",
                    time: 1470053171.8685114,
                    entity: "/tmp/wid/src/js/utils/AggregateUtils.js",
                    type: "file",
                    lines: 308
                }
            });
        })
        .then((v) => {
            assert(v);
            return eventApi.getAll({token});
        })
        .then((r) => {
            assert(r.rows.length);
            done();
        });
    });
});

const json = require("./data/data.json");

const learningjs = require("c4.5_with_random_forest");
const data_util = require("c4.5_with_random_forest/data_util");

const label = "tags";

let D = { data: [], targets: [], s_targets: {} };
let T = { data: [], targets: [], s_targets: {} };

json.forEach((line, index) => {
    let header = Object.keys(line);

    if (index === 0) {
        D.nfeatures = T.nfeatures = header.length - 1;
        D.l_featuresIndex = T.l_featuresIndex = [];
        D.featureNames = T.featureNames = [];
        D.feature_name2id = T.feature_name2id = [];
        D.featuresType = T.featuresType = [];

        header.forEach((name, i) => {
            if (name !== "tags") {
                D.l_featuresIndex.push(i - 1);
                D.featureNames.push(name);
                D.feature_name2id[name] = i - 1;
                D.featuresType.push("text");
            }
        });
    }

    let data = [];
    header.forEach((name) => {
        name !== label && data.push(line[name]);
    });

    // let R = index >= json.length / 2 ? T : D;
    let R = index % 4 === 0 ? T : D;
    R.data.push(data);

    let target = line[label];
    R.s_targets[target] = 1;

    R.targets.push(target);
});

D.l_targets = Object.keys(D.s_targets);
D.ntargets = D.l_targets.length;

T.l_targets = Object.keys(T.s_targets);
T.ntargets = T.l_targets.length;

//normalize data
// data_util.normalize(D.data, D.nfeatures);

//logistic regression. following params are optional
D.optimizer = 'sgd'; //default choice. other choice is 'gd'
D.learning_rate = 0.005;
D.l2_weight = 0.000001;
D.iterations = 100000; //increase number of iterations for better performance

console.log("================= [C4.5] ===================");
var tree = new learningjs.tree();

console.log("--- Decision Tree ----");
tree.train(D, function(model, err) {
    if (err) {
        console.log(err);
    } else {
        model.calcAccuracy(D.data, D.targets, function(acc, correct, total){
            console.log('training:', correct, '/', total, '=', (acc*100.0).toFixed(2)+'%');
        });
        model.calcAccuracy(T.data, T.targets, function(acc, correct, total){
            console.log('    test:', correct, '/', total, '=', (acc*100.0).toFixed(2)+'%');
        });
    }
});
console.log("--- Random Forest ----");
var randomForest = new learningjs.randomForest();
randomForest.train(D, 10, function(model, err) {
    if (err) {
        console.log(err);
    } else {
        model.calcAccuracy(D.data, D.targets, function(firstInAcc, existInAcc, correct, exist, total){
            console.log('training : ');
            console.log('  exists :', exist, '/', total, '=', (existInAcc*100.0).toFixed(2)+'%');
            console.log('  first in :', correct, '/', total, '=', +(firstInAcc*100.0).toFixed(2)+'%');
        });
        model.calcAccuracy(T.data, T.targets, function(firstInAcc, existInAcc, correct, exist, total) {
            console.log('test : ');
            console.log('  exists :', exist, '/', total, '=', (existInAcc*100.0).toFixed(2)+'%');
            console.log('  first in :', correct, '/', total, '=', +(firstInAcc*100.0).toFixed(2)+'%');
        });
    }
});

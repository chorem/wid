const dt = require("./decision-tree.js");
const RandomForest = require("./randomForestThroughTime.js");

// Set it to true if you want to use a forest with build tree according to time (1 tree on last year activities, 1 tree on last  6 month,...)
const USE_TIME_FOREST = false;

// Training set
// Activity ["dayInWeek","durationHours","hoursInDay","typeDev","idle","projects"];
const data = require("./data/data.json");

const trainingSet = [];
const testSet = [];
for (let i = 0, l = data.length; i < l; i++) {
    if (i > 6 * l / 7) {
        testSet.push(data[i]);
    } else {
        trainingSet.push(data[i]);
    }
}

// data.splice(0, data.length / 2);
// const trainingSet = data.splice(0, 4 * data.length / 5);

// Configuration
const config = {
    trainingSet: trainingSet,
    categoryAttr: "tags",
    ignoredAttributes: []
};

// Building Decision Tree
const decisionTree = new dt.DecisionTree(config);

// Building Random Forest
const numberOfTrees = 10;
let randomForest = new dt.RandomForest(config, numberOfTrees);
if (USE_TIME_FOREST) {
    randomForest = new RandomForest(trainingSet, set => {
        config.trainingSet = set;
        return new dt.DecisionTree(config);
    });
}

// Testing Decision Tree and Random Forest
const counter = {
    decisionTreePrediction: 0,
    existsInRandomForestPrediction: 0,
    firstInRandomForestPrediction: 0
};

let randomForestPrediction;
if (USE_TIME_FOREST) {
    randomForestPrediction = randomForest.predict(testSet);
}

testSet.forEach((d, index) => {
    const activity = d.tags;
    delete d.tags;

    const decisionTreePrediction = decisionTree.predict(d);

    if (!USE_TIME_FOREST) {
        randomForestPrediction = randomForest.predict(d);
    }

    // console.log("-------------------------------");
    // console.log("Expecting `", activity, "` / Found", decisionTreePrediction);
    if (decisionTreePrediction === activity) {
        // console.log("Exact match :", activity);
        counter.decisionTreePrediction++;
    }

    let currentRandomForestItem = randomForestPrediction;
    if (USE_TIME_FOREST) {
        currentRandomForestItem = randomForestPrediction[index];
    }

    const value = currentRandomForestItem[activity];
    if (value) {
        counter.existsInRandomForestPrediction++;
        // console.log("Exist in Forest :", activity);

        if (value === Math.max(...Object.values(currentRandomForestItem))) {
            counter.firstInRandomForestPrediction++;
        }
    }

    // console.log("---------------------------------");
    // console.log("Expecting `", activity, "`");
    // console.log("    Decision tree >> ", decisionTreePrediction);
    // console.log("    Forest >> ", randomForestPrediction[index]);
});

console.log("==================== [ID3] =====================");
console.log("Final result :");
console.log(` Decision tree : ${counter.decisionTreePrediction} / ${testSet.length} = ${(counter.decisionTreePrediction / testSet.length * 100).toFixed(3)}%`);
console.log(` RandomForest / exists : ${counter.existsInRandomForestPrediction} / ${testSet.length} = ${(counter.existsInRandomForestPrediction / testSet.length * 100).toFixed(3)}%`);
console.log(` RandomForest / firstIn : ${counter.firstInRandomForestPrediction} / ${testSet.length} = ${(counter.firstInRandomForestPrediction / testSet.length * 100).toFixed(3)}%`);



// Recursive (DFS) function for displaying inner structure of decision tree
function treeToHtml(tree) {
    if (!tree) return;

    // only leafs containing category
    if (tree.category) {
        return  ["<ul>",
                    "<li>",
                        "<a href='#'>",
                            "<b>", tree.category, "</b>",
                        "</a>",
                    "</li>",
                 "</ul>"].join("");
    }

    return ["<ul>",
                "<li>",
                    "<a href='#'>",
                        "<b>", tree.attribute, " ", tree.predicateName, " ", tree.pivot, " ?</b>",
                    "</a>",
                    "<ul>",
                        "<li>",
                            "<a href='#'>yes</a>",
                            treeToHtml(tree.match),
                        "</li>",
                        "<li>",
                            "<a href='#'>no</a>",
                            treeToHtml(tree.notMatch),
                        "</li>",
                    "</ul>",
                "</li>",
             "</ul>"].join("");
}

const fs = require("fs");
fs.writeFile(
    "results.html",
    `


    <div class="tree" id="displayTree">${treeToHtml(decisionTree.root)}</div>

    /*
 Transforming nested lists to pretty tree

 <div class="tree">
    <ul>
        <li>
            <ul>
            ...
            </ul>
        </li>
        ...
    </ul>
 </div>

Source: http://thecodeplayer.com/walkthrough/css3-family-tree

Some other advices about displaying trees: http://stackoverflow.com/questions/1695115/how-do-i-draw-the-lines-of-a-family-tree-using-html-css
*/
    <style>
    * {
        margin: 0;
        padding: 0;
    }

    .tree ul {
    	padding-top: 20px;
        position: relative;

    	transition: all 0.5s;
    	-webkit-transition: all 0.5s;
    	-moz-transition: all 0.5s;
    }

    .tree li {
        white-space: nowrap;
    	float: left;
        text-align: center;
    	list-style-type: none;
    	position: relative;
    	padding: 20px 5px 0 5px;

    	transition: all 0.5s;
    	-webkit-transition: all 0.5s;
    	-moz-transition: all 0.5s;
    }

    /*We will use ::before and ::after to draw the connectors*/

    .tree li::before, .tree li::after{
    	content: '';
    	position: absolute;
        top: 0;
        right: 50%;
    	border-top: 1px solid #ccc;
    	width: 50%;
        height: 20px;
    }
    .tree li::after{
    	right: auto;
        left: 50%;
    	border-left: 1px solid #ccc;
    }

    /*We need to remove left-right connectors from elements without
     any siblings*/
    .tree li:only-child::after, .tree li:only-child::before {
    	display: none;
    }

    /*Remove space from the top of single children*/
    .tree li:only-child{
        padding-top: 0;
    }

    /*Remove left connector from first child and
     right connector from last child*/
    .tree li:first-child::before, .tree li:last-child::after{
    	border: 0 none;
    }
    /*Adding back the vertical connector to the last nodes*/
    .tree li:last-child::before{
    	border-right: 1px solid #ccc;
    	border-radius: 0 5px 0 0;
    	-webkit-border-radius: 0 5px 0 0;
    	-moz-border-radius: 0 5px 0 0;
    }
    .tree li:first-child::after{
    	border-radius: 5px 0 0 0;
    	-webkit-border-radius: 5px 0 0 0;
    	-moz-border-radius: 5px 0 0 0;
    }

    /*Time to add downward connectors from parents*/
    .tree ul ul::before{
    	content: '';
    	position: absolute;
        top: 0;
        left: 50%;
    	border-left: 1px solid #ccc;
    	width: 0;
        height: 20px;
    }

    .tree li a{
    	border: 1px solid #ccc;
    	padding: 5px 10px;
    	text-decoration: none;
    	color: #666;
    	font-family: arial, verdana, tahoma;
    	font-size: 11px;
    	display: inline-block;

    	border-radius: 5px;
    	-webkit-border-radius: 5px;
    	-moz-border-radius: 5px;

    	transition: all 0.5s;
    	-webkit-transition: all 0.5s;
    	-moz-transition: all 0.5s;
    }

    /*Time for some hover effects*/
    /*We will apply the hover effect the the lineage of the element also*/
    .tree li a:hover, .tree li a:hover+ul li a {
    	background: #c8e4f8;
        color: #000;
        border: 1px solid #94a0b4;
    }
    /*Connector styles on hover*/
    .tree li a
    </style>
    `,
    function (err) {
        if (err) return console.log(err);
    }
);

// Displaying predictions
// document.getElementById("testingItem").innerHTML = JSON.stringify(newActivity, null, 0);
// document.getElementById("decisionTreePrediction").innerHTML = JSON.stringify(decisionTreePrediction, null, 0);
// document.getElementById("randomForestPrediction").innerHTML = JSON.stringify(randomForestPrediction, null, 0);

// Displaying Decision Tree
// document.getElementById("displayTree").innerHTML = treeToHtml(decisionTree.root);

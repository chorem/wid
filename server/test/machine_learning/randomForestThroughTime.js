const CLASSIC_TREES_COUNT = 6;

/**
 * Building array of decision trees
 */
const buildRandomForest = (trainingSet, treeBuilder) => {

    const treesCount = Math.min(CLASSIC_TREES_COUNT, trainingSet.length);

    // creating training sets for each tree
    const trainingSets = [];

    const classicTrees = Array.apply(null, Array(treesCount)).map(() => []);

    // 1 tree only on the data of the most recent year
    const lastYearTree = [];

    // 1 tree only on the data of the last 6 month
    const lastSixMonthTree = [];

    // 1 tree only on the data of the last 3 month
    const lastThreeMonthTree = [];

    // 1 tree only on the data of the current month
    const lastMonthTree = [];

    const currentDate = new Date(),
        currentYear = currentDate.getFullYear(),
        currentMonth = currentDate.getMonth() + 1;

    for (let i = 0, l = trainingSet.length; i < l; i++) {
        const activity = trainingSet[i];

        const correspondingTree = i % treesCount;
        classicTrees[correspondingTree].push(activity);

        const activityYear = +activity.year,
            activityMonth = +activity.month;

        const isInMonthPeriod = monthFromNow => {
            const monthDelta = currentMonth - monthFromNow;
            return activityYear === currentYear && activityMonth >= monthDelta
                || activityYear === currentYear - 1 && activityMonth >= 12 - monthDelta;
        };

        // last year
        if (activityYear === currentYear
            || activityYear === currentYear - 1 && activityMonth >= currentMonth) {
            lastYearTree.push(activity);

            // last 6 month
            if (isInMonthPeriod(6)) {
                lastSixMonthTree.push(activity);
            }

            // last 3 month
            if (isInMonthPeriod(3)) {
                lastThreeMonthTree.push(activity);
            }

            // last month
            if (activityYear === currentYear && activityMonth === currentMonth) {
                lastMonthTree.push(activity);
            }
        }
    }

    trainingSets.push(...classicTrees);
    lastYearTree.length && trainingSets.push(lastYearTree);
    lastSixMonthTree.length && trainingSets.push(lastSixMonthTree);
    lastThreeMonthTree.length && trainingSets.push(lastThreeMonthTree);
    lastMonthTree.length && trainingSets.push(lastMonthTree);

    // building decision trees
    const forest = [];
    for (let t = 0; t < trainingSets.length; t++) {
        let tree = treeBuilder(trainingSets[t]);
        forest.push(tree);
    }

    return forest;
};

/**
* Creates an instance of RandomForest
* with specific number of trees
*/
class RandomForest {
    constructor(trainingSet, treeBuilder) {
        this.trees = buildRandomForest(trainingSet, treeBuilder);
    }

    predict(testSet) {
        const result = [];
        for (let i = 0, l = testSet.length; i < l; i++) {
            const itemResults = {};
            result.push(itemResults);

            const item = testSet[i];
            for (let j = 0, m = this.trees.length; j < m; j++) {
                const tree = this.trees[j];
                const prediction = tree.predict(item);
                itemResults[prediction] = itemResults[prediction] ? itemResults[prediction] + 1 : 1;
            }
        }
        return result;
    }
}

module.exports = RandomForest;

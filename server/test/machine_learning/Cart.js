const {DecisionTreeClassifier, DecisionTreeRegression} = require("ml-cart");
const data = require("./data/data-cart.json");

console.log("==============================================");
console.log("        Cart decision tree classifier");
console.log("==============================================");

const getDataset = () => data.slice();
const getNumbers = () => data.map(d => d.slice(1, 8));
const getClasses = () => data.map(d => d[0]);
const getDistinctClasses = () => {
    const result = {};
    getClasses().forEach(classe => {
        result[classe] = true;
    });
    return Object.keys(result);
};

const trainingSet = getNumbers();
const distinctClasses = getDistinctClasses();
const predictions = getClasses().map(
    (elem) => distinctClasses.indexOf(elem)
);

const options = {
    gainFunction: "gini",
    maxDepth: 100,
    minNumSamples: 4
};

const classifier = new DecisionTreeClassifier(options);
classifier.train(trainingSet, predictions);

// Activity ["dayInWeek","durationHours","hoursInDay","typeDev","idle","projects"];
const newActivity = ["1", 3, 10, 1, 0, "sispea"];
const result = classifier.predict([newActivity]);

console.log("Found : ", distinctClasses[result[0]]);

/*
console.log("==============================================")
console.log("        Cart decision tree regression")
console.log("==============================================")

console.log("Preparing data");
const t = Date.now();
const x = data.map(activityDesc => activityDesc.slice(1, 8));
const y = data.map(activityDesc => activityDesc.slice(0, 1));
console.log("Preparing data took", Date.now() - t);

const reg = new DecisionTreeRegression();
reg.train(x, y);

console.log("Traingin ok");

const estimations = reg.predict(x);
console.log("Searching : ");
console.log("Found : ", estimations);
*/

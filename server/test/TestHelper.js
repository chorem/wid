/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

global.dev = true;

let userApi = require("../src/UserApi");
let applicationApi = require("../src/ApplicationApi");

module.exports.sign = (name, validate) => {
    let identity;
    let t;

    return userApi.signUp({
        username: name,
        email: name + "@",
        passwd: name
    })
    .then((i) => {
        identity = i;
    })
    .then(userApi.signIn.bind(null, {username: name, passwd: name}))
    .then(({token}) => {
        t = token;

        if (validate) {
            return applicationApi.validateIdentity({token, validation: identity.validation});
        }

        return null;
    })
    .then(() => {
        return t;
    });
};

/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

let assert = require("assert");
let helper = require("./TestHelper");
let sign = helper.sign;

let timeApi = require("../src/TimeApi");
let timebundleApi = require("../src/TimebundleApi");

describe("Timebundle", function() {

    it("Add", function(done) {
        let token;

        sign("JackAddTimebundle")
        .then((t) => {
            token = t;
            return timebundleApi.add({
                token,
                projectUrl: "http://projectUrl1",
                taskUrl: "http://taskUrl1",
                query: "test"
            });
        })
        .then((v) => {
            assert(v);
            return timebundleApi.getTasks({token, projectUrl: "http://projectUrl1"});
        })
        .then((r) => {
            assert(r.rows.length);
            done();
        });
    });

    it("Save", function(done) {
        let token;

        sign("JackSaveTimebundle")
        .then((t) => {
            token = t;
            return timebundleApi.add({
                token,
                projectUrl: "http://projectUrl1",
                taskUrl: "http://taskUrl1",
                query: "test"
            });
        })
        .then((v) => {
            assert(v);
            return timebundleApi.save({
                token,
                id: v.id,
                query: "try"
            });
        })
        .then((v) => {
            assert(v);
            return timebundleApi.getTasks({token, projectUrl: "http://projectUrl1"});
        })
        .then((r) => {
            assert(r.rows.length);
            assert.equal("try", r.rows[0].query);
            done();
        });
    });

    it("Remove", function(done) {
        let token;

        sign("JackRemoveTimebundle")
        .then((t) => {
            token = t;
            return timebundleApi.add({
                token,
                projectUrl: "http://projectUrl1",
                taskUrl: "http://taskUrl1",
                query: "test"
            });
        })
        .then((v) => {
            assert(v);
            return timebundleApi.remove({token, id: v.id});
        })
        .then((v) => {
            assert(v);
            return timebundleApi.getTasks({token, projectUrl: "http://projectUrl1"});
        })
        .then((r) => {
            assert(!r.rows.length);
            done();
        });
    });

    it("UpdateLastSync", function(done) {
        let token;
        let now = Date.now();
        let last = now + 3000;

        sign("JackUpdateLastSyncTimebundle")
        .then((t) => {
            token = t;
            return timebundleApi.add({
                token,
                projectUrl: "http://projectUrl1",
                taskUrl: "http://taskUrl1",
                query: "test"
            });
        })
        .then((v) => {
            assert(v);
            return timebundleApi.updateTaskSyncDate({token, taskUrl: "http://taskUrl1", now: last});
        })
        .then((v) => {
            assert(v);
            return timebundleApi.updateLastSync({token, refDate: now});
        })
        .then((v) => {
            assert(v);
            return timebundleApi.getSyncTasks({token});
        })
        .then((r) => {
            assert.equal(r.rows[0].lastSync, now);
            done();
        });
    });

    it("Projects", function(done) {
        let token;

        sign("JackGetProjectsTimebundle")
        .then((t) => {
            token = t;
            return timebundleApi.add({
                token,
                projectUrl: "http://projectUrl1",
                taskUrl: "http://taskUrl1",
                query: "test"
            });
        })
        .then((v) => {
            assert(v);
            return timebundleApi.add({
                token,
                projectUrl: "http://projectUrl2",
                taskUrl: "http://taskUrl2",
                query: "test"
            });
        })
        .then((v) => {
            assert(v);
            return timebundleApi.getProjects({token});
        })
        .then((r) => {
            assert.equal(2, r.rows.length);
            done();
        });
    });

    it("SyncTasks", function(done) {
        let token;

        sign("JackSyncTasksTimebundle")
        .then((t) => {
            token = t;
            return timebundleApi.add({
                token,
                projectUrl: "http://projectUrl1",
                taskUrl: "http://taskUrl1",
                query: "test"
            });
        })
        .then((v) => {
            assert(v);
            return timebundleApi.getSyncTasks({token});
        })
        .then((r) => {
            assert(r.rows.length);
            done();
        });
    });

    it("JsonForTask", function(done) {
        let begin = Date.now();
        let end = begin + 3000;
        let token;

        sign("JackJsonForTaskTimebundle")
        .then((t) => {
            token = t;
            return timebundleApi.add({
                token,
                projectUrl: "http://projectUrl1",
                taskUrl: "http://taskUrl1",
                query: "test"
            });
        })
        .then((r) => {
            assert(r);
            return timeApi.add({
                token,
                beginDateTime: begin,
                endDateTime: end,
                tags: ["js", "dev", "wid"]
            });
        })
        .then((r) => {
            assert(r);
            return timebundleApi.getJsonForTask({token, now: begin, taskUrl: "http://taskUrl1", query: "wid", lastSync: 0});
        })
        .then((r) => {
            assert(r.periods);
            done();
        });
    });

    it("UpdateTaskSyncDate", function(done) {
        let token;
        let now = Date.now();
        let last = now + 3000;

        sign("JackUpdateTaskSyncDateTimebundle")
        .then((t) => {
            token = t;
            return timebundleApi.add({
                token,
                projectUrl: "http://projectUrl1",
                taskUrl: "http://taskUrl1",
                query: "test"
            });
        })
        .then((v) => {
            assert(v);
            return timebundleApi.updateTaskSyncDate({token, taskUrl: "http://taskUrl1", now: last});
        })
        .then((v) => {
            assert(v);
            return timebundleApi.getSyncTasks({token});
        })
        .then((r) => {
            assert.equal(r.rows[0].lastSync, last);
            done();
        });
    });

});

/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

let assert = require("assert");
let helper = require("./TestHelper");
let sign = helper.sign;

let userApi = require("../src/UserApi");

describe("User", function() {

    it("Create a user", function(done) {
        userApi.signUp({
            username: "JackCreate",
            email: "JackCreate@",
            passwd: "test"
        })
        .then((r) => {
            assert(r);
            done();
        });
    });

    it("Login a user", function(done) {
        userApi.signUp({
            username: "JackLogin",
            email: "JackLogin@",
            passwd: "test"
        })
        .then(userApi.signIn.bind(null, {username: "JackLogin", passwd: "test"}))
        .then((r) => {
            assert(r);
            done();
        });
    });

    it("Change password", function(done) {
        let token;

        sign("JackChangePassword")
        .then((t) => {
            token = t;
            return userApi.changePassword({token, oldPasswd: "JackChangePassword", newPasswd: "test"});
        })
        .then(userApi.signIn.bind(null, {username: "JackChangePassword", passwd: "test"}))
        .then((r) => {
            assert(r);
            done();
        });
    });


    it("Generate password", function(done) {
        sign("JackGeneratePassword")
        .then(() => {
            return userApi.generatePassword({email: "JackGeneratePassword@"});
        })
        .then((newPasswd) => {
            return userApi.signIn({username: "JackGeneratePassword", passwd: newPasswd});
        })
        .then((r) => {
            assert(r);
            done();
        });
    });

    it("Get a user", function(done) {
        sign("JackGetUser")
        .then((t) => {
            return userApi.getUser({token: t});
        })
        .then((r) => {
            assert(r);
            done();
        });
    });

    it("False login", function(done) {
        userApi.signIn({username: "JackNoLogin", passwd: "test"})
        .catch(() => {
            done();
        });
    });
});

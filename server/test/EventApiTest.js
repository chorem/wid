/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

let assert = require("assert");
let helper = require("./TestHelper");
let sign = helper.sign;

let eventApi = require("../src/EventApi");
let timeApi = require("../src/TimeApi");

describe("Event", function() {

    it("Add", function(done) {
        let begin = Date.now();
        let end = begin + 20000;
        let token;

        sign("JackAddEvent")
        .then((t) => {
            token = t;
            return eventApi.add({
                token,
                event: {
                    beginDateTime: begin,
                    endDateTime: end,
                    type: "test",
                    source: "test"
                }
            });
        })
        .then((v) => {
            assert(v);
            return eventApi.getAll({token, beginDateTime: begin});
        })
        .then((r) => {
            assert(r.rows.length);
            done();
        });
    });

    it("AddAll", function(done) {
        let begin = Date.now();
        let end = begin + 20000;
        let token;

        sign("JackAddAllEvent")
        .then((t) => {
            token = t;
            return eventApi.addAll({
                token,
                events: [{
                    beginDateTime: begin,
                    endDateTime: end,
                    type: "test",
                    source: "test",
                    data: {source: "test"}
                },
                {
                    beginDateTime: begin,
                    endDateTime: end,
                    type: "test",
                    source: "test",
                    data: {source: "test2"}
                }]
            });
        })
        .then((v) => {
            assert(v);
            return eventApi.getAll({token, beginDateTime: begin});
        })
        .then((r) => {
            assert(r.rows.length);
            done();
        });
    });

    it("Remove", function(done) {
        let begin = Date.now();
        let end = begin + 20000;
        let token;

        sign("JackRemoveEvent")
        .then((t) => {
            token = t;
            return eventApi.add({
                token,
                event: {
                    beginDateTime: begin,
                    endDateTime: end,
                    type: "test",
                    source: "test"
                }
            });
        })
        .then((v) => {
            assert(v);
            return eventApi.remove({token, id: v.id});
        })
        .then((v) => {
            assert(v);
            return eventApi.getAll({token, beginDateTime: begin});
        })
        .then((r) => {
            assert(!r.rows.length);
            done();
        });
    });

    it("By type", function(done) {
        let begin = Date.now();
        let end = begin + 20000;
        let token;

        sign("JackType")
        .then((t) => {
            token = t;
            return eventApi.add({
                token,
                event: {
                    beginDateTime: begin,
                    endDateTime: end,
                    type: "test",
                    source: "test"
                }
            });
        })
        .then((v) => {
            assert(v);
            return eventApi.add({
                token,
                event: {
                    beginDateTime: begin,
                    endDateTime: end,
                    type: "other",
                    source: "test"
                }
            });
        })
        .then((v) => {
            assert(v);
            return timeApi.add({
                token,
                beginDateTime: begin,
                endDateTime: end,
                tags: ["test"]
            });
        })
        .then((v) => {
            assert(v);
            return eventApi.getByType({token, type: "test"});
        })
        .then((r) => {
            assert.equal(1, r.rows.length);
            done();
        });
    });

    it("Mark", function(done) {
        let begin = Date.now();
        let end = begin + 20000;
        let token;

        sign("JackEventMark")
        .then((t) => {
            token = t;
            return eventApi.add({
                token,
                event: {
                    beginDateTime: begin,
                    endDateTime: end,
                    type: "mark",
                    source: "test",
                    data: {
                        tags: ["wid", "mark"],
                        action: "start"
                    }
                }
            });
        })
        .then((v) => {
            assert(v);
            return eventApi.getAll({token, beginDateTime: begin});
        })
        .then((r) => {
            assert(r.rows.length);
        })
        .then(() => {
            return timeApi.getAll({token});
        })
        .then((r) => {
            assert(r.rows.length);
            done();
        });
    });

    it("Simple mark", function(done) {
        let begin = Date.now();
        let token;

        sign("JackEventSimpleMark")
        .then((t) => {
            token = t;
            return eventApi.add({
                token,
                event: {
                    beginDateTime: begin,
                    endDateTime: begin,
                    type: "mark",
                    source: "test",
                    data: {
                        tags: ["wid", "mark"],
                        action: "start"
                    }
                }
            });
        })
        .then((v) => {
            assert(v);
            return eventApi.getAll({token, beginDateTime: begin});
        })
        .then((r) => {
            assert(r.rows.length);
        })
        .then(() => {
            return timeApi.getAll({token});
        })
        .then((r) => {
            assert(!r.rows.length);
            done();
        });
    });
});

/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

/**
 * Represents an error when the user is not authentificated.
 * @type {UnauthorizedError}
 */
module.exports = class UnauthorizedError {
    constructor(msg) {
        this.msg = msg;
    }
};

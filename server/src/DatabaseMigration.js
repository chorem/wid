/* eslint strict: 0 */
/* eslint no-console: 0 */
// Needed in Node in order to use let
"use strict";

/**
 * Contains all migrations for user database.
 * @type {Array}
 */
module.exports.migrationsForUser = [
    // Version 0
    [
        "PRAGMA journal_mode=WAL",

        // Add version
        `CREATE TABLE IF NOT EXISTS version (
            value INTEGER PRIMARY KEY
        )`,
        "INSERT OR REPLACE INTO version (value) VALUES (0)",

        // Add timebundle
        `CREATE TABLE IF NOT EXISTS timebundle (
            id TEXT PRIMARY KEY,
            projectUrl TEXT NOT NULL,
            taskUrl TEXT UNIQUE NOT NULL,
            query TEXT NOT NULL,
            lastSync INTEGER
        )`,
        "CREATE INDEX IF NOT EXISTS IDX_TIMEBUNDLE_PROJECTURL ON timebundle (projectUrl)",
        "CREATE INDEX IF NOT EXISTS IDX_TIMEBUNDLE_TASKURL ON timebundle (taskUrl)",
        "CREATE INDEX IF NOT EXISTS IDX_LASTSYNC ON timebundle (lastSync)"
    ],
    // Version 1
    [
        "ALTER TABLE user ADD COLUMN generatedPasswd TEXT",
        "UPDATE version SET value = 1"
    ],
    // Version 2
    [
        "UPDATE event SET source = 'note' WHERE source = 'NOTE'",
        "UPDATE event SET source = 'chrome idle' WHERE source = 'EXTENSION'",
        "UPDATE event SET source = 'github push' WHERE source = 'GITHUB'",
        "UPDATE event SET source = 'gitlab push' WHERE source = 'GITLAB'",
        "UPDATE version SET value = 2"
    ],
    // Version 3
    [
        `CREATE TABLE IF NOT EXISTS event_ (
            id TEXT PRIMARY KEY,
            beginDateTime INTEGER NOT NULL,
            endDateTime INTEGER NOT NULL,
            value TEXT NOT NULL"
        )`,

        "CREATE INDEX IF NOT EXISTS IDX_EVENT__BEGIN_DATE_TIME ON event_ (beginDateTime)",
        "CREATE INDEX IF NOT EXISTS IDX_EVENT__END_DATE_TIME ON event_ (endDateTime)",
        "CREATE INDEX IF NOT EXISTS IDX_EVENT__DATE_TIME ON event_ (beginDateTime, endDateTime)",

        // migrate note
        `insert into event_ select  id, beginDateTime, endDateTime,
                 '{ "beginDateTime": '|| beginDateTime ||',
                 "endDateTime": '|| endDateTime ||',
                 "type": "note",
                 "source": "webui",
                 "data": {"content":"' || replace(value, '"', '\\"') || '"}}' as value
             from event
             where source = 'note'`,

        // migrate widand location
        `insert into event_ select  id, beginDateTime, endDateTime,
            '{ "beginDateTime": '|| beginDateTime ||',
                 "endDateTime": '|| endDateTime ||',
                 "type": "location",
                 "source": "widand",
                 "data": ' || json_set(json( '{"' ||
                    replace(replace(replace(replace(
                        value, ' speed:', '" speed:'),
                         'provider:', 'provider:"'),
                         ' ', ',"'),
                         ':', '":') || '}'),
                     '$.device', ifnull(nullif(json_extract(
                        '["' || replace(source, ' ', '","') || '"]', '$[1]'), 'location'), 'null'))
                 || '}' as value
                 from event
                 where source like 'widand%location'`,

        // migrate widand call
        `insert into event_ select  id, beginDateTime, endDateTime,
            '{ "beginDateTime": '|| beginDateTime ||',
            "endDateTime": '|| endDateTime ||',
            "type": "call",
            "source": "widand",
            "data": ' || '{
                "device":"' || ifnull(nullif(json_extract('["' || replace(source, ' ', '","') || '"]', '$[1]'), 'call'), '') || '",
                 "type":"' || json_extract('["' || replace(value, ' ', '","') || '"]', '$[0]') || '",
                 "number":"' || json_extract('["' || replace(value, ' ', '","') || '"]', '$[1]') || '",
                 "name":"' || replace(replace(
                    ifnull(json_extract('["' || replace(value, ' ', '","') || '"]', '$[2]'), ''),
                     '_', ' '),
                     '"', '\\"')
             || '"}' || '}' as value
             from event
             where source like 'widand%call'`,

        // migrate widand step
        `insert into event_ select  id, beginDateTime, endDateTime,
            '{"beginDateTime": '|| beginDateTime ||',
             "endDateTime": '|| endDateTime ||',
             "type": "step",
             "source": "widand",
             "data": ' || '{
                "device":"' || ifnull(nullif(json_extract('["' || replace(source, ' ', '","') || '"]', '$[1]'), 'step'), '') || '",
                 "number":'|| value
             ||'}' || '}' as value
             from event
             where source like 'widand%step'`,

        // migrate widand application
        `insert into event_ select  id, beginDateTime, endDateTime,
            '{"beginDateTime": '|| beginDateTime ||',
             "endDateTime": '|| endDateTime ||',
             "type": "application",
             "source": "widand",
             "data": ' || '{
                "device":"' || ifnull(nullif(json_extract('["' || replace(source, ' ', '","') || '"]', '$[1]'), 'application'), '') || '",
                 "name":"'|| replace(replace(value, '_', ' '), '"', '\\"')
             ||'"}' || '}' as value
             from event
             where source like 'widand%application'`,

        // migrate widand calendar
        `insert into event_ select  id, beginDateTime, endDateTime,
            '{"beginDateTime": '|| beginDateTime ||',
             "endDateTime": '|| endDateTime ||',
             "type": "calendar",
             "source": "widand",
             "data": ' || '{
                "device":"' || ifnull(nullif(json_extract('["' || replace(source, ' ', '","') || '"]', '$[1]'), 'calendar'), '') || '",
                 "title":"'|| replace(replace(value, '_', ' '), '"', '\\"')
             ||'"}' || '}' as value
             from event
             where source like 'widand%calendar'`,

        // missing wakatime date migration

        "DROP TABLE IF EXISTS event",
        "ALTER TABLE event_ RENAME TO event",

        "DROP INDEX IF EXISTS IDX_EVENT__BEGIN_DATE_TIME",
        "DROP INDEX IF EXISTS IDX_EVENT__END_DATE_TIME",
        "DROP INDEX IF EXISTS IDX_EVENT__DATE_TIME",

        "CREATE INDEX IF NOT EXISTS IDX_EVENT_BEGIN_DATE_TIME ON event (beginDateTime)",
        "CREATE INDEX IF NOT EXISTS IDX_EVENT_END_DATE_TIME ON event (endDateTime)",
        "CREATE INDEX IF NOT EXISTS IDX_EVENT_DATE_TIME ON event (beginDateTime, endDateTime)",

        "UPDATE version SET value = 3"
    ],
    // Version 4
    [
        "ALTER TABLE event ADD COLUMN key TEXT",
        "CREATE INDEX IF NOT EXISTS IDX_EVENT_KEY ON event (key)",
        "UPDATE version SET value = 4"
    ],
    // Version 5
    [
        "ALTER TABLE report ADD COLUMN selectBeginDateTime INTEGER",
        "ALTER TABLE report ADD COLUMN selectEndDateTime INTEGER",
        "UPDATE version SET value = 5"
    ],
    // Version 6
    [
        `CREATE TABLE IF NOT EXISTS priorities (
            tags TEXT NOT NULL
        )`,
        "UPDATE version SET value = 6"
    ],
    // Version 7
    [
        "INSERT INTO priorities (tags) VALUES ('')",
        "UPDATE version SET value = 7"
    ],
    // Version 8
    [
        "UPDATE version SET value = 8"
    ],
    // Version 9
    [
        "DROP TABLE IF EXISTS activity",
        `CREATE TABLE IF NOT EXISTS activity (
            beginDateTime INTEGER NOT NULL,
            endDateTime INTEGER NOT NULL,
            projectName TEXT NOT NULL,
            count INTEGER
        )`,
        "UPDATE version SET value = 9"
    ],
    // Version 10
    [
        "CREATE INDEX IF NOT EXISTS IDX_ACTIVITY_BEGIN_DATE_TIME ON activity (beginDateTime)",
        "CREATE INDEX IF NOT EXISTS IDX_ACTIVITY_END_DATE_TIME ON activity (endDateTime)",
        "CREATE INDEX IF NOT EXISTS IDX_ACTIVITY_PROJECT ON activity (projectName)",
        "CREATE INDEX IF NOT EXISTS IDX_ACTIVITY_DATE_TIME ON activity (beginDateTime, endDateTime)",

        "UPDATE version SET value = 10"
    ],
    // Version 11
    [
        "DELETE FROM activity WHERE count IS NULL",

        "UPDATE version SET value = 11"
    ]
];

/**
 * Contains all migrations for global database.
 * @type {Array}
 */
module.exports.migrationsForGlobal = [
    // Version 0
    [
        "PRAGMA journal_mode=WAL",

        // Add version
        `CREATE TABLE IF NOT EXISTS version (
            value INTEGER PRIMARY KEY
        )`,
        "INSERT OR REPLACE INTO version (value) VALUES (0)"
    ],
    // Version 1
    [
        // Drop table
        "DROP TABLE IF EXISTS application",
        `CREATE TABLE IF NOT EXISTS application (
                id TEXT PRIMARY KEY,
                name TEXT UNIQUE,
                database TEXT,
                key TEXT
            ")`,
        "UPDATE version SET value = 1"
    ],
    // Version 2
    [
        "CREATE TEMP TABLE IF NOT EXISTS applicationTemp AS SELECT * FROM application",
        "DROP TABLE IF EXISTS application",
        `CREATE TABLE IF NOT EXISTS application (
            id TEXT PRIMARY KEY,
            name TEXT,
            database TEXT,
            key TEXT"
        )`,
        "INSERT OR IGNORE INTO application select * from applicationTemp",
        "DROP TABLE IF EXISTS applicationTemp",
        "UPDATE version SET value = 2"
    ],
    // Version 3
    [
        "ALTER TABLE identity ADD COLUMN validation TEXT",
        "CREATE INDEX IF NOT EXISTS IDX_IDENTITY_VALIDATION ON identity (validation)",
        "UPDATE identity SET validation = 'done'",
        "UPDATE version SET value = 3"
    ]
];

/**
 * Contains the schema for user database.
 * @type {Array}
 */
module.exports.schemaForUser = [
    "PRAGMA journal_mode=WAL",

    `CREATE TABLE IF NOT EXISTS user (
        id TEXT PRIMARY KEY,
        username TEXT UNIQUE,
        email TEXT UNIQUE,
        passwd TEXT,
        generatedPasswd TEXT
    )`,

    `CREATE TABLE IF NOT EXISTS token (
        id TEXT PRIMARY KEY
    )`,

    `CREATE TABLE IF NOT EXISTS event (
        id TEXT PRIMARY KEY,
        beginDateTime INTEGER NOT NULL,
        endDateTime INTEGER NOT NULL,
        value TEXT NOT NULL,
        key TEXT
    )`,

    "CREATE INDEX IF NOT EXISTS IDX_EVENT_BEGIN_DATE_TIME ON event (beginDateTime)",
    "CREATE INDEX IF NOT EXISTS IDX_EVENT_END_DATE_TIME ON event (endDateTime)",
    "CREATE INDEX IF NOT EXISTS IDX_EVENT_DATE_TIME ON event (beginDateTime, endDateTime)",
    "CREATE INDEX IF NOT EXISTS IDX_EVENT_KEY ON event (key)",

    `CREATE TABLE IF NOT EXISTS report (
        id TEXT PRIMARY KEY,
        name TEXT,
        tags TEXT,
        period INTEGER,
        widget INTEGER,
        selectBeginDateTime INTEGER,
        selectEndDateTime INTEGER
    )`,

    `CREATE TABLE IF NOT EXISTS time (
        id TEXT PRIMARY KEY,
        beginDateTime INTEGER,
        endDateTime INTEGER,
        tag TEXT
    )`,

    "CREATE INDEX IF NOT EXISTS IDX_TIME_BEGIN_DATE_TIME ON time (beginDateTime)",
    "CREATE INDEX IF NOT EXISTS IDX_TIME_END_DATE_TIME ON time (endDateTime)",
    "CREATE INDEX IF NOT EXISTS IDX_TIME_DATE_TIME ON time (beginDateTime, endDateTime)",
    "CREATE INDEX IF NOT EXISTS IDX_TIME_TAG ON time (tag)",

    `CREATE TABLE IF NOT EXISTS version (
        value INTEGER PRIMARY KEY
    )`,
    "INSERT OR REPLACE INTO version (value) VALUES (" + (module.exports.migrationsForUser.length - 1) + ")",

    `CREATE TABLE IF NOT EXISTS timebundle (
        id TEXT PRIMARY KEY,
        projectUrl TEXT NOT NULL,
        taskUrl TEXT UNIQUE NOT NULL,
        query TEXT NOT NULL,
        lastSync INTEGER
    )`,

    `CREATE TABLE IF NOT EXISTS priorities (
        tags TEXT NOT NULL
    )`,
    "INSERT INTO priorities (tags) VALUES ('')",

    "CREATE INDEX IF NOT EXISTS IDX_TIMEBUNDLE_PROJECTURL ON timebundle (projectUrl)",
    "CREATE INDEX IF NOT EXISTS IDX_TIMEBUNDLE_TASKURL ON timebundle (taskUrl)",
    "CREATE INDEX IF NOT EXISTS IDX_LASTSYNC ON timebundle (lastSync)",

    `CREATE TABLE IF NOT EXISTS activity (
        beginDateTime INTEGER NOT NULL,
        endDateTime INTEGER NOT NULL,
        projectName TEXT NOT NULL,
        count INTEGER
    )`,
    "CREATE INDEX IF NOT EXISTS IDX_ACTIVITY_BEGIN_DATE_TIME ON activity (beginDateTime)",
    "CREATE INDEX IF NOT EXISTS IDX_ACTIVITY_END_DATE_TIME ON activity (endDateTime)",
    "CREATE INDEX IF NOT EXISTS IDX_ACTIVITY_PROJECT ON activity (projectName)",
    "CREATE INDEX IF NOT EXISTS IDX_ACTIVITY_DATE_TIME ON activity (beginDateTime, endDateTime)"
];

/**
 * Contains the schema for global database.
 * @type {Array}
 */
module.exports.schemaForGlobal = [
    "PRAGMA journal_mode=WAL",

    `CREATE TABLE IF NOT EXISTS identity (
        id TEXT PRIMARY KEY,
        identifier TEXT UNIQUE,
        database TEXT,
        validation TEXT
    )`,
    "CREATE INDEX IF NOT EXISTS IDX_IDENTITY_VALIDATION ON identity (validation)",

    `CREATE TABLE IF NOT EXISTS application (
        id TEXT PRIMARY KEY,
        name TEXT,
        database TEXT,
        key TEXT
    )`,

    `CREATE TABLE IF NOT EXISTS version (
        value INTEGER PRIMARY KEY
    )`,
    "INSERT OR REPLACE INTO version (value) VALUES (" + (module.exports.migrationsForGlobal.length - 1) + ")"
];

/**
 * Keep the current migration for an user.
 */
let users = new Map();

/**
 * Get the current version of the database.
 * @param  {object} db  database from sqlite3
 * @return {Promise}    the promise is resolved with the version otherwise the promise is resolved
 *                      with -1 if there is an error
 */
function getVersion(db) {
    return new Promise((resolve) => {
        db.get("SELECT value FROM version", (err, row) => {
            if (err) {
                resolve(-1);
            } else {
                resolve(row.value);
            }
        });
    });
}

/**
 * Serialize an array of sql queries.
 * @param  {object} db   database from sqlite3
 * @param  {array} sqls sql queries to execute
 * @return {Promise}     the promise is resolved if all queries is executed otherwise the promise is rejected
 *                       if there is an error
 */
module.exports.doSqls = (db, sqls) => {
    if (!sqls) {
        return Promise.resolve();
    }

    let promises;
    db.serialize(function() {
        promises = sqls.map((sql) => {
            return new Promise((resolve, reject) => {
                // console.log(">", sql);
                db.run(sql, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            });
        });
    });

    return Promise.all(promises);
};

/**
 * Do the migrations.
 * @param  {object} db             database from sqlite3
 * @param  {array} migrations      migrations to do
 * @param  {number} currentVersion current version of the database
 * @return {Promise}               the promise is resolved if all migrations is executed otherwise the promise is rejected
 *                                 if there is an error
 */
function migrate(db, migrations, currentVersion) {
    if (currentVersion < migrations.length - 1) {
        let sqls = migrations[currentVersion + 1];
        console.log(`Migration for ${db.filename} from ${currentVersion} to ${currentVersion + 1}`);
        return module.exports.doSqls(db, sqls)
        .then(() => {
            return migrate(db, migrations, currentVersion + 1);
        });
    }

    return Promise.resolve();
}

/**
 * Apply all migrations needed.
 * @param  {object} db         database from sqlite3
 * @param  {array} migrations  migrations to do
 * @return {Promise}           the promise is resolved if all migrations is executed otherwise the promise is rejected
 *                             if there is an error
 */
module.exports.applyMigration = (db, migrations) => {
    let fileName = db.filename;

    let promise = users.get(fileName);
    if (!promise) {
        promise = getVersion(db)
        .then((currentVersion) => {
            return migrate(db, migrations, currentVersion);
        });
        users.set(fileName, promise);
    }

    return promise;
};

/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

let userApi = require("./UserApi");
let timeApi = require("./TimeApi");
let moment = require("moment");
let fs = require("fs");
let dbHelper = require("./DatabaseHelper");

let tmpDir = process.env["npm_package_config_WID_TMP_DIR_" + process.env.npm_lifecycle_event]
                        || process.env.npm_package_config_WID_TMP_DIR;

module.exports.getDecisionTreeDataV1 = ({token}) => {
    return userApi.isAuth({token})
    .then((db) => {
        return db.get(`
            WITH
            activities AS (
                SELECT
                group_concat(tag) AS tags,
                beginDateTime,
                enddatetime,
                strftime('%w', datetime(beginDateTime / 1000, 'unixepoch')) as dayOfWeek,
                cast ((enddatetime - beginDateTime) / (1000 * 60 * 60) as integer) as durationInHours,
                cast (strftime('%H', datetime(beginDateTime / 1000, 'unixepoch')) as integer) as hoursOfBeginDateTime,

                EXISTS (
                    SELECT 1 FROM event e
                    WHERE e.beginDateTime <= v.enddatetime AND e.endDateTime >= v.beginDateTime
                    AND (json_extract(e.value, '$.type') = 'code' OR json_extract(e.value, '$.type') = 'scm')
                ) AS typeDev,

                (
                    SELECT cast (SUM(enddatetime - beginDateTime) / (1000 * 60 * 60) as integer) FROM event e
                    WHERE e.beginDateTime <= v.enddatetime AND e.endDateTime >= v.beginDateTime
                    AND json_extract(e.value, '$.type') = 'idle'
                ) AS idle,

                (
                    SELECT group_concat(p.project)
                    FROM (
                        SELECT DISTINCT json_extract(e.value, '$.data.project') AS project
                        FROM event e
                        WHERE e.beginDateTime <= v.enddatetime AND e.endDateTime >= v.beginDateTime
                        ORDER BY project
                    ) p
                ) AS projects

                FROM (SELECT * FROM time t ORDER BY tag) v
                GROUP BY beginDateTime, enddatetime
            )

            SELECT json_object(
                'tags', tags,
                'dayOfWeek', dayOfWeek,
                'durationInHours', durationInHours,
                'hoursOfBeginDateTime', hoursOfBeginDateTime,
                'typeDev', typeDev,
                'idle', idle,
                'projects', projects
            ) AS json
            FROM activities order by tags;
        `);
    });
};

module.exports.getDecisionTreeDataV2 = ({token, tags, path}) => {
    return userApi.isAuth({token})
    .then((db) => {
        return db.find(`
            WITH
            activities AS (
                SELECT
                tag,
                beginDateTime,
                enddatetime,
                strftime('%w', datetime(beginDateTime / 1000, 'unixepoch')) as dayOfWeek,
            	strftime('%m', datetime(beginDateTime / 1000, 'unixepoch')) as month,
            	strftime('%Y', datetime(beginDateTime / 1000, 'unixepoch')) as year,
            	strftime('%j', datetime(endDateTime / 1000, 'unixepoch')) - strftime('%j', datetime(beginDateTime / 1000, 'unixepoch')) as durationInDays,
            	cast ((enddatetime - beginDateTime) / (1000 * 60 * 60) as integer) as durationInHours,
            	cast (strftime('%H', datetime(beginDateTime / 1000, 'unixepoch')) as integer) as hoursOfBeginDateTime

                FROM (
                    SELECT *
                    FROM time t
                    WHERE tag IN (?${", ?".repeat(tags.length - 1)})
                    ${path.map(() => "AND EXISTS (SELECT 1 FROM time tt WHERE tt.beginDateTime = t.beginDateTime AND tag = ?)").join(" ")}

                    OR 1 = 1 ${path.map(() => "AND EXISTS (SELECT 1 FROM time tt WHERE tt.beginDateTime = t.beginDateTime AND tag = ?)").join(" ")}
                    AND NOT EXISTS (SELECT 1 FROM time tt WHERE tt.beginDateTime = t.beginDateTime AND tag IN (?${", ?".repeat(tags.length - 1)}))
                    GROUP BY beginDateTime
                ) v
                GROUP BY beginDateTime, enddatetime
            )

            SELECT json_group_array(json_object(
            	'tag', tag,
            	'dayOfWeek', dayOfWeek,
            	'month', month,
            	'year', year,
            	'durationInDays', durationInDays,
            	'durationInHours', durationInHours,
            	'hoursOfBeginDateTime', hoursOfBeginDateTime
            )) AS json
            FROM activities order by beginDateTime;
        `, ...tags, ...path, ...path, ...tags);
    });
};


module.exports.getAllProjects = ({token}) => {
    let database;
    return userApi.isAuth({token})
    .then((db) => {
        database = db;
        return database.all(`
            SELECT DISTINCT projectName
            FROM activity
            WHERE projectName IS NOT NULL
            ORDER BY projectName
        `);
    });
};

/**
// CREATE VIEW project (beginDateTime, endDateTime, tag, project)
// AS
// SELECT t.beginDateTime, t.endDateTime, t.tag, replace(replace(lower(substr(
//             	json_extract(e.value, '$.data.project'),
//             	0,
//             	instr(
//             		json_extract(e.value, '$.data.project') || ' ',
//             		' '
//             	)
//             )), '-', '_'), '.', '_') AS project
// FROM event e, time t
// WHERE e.beginDateTime <= t.enddatetime AND e.endDateTime >= t.beginDateTime
//
//
// or use a table :
//
// DROP VIEW IF EXISTS project;
// DROP TABLE IF EXISTS project;
// CREATE TABLE project
// AS
// 	SELECT t.beginDateTime, t.endDateTime, t.tag,
// 	replace(
//       replace(
// 	    lower(
//             substr(
//                 json_extract(e.value, '$.data.project'),
// 				0,
//             	instr(
//             		json_extract(e.value, '$.data.project') || ' ',
//             		' '
//             	)
//             )
// 		), '-', '_'
// 	  ),
// 	'.', '_') AS project
// 	FROM event e, time t
// 	WHERE e.beginDateTime <= t.enddatetime AND e.endDateTime >= t.beginDateTime;
//
// CREATE INDEX IDX_PROJECT_BEGIN_DATE_TIME ON project (beginDateTime);
// CREATE INDEX IDX_PROJECT_END_DATE_TIME ON project (endDateTime);
// CREATE INDEX IDX_PROJECT_PROJECT ON project (project);
// CREATE INDEX IDX_PROJECT_DATE_TIME ON project (beginDateTime, endDateTime)
  */
module.exports.getDecisionTreeDataV3 = ({token, projects, tags, path}) => {
    let database;
    return userApi.isAuth({token})
    .then((db) => {
        database = db;
        return database.find(`
            WITH
            activities AS (
                SELECT
                tag,
                beginDateTime,
                enddatetime,
                ${projects.map((p) => "coalesce(sum(v.project = '" + p + "'), 0) as " + p + ",").join("\n")}
                strftime('%w', datetime(beginDateTime / 1000, 'unixepoch')) as dayOfWeek,
            	strftime('%m', datetime(beginDateTime / 1000, 'unixepoch')) as month,
            	strftime('%Y', datetime(beginDateTime / 1000, 'unixepoch')) as year,
            	strftime('%j', datetime(endDateTime / 1000, 'unixepoch')) - strftime('%j', datetime(beginDateTime / 1000, 'unixepoch')) as durationInDays,
            	cast ((enddatetime - beginDateTime) / (1000 * 60 * 60) as integer) as durationInHours,
            	cast (strftime('%H', datetime(beginDateTime / 1000, 'unixepoch')) as integer) as hoursOfBeginDateTime

                FROM (
                    SELECT *
                    FROM project t
                    WHERE tag IN (?${", ?".repeat(tags.length - 1)})
                    ${path.map(() => "AND EXISTS (SELECT 1 FROM time tt WHERE tt.beginDateTime = t.beginDateTime AND tag = ?)").join("\n")}

                    OR 1 = 1 ${path.map(() => "AND EXISTS (SELECT 1 FROM time tt WHERE tt.beginDateTime = t.beginDateTime AND tag = ?)").join("\n")}
                    AND NOT EXISTS (SELECT 1 FROM time tt WHERE tt.beginDateTime = t.beginDateTime AND tag IN (?${", ?".repeat(tags.length - 1)}))
                ) v
                GROUP BY beginDateTime, enddatetime
            )

            SELECT json_group_array(json_object(
            	'tag', tag,
            	${projects.map((p) => "'" + p + "'," + p + ",").join("\n")}
            	'dayOfWeek', dayOfWeek,
            	'month', month,
            	'year', year,
            	'durationInDays', durationInDays,
            	'durationInHours', durationInHours,
            	'hoursOfBeginDateTime', hoursOfBeginDateTime
            )) AS json,

            json_group_array(json_array(
            	tag,
            	${projects.map((p) => p + ",").join("\n")}
            	dayOfWeek,
            	month,
            	year,
            	durationInDays,
            	durationInHours,
            	hoursOfBeginDateTime
            )) AS data

            FROM activities order by beginDateTime;
        `, ...tags, ...path, ...path, ...tags);
    });
};


module.exports.getDecisionTreeDataV4 = ({token, projects, tags, path}) => {
    let database;
    return userApi.isAuth({token})
    .then((db) => {
        database = db;
        return database.find(`
            WITH
            activities AS (
                SELECT
                tag,
                beginDateTime,
                enddatetime,
                ${projects.map((p) => "coalesce(sum(v.project = '" + p + "'), 0) as " + p + ",").join("\n")}
                strftime('%w', datetime(beginDateTime / 1000, 'unixepoch')) as dayOfWeek,
            	strftime('%m', datetime(beginDateTime / 1000, 'unixepoch')) as month,
            	strftime('%Y', datetime(beginDateTime / 1000, 'unixepoch')) as year,
            	strftime('%j', datetime(endDateTime / 1000, 'unixepoch')) - strftime('%j', datetime(beginDateTime / 1000, 'unixepoch')) as durationInDays,
            	cast ((enddatetime - beginDateTime) / (1000 * 60 * 60) as integer) as durationInHours,
            	cast (strftime('%H', datetime(beginDateTime / 1000, 'unixepoch')) as integer) as hoursOfBeginDateTime

                FROM (
                    SELECT *
                    FROM project p
                    WHERE tag IN (?${", ?".repeat(tags.length - 1)})
                    ${path.map(() => "AND EXISTS (SELECT 1 FROM time t WHERE t.beginDateTime = p.beginDateTime AND t.endDateTime = p.endDateTime AND tag = ?)").join("\n")}

                    OR 1 = 1 ${path.map(() => "AND EXISTS (SELECT 1 FROM time t WHERE t.beginDateTime = p.beginDateTime AND t.endDateTime = p.endDateTime AND tag = ?)").join("\n")}
                    AND NOT EXISTS (SELECT 1 FROM time t WHERE t.beginDateTime = p.beginDateTime AND t.endDateTime = p.endDateTime AND tag IN (?${", ?".repeat(tags.length - 1)}))
                ) v
                GROUP BY beginDateTime, enddatetime
            )

            SELECT json_group_array(json_array(
            	tag,
            	${projects.map((p) => p + ",").join("\n")}
            	dayOfWeek,
            	month,
            	year,
            	durationInDays,
            	durationInHours,
            	hoursOfBeginDateTime
            )) AS data

            FROM activities order by beginDateTime;
        `, ...tags, ...path, ...path, ...tags);
    });
};

/**
 * To be able to use this v5 api, you need the following change in your db :
 * -- MAj de la table event
 * ALTER TABLE event ADD COLUMN projectName;
 * UPDATE event
 * SET projectName = replace(
 *       replace(
 * 	    lower(
 *             substr(
 *                 json_extract(value, '$.data.project'),
 * 				0,
 *             	instr(
 *             		json_extract(value, '$.data.project') || ' ',
 *             		' '
 *             	)
 *             )
 * 		), '-', '_'
 * 	  ),
 * 	'.', '_');
 *
 * -- Création de la table activiy
 * DROP TABLE IF EXISTS activity;
 * CREATE TABLE activity
 * AS
 * SELECT distinct t.beginDateTime as beginDateTime, t.endDateTime as endDateTime, e.projectName as projectName, count(e.projectName) as count
 * FROM time t
 * JOIN event e
 * ON e.beginDateTime <= t.enddatetime AND e.endDateTime >= t.beginDateTime
 * WHERE e.projectName is not null
 * GROUP BY t.beginDateTime, t.endDateTime, t.tag, e.projectName;
 *
 * CREATE INDEX IDX_ACTIVITY_BEGIN_DATE_TIME ON activity (beginDateTime);
 * CREATE INDEX IDX_ACTIVITY_END_DATE_TIME ON activity (endDateTime);
 * CREATE INDEX IDX_ACTIVITY_PROJECT ON activity (projectName);
 * CREATE INDEX IDX_ACTIVITY_DATE_TIME ON activity (beginDateTime, endDateTime);
 */
module.exports.getDecisionTreeDataV5 = ({token, projects, tags, path}) => {
    let database;
    return userApi.isAuth({token})
    .then((db) => {
        database = db;

        let parameters = [];
        let query = `
            WITH
            activities AS (
                SELECT
                    tag,
                    beginDateTime,
                    enddatetime,
                    `;

        // Count the number of event for each existing project during the activity
        projects.forEach(p => {
            parameters.push(p);
            query += `(
                        SELECT 0 UNION
                        SELECT a.count c
                        FROM activity a
                        WHERE a.beginDateTime = t.beginDateTime and a.endDateTime = t.endDateTime
                        AND a.projectName = ?
                        ORDER BY c desc LIMIT 1
                    ) as ${p},`;
        });

        // Compute date and time descriptors for each concerned activity
        query += `
                    strftime('%w', datetime(beginDateTime / 1000, 'unixepoch')) as dayOfWeek,
                    strftime('%m', datetime(beginDateTime / 1000, 'unixepoch')) as month,
                    strftime('%Y', datetime(beginDateTime / 1000, 'unixepoch')) as year,
                    cast((endDateTime - beginDateTime) / (1000 * 60 * 60 * 24) as integer) as durationInDays,
                    cast ((enddatetime - beginDateTime) / (1000 * 60 * 60) as integer) as durationInHours,
                    cast ((enddatetime - beginDateTime) / (1000 * 60) as integer) as durationInMinutes,
                    cast (strftime('%H', datetime(beginDateTime / 1000, 'unixepoch')) as integer) as hoursOfBeginDateTime

                FROM time t
                `;

        query += "WHERE (0 = 1 ";

        const previousTags = [];
        tags.forEach(tag => {
            parameters.push(tag, ...previousTags);

            query += " OR t.tag = ?";
            if (previousTags.length) {
                query += ` AND t.tag NOT IN (?${", ?".repeat(previousTags.length - 1)})`;
            }

            previousTags.push(tag);
        });

        query += ") ";

        if (path.length) {
            // We want to keep only the time entries wich match the given 'tags' but for which it also exists a time entry for each token in the given 'path'
            // Example :
            // path A > B
            // tags C, D
            // We want all entries from time table with a match to C or D but for each it exists a corresponding time entry with A and B
            path.forEach(chunk => {
                parameters.push(chunk);
                query += `
                AND EXISTS (
                    SELECT 1 FROM time tt
                    WHERE tt.beginDateTime = t.beginDateTime AND tt.endDateTime = t.endDateTime
                    AND tt.tag = ?
                )`;
            });

            // We also want to add the time entries corresponding to the last chunk of the path and for which there is no time entry with the given tags
            // Example :
            // path A > B
            // tags C, D
            // We want all entries from time table with a match to A and B for which there is no time entry matching C or D
            parameters.push(...tags);
            query += `
                OR
                NOT EXISTS (
                    SELECT 1 FROM time tt
                    WHERE tt.beginDateTime = t.beginDateTime AND tt.endDateTime = t.endDateTime
                    AND tt.tag IN (?${", ?".repeat(tags.length - 1)})
                )`;


            path.forEach((chunk, index) => {
                parameters.push(chunk);

                if (index < path.length - 1) {
                    query += `
                    AND EXISTS (
                        SELECT 1 FROM time tt
                        WHERE tt.beginDateTime = t.beginDateTime AND tt.endDateTime = t.endDateTime
                        AND tt.tag = ?
                    )`;
                } else {
                    query += `
                    AND t.tag = ?
                    `;
                }
            });
        }

        query += `
            )

            SELECT json_group_array(json_array(
                tag,
                ${projects.map((p) => p + ",").join("\n")}
                dayOfWeek,
                month,
                year,
                durationInDays,
                durationInHours,
                durationInMinutes,
                hoursOfBeginDateTime
            )) AS data

            FROM activities order by beginDateTime;
        `;

        return database.find(query, ...parameters);
    });
};

module.exports.getProject = ({token, projects, beginDateTime = 0, endDateTime = Number.MAX_VALUE}) => {
    return userApi.isAuth({token})
    .then((db) => {
        if (!projects.length) {
            return {json: "{}"};
        }

        return db.find(`
            WITH
            projects AS (
                SELECT
                    replace(
                      replace(
                	    lower(
                            substr(
                                json_extract(value, '$.data.project'),
                				0,
                            	instr(
                            		json_extract(value, '$.data.project') || ' ',
                            		' '
                            	)
                            )
                		), '-', '_'
                	),
                	'.', '_') AS project
                FROM event e
                WHERE project IS NOT NULL
                AND beginDateTime <= ? AND endDateTime >= ?

				UNION ALL SELECT 'idle'
            ),

            idle_count AS (
                SELECT coalesce(sum(min(e.endDateTime, ?) - max(e.beginDateTime, ?)) / 1000 / 60, 0) as count
                FROM event e
                WHERE json_extract(value, "$.type") = "idle"
                AND beginDateTime <= ? AND endDateTime >= ?
            ),

            project_count AS (
                SELECT ${projects.map(p => {
                    if (p === "idle") {
                        return "(SELECT count FROM idle_count) as idle";
                    }
                    return "coalesce(sum(project = '" + p + "'), 0) as " + p;
                }).join(",")}
                FROM projects
            )

            SELECT json_object(
            	${projects.map((p) => "'" + p + "'," + p).join(",")}
            ) AS json
            FROM project_count;

        `, +endDateTime, +beginDateTime, +endDateTime, +beginDateTime, +endDateTime, +beginDateTime);
    });
};

/**
 * This function generates the needed files for the c5.0 native module.
 */
const writeC50Files = (token, projects, input, path, subtags, values) => {
    const username = dbHelper.getName(token);
    const applicationPath = `${tmpDir}/c50_${username}_${path.length}`;
    const outputC50Path = `${tmpDir}/c50_${username}_${path.length}.output`;
    const outputPredicPath = `${tmpDir}/predic_${username}_${path.length}.output`;

    // Attributes descriptor file
    // The projects ratio represents a frequence of each project usage within a period of time
    fs.writeFileSync(`${applicationPath}.names`, `tag.
tag: ${subtags.concat(path).join(",")}.
${projects.map(name => name + ": continuous.").join("\n")}
dayOfWeek: continuous.
month: continuous.
year: continuous.
durationInDays: continuous.
durationInHours: continuous.
durationInMinutes: continuous.
hoursOfBeginDateTime: continuous.
${projects.map(name => `${name}Ratio := ${name} / durationInMinutes.`).join("\n")}
attributes excluded: durationInMinutes${projects.length ? "," : ""}${projects.join(",")}.
    `);

    // Training data file
    fs.writeFileSync(`${applicationPath}.data`,
        JSON.parse(values).map(row => row.join(",")).join("\n")
    );

    // Attributes values of the case we want to predict
    fs.writeFileSync(`${applicationPath}.cases`,
        "?,"
        + projects.map(name => input[name]).join(",") + (projects.length ? "," : "")
        + input.dayOfWeek + ","
        + input.month + ","
        + input.year + ","
        + input.durationInDays + ","
        + input.durationInHours + ","
        + input.durationInMinutes + ","
        + input.hoursOfBeginDateTime
    );

    return {applicationPath, outputC50Path, outputPredicPath};
};

// http://localhost:9090/api/decision?beginDateTime=1508767087921&endDateTime=1508771212875
module.exports.getDecision = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, tags = []}) => {
    let begin = moment(+beginDateTime);
    let end = moment(+endDateTime);

    let input = {
        dayOfWeek: begin.format("d"),
        month: begin.format("MM"),
        year: begin.format("YYYY"),
        durationInDays: end.diff(begin, "days"),
        durationInHours: end.diff(begin, "hours"),
        durationInMinutes: end.diff(begin, "minutes"),
        hoursOfBeginDateTime: begin.format("HH")
    };

    let projects;

    return module.exports.getAllProjects({token})
    .then(({rows}) => {
        projects = rows.map(p => p.projectName).sort();

        return module.exports.getProject({token, projects, beginDateTime, endDateTime});
    })
    .then(({json}) => {
        const projectsActivity = JSON.parse(json);

        Object.assign(input, projectsActivity);
        let lastYear = moment(+beginDateTime).subtract(1, "year");
        return timeApi.getTreeFacet({token, tags, beginDateTime: +lastYear});
    })
    .then((tree) => {
        let subtags = Object.keys(tree).filter(tag => tag !== "__meta__");
        subtags.sort((first, second) => {
            return tree[second].__meta__.duration - tree[first].__meta__.duration;
        });

        subtags = subtags.slice(0, 40);
        if (!subtags.length) {
            return tags;
        }

        return module.exports.getDecisionTreeDataV5({token, projects, tags: subtags, path: tags})
        .then(({data}) => {

            // Writing C5.0 files so that we can call the native decision tree module
            const {applicationPath, outputC50Path, outputPredicPath} = writeC50Files(token, projects, input, tags, subtags, data);

            // Calling native modules to generate a decision tree and then to predict the current activity
            const c50 = require("c50");
            const predict = require("predict");
            c50(applicationPath, outputC50Path);
            let result = predict(applicationPath, outputPredicPath);
            // console.log(`Prediction : ${result}`);

            let lastTag = tags[tags.length - 1];
            if (lastTag === result) {
                return tags;
            }

            return module.exports.getDecision({token, beginDateTime, endDateTime, tags: tags.concat(result)});
        });
    });
};

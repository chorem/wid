/* eslint strict: 0 */
/* eslint new-cap: 0 */
/* eslint no-console: 0 */
// Needed in Node in order to use let
"use strict";

let dbHelper = require("./DatabaseHelper");
let dbMigration = require("./DatabaseMigration");
let uuid = require("uuid");
let userApi = require("./UserApi");

let moment = require("moment");
let fs = require("fs");
let globule = require("globule");
let AdmZip = require("adm-zip");
let rmdir = require("rimraf");
let streamifier = require("streamifier");
let path = require("path");
let Iconv = require("iconv").Iconv;

/**
 * Drop all tables into database if needed.
 * @param  {Database} database user database
 * @param  {boolean} drop      true if need to drop table
 * @return {Promise}           the promise is resolved if the tables is dropped or not otherwise the promise is rejected
 *                             if there is an error
 */
let dropImportDb = (database, drop) => {
    if (drop) {
        return dbHelper.dropAllTables(database)
                .then(() => {
                    return dbMigration.doSqls(database.db, dbMigration.schemaForUser);
                });
    }
    return Promise.resolve();
};

/**
 * Inserts all data into the database comes from content. The content contains have the table name as key
 * and each key contains an array of object as key the column and as value the value.
 * @param  {database} db      user database
 * @param  {Object} content    all object to insert
 * @return {Promise}           the promise is resolved if the tables is dropped or not otherwise the promise is rejected
 *                             if there is an error
 */
let execInserts = (db, content) => {
    let promises = [];
    let tableNames = Object.keys(content);
    tableNames.forEach((tableName) => {
        if (dbHelper.NO_IMPORT_TABLES.indexOf(tableName) === -1) {

            let rows = content[tableName];
            rows.forEach((row) => {
                let keys = Object.keys(row);
                let columns = keys.join(",");
                let values = keys.map((key) => {return row[key];});
                let params = keys.map(() => {return "?";}).join(",");

                let promise = db.run(`INSERT INTO ${tableName} (${columns}) VALUES (${params})`, ...values);
                promises.push(promise);
            });
        }
    });

    return Promise.all(promises);
};

/**
 * Import basic data for user.
 * FIXME: jru 20160812 do the migration if it is needed.
 * @param  {String} token  security token
 * @param  {String} data   data to import
 * @param  {boolean} drop  ask to drop database before import
 * @return {Promise}       the promise is resolved if the data is imported otherwise the promise is rejected
 *                         if there is an error
 */
module.exports.importDb = ({token, data, drop}) => {
    let content;
    let database;

    return userApi.isAuth({token})
        .then((db) => {
            database = db;

            try {
                content = JSON.parse(data);
            } catch (err) {
                return Promise.reject(err);
            }

            return db.beginTransaction();
        })
        .then(() => {
            return dropImportDb(database, drop);
        })
        .then(() => {
            return execInserts(database, content);
        })
        .catch((err) => {
            return database.rollbackTransaction(err);
        })
        .then(() => {
            return database.commitTransaction();
        })
        .then(() => {
            return true;
        });
};

/**
 * Prepare the data from jtimer files.
 * @param  {String} baseDir dirrectory where are the jtimer files
 * @return {object}         contains all times and events to insert
 */
let extractJtimerData = (baseDir) => {
    let data = {
        times: [],
        events: []
    };
    let filePaths = globule.find(baseDir + "*.task");

    let endDateTimes = new Map();
    let iconv = new Iconv("ISO-8859-1", "UTF-8");

    filePaths.forEach((filePath) => {
        let taskContent = fs.readFileSync(filePath);
        if (taskContent.length) {
            let taskLines = iconv.convert(taskContent).toString().split("\n");
            // console.log(filePath);

            let tasks = taskLines[1].split("/");
            tasks[0] = tasks[0].substring("Name: ".length);

            let projectFile = baseDir + taskLines[4].substring("Project: ".length) + ".project";
            let projectContent = fs.readFileSync(projectFile);
            if (projectContent.length) {

                // console.log(projectFile);
                let projectLines = iconv.convert(projectContent).toString().split("\n");
                let projectName = projectLines[1].substring("Name: ".length);
                if (projectName[0] === "#") {
                    projectName = projectName.substring(1);
                }
                tasks.unshift(projectName);

                let annMap = new Map();
                let annFile = baseDir + path.basename(filePath, ".task") + ".ann";
                try {
                    let annContent = fs.readFileSync(annFile);
                    if (annContent.length) {
                        // console.log(annFile);
                        let annLines = iconv.convert(annContent).toString().split("\n");

                        annLines.forEach((ann) => {
                            let match = ann.match(/([0-9]+) (.*)/);
                            let date = match[1];
                            let value = match[2];
                            let key = moment(date * 1000).format("YYYYMMDD");

                            let values = annMap.get(key);
                            if (!values) {
                                values = [];
                                annMap.set(key, values);
                            }

                            values.push(value);
                        });
                    }
                } catch (err) {
                    // Do nothing no annotation file exists
                }

                let times = taskLines.slice(6);
                times.forEach((time) => {
                    let [date, duration] = time.split(" ");

                    if (duration && duration !== "0") {
                        let beginDateTime = moment(endDateTimes.get(date) || date);

                        let endDateTime = moment(+beginDateTime).add(duration, "s");
                        endDateTimes.set(date, +endDateTime);

                        let annotations = annMap.get(date);
                        if (annotations) {
                            annotations.forEach((annotation) => {
                                data.events.push({
                                    id: uuid.v1(),
                                    beginDateTime: +beginDateTime,
                                    endDateTime: +beginDateTime,
                                    type: "note",
                                    source: "jtimer",
                                    data: {
                                        content: annotation
                                    }
                                });
                            });
                        }

                        tasks.forEach((tag) => {
                            data.times.push({
                                id: uuid.v1(),
                                beginDateTime: +beginDateTime,
                                endDateTime: +endDateTime,
                                tag: tag.toLowerCase().replace(/ /g, "_").replace(/-/g, "_")
                            });
                        });
                    }
                });
            }
        }
    });

    return data;
};

/**
 * Insert the data from jtimer into database.
 * @param  {Database} db   user database
 * @param  {object} data   jtimer data comes from extractJtimerData function
 * @return {Promise}       the promise is resolved if the data is imported otherwise the promise is rejected
 *                         if there is an error
 */
let importData = (db, data) => {
    let insertTime = db.prepare("INSERT INTO time (id, beginDateTime, endDateTime, tag) VALUES (?, ?, ?, ?)");
    let insertEvent = db.prepare("INSERT INTO event (id, beginDateTime, endDateTime, value) VALUES (?, ?, ?, ?)");

    let promises = [];

    data.times.forEach((time) => {
        let promise = db.runPrepare(insertTime, time.id, time.beginDateTime, time.endDateTime, time.tag);
        promises.push(promise);
    });

    data.events.forEach((event) => {
        let promise = db.runPrepare(insertEvent, event.id, event.beginDateTime, event.endDateTime, JSON.stringify(event));
        promises.push(promise);
    });

    return promises;
};

/**
 * Import jtimer data from zip file of files in data folder of jtimer for an user.
 * @param  {String} token  security token
 * @param  {String} data   zip file content
 * @return {Promise}       the promise is resolved if the data is imported otherwise the promise is rejected
 *                         if there is an error
 */
module.exports.importJtimer = ({token, data}) => {

    return new Promise((resolve, reject) => {
        let baseDir = "/tmp/data-" + token + "/";
        let stream = streamifier.createReadStream(data);

        var zip = new AdmZip(stream);
        zip.extractAllTo(baseDir);

        stream.on("close", () => {
            let database;

            userApi.isAuth({token})
            .then((db) => {
                database = db;
                return db.beginTransaction();
            })
            .then(() => {
                let jtimerData = extractJtimerData(baseDir);
                let promises = importData(database, jtimerData);
                return Promise.all(promises);
            })
            .catch((err) => {
                return database.rollbackTransaction(err);
            })
            .then(() => {
                return database.commitTransaction();
            })
            .then(() => {
                rmdir(baseDir, () => {
                    resolve(true);
                });
            })
            .catch(() => {
                rmdir(baseDir, () => {
                    reject("Error during import");
                });
            });
        });
    });
};

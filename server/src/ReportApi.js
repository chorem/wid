/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

let uuid = require("uuid");
let userApi = require("./UserApi");
let timeApi = require("./TimeApi");
let eventApi = require("./EventApi");
let moment = require("moment");

// const PERIOD_TYPE [
//     "TODAY",
//     "WEEK",
//     "MONTH",
//     "YEAR"
// ];

/**
 * Add an report for an user.
 * @param  {String} token                   security token
 * @param  {String} name                    name of the report
 * @param  {String} tags                    query of the report
 * @param  {Number} period                  period of the report
 * @param  {boolean} widget                 specify if the report is visible on timeline
 * @param  {Number} selectBeginDateTime     select begin date for the report
 * @param  {Number} selectEndDateTime       select end date for the report
 * @return {Promise}                        the promise is resolved with the report object otherwise
 *                                          the promise is rejected if there is an error
 */
module.exports.add = ({token, name, tags, period, widget = 0, selectBeginDateTime = 0, selectEndDateTime = 0}) => {
    let id = uuid.v1();

    return userApi.isAuth({token})
            .then((db) => {
                return db.run(
                    "INSERT INTO report (id, name, tags, period, widget, selectBeginDateTime, selectEndDateTime) VALUES (?, ?, ?, ?, ?, ?, ?)",
                    id, name, tags, period, widget, selectBeginDateTime, selectEndDateTime
                );
            })
            .then(() => {
                return {id};
            });
};

/**
 * Save an report for an user.
 * @param  {String} token                   security token
 * @param  {String} id                      id of the report
 * @param  {String} name                    name of the report
 * @param  {String} tags                    query of the report
 * @param  {Number} period                  period of the report
 * @param  {boolean} widget                 specify if the report is visible on timeline
 * @param  {Number} selectBeginDateTime     select begin date for the report
 * @param  {Number} selectEndDateTime       select end date for the report
 * @return {Promise}                        the promise is resolved with the report object otherwise
 *                                          the promise is rejected if there is an error
 */
module.exports.save = ({token, id, name, tags, period, widget = 0, selectBeginDateTime = 0, selectEndDateTime = 0}) => {
    return userApi.isAuth({token})
            .then((db) => {
                return db.run(
                    "UPDATE report SET name = ?, tags = ?, period = ?, widget = ?, selectBeginDateTime = ?, selectEndDateTime = ? WHERE id = ?",
                    name, tags, period, widget, selectBeginDateTime, selectEndDateTime, id
                );
            })
            .then(() => {
                return {id};
            });
};

/**
 * Remove an report from id for an user.
 * @param  {String} token security token
 * @param  {String} id    id of report to remove
 * @return {Promise}      the promise is resolved if the report is removed otherwise
 *                        the promise is rejected if there is an error
 */
module.exports.remove = ({token, id}) => {
    return userApi.isAuth({token})
            .then((db) => {
                return db.run("DELETE FROM report WHERE id = ?", id);
            });
};

/**
 * Get all reports for an user.
 * @param  {String} token security token
 * @return {Promise}      the promise is resolved with reports otherwise
 *                        the promise is rejected if there is an error
 */
module.exports.getAll = ({token}) => {
    return userApi.isAuth({token})
            .then((db) => {
                return db.all("SELECT id, name, tags, period, widget, selectBeginDateTime, selectEndDateTime FROM report ORDER BY name");
            });
};

/**
 * Get all reports use as widget for an user.
 * @param  {String} token security token
 * @return {Promise}      the promise is resolved with reports otherwise
 *                        the promise is rejected if there is an error
 */
module.exports.getAllWidgets = ({token}) => {
    return userApi.isAuth({token})
            .then((db) => {
                return db.all("SELECT id, name, tags, period, widget, selectBeginDateTime, selectEndDateTime FROM report WHERE widget = 1 ORDER BY name DESC");
            });
};

/**
 * Get a report from id for an user.
 * @param  {String} token security token
 * @param  {String} id    id of report
 * @return {Promise}      the promise is resolved with report otherwise
 *                        the promise is rejected if there is an error
 */
module.exports.get = ({token, id}) => {
    return userApi.isAuth({token})
            .then((db) => {
                return db.get("SELECT * FROM report WHERE id = ?", id);
            });
};

/**
 * Get the duration for a report between dates for an user.
 * @param  {String} token         security token
 * @param  {String} id            report id
 * @param  {Number} beginDateTime min date
 * @param  {Number} endDateTime   max date
 * @return {Promise}      the promise is resolved with duration otherwise
 *                        the promise is rejected if there is an error
 */
module.exports.getTotalTime = ({token, id, beginDateTime, endDateTime}) => {
    return module.exports.get({token, id})
            .then((report) => {
                let parameters = {
                    token,
                    beginDateTime: +beginDateTime,
                    endDateTime: +endDateTime,
                    query: report.tags
                };
                return timeApi.getTotalTimeForReport(parameters);
            });
};

/**
 * Get all information for a report between dates for an user.
 * @param  {String} token         security token
 * @param  {String} id            report id
 * @param  {Number} beginDateTime min date
 * @param  {Number} endDateTime   max date
 * @param  {type} tags            filter the query with other tags
 * @return {Promise}              the promise is resolved with details otherwise
 *                                the promise is rejected if there is an error
 */
module.exports.getDetailsTime = ({token, id, beginDateTime, endDateTime = Number.MAX_VALUE}) => {
    return module.exports.get({token, id})
            .then((report) => {
                let promises = [];
                let parameters = {
                    token,
                    beginDateTime: +beginDateTime,
                    endDateTime: +endDateTime,
                    query: report.tags,
                    type: "note",
                    period: report.period
                };

                let promiseTime = timeApi.getTimeForReport(parameters);
                promises.push(promiseTime);

                let promiseFacet = timeApi.getTreeReport(parameters);
                promises.push(promiseFacet);

                let promiseNotes = eventApi.getByType(parameters);
                promises.push(promiseNotes);

                let promiseTotal = timeApi.getTotalTimeForReport(parameters);
                promises.push(promiseTotal);

                let period = report.period;
                let number;
                let unit;
                if (period === 0) {
                    number = 11;
                    unit = "days";

                } else if (period === 1) {
                    number = 7 * 11;
                    unit = "days";

                } else if (period === 2) {
                    number = 11;
                    unit = "months";

                } else if (period === 3) {
                    number = 11;
                    unit = "years";

                } else if (period === 4) {
                    number = 0;
                    unit = "days";
                }

                parameters.beginDateTime = moment(+beginDateTime).subtract(number, unit);

                let languageAggregation = module.exports.getChartCodeLanguage(parameters);
                promises.push(languageAggregation);

                let projectAggregation = module.exports.getChartCodeProject(parameters);
                promises.push(projectAggregation);

                let durationAggregation = module.exports.getChartDuration(parameters);
                promises.push(durationAggregation);

                return Promise.all(promises)
                        .then((result) => {
                            return {
                                report: {
                                    name: report.name,
                                    period: report.period,
                                    tags: report.tags
                                },
                                details: result[0].rows,
                                facets: result[1],
                                notes: result[2].rows,
                                total: result[3].time,
                                languages: result[4].rows,
                                projects: result[5].rows,
                                durations: result[6].rows
                            };
                        });
            });
};

/**
 * Get duration for all widget reports between dates for an user.
 * @param  {String} token         security token
 * @param  {Number} beginDateTime min date
 * @param  {Number} endDateTime   max date
 * @return {Promise}              the promise is resolved with times of each widget report otherwise
 *                                the promise is rejected if there is an error
 */
module.exports.getWidgetsTime = ({token}) => {
    let reports;

    return module.exports.getAllWidgets({token})
            .then(({rows}) => {
                reports = rows;
                let promises = rows.map((report) => {

                    let {period} = report;
                    let beginDateTime = moment();
                    let endDateTime = beginDateTime.clone();

                    if (period === 0) {
                        beginDateTime.startOf("day");
                        endDateTime.endOf("day");

                    } else if (period === 1) {
                        beginDateTime.startOf("week");
                        endDateTime.endOf("week");

                    } else if (period === 2) {
                        beginDateTime.startOf("month");
                        endDateTime.endOf("month");

                    } else if (period === 3) {
                        beginDateTime.startOf("year");
                        endDateTime.endOf("year");

                    } else if (period === 4) {
                        beginDateTime = report.selectBeginDateTime;
                        endDateTime = report.selectEndDateTime;
                    }

                    let parameters = {
                        token,
                        beginDateTime: +beginDateTime,
                        endDateTime: +endDateTime,
                        query: report.tags
                    };
                    return timeApi.getTotalTimeForReport(parameters);
                });

                return Promise.all(promises);
            })
            .then((times) => {
                let widgets = times.map((time, index) => {
                    return Object.assign(time, reports[index]);
                });

                return {widgets};
            });
};

/**
 * Sql query part in order to group the result on a specific period.
 * @type {Array}
 */
const steps = [
    "'+1 day'",    // day
    "'+7 day'",    // week
    "'+1 month', 'start of month'",  // month
    "'+1 year'",   // year
    "null"      // between dates
];

/**
 * Sql to create interval with a specific step
 * @param  {String} period  step used between dates
 * @return {string}         sql to execute in clause with to get all intervals
 */

let periodWithClause = (period) => `
periods(beginDate, endDate) AS (
    SELECT ?, CAST(strftime('\%s', datetime(? / 1000, 'unixepoch', ${period})) AS INTEGER) * 1000
    UNION ALL SELECT
        endDate,
        CAST(strftime('\%s', datetime(endDate / 1000, 'unixepoch', ${period})) AS INTEGER) * 1000
    FROM periods
    WHERE endDate < ?
)`;

/**
 * Sql queries to display somes chart in report details.
 * @type {Object}
 */
const chartQueries = {
    codeLanguage(period, clauses) {
        return `
            WITH RECURSIVE
            ${periodWithClause(period)},
            languages(language, begindate, enddate, number) AS (
                SELECT substr(json_extract(value, '$.data.language') || '+', 0, instr(json_extract(value, '$.data.language') || '+', '+')) as language,
                min(e.beginDateTime) as beginDateTime, max(e.endDateTime) as endDateTime, count(*)
                FROM event e, time t
                WHERE e.beginDateTime >= t.beginDateTime AND e.beginDateTime <= t.endDateTime
                AND t.begindatetime <= ? AND t.enddatetime >= ? ${clauses}
                AND json_extract(value, '$.type') = 'code'
                GROUP BY t.beginDateTime, t.endDateTime, language
            ),

			numbers(begindate, number) AS (
				SELECT p.beginDate, sum(c.number)
				FROM languages c, periods p
				WHERE c.endDate >= p.beginDate AND c.beginDate <= p.endDate
				GROUP BY p.beginDate
			)

            SELECT c.language,
            sum(min(c.endDate, p.endDate) - max(c.beginDate, p.beginDate)) * sum(c.number)*1.0 / n.number duration,
            p.beginDate period
            FROM languages c, periods p, numbers n
            WHERE c.endDate >= p.beginDate AND c.beginDate <= p.endDate
			AND p.beginDate = n.beginDate
            GROUP BY p.beginDate, c.language
            ORDER BY p.beginDate DESC
        `;
    },

    codeProject(period, clauses) {
        return `
            WITH RECURSIVE
            ${periodWithClause(period)},
            projects(project, begindate, enddate) AS (
                SELECT json_extract(value, '$.data.project') as project, min(e.beginDateTime) as beginDateTime, max(e.endDateTime) as endDateTime
                FROM event e, time t
                WHERE e.beginDateTime >= t.beginDateTime AND e.beginDateTime <= t.endDateTime
                AND t.begindatetime <= ? AND t.enddatetime >= ? ${clauses}
                AND json_extract(value, '$.type') = 'code'
                GROUP BY t.beginDateTime, t.endDateTime, project
            )

            SELECT c.project, sum(min(c.endDate, p.endDate) - max(c.beginDate, p.beginDate)) duration, p.beginDate period
            FROM projects c, periods p
            WHERE c.endDate >= p.beginDate AND c.beginDate <= p.endDate
            GROUP BY p.beginDate, c.project
            ORDER BY p.beginDate DESC
        `;
    },

    duration(period, clauses) {
        return `
            WITH RECURSIVE
            ${periodWithClause(period)},
            times(tags, begindate, enddate) AS (
                SELECT group_concat(t.tag), t.begindatetime, t.enddatetime
                FROM time t
                WHERE t.begindatetime <= ? AND t.enddatetime >= ? ${clauses}
                GROUP BY t.begindatetime, t.enddatetime
            )

            SELECT sum(min(t.endDate, p.endDate) - max(t.beginDate, p.beginDate)) duration, p.beginDate period
            FROM times t, periods p
            WHERE t.endDate >= p.beginDate AND t.beginDate <= p.endDate
            GROUP BY p.beginDate
            ORDER BY p.beginDate DESC
        `;
    }
};

/**
 * Get chart from code language events for an user.
 * @param  {String} token         security token
 * @param  {Number} period        period use for the chart
 * @param  {String} query         report query
 * @param  {Number} beginDateTime report min date
 * @param  {Number} endDateTime   report max date
 * @return {Promise}              the promise is resolved with chart data otherwise
 *                                the promise is rejected if there is an error
 */
module.exports.getChartCodeLanguage = ({token, period, query, beginDateTime, endDateTime}) => {
    return module.exports.getChartValues({token, call: chartQueries.codeLanguage, period, query, beginDateTime, endDateTime});
};

/**
 * Get chart from code project events for an user.
 * @param  {String} token         security token
 * @param  {Number} period        period use for the chart
 * @param  {String} query         report query
 * @param  {Number} beginDateTime report min date
 * @param  {Number} endDateTime   report max date
 * @return {Promise}              the promise is resolved with chart data otherwise
 *                                the promise is rejected if there is an error
 */
module.exports.getChartCodeProject = ({token, period, query, beginDateTime, endDateTime}) => {
    return module.exports.getChartValues({token, call: chartQueries.codeProject, period, query, beginDateTime, endDateTime});
};

/**
 * Get chart from duration tags for an user.
 * @param  {String} token         security token
 * @param  {Number} period        period use for the chart
 * @param  {String} query         report query
 * @param  {Number} beginDateTime report min date
 * @param  {Number} endDateTime   report max date
 * @return {Promise}              the promise is resolved with chart data otherwise
 *                                the promise is rejected if there is an error
 */
module.exports.getChartDuration = ({token, period, query, beginDateTime, endDateTime}) => {
    return module.exports.getChartValues({token, call: chartQueries.duration, period, query, beginDateTime, endDateTime});
};

/**
 * Get chart for an user.
 * @param  {String} token         security token
 * @param  {Function} call        call to create the sql query
 * @param  {Number}  period       period use for the chart
 * @param  {String} query         report query
 * @param  {Number} beginDateTime report min date
 * @param  {Number} endDateTime   report max date
 * @return {Promise}              the promise is resolved with chart data otherwise
 *                                the promise is rejected if there is an error
 */
module.exports.getChartValues = ({token, call, period, query, beginDateTime, endDateTime}) => {
    let {request, parameters} = timeApi.getClauses(query);
    let sql = call(steps[period], request);

    return userApi.isAuth({token})
            .then((db) => {
                return db.all(sql, +beginDateTime, +beginDateTime, +endDateTime, +endDateTime, +beginDateTime, ...parameters);
            });
};

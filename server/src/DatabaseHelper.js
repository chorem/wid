/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

let sqlite3 = require("sqlite3").verbose();
let baseDir = process.env["npm_package_config_WID_DATA_DIR_" + process.env.npm_lifecycle_event]
                        || process.env.npm_package_config_WID_DATA_DIR;
let dbMigration = require("./DatabaseMigration");
let fs = require("fs");

/**
 * Test if the database file is created.
 * @param  {String}  dbPath path of the file
 * @return {Boolean}        the promise returns true if the file is created otherwise false
 */
function isCreated(dbPath) {
    if (global.dev) {
        return Promise.resolve(false);
    }

    return new Promise((resolve) => {
        fs.access(dbPath, fs.F_OK, function(err) {
            return resolve(err === null);
        });
    });
}

/**
 * Test if the database file exists.
 * @param  {String}  dbPath path of the file
 * @return {Boolean}        the promise is resolve if the file exists otherwise is rejected
 */
function exists(dbPath) {
    if (global.dev) {
        return new Promise((resolve, reject) => {
            if (global.memoryDb) {
                resolve();
            } else {
                reject("Memory Database (dev mode) does not exist");
            }
        });
    }

    return new Promise((resolve, reject) => {
        fs.access(dbPath, fs.F_OK, function(err) {
            if (err === null) {
                resolve();
            } else {
                reject("Database " + dbPath + " does not exist");
            }
        });
    });
}

/**
 * Get the full path of the database file from the iser name.
 * @param  {String} name username represents the file name
 * @return {String}      full path to the database file
 */
function getDbPath(name) {
    return baseDir + name + ".sqlite";
}

/**
 * Valid the database name to avoid security problem
 * @param  {String} username represents the file name
 * @return {boolean}         true if the name is valid otherwise false
 */
function validDb(username) {
    return username && username !== "global" && username.match(/^\w+$/);
}

/**
 * Wrapper on database object from sqlite3 in order to have promise on each call,
 * and avoid create promise in each services.
 */
class Database {

    /**
     * Contructor to keep the real database from sqlite3.
     * @param  {object} db   database object from sqlite3
     * @param  {String} name username
     * @return {object}      wrapper of database from sqlite3
     */
    constructor(db, name) {
        this.db = db;
        this.name = name;
    }

    /**
     * Execute a sql query.
     * @param  {String} sql         the query
     * @param  {array}  parameters  the parameters of the query
     * @return {Promise}            the promise is resolve(true) if the query is execute without error and made changes,
     *                              resolve(false) if the query is execute without error and made no changes otherwise
     *                              the promise is rejected
     */
    run(sql, ...parameters) {
        let e = new Error();
        return new Promise((resolve, reject) => {
            this.db.run(sql, ...parameters, function(err) {
                if (err) {
                    e.message = `Error: ${err}\nSQL: ${sql}\nParameters: ${parameters}\n`;
                    reject(e);
                } else {
                    resolve(!!this.changes);
                }
            });
        });
    }

    /**
     * Get all rows of the sql query.
     * @param  {String} sql         the query
     * @param  {array}  parameters  the parameters of the query
     * @return {Promise}            the promise is resolve if the query return rows without error otherwise reject
     */
    all(sql, ...parameters) {
        let e = new Error();
        return new Promise((resolve, reject) => {
            this.db.all(sql, ...parameters, function(err, rows) {
                if (err || !rows) {
                    e.message = `Error: ${err}\nSQL: ${sql}\nParameters: ${parameters}\n`;
                    reject(e);
                } else {
                    resolve({rows});
                }
            });
        });
    }

    /**
     * Get row of the sql query.
     * @param  {String} sql         the query
     * @param  {array}  parameters  the parameters of the query
     * @return {Promise}            the promise is resolve if the query return row without error otherwise reject
     */
    get(sql, ...parameters) {
        let e = new Error();
        return new Promise((resolve, reject) => {
            this.db.get(sql, ...parameters, function(err, row) {
                if (err || !row) {
                    e.message = `Error: ${err}\nSQL: ${sql}\nParameters: ${parameters}\n`;
                    reject(e);
                } else {
                    resolve(row);
                }
            });
        });
    }

    /**
     * Get row of the sql query.
     * @param  {String} sql         the query
     * @param  {array}  parameters  the parameters of the query
     * @return {Promise}            the promise is resolve if the query return row or not without error otherwise reject
     */
    find(sql, ...parameters) {
        let e = new Error();
        return new Promise((resolve, reject) => {
            this.db.get(sql, ...parameters, function(err, row) {
                if (err) {
                    e.message = `Error: ${err}\nSQL: ${sql}\nParameters: ${parameters}\n`;
                    reject(e);
                } else {
                    resolve(row);
                }
            });
        });
    }

    /**
     * Prepare the sql query in order to reuse multiple the query. After call runPrepare.
     * @param  {String} sql         the query
     * @return {Object}             the prepared query
     */
    prepare(sql) {
        return this.db.prepare(sql);
    }

    /**
     * Execute the prepared query.
     * @param  {object} prepared    a prepared query from the call  this#prepare
     * @param  {array}  parameters  the parameters of the query
     * @return {Promise}            the promise is resolve(true) if the query is execute without error and made changes,
     *                              resolve(false) if the query is execute without error and made no changes otherwise
     *                              the promise is rejected
     */
    runPrepare(prepared, ...parameters) {
        let e = new Error();
        return new Promise((resolve, reject) => {
            prepared.run(...parameters, function(err) {
                if (err) {
                    e.message = `Error: ${err}\nSQL: ${prepared.sql}\nParameters: ${parameters}\n`;
                    reject(e);
                } else {
                    resolve(!!this.changes);
                }
            });
        });
    }

    /**
     * Create an transaction.
     * @return {Promise}    the promise is resolve if the transaction is created without error
     *                      otherwise the promise is rejected
     */
    beginTransaction() {
        let e = new Error();
        return new Promise((resolve, reject) => {
            this.db.run("BEGIN", (err) => {
                if (err) {
                    e.message = `Error: ${err}\nSQL: BEGIN\n`;
                    reject(e);
                } else {
                    this.inTransaction = true;
                    resolve(true);
                }
            });
        });
    }

    /**
     * Rollback the current transaction.
     * @param  {Error} error the error is thrown the rollback
     * @return {Promise}       the promise is allways rejected with the error
     */
    rollbackTransaction(error) {
        let e = new Error();
        this.inTransaction = false;

        return new Promise((resolve, reject) => {
            this.db.run("ROLLBACK", (err) => {
                e.message = `Error: ${error || err}\nSQL: ROLLBACK\n`;
                reject(e);
            });
        });
    }

    /**
     * Commit the current transaction. Try to rollback the transaction if the
     * commit thrown an error.
     * @return {Promise}    the promise is resolved if the commit is done without error
     *                      otherwise the transaction is rollbacked and  the promise is rejected
     */
    commitTransaction() {
        let e = new Error();
        this.inTransaction = false;

        return new Promise((resolve, reject) => {
            this.db.run("COMMIT", (err) => {
                if (err) {
                    e.message = `Error: ${err}\nSQL: COMMIT\n`;
                    reject(e);
                } else {
                    resolve(true);
                }
            });
        })
        .catch(this.rollbackTransaction);
    }
}

/**
 * Get the username for database from the token.
 * @param  {String}     security token
 * @return {String}     the username from the database otherwise null
 */
module.exports.getName = (token) => {
    if (token) {
        let split = token.split("@");
        let length = split.length || 0;
        if (length === 2) {
            return split[1];
        }
    }
    return null;
};


/**
 * Create the user database. The database is migrated if it is needed.
 * @param  {String} username  username uses as database name
 * @return {Promise}          the promise is resolved with the database otherwise
 *                            the promise is rejected if there is an error.
 */
module.exports.createDb = (username) => {
    if (!validDb(username)) {
        return Promise.reject("Invalid username");
    }

    global.memoryDb = global.dev ? new sqlite3.Database(":memory:") : null;
    let db = global.memoryDb || new sqlite3.Database(baseDir + username + ".sqlite");

    return dbMigration.doSqls(db, dbMigration.schemaForUser)
    .then(() => {
        return new Database(db);
    });
};

/**
 * Get the user database from a name. The database is migrated if it is needed.
 * @param  {String} name    database name
 * @return {Promise}        the promise is resolved with the database otherwise
 *                          the promise is rejected if there is an error.
 */
module.exports.getDbByName = (name) => {
    if (!validDb(name)) {
        return Promise.reject("Invalid username");
    }

    let dbPath = getDbPath(name);
    let db;

    return exists(dbPath)
    .then(() => {
        db = global.memoryDb || new sqlite3.Database(dbPath);
        return dbMigration.applyMigration(db, dbMigration.migrationsForUser);
    })
    .then(() => {
        return new Database(db, name);
    });
};

/**
* Get the user database from a token. The database is migrated if it is needed.
* @param  {String} token   security token
* @return {Promise}        the promise is resolved with the database otherwise
*                          the promise is rejected if there is an error.
*/
module.exports.getDb = (token) => {
    if (token instanceof Database) {
        return dbMigration.applyMigration(token.db, dbMigration.migrationsForUser)
        .then(() => {
            return token;
        });
    }

    if (typeof token !== "string") {
        return Promise.reject("Invalid token");
    }

    let name = module.exports.getName(token);
    if (!validDb(name)) {
        return Promise.reject("Invalid username");
    }

    let dbPath = getDbPath(name);
    let db;

    return exists(dbPath)
    .then(() => {
        db = global.memoryDb || new sqlite3.Database(dbPath);
        return dbMigration.applyMigration(db, dbMigration.migrationsForUser);
    })
    .then(() => {
        return new Database(db, name);
    });
};

let globalDBPromise = null;
/**
 * Get global database, create or migrate if it is needed.
 * @return {Promise}    the promise is resolved with the database otherwise
 *                      the promise is rejected if there is an error
 */
module.exports.getGlobalDb = () => {
    if (!globalDBPromise) {
        let globalDB = null;

        let dbPath = baseDir + "/global.sqlite";
        globalDBPromise = isCreated(dbPath)
        .then((created) => {
            globalDB = global.dev ? new sqlite3.Database(":memory:") : new sqlite3.Database(dbPath);

            if (!created) {
                return dbMigration.doSqls(globalDB, dbMigration.schemaForGlobal);
            }

            return Promise.resolve();
        })
        .then(() => {
            return dbMigration.applyMigration(globalDB, dbMigration.migrationsForGlobal);
        })
        .then(() => {
            return new Database(globalDB);
        });
    }

    return globalDBPromise;
};

/**
 * Get all table names in database.
 * @param  {Database} db    the database
 * @return {Promise}        the promise is resolve with the table names otherwise
 *                          is rejected if there is an error
 */
module.exports.getAllTableNames = (db) => {
    return db.all("SELECT name as tableName FROM sqlite_master WHERE type='table'");
};

/**
 * Drop a table.
 * @param  {Database} db        the database
 * @param  {String} tableName   table name
 * @return {Promise}        the promise is resolve otherwise is rejected if there is an error
 */
module.exports.dropTable = (db, tableName) => {
    return db.run("DROP TABLE IF EXISTS " + tableName);
};

/**
 * List of tables not exported.
 * @type {Array}
 */
module.exports.NO_EXPORT_TABLES = ["user", "token"];

/**
 * List of tables not imported.
 * @type {Array}
 */
module.exports.NO_IMPORT_TABLES = ["user", "token", "version"];

/**
 * Drop all tables if not in NO_IMPORT_TABLES variable.
 * @param  {Database} db    the database
 * @return {Promise}        the promise is resolve otherwise is rejected if there is an error
 */
module.exports.dropAllTables = (db) => {
    return module.exports.getAllTableNames(db)
    .then((tableNames) => {
        let promises = [];

        tableNames.rows.forEach(({tableName}) => {
            if (module.exports.NO_IMPORT_TABLES.indexOf(tableName) === -1) {
                promises.push(module.exports.dropTable(db, tableName));
            }
        });

        return Promise.all(promises);
    });
};

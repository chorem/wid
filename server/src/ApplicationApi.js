/* eslint strict: 0 */
/* eslint no-process-env: 0 */
// Needed in Node in order to use let
"use strict";

let path = require("path");
let uuid = require("uuid");
let userApi = require("./UserApi");
let eventApi = require("./EventApi");
let dbHelper = require("./DatabaseHelper");
let nodemailer = require("nodemailer");
let moment = require("moment");

let serverMail = JSON.parse(process.env["npm_package_config_WID_SERVER_MAIL_" + process.env.npm_lifecycle_event]
                            || process.env.npm_package_config_WID_SERVER_MAIL);
let serverFrom = process.env["npm_package_config_WID_SERVER_MAIL_FROM_" + process.env.npm_lifecycle_event]
                        || process.env.npm_package_config_WID_SERVER_MAIL_FROM;

let transport = nodemailer.createTransport(serverMail);

/**
 * Add an identity for a user in global database. An email is sent to email, the user
 * have to validate the email.
 * @param  {String} identifier          email to add
 * @param  {Database} database          username represents the database file
 * @param  {String} [location=""}]      location of the server
 * @return {Promise}                    the promise is resolved with the identity object otherwise
 *                                      the promise is rejected if there is an error
 */
module.exports.addIdentity = ({identifier, database, location = ""}) => {
    let validation = uuid.v4();
    let id = uuid.v1();

    return dbHelper.getGlobalDb()
            .then((db) => {
                return db.run(
                    "INSERT INTO identity (id, identifier, database, validation) VALUES (?, ?, ?, ?)",
                    id, identifier, database, validation
                );

            })
            .then(() => {
                let identity = {id, identifier, database};

                return new Promise((resolve, reject) => {
                    // No email for test
                    if (global.dev) {
                        resolve(Object.assign(identity, {validation}));
                    } else {

                        transport.sendMail({
                            from: serverFrom,
                            to: identifier,
                            subject: "New identity for Wid",
                            text: `Please you need to validate the email ${identifier} before using it.\nFollow this link ${location}/${validation}.\n\nRegards.\nWid teams`
                        }, (err) => {
                            if (err) {
                                reject(err);
                            } else {
                                resolve(identity);
                            }
                        });
                    }
                });
            });
};

/**
 * Validate the email. If the email is not validate, the user cannot receive event
 * for this email.
 * @param  {String} token       security token
 * @param  {String} validation  validation token generate when a new identity is added
 * @return {Promise}            the promise is resolved otherwise the promise is rejected
 *                              if there is an error
 */
module.exports.validateIdentity = ({token, validation}) => {
    let database;

    return userApi.getUser({token})
            .then((user) => {
                database = user.username;
                return dbHelper.getGlobalDb();
            })
            .then((db) => {
                return db.run(
                    "UPDATE identity SET validation = 'done' WHERE database = ? AND validation = ?",
                    database, validation
                );
            })
            .then((r) => {
                if (!r) {
                    return Promise.reject("Invalid email");
                }
                return Promise.resolve(r);
            });
};

/**
 * Create a new identity for an user.
 * @param  {String} token               security token
 * @param  {String} identifier          email to add
 * @param  {String} [location=""}]      location of server
 * @return {Promise}                    the promise is resolved with the identity object otherwise
 *                                      the promise is rejected if there is an error.
 */
module.exports.createIdentity = ({token, identifier, location = ""}) => {
    return userApi.getUser({token})
            .then((user) => {
                return module.exports.addIdentity({identifier, database: user.username, location});
            });
};

/**
 * Remove an identity from an id for an user.
 * @param  {String} token security token
 * @param  {String} id    id of identity to remove
 * @return {Promise}            the promise is resolved otherwise the promise is rejected
 *                              if there is an error
 */
module.exports.removeIdentity = ({token, id}) => {
    let database;

    return dbHelper.getGlobalDb()
            .then((db) => {
                database = db;
                return userApi.getUser({token});
            })
            .then((user) => {
                return database.run(
                    "DELETE FROM identity WHERE id = ? AND database = ?",
                    id, user.username
                );
            });
};

/**
 * Get all identities for an user.
 * @param  {String} token security token
 * @return {Promise}            the promise is resolved with identities otherwise the promise is rejected
 *                              if there is an error
 */
module.exports.getIdentities = ({token}) => {
    let database;

    return dbHelper.getGlobalDb()
            .then((db) => {
                database = db;
                return userApi.getUser({token});
            })
            .then((user) => {
                return database.all(
                    "SELECT id, identifier, (validation = 'done') as valid FROM identity WHERE database = ?",
                    user.username
                );
            });
};

/**
 * Get the database name from an identity.
 * @param  {String} identifier          email of identity
 * @param  {boolean} [validation=true}] test if the identity is validated
 * @return {Promise}                    the promise is resolved with database name otherwise the promise is rejected
 *                                      if there is an error
 */
module.exports.getDatabase = ({identifier, validation = true}) => {
    return dbHelper.getGlobalDb()
            .then((db) => {
                return db.get(
                    "SELECT database FROM identity WHERE identifier = ?" + (validation ? " AND validation = 'done'" : ""),
                    identifier
                );
            });
};

/**
 * Create an api key with name for an user.
 * @param  {String} token security token
 * @param  {String} name  name of api key
 * @return {Promise}      the promise is resolved with key object otherwise the promise is rejected
 *                        if there is an error
 */
module.exports.createKey = ({token, name}) => {
    let database;
    let id = uuid.v1();
    let key = uuid.v1();

    return dbHelper.getGlobalDb()
            .then((db) => {
                database = db;
                return userApi.getUser({token});
            })
            .then((user) => {
                return database.run(
                    "INSERT INTO application (id, name, database, key) VALUES (?, ?, ?, ?)",
                    id, name, user.username, key
                );
            })
            .then(() => {
                return {id, name, key};
            });
};

/**
 * Remove an api key from an id for an user.
 * @param  {String} token security token
 * @param  {String} id    id of key to remove
 * @return {Promise}      the promise is resolved otherwise the promise is rejected
 *                        if there is an error
 */
module.exports.removeKey = ({token, id}) => {
    let database;

    return dbHelper.getGlobalDb()
            .then((db) => {
                database = db;
                return userApi.getUser({token});
            })
            .then((user) => {
                return database.run(
                    "DELETE FROM application WHERE id = ? AND database = ?",
                    id, user.username
                );
            });
};

/**
 * Get all api keys for an user.
 * @param  {String} token security token
 * @return {Promise}      the promise is resolved with the keys otherwise the promise is rejected
 *                        if there is an error
 */
module.exports.getKeys = ({token}) => {
    let database;

    return dbHelper.getGlobalDb()
            .then((db) => {
                database = db;
                return userApi.getUser({token});
            })
            .then((user) => {
                return database.all(
                    "SELECT id, name, key FROM application WHERE database = ?",
                    user.username
                );
            });
};

/**
 * Get the database name for a key.
 * @param  {String} key  api key
 * @return {Promise}     the promise is resolved with the database name otherwise the promise is rejected
 *                       if there is an error
 */
module.exports.isAutorize = ({key}) => {
    return dbHelper.getGlobalDb()
            .then((db) => {
                return db.get(
                    "SELECT database FROM application where key = ?",
                    key
                );
            });
};

/**
 * Create a virtual token to be able to call the authotificated service from a key and an identity.
 * The identifier must be validate.
 * @param  {String} key         api key
 * @param  {String} identifier  email of identity
 * @return {Promise}            the promise is resolved with the token otherwise the promise is rejected
 *                              if there is an error
 */
module.exports.createToken = ({key, identifier}) => {
    let token;

    return module.exports.isAutorize({key})
            .then(() => {
                return module.exports.getDatabase({identifier, validation: true});
            })
            .then(({database}) => {
                token = uuid.v4() + "@" + database;
                return userApi.addToken(database, token);
            })
            .then(() => {
                return token;
            });
};

/**
 * Create a virtual token to be able to call the authenticated service from a key.
 * @param  {String} key  api key
 * @return {Promise}     the promise is resolved with the token otherwise the promise is rejected
 *                       if there is an error
 */
module.exports.createTokenWithKey = ({key}) => {
    let token;

    return module.exports.isAutorize({key})
            .then(({database}) => {
                token = uuid.v4() + "@" + database;
                return userApi.addToken(database, token);
            })
            .then(() => {
                return token;
            });
};

/**
 * Utility for add events from an repository webhook.
 * @param  {String} key        api key
 * @param  {String} identifier email of identity
 * @param  {String} data       events to add
 * @return {Promise}           the promise is resolved with the token and created events otherwise the promise is rejected
 *                             if there is an error
 */
module.exports.onRepoWebHook = ({key, identifier, data}) => {
    let token;
    let events;

    return module.exports.createToken({key, identifier})
            .then((tokenValue) => {
                token = tokenValue;
                return eventApi.addAll({token, events: data, key});
            })
            .then((eventsCreated) => {
                events = eventsCreated.events;
                return userApi.removeToken({token});
            })
            .then(() => {
                return {token, events};
            });
};

/**
 * Generic add events from an webhook.
 * @param  {String} key    api key
 * @param  {String} data   events to add
 * @return {Promise}       the promise is resolved with the token and created events otherwise the promise is rejected
 *                         if there is an error
 */
module.exports.onEventsWebHook = ({data, key}) => {
    let token;
    let events;

    return module.exports.createTokenWithKey({key})
            .then((tokenValue) => {
                token = tokenValue;
                return eventApi.addAll({token, events: data, key});
            })
            .then((eventsCreated) => {
                events = eventsCreated.events;
                return userApi.removeToken({token});
            })
            .then(() => {
                return {token, events};
            });
};

/**
 * Webhook for Gitlab.
 * @param  {object} data data sent by Gitlab
 * @param  {String} key  api key
 * @return {Promise}     the promise is resolved with the token and created events otherwise the promise is rejected
 *                       if there is an error
 */
module.exports.onGitLabWebHook = ({data, key}) => {
    let email = data.user_email;

    let events = data.commits.map((commit) => {
        return {
            beginDateTime: +moment(commit.timestamp),
            endDateTime: +moment(commit.timestamp),
            type: "scm",
            source: "gitlab",
            data: {
                project: data.project.name,
                namespace: data.project.namespace,
                type: "commit",
                id: commit.id,
                message: commit.message,
                author: commit.author.email,
                url: commit.url,
                added: commit.added,
                modified: commit.modified,
                removed: commit.removed
            }
        };
    });

    return module.exports.onRepoWebHook({key, identifier: email, data: events});
};

/**
 * Webhook for Github.
 * @param  {object} data data sent by Github
 * @param  {String} key  api key
 * @return {Promise}     the promise is resolved with the token and created events otherwise the promise is rejected
 *                       if there is an error
 */
module.exports.onGitHubWebHook = ({data, key}) => {
    let commits = data.commits;
    if (commits) {
        let email = commits[0].author.email;

        let events = data.commits.map((commit) => {
            return {
                beginDateTime: +moment(commit.timestamp),
                endDateTime: +moment(commit.timestamp),
                type: "scm",
                source: "github",
                data: {
                    project: data.repository.name,
                    namespace: data.repository.full_name,
                    type: "commit",
                    id: commit.id,
                    message: commit.message,
                    author: commit.author.email,
                    url: commit.url,
                    added: commit.added,
                    modified: commit.modified,
                    removed: commit.removed
                }
            };
        });

        return module.exports.onRepoWebHook({key, identifier: email, data: events});
    }

    // Ping call
    return Promise.resolve();
};

/**
 * Webhook for Wakatime plugin.
 * @param  {object} data data sent by Wakatime plugin
 * @param  {String} key  api key
 * @return {Promise}     the promise is resolved with the token and created events otherwise the promise is rejected
 *                       if there is an error
 */
module.exports.onWakatimeWebHook = ({headers, data, key}) => {
    let plugin = headers["user-agent"].split(/\s/).pop().split(/-wakatime|\//).shift();

    let events = [];
    events.pushEvent = function(wakatimeEvent) {
        const time = wakatimeEvent.time * 1000;
        const result = {
            beginDateTime: time,
            endDateTime: time,
            type: "code",
            source: "wakatime",
            data: {
                host: headers["x-machine-name"],
                userAgent: headers["user-agent"],
                project: wakatimeEvent.project,
                language: wakatimeEvent.language,
                branch: wakatimeEvent.branch,
                entity: wakatimeEvent.entity,
                file: path.basename(wakatimeEvent.entity),
                type: wakatimeEvent.type,
                lines: wakatimeEvent.lines,
                plugin
            }
        };
        this.push(result);
    };

    // The new wakatime api is retruning an array of events in data attribute
    if (Array.isArray(data)) {
        data.forEach(wakatimeEvent => events.pushEvent(wakatimeEvent));
    } else {
        events.pushEvent(data);
    }

    return module.exports.onEventsWebHook({data: events, key});
};

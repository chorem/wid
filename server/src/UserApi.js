/* eslint strict: 0 */
/* eslint no-process-env: 0 */
// Needed in Node in order to use let
"use strict";

let uuid = require("uuid");
let UnauthorizedError = require("./UnauthorizedError");
let timeApi = require("./TimeApi");
let applicationApi = require("./ApplicationApi");
let dbHelper = require("./DatabaseHelper");
let crypto = require("crypto");
let nodemailer = require("nodemailer");

let serverMail = JSON.parse(process.env["npm_package_config_WID_SERVER_MAIL_" + process.env.npm_lifecycle_event]
                            || process.env.npm_package_config_WID_SERVER_MAIL);
let serverFrom = process.env["npm_package_config_WID_SERVER_MAIL_FROM_" + process.env.npm_lifecycle_event]
                        || process.env.npm_package_config_WID_SERVER_MAIL_FROM;

let transport = nodemailer.createTransport(serverMail);

/**
 * Hash the password with a salt.
 * @param  {String} passwd password to hash
 * @param  {String} salt   salt use to hash
 * @return {String}        password hashed with salt
 */
let cryptPassword = (passwd, salt) => {
    salt = salt || crypto.randomBytes(128).toString("base64");
    let crypted = crypto.pbkdf2Sync(passwd, salt, 10000, 512, "sha512");
    return ("sha512$" + salt + "$" + crypted.toString("base64"));
};

/**
 * Check if the password is correct from the database.
 * @param  {database} db     user database
 * @param  {String} passwd   password to verify
 * @return {Promise}         the promise is resolved if the password is correct data otherwise
 *                           he promise is rejected or if there is an error
 */
let verifyPassword = (db, passwd) => {
    return db.get("SELECT id, username, email, passwd, generatedPasswd FROM user")
            .then((row) => {
                let mainSplit = row.passwd.split("$");
                let mainPasswd = mainSplit[2];

                // Plain password
                if (!mainPasswd && row.passwd === passwd) {
                    return row;
                }

                // Main password
                let saltMainPasswd = mainSplit[1];
                let cryptPasswd = cryptPassword(passwd, saltMainPasswd);
                if (cryptPasswd === row.passwd) {
                    return row;
                }

                // Generated password
                if (row.generatedPasswd) {
                    let generateSplit = row.generatedPasswd.split("$");
                    let generatedSalt = generateSplit[1];
                    let generatedDate = parseInt(generateSplit[3], 10);

                    if (Date.now() - generatedDate <= 24 * 60 * 60 * 1000) { // 24h
                        cryptPasswd = cryptPassword(passwd, generatedSalt) + "$" + generatedDate;

                        if (cryptPasswd === row.generatedPasswd) {
                            return row;
                        }
                    }
                }

                return Promise.reject("Invalid username or password");
            });
};

/**
 * Create an account.
 * @param  {String} username       account username
 * @param  {String} email          account email
 * @param  {String} passwd         account password
 * @param  {String} [location=""}] location of the server, use in email validation
 * @return {Promise}               the promise is resolved with new identity otherwise
 *                                 the promise is rejected if there is an error
 */
module.exports.signUp = ({username, email, passwd, location = ""}) => {
    return dbHelper.createDb(username)
            .then((db) => {
                return db.run("INSERT INTO user (id, username, email, passwd) VALUES (?, ?, ?, ?)", uuid.v1(), username, email, cryptPassword(passwd));
            })
            .then(() => {
                return applicationApi.addIdentity({identifier: email, database: username, location});
            });
};

/**
 * Add in user object the last time where a tag is enter for an user.
 * @param  {String} token security token
 * @param  {object} user  user object to add the date
 * @return {Promise}      the promise is resolved with date into the user otherwise
 *                        the promise is rejected if there is an error
 */
let addBeginDateTimeForUser = (token, user) => {
    return timeApi.getBeginDateTime({token})
            .catch(() => {
                return {beginDateTime: null};
            })
            .then((beginDateTime) => {
                return Object.assign({token}, user, beginDateTime);
            });
};

/**
 * Add a security token for an user.
 * @param  {String} username represents the database name
 * @param  {String} token    security token
 * @return {Promise}         the promise is resolved if the token is created otherwise
 *                           the promise is rejected if there is an error
 */
module.exports.addToken = (username, token) => {
    return dbHelper.getDbByName(username)
            .then((db) => {
                return db.run("INSERT INTO token (id) VALUES (?)", token);
            });
};

/**
 * Login an user. Verify if the database exists for the username then verify the password.
 * Finally generate the security token.
 * @param  {String} username account username
 * @param  {String} passwd   account password
 * @return {Promise}         the promise is resolved with the user if the user is logged otherwise
 *                           the promise is rejected if there is an error
 */
module.exports.signIn = ({username, passwd}) => {
    return dbHelper.getDbByName(username)
            .then((db) => {
                return verifyPassword(db, passwd);
            })
            .then((user) => {
                let token = uuid.v4() + "@" + username;

                return module.exports.addToken(username, token)
                            .then(() => {
                                return addBeginDateTimeForUser(token, user);
                            });
            });
};

/**
 * Generate and send a temporary password.
 * @param  {String} email identity where send the email
 * @return {Promise}         the promise is resolved if a temporary password is sent otherwise
 *                           the promise is rejected if there is an error
 */
module.exports.generatePassword = ({email}) => {
    let newPasswd;

    return applicationApi.getDatabase({identifier: email, validation: false})
    .then(({database}) => {
        return dbHelper.getDbByName(database);
    })
    .then((db) => {
        newPasswd = Math.random().toString(36).slice(-8);
        return db.run("UPDATE user SET generatedPasswd = ?", cryptPassword(newPasswd) + "$" + Date.now());
    })
    .then(() => {
        return new Promise((resolve, reject) => {
            // No email for test
            if (global.dev) {
                resolve(newPasswd);

            } else {
                transport.sendMail({
                    from: serverFrom,
                    to: email,
                    subject: "New password for Wid",
                    text: `Your new password is ${newPasswd}. The password is valid 24h.\n\nRegards.\nWid teams`
                }, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(true);
                    }
                });
            }
        });
    });
};

/**
 * Change the password for an user.
 * @param  {String} token     security token
 * @param  {String} oldPasswd old password
 * @param  {String} newPasswd new password
 * @return {Promise}          the promise is resolved if the password is changed otherwise
 *                            the promise is rejected if there is an error
 */
module.exports.changePassword = ({token, oldPasswd, newPasswd}) => {
    let database;
    return module.exports.isAuth({token})
            .then((db) => {
                database = db;
                return verifyPassword(db, oldPasswd);
            })
            .then(() => {
                return database.run("UPDATE user SET passwd = ?", cryptPassword(newPasswd));
            });
};

/**
 * Verify the security token is authorized.
 * @param  {String} token security token to verify
 * @return {Promise}          the promise is resolved with Database if the token is valid otherwise
 *                            the promise is rejected if there is an error or the token is invalid
 */
module.exports.isAuth = ({token}) => {
    let database;

    return dbHelper.getDb(token)
            .then((db) => {
                if (db === token) {
                    database = db;
                    return true;
                }

                database = db;
                return db.get("SELECT 1 FROM token where id = ?", token);
            })
            .then(() => {
                return database;
            })
            .catch((err) => {
                return Promise.reject(new UnauthorizedError(err));
            });
};

/**
 * Get user information from token.
 * @param  {String} token security token
 * @return {Promise}          the promise is resolved with user otherwise
 *                            the promise is rejected if there is an error
 */
module.exports.getUser = ({token}) => {
    return module.exports.isAuth({token})
            .then((db) => {
                return db.get("SELECT id, username, email FROM user");
            })
            .then((row) => {
                return addBeginDateTimeForUser(token, row);
            });
};

/**
 * Invalidate the security token.
 * @param  {String} token security token
 * @return {Promise}      the promise is resolved if the token is invalidated otherwise
 *                        the promise is rejected if there is an error
 */
module.exports.removeToken = module.exports.signOut = ({token}) => {
    return module.exports.isAuth({token})
            .then((db) => {
                return db.run("DELETE FROM token WHERE id = ?", token);
            });
};

/**
 * Export only one table for an user.
 * @param  {String} token     security token
 * @param  {String} tableName table name to export
 * @return {[type]}           [description]
 */
function exportTable({token, tableName}) {
    return module.exports.isAuth({token})
            .then((db) => {
                return db.all("SELECT * FROM " + tableName);

            })
            .then(({rows}) => {
                let content = JSON.stringify(rows, null, " ");
                return "\"" + tableName + "\":" + content;
            });
}

/**
 * Export all database for an user.
 * @param  {String} token security token
 * @return {Promise}      the promise is resolved with dump of database otherwise
 *                        the promise is rejected if there is an error
 */
module.exports.exportDb = ({token}) => {
    let database;

    return module.exports.isAuth({token})
            .then((db) => {
                database = db;
                return dbHelper.getAllTableNames(db);
            })
            .then((tableNames) => {
                let promises = [];

                tableNames.rows.forEach(({tableName}) => {
                    if (dbHelper.NO_EXPORT_TABLES.indexOf(tableName) === -1) {
                        promises.push(exportTable({token, tableName}));
                    }
                });

                return Promise.all(promises);
            })
            .then((contents) => {
                return {
                    name: "wid-" + database.name + "-" + Date.now(),
                    file: "{" + contents.join(",\n") + "}"
                };
            });
};

module.exports.getPriorites = ({token}) => {
    return module.exports.isAuth({token})
            .then((db) => {
                return db.get("SELECT tags FROM priorities");
            })
            .then((row) => {
                return row.tags.split(",");
            });
};

module.exports.setPriorites = ({token, tags}) => {
    return module.exports.isAuth({token})
            .then((db) => {
                return db.run("UPDATE priorities SET tags = ?", tags.join(","));
            });
};

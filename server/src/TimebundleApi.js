/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

let userApi = require("./UserApi");
let timeApi = require("./TimeApi");
let uuid = require("uuid");
let moment = require("moment");
let fetch = require("node-fetch");

/**
 * Add query on timebundle task for an user.
 * @param  {String} token      security token
 * @param  {String} projectUrl project url on timebundle
 * @param  {String} taskUrl    task url on timebundle
 * @param  {String} query      query to find activities to synchronize
 * @return {Promise}      the promise is resolved with the query if it is added otherwise
 *                        the promise is rejected if there is an error
 */
module.exports.add = ({token, projectUrl, taskUrl, query}) => {
    let id = uuid.v1();

    return userApi.isAuth({token})
            .then((db) => {
                return db.run(
                    "INSERT INTO timebundle (id, projectUrl, taskUrl, query) VALUES (?, ?, ?, ?)",
                    id, projectUrl, taskUrl, query
                );
            })
            .then(() => {
                return {id, projectUrl, taskUrl, query};
            });
};

/**
 * Replace the query for a user task.
 * @param  {String} token security token
 * @param  {String} id    query id to modified
 * @param  {String} query new query to apply
 * @return {Promise}      the promise is resolved if the query is modified otherwise
 *                        the promise is rejected if there is an error
 */
module.exports.save = ({token, id, query}) => {
    return userApi.isAuth({token})
        .then((db) => {
            return db.run(
                "UPDATE timebundle SET query = ? WHERE id = ?",
                query, id
            );
        });
};

/**
 * Remove a query for a user.
 * @param  {String} token security token
 * @param  {String} id    query id to remove
 * @return {Promise}      the promise is resolved if the query is removed otherwise
 *                        the promise is rejected if there is an error
 */
module.exports.remove = ({token, id}) => {
    return userApi.isAuth({token})
            .then((db) => {
                return db.run(
                    "DELETE FROM timebundle WHERE id = ?",
                    id
                );
            });
};

/**
 * Update last synchronization from a date for all queries. Call when a activity changes.
 * @param  {String} token   security token
 * @param  {Number} refDate reference date
 * @return {Promise}        the promise is resolved if the date is updated otherwise
 *                          the promise is rejected if there is an error
 */
module.exports.updateLastSync = ({token, refDate}) => {
    return userApi.isAuth({token})
            .then((db) => {
                return db.run(
                    "UPDATE timebundle SET lastSync = ? WHERE lastSync >= ? AND lastSync is not null",
                    refDate, refDate
                );
            });
};

/**
 * Listen activity changes to call updateLastSync.
 */
timeApi.timeChange.on("change", (token, dates) => {
    let refDate = Math.min.apply(Math, dates);
    module.exports.updateLastSync({token, refDate});
});

/**
 * Get all project url for an user.
 * @param  {String} token   security token
 * @return {Promise}        the promise is resolved with projects otherwise
 *                          the promise is rejected if there is an error
 */
module.exports.getProjects = ({token}) => {
    return userApi.isAuth({token})
            .then((db) => {
                return db.all("SELECT DISTINCT projectUrl FROM timebundle");
            });
};

/**
 * Get all tasks for a project url.
 * @param  {String} token      security token
 * @param  {String} projectUrl project url
 * @return {Promise}           the promise is resolved with tasks otherwise
 *                             the promise is rejected if there is an error
 */
module.exports.getTasks = ({token, projectUrl}) => {
    return userApi.isAuth({token})
            .then((db) => {
                return db.all(
                    "SELECT id, taskUrl, query, lastSync FROM timebundle WHERE projectUrl = ?",
                    projectUrl
                );
            });
};

/**
 * Get all tasks for an user.
 * @param  {String} token security token
 * @return {Promise}      the promise is resolved with tasks otherwise
 *                        the promise is rejected if there is an error
 */
module.exports.getSyncTasks = ({token}) => {
    return userApi.isAuth({token})
            .then((db) => {
                return db.all("SELECT taskUrl, query, lastSync FROM timebundle");
            });
};

/**
 * Get times to json format for a task url.
 * @param  {String}  token         security token
 * @param  {Number}  now           now date
 * @param  {String}  taskUrl       task url
 * @param  {String}  query         query to get times
 * @param  {Number}  lastSync      reference date to get times
 * @param  {Boolean} force         force the sync of all day
 * @return {Promise}               the promise is resolved with times to json format otherwise
 *                                 the promise is rejected if there is an error
 */
module.exports.getJsonForTask = ({token, now, taskUrl, query, lastSync = 0, force}) => {
    let beginDateTime = force ? 0 : lastSync;
    return timeApi.getTimeForReport({token, beginDateTime, endDateTime: now, query})
            .then(({rows}) => {
                let json = {
                    URL: taskUrl,
                    startDate: moment(lastSync || 0).format(moment.defaultFormat),
                    endDate: moment(now).format(moment.defaultFormat),
                    periods: []
                };

                let periods = json.periods;
                rows.forEach((item) => {
                    periods.push({
                        id: "wid-" + item.beginDateTime,
                        startDate: moment(item.beginDateTime).format(moment.defaultFormat),
                        duration: ~~((item.endDateTime - item.beginDateTime) / 1000),
                        info: item.tags
                    });
                });

                return json;
            });
};

/**
 * Call timebundle server to synchronize times for a task.
 * @param  {String} taskUrl task url to Call
 * @param  {object} json    contains all times to synchronize
 * @return {Promise}        the promise is resolved if the call succeded otherwise
 *                          the promise is rejected if there is an error
 */
module.exports.syncTaskOnServer = ({taskUrl, json}) => {
    return fetch(taskUrl, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(json)
    })
    .then((response) => {
        if (response.status >= 400) {
            return Promise.reject("Error during sync");
        }
        return Promise.resolve(true);
    });
};

/**
 * Update last synchronization from a date and task url for all queries.
 * @param  {String} token   security token
 * @param  {String} taskUrl task url
 * @param  {Number} now     reference date
 * @return {Promise}        the promise is resolved if the date is updated otherwise
 *                          the promise is rejected if there is an error
 */
module.exports.updateTaskSyncDate = ({token, taskUrl, now}) => {
    return userApi.isAuth({token})
            .then((db) => {
                return db.run(
                    "UPDATE timebundle SET lastSync = ? WHERE taskUrl = ?",
                    now, taskUrl
                );
            });
};

/**
 * Synchronize a task for an user.
 * @param  {String} token         security token
 * @param  {Number} now           reference date
 * @param  {String} taskUrl       task url to synchronize
 * @param  {String} query         query to get times
 * @param  {Number} lastSync      last synchronization date
 * @param  {Boolean} force        force the sync of all day
 * @return {Promise}              the promise is resolved if the task is synchronized otherwise
 *                                the promise is rejected if there is an error
 */
module.exports.syncTask = ({token, now, taskUrl, query, lastSync = 0, force}) => {
    return module.exports.getJsonForTask({token, now, taskUrl, query, lastSync, force})
            .then((json) => {
                return module.exports.syncTaskOnServer({taskUrl, json});
            })
            .then(() => {
                return module.exports.updateTaskSyncDate({token, taskUrl, now});
            });
};

/**
 * Synchronize all task urls for an user.
 * @param  {String}  token security token
 * @param  {Number}  now   reference date
 * @param  {Array}   rows  array of task urls
 * @param  {Boolean} force force the sync of all day
 * @return {Promise}       the promise is resolved if the tasks are synchronized otherwise
 *                         the promise is rejected if there is an error
 */
module.exports.syncTasks = ({token, now, rows, force}) => {
    if (rows.length) {
        let item = rows.shift();
        return module.exports.syncTask(Object.assign(item, {
            token,
            now: +now,
            force
        }))
        .then(() => {
            return module.exports.syncTasks({token, now, rows, force});
        });
    }

    return Promise.resolve();
};

/**
 * Synchronize all tasks for an user.
 * @param  {String}  token security token
 * @param  {Number}  now   reference date
 * @param  {Boolean} force force the sync of all day
 * @return {Promise}       the promise is resolved if the tasks are synchronized otherwise
 *                         the promise is rejected if there is an error
 */
module.exports.sync = ({token, now, force = false}) => {
    return module.exports.getSyncTasks({token, now: +now})
            .then(({rows}) => {
                return module.exports.syncTasks({token, now, rows, force: parseInt(force)});
            })
            .then(() => {
                return Promise.resolve(true);
            });
};

/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

let userApi = require("./UserApi");
let timeApi = require("./TimeApi");
let uuid = require("uuid");

/**
 * Add an event for an user.
 * @param  {String} token security token
 * @param  {object} event event to add, the event must contains type and source
 * @param  {String} key   api key where the event comes from
 * @return {Promise}      the promise is resolved with created event otherwise the promise is rejected
 *                        if there is an error
 */
module.exports.add = ({token, event, key}) => {
    return module.exports.addAll({token, events: [event], key})
    .then(({events}) => {
        return events[0];
    });
};

/**
 * Add all events for an user.
 * @param  {String} token security token
 * @param  {array} events array of event to add, the event must contains type and source
 * @param  {String} key   api key where the event comes from
 * @return {Promise}      the promise is resolved with all created events otherwise the promise is rejected
 *                        if there is an error
 */
module.exports.addAll = ({token, events, key}) => {
    let database;
    let result;

    return userApi.isAuth({token})
    .then((db) => {
        database = db;
        return db.beginTransaction();
    })
    .then(() => {
        let insert = database.prepare("INSERT INTO event (id, beginDateTime, endDateTime, value, key) VALUES (?, ?, ?, ?, ?)");

        let promises = events.map((event) => {

            let id = uuid.v1();
            let {beginDateTime, endDateTime} = event;
            let value = JSON.stringify(event);

            // Test type and source is mandatory in event
            if (!event.type || !event.source) {
                return Promise.reject("Type and source is required");
            }

            // Need to create an activity
            let promiseMarkEvent;
            if (event.type === "mark"
                    && beginDateTime !== endDateTime
                    && event.data
                    && event.data.tags
                    && event.data.tags.length) {

                promiseMarkEvent = timeApi.getAll({
                    token,
                    beginDateTime,
                    endDateTime
                })
                .then(({rows}) => {
                    // No activity already exist
                    if (!rows.length) {
                        return timeApi.add({
                            token: database,
                            beginDateTime,
                            endDateTime,
                            tags: event.data.tags
                        });
                    }

                    return null;
                });

            } else {
                promiseMarkEvent = Promise.resolve();
            }

            return promiseMarkEvent
            .then(() => {
                return database.runPrepare(insert, id, beginDateTime, endDateTime, value, key);
            })
            .then(() => {
                return {id, beginDateTime, endDateTime, value};
            });
        });

        return Promise.all(promises);
    })
    .catch((err) => {
        return database.rollbackTransaction(err);
    })
    .then((r) => {
        result = r;
        return database.commitTransaction();
    })
    .then(() => {
        return {events: result};
    });
};

/**
 * Remove an event from id for an user.
 * @param  {String} token security token
 * @param  {String} id    event id to remove
 * @return {Promise}      the promise is resolved if the event is removed otherwise the promise is rejected
 *                        if there is an error
 */
module.exports.remove = ({token, id}) => {
    return userApi.isAuth({token})
    .then((db) => {
        return db.run("DELETE FROM event WHERE id = ?", id);
    });
};

/**
 * Get all events between dates from an user.
 * @param  {String} token                           security token
 * @param  {Number} [beginDateTime=0]               min date
 * @param  {Number} [endDateTime=Number.MAX_VALUE}] max date
 * @return {Promise}                                the promise is resolved with events otherwise the promise is rejected
 *                                                  if there is an error
 */
module.exports.getAll = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE}) => {
    return userApi.isAuth({token})
    .then((db) => {
        return db.all(
            `SELECT id,
            json_remove(value, '$.data.userAgent', '$.data.entity',
            '$.data.url', '$.data.modified', '$.data.removed',
            '$.data.type', '$.data.lines', '$.data.plugin',
            '$.data.branch', '$.data.language', '$.data.host',
            '$.data.path', '$.data.shell', '$.data.duration',
            '$.data.status', '$.data.program', '$.data.user',
            '$.data.hostname', '$.data.ip', '$.data.city',
            '$.data.country_name', '$.data.country_code', '$.data.postal_code',
            '$.source', '$.beginDateTime', '$.endDateTime'
            ) value,
            beginDateTime, endDateTime
            FROM event
            WHERE beginDateTime < ? AND endDateTime > ? OR beginDateTime = ?
            ORDER BY beginDateTime DESC, endDateTime DESC`,
            endDateTime, beginDateTime, beginDateTime
        );
    });
};

/**
 * Get all events between dates and for type from an user.
 * @param  {String} token                           security token
 * @param  {Number} [beginDateTime=0]               min date
 * @param  {Number} [endDateTime=Number.MAX_VALUE}] max date
 * @param  {String} type                            event type
 * @return {Promise}                                the promise is resolved with events otherwise the promise is rejected
 *                                                  if there is an error
 */
module.exports.getByType = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, type}) => {
    return userApi.isAuth({token})
    .then((db) => {
        return db.all(
            `WITH agg as (SELECT beginDateTime, endDateTime, group_concat(DISTINCT tag) as tags FROM time GROUP BY beginDateTime, endDateTime)

             SELECT e.id, e.value, e.beginDateTime, e.endDateTime, agg.tags
             FROM event e, agg
             WHERE e.beginDateTime < agg.endDateTime AND e.endDateTime >= agg.beginDateTime
             AND e.beginDateTime >= ? AND e.beginDateTime < ?
             AND json_extract(e.value, '$.type') = ?
             ORDER BY e.beginDateTime DESC, e.endDateTime DESC`,
            beginDateTime, endDateTime, type
        );
    });
};

/**
 * Add a row for the interval into activity table
 * @param {[type]} token         security token
 * @param {[type]} beginDateTime begin date of the interval
 * @param {[type]} endDateTime   end date of the interval
 */
module.exports.addActivityInterval = ({token, beginDateTime, endDateTime}) => {
    return userApi.isAuth({token})
    .then((db) => {
        return db.run(`INSERT OR REPLACE INTO activity
                        SELECT
                        ? as beginDateTime,
                        ? as endDateTime,
                        projectName,
                        count(*) as count

                        FROM (
                            SELECT replace(
                                replace(
                                    lower(
                                        substr(
                                            json_extract(value, '$.data.project'),
                                            0,
                                            instr(
                                                json_extract(value, '$.data.project') || ' ',
                                                ' '
                                            )
                                        )
                                    ), '-', '_'
                                ),
                            '.', '_') as projectName FROM event e
                            WHERE e.beginDateTime <= ? AND e.endDateTime >= ?
                            AND projectName IS NOT NULL
                        )
                        GROUP BY projectName
                        UNION
                        SELECT ? as beginDateTime, ? as endDateTime, "idle" as projectName, sum(min(e.endDateTime, ?) - max(e.beginDateTime, ?)) / 1000 / 60 as count
                        FROM event e
                        WHERE json_extract(value, "$.type") = "idle"
                        AND beginDateTime <= ? AND endDateTime >= ?
                        GROUP BY projectName
                        HAVING count IS NOT NULL
                        `,
            +beginDateTime, +endDateTime, +endDateTime, +beginDateTime, +beginDateTime, +endDateTime, +endDateTime, +beginDateTime, +endDateTime, +beginDateTime);
    });
};

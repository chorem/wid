/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

let uuid = require("uuid");
let userApi = require("./UserApi");
let eventApi = require("./EventApi");
let EventEmitter = require("events");

/**
 * Event use to update last synch in Timebundle
 */
module.exports.timeChange = new EventEmitter();

/**
 * Add an activity for an user.
 * @param  {String} token         security token
 * @param  {Number} beginDateTime begin date time for activity
 * @param  {Number} endDateTime   end date time for activity
 * @param  {array} tags           array of tags
 * @param  {String} note          associate note event
 * @return {Promise}              the promise is resolved if the activity is added otherwise
 *                                the promise is rejected if there is an error
 */
module.exports.add = ({token, beginDateTime, endDateTime, tags = "", note}) => {
    let values = Array.isArray(tags) ? tags : [tags];
    values = Array.from(new Set(values)); // remove doublon

    let notes = Array.isArray(note) ? note : [note];

    let database;
    let beginDateValue;

    return userApi.isAuth({token})
    .then((db) => {
        database = db;

        let beginDatePromise;
        if (beginDateTime) {
            beginDatePromise = Promise.resolve({beginDateTime});
        } else {
            beginDatePromise = module.exports.getBeginDateTime({token});
        }

        return beginDatePromise;
    })
    .then(({beginDateTime: beginDate}) => {
        beginDateValue = beginDate;

        let promiseForNote = Promise.resolve();
        if (note) {

            let events = notes.map((content) => {
                return {
                    beginDateTime: beginDateValue,
                    endDateTime: beginDateValue,
                    type: "note",
                    source: "webui",
                    data: {content}
                };
            });
            promiseForNote = eventApi.addAll({
                token,
                events
            });
        }

        return promiseForNote;
    })
    .then(() => {
        return module.exports.mergeTags({
            token,
            beginDateTime: beginDateValue,
            endDateTime: endDateTime,
            tags: values.sort().join(",")
        });
    })
    .then((row) => {
        let inserts = () => {
            // Test if the sames tags between previous tags
            let deletes = [];
            let {newBeginDateTime, newEndDateTime} = row || {};

            if (newBeginDateTime && newBeginDateTime < beginDateValue) {
                deletes.push(database.run("DELETE FROM time WHERE beginDateTime = ? AND endDateTime = ?", newBeginDateTime, beginDateTime));
                beginDateValue = newBeginDateTime;
            }

            if (newEndDateTime && newEndDateTime > endDateTime) {
                deletes.push(database.run("DELETE FROM time WHERE beginDateTime = ? AND endDateTime = ?", endDateTime, newEndDateTime));
                endDateTime = newEndDateTime;
            }

            let insert = database.prepare("INSERT INTO time (id, beginDateTime, endDateTime, tag) VALUES (?, ?, ?, ?)");

            let promises = values.map((tag) => {
                let timeId = uuid.v1();
                return database.runPrepare(insert, timeId, beginDateValue, endDateTime, tag);
            });

            return Promise.all(deletes.concat(promises));
        };

        if (database.inTransaction) {
            return inserts();
        }

        return database.beginTransaction()
        .then(inserts)
        .catch((err) => {
            return database.rollbackTransaction(err);
        })
        .then(() => {
            return database.commitTransaction();
        })
        .then(() => {
            return eventApi.addActivityInterval({
                token,
                beginDateTime: beginDateValue,
                endDateTime: endDateTime
            });
        })
        .then(() => {
            module.exports.timeChange.emit("change", token, [beginDateValue]);
            return true;
        });
    });
};

/**
 * Test if the sames tags between previous tags, and merge
 * @param  {String} token         security token
 * @return {Promise}              the promise is resolved if the tags is merged otherwise
 *                                the promise is rejected if there is an error
 */
module.exports.mergeTags = ({token, beginDateTime, endDateTime, tags}) => {
    return userApi.isAuth({token})
    .then((db) => {
        return db.get(
            `SELECT min(beginDateTime) newBeginDateTime, max(enddatetime) newEndDateTime
                FROM
                (
                	SELECT group_concat(tag) AS tags, beginDateTime, enddatetime
                	FROM (SELECT * FROM time t WHERE t.tag IS NOT NULL AND (beginDateTime = ? OR enddatetime = ?) ORDER BY tag) v
                	GROUP BY beginDateTime, enddatetime
                ) o
                WHERE o.tags = ?`,
            endDateTime, beginDateTime, tags
        );
    });
};

/**
 * Remove tag from between dates for an user.
 * @param  {String} token         security token
 * @param  {Number} beginDateTime date boundary min
 * @param  {Number} endDateTime   date boundary max
 * @return {Promise}              the promise is resolved if the tags is removed otherwise
 *                                the promise is rejected if there is an error
 */
module.exports.removeOnDate = ({token, beginDateTime, endDateTime}) => {
    return userApi.isAuth({token})
    .then((db) => {
        return db.run(
            "DELETE FROM time WHERE beginDateTime = ? AND endDateTime = ?",
            beginDateTime, endDateTime
        );
    })
    .then(() => {
        module.exports.timeChange.emit("change", token, [beginDateTime]);
        return true;
    });
};

/**
 * Remove tag from between dates and contains tags for an user.
 * @param  {String} token         security token
 * @param  {Number} beginDateTime date boundary min
 * @param  {Number} endDateTime   date boundary max
 * @param  {array} tags           tags to find
 * @return {Promise}              the promise is resolved if the tags is removed otherwise
 *                                the promise is rejected if there is an error
 */
module.exports.removeOnDateAndTag = ({token, beginDateTime, endDateTime, tags}) => {
    return userApi.isAuth({token})
    .then((db) => {
        return db.run(
            "DELETE FROM time WHERE beginDateTime = ? AND endDateTime = ? AND tag IN (?" + ", ?".repeat(tags.length - 1) + ")",
            beginDateTime, endDateTime, ...tags
        );
    })
    .then(() => {
        module.exports.timeChange.emit("change", token, [beginDateTime]);
        return true;
    });
};

/**
 * Add or update an activity for an user.
 * @param  {String} token         security token
 * @param  {Number} beginDateTime begin date time for activity
 * @param  {Number} endDateTime   end date time for activity
 * @param  {array} tags           array of tags
 * @param  {String} note          associate note event
 * @return {Promise}              the promise is resolved if the activity is added or updated otherwise
 *                                the promise is rejected if there is an error
 */
module.exports.createOrUpdate = ({token, beginDateTime, endDateTime, tags, note}) => {
    return module.exports.removeOnDate({token, beginDateTime, endDateTime})
    .then(() => {
        return module.exports.add({token, beginDateTime, endDateTime, tags, note});
    });
};

/**
 * Modify date for an activity on specify dates.
 * @param  {String} token            security token
 * @param  {Number} oldBeginDateTime old begin date time for the activity
 * @param  {Number} oldEndDateTime   old end date time for the activity
 * @param  {Number} newBeginDateTime new begin date time for the activity
 * @param  {Number} newEndDateTime   new end date time for the activity
 * @return {Promise}                 the promise is resolved if the activity is modified otherwise
 *                                   the promise is rejected if there is an error
 */
module.exports.modifyDate = ({token, oldBeginDateTime, oldEndDateTime, newBeginDateTime, newEndDateTime}) => {
    let db;
    return userApi.isAuth({token})
    .then((database) => {
        db = database;
        if (newBeginDateTime === newEndDateTime) {
            return module.exports.removeOnDate({token, beginDateTime: oldBeginDateTime, endDateTime: oldEndDateTime});
        }

        return db.run(
            "UPDATE time SET beginDateTime = ?, endDateTime = ? WHERE beginDateTime = ? AND endDateTime = ?",
            newBeginDateTime, newEndDateTime, oldBeginDateTime, oldEndDateTime
        )
        .then((updated) => {
            if (!updated) {
                return null;
            }
            return eventApi.addActivityInterval({
                token,
                beginDateTime: newBeginDateTime,
                endDateTime: newEndDateTime
            });
        });
    })

    // Search if need to merge tags
    .then(() => {
        if (+oldBeginDateTime !== +newBeginDateTime) {
            return module.exports.getContainerTags({token, beginDateTime: newBeginDateTime, endDateTime: newBeginDateTime});
        }
        return module.exports.getContainerTags({token, beginDateTime: newEndDateTime, endDateTime: newEndDateTime});
    })
    .then(({rows}) => {
        if (!rows) {
            return true;
        }

        let [previous, next] = rows;
        if (previous && next) {
            let previousTags = previous.tags && previous.tags.split(" ") || [];
            let nextTags = next.tags && next.tags.split(" ") || [];

            if (previousTags.length === nextTags.length) {

                let outerIntersection = previousTags.filter((item) => {
                    return nextTags.indexOf(item) === -1;
                });

                // Same tags ?
                if (outerIntersection.length === 0) {
                    return module.exports.removeOnDate({
                        token, beginDateTime: previous.beginDateTime, endDateTime: previous.endDateTime
                    })
                    .then(() => {
                        return db.run(
                            "UPDATE time SET beginDateTime = ?, endDateTime = ? WHERE beginDateTime = ? AND endDateTime = ?",
                            previous.beginDateTime, next.endDateTime, next.beginDateTime, next.endDateTime
                        );
                    })
                    .then((updated) => {
                        if (!updated) {
                            return null;
                        }
                        return eventApi.addActivityInterval({
                            token,
                            beginDateTime: previous.beginDateTime,
                            endDateTime: next.endDateTime
                        });
                    });
                }
            }
        }

        return true;
    })
    .then(() => {
        module.exports.timeChange.emit("change", token, [oldBeginDateTime, newBeginDateTime]);
        return true;
    });
};

/**
 * Get all tags between dates for an user.
 * @param  {String} token         security token
 * @param  {Number} beginDateTime date boundary min
 * @param  {Number} endDateTime   date boundary max
 * @return {Promise}                 the promise is resolved with contactenate tags otherwise
 *                                   the promise is rejected if there is an error
 */
module.exports.getContainerTags = ({token, beginDateTime, endDateTime}) => {
    return userApi.isAuth({token})
    .then((db) => {
        return db.all(
            `SELECT group_concat(tag, ' ') as tags, beginDateTime, endDateTime FROM time
            WHERE beginDateTime <= ? AND endDateTime >= ?
            GROUP BY beginDateTime, endDateTime
            ORDER BY beginDateTime`,
            +beginDateTime, +endDateTime
        );
    });
};

/**
 * Insert activity between other activities for an user.
 * @param  {String} token         security token
 * @param  {Number} beginDateTime begin date time to insert
 * @param  {Number} endDateTime   end date time to insert
 * @param  {array} tags           array of tags
 * @param  {String} note          associate note event
 * @return {Promise}              the promise is resolved if activity is inserted tags otherwise
 *                                the promise is rejected if there is an error
 */
module.exports.insert = ({token, beginDateTime, endDateTime, tags, note}) => {
    let containerBeginDate;
    let containerEndDate;
    let containerTags;

    return module.exports.getContainerTags({token, beginDateTime, endDateTime})
    .then(({rows}) => {
        if (!rows[0]) {
            return true;
        }

        containerBeginDate = rows[0].beginDateTime;
        containerEndDate = rows[0].endDateTime;
        containerTags = rows[0].tags && rows[0].tags.split(" ") || [];

        return module.exports.removeOnDate({token, beginDateTime: containerBeginDate, endDateTime: containerEndDate});
    })
    .then(() => {
        return module.exports.add({token, beginDateTime, endDateTime, tags, note});
    })
    .then(() => {
        if (containerBeginDate !== +beginDateTime) {
            return module.exports.add({token, beginDateTime: containerBeginDate, endDateTime: beginDateTime, tags: containerTags});
        }

        return true;
    })
    .then(() => {
        if (containerEndDate !== +endDateTime) {
            return module.exports.add({token, beginDateTime: endDateTime, endDateTime: containerEndDate, tags: containerTags});
        }

        return true;
    });
};

/**
 * Get date to excute merge up.
 * @param  {String} token          security token
 * @param  {Number} oldEndDateTime reference date
 * @return {Promise}               the promise is resolved with date otherwise
 *                                 the promise is rejected if there is an error
 */
module.exports.getDateMergeUp = ({token, oldEndDateTime}) => {
    return userApi.isAuth({token})
    .then((db) => {
        return db.get(
            "SELECT min(beginDateTime) as beginDateTime, endDateTime FROM time WHERE beginDateTime >= ?",
            +oldEndDateTime
        );
    });
};

/**
 * Get date to excute merge down.
 * @param  {String} token            security token
 * @param  {Number} oldBeginDateTime reference date
 * @return {Promise}                 the promise is resolved with date otherwise
 *                                   the promise is rejected if there is an error
 */
module.exports.getDateMergeDown = ({token, oldBeginDateTime}) => {
    return userApi.isAuth({token})
    .then((db) => {
        return db.get(
            "SELECT beginDateTime, max(endDateTime) as endDateTime FROM time WHERE endDateTime <= ?",
            +oldBeginDateTime
        );
    });
};

/**
 * Merge up the activity for an user.
 * @param  {String} token             security token
 * @param  {Number} oldBeginDateTime  old begin date time of activity to merge
 * @param  {Number} oldEndDateTime    old and date time of activity to merge
 * @param  {boolean} overwrite        keep or not the activity tag
 * @return {Promise}                  the promise is resolved if the activity is merged otherwise
 *                                    the promise is rejected if there is an error
 */
module.exports.mergeUp = ({token, oldBeginDateTime, oldEndDateTime, overwrite}) => {
    return module.exports.getDateMergeUp({token, oldEndDateTime})
    .then(({beginDateTime, endDateTime}) => {
        return module.exports.mergeDate({
            token,
            oldBeginDateTime,
            oldEndDateTime,
            refBeginDateTime: beginDateTime,
            refEndDateTime: endDateTime,
            overwrite
        });
    });
};

/**
 * Merge down the activity for an user.
 * @param  {String} token             security token
 * @param  {Number} oldBeginDateTime  old begin date time of activity to merge
 * @param  {Number} oldEndDateTime    old and date time of activity to merge
 * @param  {boolean} overwrite        keep or not the activity tag
 * @return {Promise}                  the promise is resolved if the activity is merged otherwise
 *                                    the promise is rejected if there is an error
 */
module.exports.mergeDown = ({token, oldBeginDateTime, oldEndDateTime, overwrite}) => {
    return module.exports.getDateMergeDown({token, oldBeginDateTime})
    .then(({beginDateTime, endDateTime}) => {
        return module.exports.mergeDate({
            token,
            oldBeginDateTime,
            oldEndDateTime,
            refBeginDateTime: beginDateTime,
            refEndDateTime: endDateTime,
            overwrite
        });
    });
};

/**
 * Remove duplicate tags on the same activity.
 * @param  {String} token         security token
 * @param  {Number} beginDateTime date boundary min
 * @param  {Number} endDateTime   date boundary max
 * @return {Promise}              the promise is resolved if the duplicate tags are removed is merged otherwise
 *                                the promise is rejected if there is an error
 */
module.exports.mergeDuplicateTags = ({token, beginDateTime, endDateTime}) => {
    return userApi.isAuth({token})
    .then((db) => {
        return db.run(
            `DELETE FROM time WHERE beginDateTime >= ? AND endDateTime <= ? AND id NOT IN (SELECT min(id) FROM time
            WHERE beginDateTime >= ? AND endDateTime <= ? GROUP BY beginDateTime, endDateTime, tag)`,
            +beginDateTime, +endDateTime, +beginDateTime, +endDateTime
        );
    });
};

/**
 * Utility function to merge activity.
 * @param  {String} token             security token
 * @param  {Number} oldBeginDateTime  old begin date time of activity to merge
 * @param  {Number} oldEndDateTime    old and date time of activity to merge
 * @param  {Number} refBeginDateTime  new begin date time
 * @param  {Number} refEndDateTime    new end date time
 * @param  {boolean} overwrite        keep or not the activity tag
 * @return {Promise}                  the promise is resolved if the activity is merged otherwise
 *                                    the promise is rejected if there is an error
 */
module.exports.mergeDate = ({token, oldBeginDateTime, oldEndDateTime, refBeginDateTime, refEndDateTime, overwrite}) => {
    if (!refBeginDateTime || !refEndDateTime) {
        return Promise.resolve(false);
    }

    let database;
    let newBeginDateTime = oldBeginDateTime;
    let newEndDateTime = oldEndDateTime;

    if (+oldEndDateTime === +refBeginDateTime) {
        newEndDateTime = refEndDateTime;

    } else if (+oldBeginDateTime === +refEndDateTime) {
        newBeginDateTime = refBeginDateTime;

    } else {
        // Merge hole
        overwrite = false;
        if (+oldEndDateTime < +refBeginDateTime) {
            newEndDateTime = refBeginDateTime;
        } else if (+oldBeginDateTime > +refEndDateTime) {
            newBeginDateTime = refEndDateTime;
        }

        refBeginDateTime = -1;
        refEndDateTime = -1;
    }

    return userApi.isAuth({token})
    .then((db) => {
        database = db;

        if (overwrite) {
            return module.exports.removeOnDate({token, beginDateTime: refBeginDateTime, endDateTime: refEndDateTime});
        }

        return true;
    })
    .then(() => {
        return database.run(
            "UPDATE time SET beginDateTime = ?, endDateTime = ? WHERE (beginDateTime = ? AND endDateTime = ? OR beginDateTime = ? AND endDateTime = ?)",
            newBeginDateTime, newEndDateTime, oldBeginDateTime, oldEndDateTime, refBeginDateTime, refEndDateTime
        );
    })
    .then(() => {
        module.exports.timeChange.emit("change", token, [newBeginDateTime, oldBeginDateTime, refBeginDateTime]);
        return true;
    })
    .then(() => {
        return module.exports.mergeDuplicateTags({token, beginDateTime: newBeginDateTime, endDateTime: newEndDateTime});
    });
};

/**
 * Get all activity between dates for an user.
 * @param  {String} token                           security token
 * @param  {Number} [beginDateTime=0]               date boundary min
 * @param  {Number} [endDateTime=Number.MAX_VALUE}] date boundary max
 * @return {Promise}                                the promise is resolved with the activities otherwise
 *                                                  the promise is rejected if there is an error
 */
module.exports.getAll = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE}) => {
    return userApi.isAuth({token})
    .then((db) => {
        return db.all(
            "SELECT * FROM time " +
            "WHERE beginDateTime <= ? AND endDateTime >= ? " +
            "ORDER BY beginDateTime DESC, endDateTime DESC",
            endDateTime, beginDateTime
        );
    });
};

/**
 * Get all tags from date for an user.
 * @param  {String} token       security token
 * @param  {Number} endDateTime reference date to search
 * @return {Promise}            the promise is resolved with the activities otherwise
 *                              the promise is rejected if there is an error
 */
module.exports.getTagsByEndDateTime = ({token, endDateTime}) => {
    return userApi.isAuth({token})
    .then((db) => {
        return db.all("SELECT * FROM time WHERE endDateTime = ?", endDateTime);
    });
};

/**
 * Get clauses to add in sql query from a query.
 * example of query: +wid+dev-pause
 * -pause
 * +dev-wid
 * @param  {String} query report query
 * @return {object}       contains the query as string and parameters
 */
module.exports.getClauses = (query) => {
    if (!query.trim()) {
        return {request: " AND t.tag IS NOT NULL AND t.tag != '' ", parameters: []};
    }

    let request = " AND t.tag IS NOT NULL AND t.tag != '' AND ( ";
    let separatorClause = "";
    let clauses = query.trim().split(" ");
    let parameters = [];

    clauses.forEach((clause) => {
        let expressions = clause.match(/(\+|-)?[^+-]+/g);

        let separatorExp = "";
        request += separatorClause + " (";

        expressions.forEach((exp) => {
            if (exp[0] === "-") {
                request += separatorExp + `NOT EXISTS (
                        SELECT 1 FROM time a
                        WHERE a.beginDateTime == t.beginDateTime
                        AND a.endDateTime == t.endDateTime
                        AND a.tag = ?) `;
                parameters.push(exp.substring(1));

            } else {
                request += separatorExp + `EXISTS (
                        SELECT 1 FROM time a
                        WHERE a.beginDateTime == t.beginDateTime
                        AND a.endDateTime == t.endDateTime
                        AND a.tag = ?) `;

                if (exp[0] === "+") {
                    parameters.push(exp.substring(1));
                } else {
                    parameters.push(exp);
                }
            }

            separatorExp = " AND ";
        });

        request += ") ";
        separatorClause = " OR ";
    });

    request += " ) ";

    return {request, parameters};
};

/**
 * Get the duration between dates and a query for an user. (Use in report API)
 * @param  {String} token                          security token
 * @param  {Number} [beginDateTime=0]              boundary min date
 * @param  {Number} [endDateTime=Number.MAX_VALUE] boundary max date
 * @param  {String} [query=""}]                    query research
 * @return {Promise}                               the promise is resolved with duration otherwise
 *                                                 the promise is rejected if there is an error
 */
module.exports.getTotalTimeForReport = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, query = ""}) => {
    return userApi.isAuth({token})
    .then((db) => {
        let {request, parameters} = module.exports.getClauses(query);

        return db.get(
            `SELECT sum(duration) as time FROM (
                SELECT (min(t.endDateTime, ?) - max(t.beginDateTime, ?)) as duration FROM time t
                WHERE t.beginDateTime <= ? AND t.endDateTime >= ? ${request}
                GROUP BY t.beginDateTime, t.endDateTime)`,
            +endDateTime, +beginDateTime, +endDateTime, +beginDateTime, ...parameters
        );
    });
};

/**
 * Get activities between dates and a query for an user. (Use in report API)
 * @param  {String} token                          security token
 * @param  {Number} [beginDateTime=0]              boundary min date
 * @param  {Number} [endDateTime=Number.MAX_VALUE] boundary max date
 * @param  {String} [query=""}]                    query research
 * @return {Promise}                               the promise is resolved with activities otherwise
 *                                                 the promise is rejected if there is an error
 */
module.exports.getTimeForReport = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, query = ""}) => {
    return userApi.isAuth({token})
    .then((db) => {
        let {request, parameters} = module.exports.getClauses(query);

        return db.all(
            `SELECT group_concat(t.tag, ' ') as tags, t.beginDateTime, t.endDateTime,
                    (min(t.endDateTime, ?) - max(t.beginDateTime, ?)) as duration
                FROM time t
                WHERE t.beginDateTime <= ? AND t.endDateTime >= ? ${request}
                GROUP BY t.beginDateTime, t.endDateTime`,
            +endDateTime, +beginDateTime, +endDateTime, +beginDateTime, ...parameters
        );
    });
};

/**
 * Get last date of entering activity for an user.
 * @param  {String} token security token
 * @return {Promise}      the promise is resolved with date otherwise
 *                        the promise is rejected if there is an error
 */
module.exports.getBeginDateTime = ({token}) => {
    return userApi.isAuth({token})
    .then((db) => {
        return db.get("SELECT max(endDateTime) as beginDateTime FROM time");
    });
};

/**
 * Get all tags start by tag.
 * @param  {String} token security token
 * @param  {String} tag   tag to compare
 * @return {Promise}      the promise is resolved with tags otherwise
 *                        the promise is rejected if there is an error
 */
module.exports.getStartWith = ({token, tag}) => {
    return userApi.isAuth({token})
    .then((db) => {
        return db.all("SELECT DISTINCT tag FROM time WHERE tag LIKE ?", tag + "%");
    });
};

/**
 * Get weight for each tags, the weight for a tag is the total of duration.
 * @param  {String} token  security token
 * @return {Promise}       the promise is resolved with tags weights
 *                         the promise is rejected if there is an error
 */
module.exports.getTagsWeight = ({token}) => {
    return userApi.isAuth({token})
    .then((db) => {
        return db.all("SELECT tag, SUM(enddatetime - begindatetime) AS total FROM time GROUP BY tag ORDER BY total DESC");
    });
};

/**
 * Get priorities for each tags.
 * @param  {String} token  security token
 * @return {Promise}       the promise is resolved with tags priorities
 *                         the promise is rejected if there is an error
 */
module.exports.getTagsPriorities = ({token}) => {
    return userApi.isAuth({token})
    .then((db) => {
        return db.all("SELECT tags FROM priorities LIMIT 1");
    })
    .then(({rows}) => {
        if (rows.length) {
            return rows[0].tags.split(",");
        }

        return [];
    });
};

/**
 * Get all paths formed by the tags, you can filter with query or tags facet.
 * @param  {String} token                          security token
 * @param  {Number} [beginDateTime=0]              boundary min date
 * @param  {Number} [endDateTime=Number.MAX_VALUE] boundary max date
 * @param  {String} [query=""]                     query research
 * @param  {Array}  [includes=[]}]                 includes use to filter the result
 * @return {Promise}                               the promise is resolved with the paths formed by the tags
 *                                                 the promise is rejected if there is an error
 */
module.exports.getTreePaths = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, query = "", includes = []}) => {
    let values = Array.isArray(includes) ? includes : [includes];
    let {request, parameters} = module.exports.getClauses(query);

    return userApi.isAuth({token})
    .then((db) => {

        let requestFacet = "";
        let separator = "";
        if (values.length) {
            requestFacet += " AND ( ";

            values.forEach(() => {
                requestFacet += separator + "EXISTS (SELECT 1 FROM time b WHERE b.beginDateTime == t.beginDateTime AND b.endDateTime == t.endDateTime AND b.tag = ?) ";
                separator = "AND ";
            });

            values.forEach(() => {
                requestFacet += "AND tag != ? ";
            });
            requestFacet += " ) ";
        }

        return db.all(`SELECT tags, count(tags) as count, sum(enddatetime - begindatetime) as duration
                        FROM (
                            SELECT DISTINCT group_concat(tag) AS tags,
                            max(beginDateTime, ?) AS beginDateTime,
                            min(endDatetime, ?) AS endDatetime
                            FROM (
                                SELECT *
                                FROM time t
                                WHERE t.tag IS NOT NULL AND t.beginDateTime <= ? AND t.endDateTime >= ? ${request} ${requestFacet}
                                ORDER BY tag
                            ) v
                            GROUP BY begindatetime, enddatetime
                        )
                        GROUP BY tags;
                        `, +beginDateTime, +endDateTime, +endDateTime, +beginDateTime, ...parameters, ...values, ...values);
    });
};

/**
 * Get tree as object formed by the tags, you can filter with query or tags facet.
 * @param  {String} token                          security token
 * @param  {Number} [beginDateTime=0]              boundary min date
 * @param  {Number} [endDateTime=Number.MAX_VALUE] boundary max date
 * @param  {String} [query=""]                     query research
 * @param  {Array}  [includes=[]}]                 includes use to filter the result
 * @return {Promise}                               the promise is resolved with the tree formed by the tags
 *                                                 the promise is rejected if there is an error
 */
module.exports.getTree = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, query = "", tags = []}) => {
    let weights = new Map();
    let priorities = new Map();

    let compareWeights = function(tag, other) {
        let result = weights.get(other) - weights.get(tag);
        if (result === 0) {
            return tag.localeCompare(other);
        }
        return result;
    };

    let comparePriorities = function(tag, other) {
        let size = priorities.size + 1;
        let tagPriority = priorities.get(tag) || size;
        let otherPriority = priorities.get(other) || size;

        return tagPriority - otherPriority;
    };

    return module.exports.getTagsPriorities({token})
    .then((result) => {
        result.forEach((tag, index) => {
            priorities.set(tag, index + 1);
        });

        return module.exports.getTagsWeight({token});
    })
    .then((result) => {
        result.rows.forEach((item) => {
            weights.set(item.tag, item.total);
        });

        return module.exports.getTreePaths({token, beginDateTime, endDateTime, query, includes: tags});
    })
    .then((result) => {
        let paths = result.rows.map((item) => {
            let array = item.tags.split(",");
            array.sort(compareWeights);
            array.sort(comparePriorities);
            array.count = item.count;
            array.duration = item.duration;
            return array;
        });

        return paths;
    })
    .then((paths) => {
        let tree = {
            __meta__: {
                count: 0,
                duration: 0
            }
        };
        paths.forEach((values) => {
            let current = tree;
            tree.__meta__.count += values.count;
            tree.__meta__.duration += values.duration;

            values.forEach((tag) => {
                let nextLevel = current[tag];
                if (!nextLevel) {
                    nextLevel = current[tag] = {
                        __meta__: {
                            tag,
                            count: values.count,
                            duration: values.duration
                        }
                    };
                } else {
                    nextLevel.__meta__.count += values.count;
                    nextLevel.__meta__.duration += values.duration;
                }
                current = nextLevel;
            });
        });

        return tree;
    });
};

/**
 * Get all most tags used filter by tags.
 * @param  {String} token                          security token
 * @param  {Number} [beginDateTime=0]              boundary min date
 * @param  {Number} [endDateTime=Number.MAX_VALUE] boundary max date
 * @param  {Array}  [tags=[]}]                     tags use to filter the result
 * @return {Promise}                               the promise is resolved with all most tags used otherwise
 *                                                 the promise is rejected if there is an error
 */
module.exports.getTreeFacet = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, tags = []}) => {
    return module.exports.getTree({token, beginDateTime, endDateTime, tags});
};

/**
 * Get all most tags used filter by query from a report.
 * @param  {String} token                          security token
 * @param  {Number} [beginDateTime=0]              boundary min date
 * @param  {Number} [endDateTime=Number.MAX_VALUE] boundary max date
 * @param  {String} [query=""]                     query research
 * @return {Promise}                               the promise is resolved with all most tags used otherwise
 *                                                 the promise is rejected if there is an error
 */
module.exports.getTreeReport = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, query = ""}) => {
    return module.exports.getTree({token, beginDateTime, endDateTime, query});
};

/**
 * Propose tags to the user. Use getTreeFacet and the getStartWith function to find tags.
 * @param  {String} token                          security token
 * @param  {Number} [beginDateTime=0]              boundary min date
 * @param  {Number} [endDateTime=Number.MAX_VALUE] boundary max date
 * @param  {Array}  [tags=[]]                      tag already used
 * @return {Promise}                               the promise is resolved with tags otherwise
 *                                                 the promise is rejected if there is an error
 */
module.exports.getTreeCompletion = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, tags = []}) => {
    return module.exports.getTreeFacet({token, beginDateTime, endDateTime, tags})
    .then((result) => {
        if (Object.keys(result).length !== 1) {
            return result;
        }

        let values = Array.isArray(tags) ? tags : [tags];
        let startTag = values.pop();

        return module.exports.getTreeFacet({token, beginDateTime, endDateTime, tags: values})
        .then((previousResult) => {
            Object.keys(previousResult).forEach((key) => {
                if (key !== "__meta__" && !key.startsWith(startTag) || key === startTag) {
                    delete previousResult[key];
                }
            });

            if (Object.keys(previousResult).length !== 1) {
                previousResult.__meta__.replace = true;
                return previousResult;
            }

            return module.exports.getStartWith({token, tag: startTag})
            .then((all) => {
                let startWithResult = {__meta__: {}};

                all.rows.forEach(({tag}) => {
                    if (tag !== startTag) {
                        startWithResult[tag] = {
                            __meta__: {
                                count: 0,
                                duration: 0
                            }
                        };
                    }
                });

                startWithResult.__meta__.replace = Object.keys(startWithResult).length !== 1;
                return startWithResult;
            });
        });
    });
};

/**
 * Global tags modification. The user can add and remove tags on a query.
 * @param  {String} token                          security token
 * @param  {Number} [beginDateTime=0]              boundary min date
 * @param  {Number} [endDateTime=Number.MAX_VALUE] boundary max date
 * @param  {String} [query=""]                     query research
 * @param  {Array}  [removed=[]]                   tags to removed
 * @param  {Array}  [added=[]]                     tags to added
 * @return {Promise}                               the promise is resolved if the tags is updated otherwise
 *                                                 the promise is rejected if there is an error
 */
module.exports.updateTags = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, query = "", removed = [], added = []}) => {
    let database;
    let rows;
    let beginDateTimes = [];

    return module.exports.getTimeForReport({token, beginDateTime, endDateTime, query})
    .then((result) => {
        rows = result.rows;
        return userApi.isAuth({token});
    })
    .then((db) => {
        database = db;
        return database.beginTransaction();
    })
    .then(() => {
        let promises = [];
        rows.forEach((row) => {
            let remove = Array.isArray(removed) ? removed : [removed];
            let add = Array.isArray(added) ? added : [added];

            beginDateTimes.push(row.beginDateTime);
            if (remove.length) {
                let promise = module.exports.removeOnDateAndTag({token: database, beginDateTime: row.beginDateTime, endDateTime: row.endDateTime, tags: remove});
                promises.push(promise);
            }
            if (add.length) {
                let promise = module.exports.add({token: database, beginDateTime: row.beginDateTime, endDateTime: row.endDateTime, tags: add});
                promises.push(promise);
            }
        });

        return Promise.all(promises);
    })
    .catch((err) => {
        return database.rollbackTransaction(err);
    })
    .then(() => {
        return database.commitTransaction();
    })
    .then(() => {
        module.exports.timeChange.emit("change", token, beginDateTimes);
        return module.exports.mergeDuplicateTags({token: database, beginDateTime, endDateTime});
    });
};

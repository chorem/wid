/* eslint strict: 0 */
/* eslint new-cap: 0 */
/* eslint no-console: 0 */
/* eslint no-process-env: 0 */

// Needed in Node in order to use let
"use strict";

let express = require("express");
let cookie = require("cookie");
let cookieParser = require("cookie-parser");
let bodyParser = require("body-parser");

let multer = require("multer");
let upload = multer({path: "/tmp"});

let dbHelper = require("./src/DatabaseHelper");
let UnauthorizedError = require("./src/UnauthorizedError");

let serverContext = process.env["npm_package_config_WID_SERVER_CONTEXT_" + process.env.npm_lifecycle_event]
                        || process.env.npm_package_config_WID_SERVER_CONTEXT;
let port = process.env["npm_package_config_WID_SERVER_PORT_" + process.env.npm_lifecycle_event]
                || process.env.npm_package_config_WID_SERVER_PORT;

let app = express();
let router = express.Router();

let http = require("http").Server(app);
let io = require("socket.io")(http, {path: "/ws"});
io.listen(http);

router.use(cookieParser());
// parse application/x-www-form-urlencoded
router.use(bodyParser.urlencoded({extended: false}));
// parse application/json
router.use(bodyParser.json({limit: "2mb"}));

// Add server version in each response
let uuid = require("uuid");
let serverVersion = uuid.v4();
router.use(function(req, res, next) {
    res.set("X-WID-Server-Version", serverVersion);
    next();
});

// Wrapper use to call the API and return the result in the response
let callApi = (call, done) => {
    return (req, res) => {
        let params = Object.assign({}, req.query, req.cookies);

        if (req.file) {
            params[req.file.fieldname] = req.file.buffer;
        }

        call(params)
        .then((result) => {
            if (done) {
                done(req, res, result);
            } else {
                res.json(result);
            }
        })
        .catch((err) => {
            console.error(req.originalUrl, err && err.stack || err);
            if (err instanceof UnauthorizedError) {
                res.status(401).send(JSON.stringify({err: "Server error"}));
            } else {
                res.status(500).send(JSON.stringify({err: "Server error"}));
            }
        });
    };
};

// Extract body from request
let extractBody = (name) => {
    return (req, res, next) => {
        req.query[name] = req.body;
        next();
    };
};

app.use(function(req, res, next) {
    let origin = req.get("Origin");
    res.header("Access-Control-Allow-Origin", origin);
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Authorization, Accept");
    next();
});

// USER API
let userApi = require("./src/UserApi");

router.get("/api/user", callApi(userApi.getUser));
router.get("/api/signup", callApi(userApi.signUp));
router.get("/api/signin", callApi(userApi.signIn, (req, res, user) => {
    res.cookie("token", user.token, {httpOnly: true});
    res.json(user);
}));
router.get("/api/signout", callApi(userApi.signOut));
router.get("/api/export", callApi(userApi.exportDb, (req, res, result) => {
    res.header("Content-Disposition", "attachment; filename=\"" + result.name + ".json\"");
    res.send(result.file);
}));
router.get("/api/password/reset", callApi(userApi.generatePassword));
router.get("/api/password/change", callApi(userApi.changePassword));

router.get("/api/priorities", callApi(userApi.getPriorites));
router.post("/api/priorities", extractBody("tags"), callApi(userApi.setPriorites));

// TIME API
let timeApi = require("./src/TimeApi");

router.get("/api/times", callApi(timeApi.getAll));
router.get("/api/time/add", callApi(timeApi.add));
router.get("/api/time/save", callApi(timeApi.createOrUpdate));
router.get("/api/time/insert", callApi(timeApi.insert));
router.get("/api/time/modify", callApi(timeApi.modifyDate));
router.get("/api/time/merge/up", callApi(timeApi.mergeUp));
router.get("/api/time/merge/down", callApi(timeApi.mergeDown));
router.get("/api/time/tree", callApi(timeApi.getTree));
router.get("/api/time/tree/completion", callApi(timeApi.getTreeCompletion));
router.get("/api/time/tree/facet", callApi(timeApi.getTreeFacet));
router.get("/api/time/tree/report", callApi(timeApi.getTreeReport));
router.get("/api/time/tags/update", callApi(timeApi.updateTags));

// IMPORT API
let importApi = require("./src/ImportApi");

router.post("/api/import/jtimer", upload.single("data"), callApi(importApi.importJtimer));
router.post("/api/import/json", upload.single("data"), callApi(importApi.importDb));

// REPORT API
let reportApi = require("./src/ReportApi");

router.get("/api/reports", callApi(reportApi.getAll));
router.get("/api/report", callApi(reportApi.get));
router.get("/api/report/add", callApi(reportApi.add));
router.get("/api/report/save", callApi(reportApi.save));
router.get("/api/report/remove", callApi(reportApi.remove));
router.get("/api/report/total", callApi(reportApi.getTotalTime));
router.get("/api/report/widgets", callApi(reportApi.getWidgetsTime));
router.get("/api/report/details", callApi(reportApi.getDetailsTime));

// EVENT API
let eventApi = require("./src/EventApi");

// let eventUpdate = (req, res, result) => {
//     let token = req.cookies.token;
//     let name = dbHelper.getName(token);
//     io.to(name).emit("eventUpdate", [result]);
//     res.json(result);
// };

router.get("/api/events", callApi(eventApi.getAll));
router.post("/api/event/add", extractBody("event"), callApi(eventApi.add));
// router.get("/api/event/start", callApi(eventApi.start, eventUpdate));
// router.get("/api/event/end", callApi(eventApi.end, eventUpdate));
router.get("/api/event/remove", callApi(eventApi.remove));

// TIMEBUNDLE API
let timebundleApi = require("./src/TimebundleApi");

router.get("/api/timebundle/add", callApi(timebundleApi.add));
router.get("/api/timebundle/remove", callApi(timebundleApi.remove));
router.get("/api/timebundle/save", callApi(timebundleApi.save));
router.get("/api/timebundle/projects", callApi(timebundleApi.getProjects));
router.get("/api/timebundle/tasks", callApi(timebundleApi.getTasks));
router.get("/api/timebundle/sync", callApi(timebundleApi.sync));

// TIMEBUNDLE API
let decisionTreesApi = require("./src/DecisionTreesApi");

router.get("/api/decision", callApi(decisionTreesApi.getDecision));

// WEB SOCKET
io.on("connection", function(socket) {
    let clientCookies = socket.client.request.headers.cookie;
    if (clientCookies) {

        let token = cookie.parse(clientCookies).token;
        if (token) {
            let name = dbHelper.getName(token);

            if (name) {
                io.to(name).emit("lock");
                socket.join(name);
            }
        }
    }
});

// APPLICATION API
let applicationApi = require("./src/ApplicationApi");

router.get("/api/keys", callApi(applicationApi.getKeys));
router.get("/api/key/add", callApi(applicationApi.createKey));
router.get("/api/key/remove", callApi(applicationApi.removeKey));

router.get("/api/identities", callApi(applicationApi.getIdentities));
router.get("/api/identity/add", callApi(applicationApi.createIdentity));
router.get("/api/identity/remove", callApi(applicationApi.removeIdentity));
router.get("/api/identity/validate", callApi(applicationApi.validateIdentity));

// ssh -R *:9643:localhost:9090 maven-release@goh.codelutin.com
router.post("/webhook/:key", (req, res) => {
    let body = req.body;
    let key = req.params.key;
    let headers = req.headers;

    let hook;
    if (req.header("X-Gitlab-Event")) {
        hook = applicationApi.onGitLabWebHook;
    } else if (req.header("X-GitHub-Event")) {
        hook = applicationApi.onGitHubWebHook;
    } else if (req.header("user-agent").startsWith("wakatime")) {
        hook = applicationApi.onWakatimeWebHook;
    } else {
        hook = applicationApi.onEventsWebHook;
    }

    hook({headers, key, data: body})
    .then((result) => {
        if (result) {
            let name = dbHelper.getName(result.token);
            io.to(name).emit("eventUpdate", result.events);
        }
        res.sendStatus(200);
    })
    .catch((err) => {
        console.error(req.originalUrl, err.stack || err);
        res.status(500).send(JSON.stringify({err: "Server error"}));
    });
});

// Start the server
// app.listen(9090, "0.0.0.0");
app.use(serverContext, router);

http.listen(port);
console.log(`Server start on port ${port}`);

import Service from "./Service";
import io from "socket.io-client";

class SocketService extends Service {

    constructor() {
        super();

        // Overwrite server location without the context
        this.serverLocation = window.WID_SERVER_URL;
        if (!this.serverLocation.startsWith("http")) {
            this.serverLocation = window.location.origin;
        }
    }

    connect() {
        this.locked = false;

        this.socket = io.connect(this.serverLocation, {path: "/ws"});
        this.socket.on("eventUpdate", this.trigger.bind(this, "eventUpdate"));
        this.socket.on("lock", () => {
            this.locked = true;
            this.trigger("lock");
        });
    }

    disconnect() {
        this.socket.disconnect();
    }

    isLocked() {
        return this.locked;
    }
}

export default new SocketService();

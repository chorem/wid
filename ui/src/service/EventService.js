import Service from "./Service";
import Events from "../model/Events";
import {diffValue} from "../utils/DateTimeUtils";

class EventService extends Service {

    constructor() {
        super();
        this.events = new Events();

        // Initialize the ServiceWorker
        this.channel = new MessageChannel();
        if (navigator.serviceWorker) {
            navigator.serviceWorker.register(this.uiLocation + "/sw.js");
            navigator.serviceWorker.ready
            .then(registration => {
                registration.active.postMessage("", [this.channel.port2]);
            });
        }
    }

    clearEvents() {
        this.events.clear();
    }

    shiftEvents(data) {
        return this.events.shift(data);
    }

    getEvents(beginDateTime, endDateTime) {
        return this.getApi(`/api/events?beginDateTime=${+beginDateTime}&endDateTime=${+endDateTime}`)
        .then((data) => {
            this.events.concat(data.rows);
        });
    }

    getEventsShifted(beginDateTime, endDateTime) {
        return this.getApi(`/api/events?beginDateTime=${+beginDateTime}&endDateTime=${+endDateTime}`)
        .then((data) => {
            this.events.shift(data.rows);
        });
    }

    addEvent(event) {
        return this.postApi("/api/event/add", event)
        .then(() => {
            this.events.add(event);
        });
    }

    removeEvent(id) {
        return this.getApi(`/api/event/remove?id=${id}`)
        .then(() => {
            this.events.remove(id);
        });
    }

    addNote(beginDateTime, value) {
        let event = {
            beginDateTime: +beginDateTime,
            endDateTime: +beginDateTime,
            type: "note",
            source: "webui",
            data: {content: value}
        };
        return this.addEvent(event);
    }

    getEventsAggregation(activity, events) {
        let values = events.values;
        let length = events.values.length;
        let firstData = length && values[length - 1].data || {};
        let {beginDateTime, endDateTime} = activity;

        switch (events.type) {
        case "idle":
            let total = values.reduce((sum, e) => {
                return sum + (Math.min(+e.endDateTime, +endDateTime)) - Math.max(+e.beginDateTime, +beginDateTime);
            }, 0);
            return diffValue(total);
        case "note":
            return firstData.content;
        case "code":
            return firstData.project;
        case "scm":
            return firstData.message;
        case "application":
            return firstData.name;
        case "shell":
            return firstData.command;
        case "task":
            return firstData.description;
        case "call":
            return firstData.name;
        case "calendar":
            return firstData.title;
        case "step":
            let steps = values.reduce((sum, e) => {
                return sum + e.data.number;
            }, 0);
            return steps;
        case "heartbeat":
            let pulses = values.reduce((sum, e) => {
                return sum + e.data.pulse;
            }, 0);
            return pulses;
        case "location":
            return firstData.place || firstData.latitude + "/" + firstData.longitude;
        case "mark":
            return firstData.tags.join(" ");
        default:
            return "";
        }
    }

    getEventClass(event) {
        switch (event.type) {
        case "idle":
            return "fa-eye-slash";
        case "note":
            return "fa-pencil-square-o";
        case "scm":
            return "fa-code-fork";
        case "code":
            return "fa-code";
        case "application":
            return "fa-cog";
        case "shell":
            return "fa-terminal";
        case "task":
            return "fa-tasks";
        case "call":
            return "fa-phone";
        case "calendar":
            return "fa-calendar";
        case "step":
            return "fa-male";
        case "heartbeat":
            return "fa-heart";
        case "location":
            return "fa-compass";
        case "mark":
            return "fa-thumb-tack";
        default:
            return "fa-question";
        }
    }

    getEventLabel(event) {
        let data = event.data;

        switch (event.type) {
        case "idle":
            return "Idle";
        case "note":
            return data.content;
        case "scm":
            return `${data.project} (${data.message})`;
        case "code":
            return `${data.project} (${data.file})`;
        case "application":
            return `${data.name} (${data.title})`;
        case "shell":
            return `${data.device} (${data.command})`;
        case "task":
            return `${data.description} (${data.status})`;
        case "call":
            return `${data.name} (${data.number})`;
        case "calendar":
            return `${data.title} (${data.description})`;
        case "step":
            return data.number;
        case "heartbeat":
            return data.pulse;
        case "location":
            return `${data.place || data.latitude + "/" + data.longitude} (${data.provider})`;
        case "mark":
            return data.tags.join(" ");
        default:
            return "Unknow event";
        }
    }

    notifyIdleEventDetected(idleEvent) {
        if (!navigator.serviceWorker) {
            return Promise.reject();
        }

        return new Promise((resolve, reject) => {

            Notification.requestPermission(result => {
                if (result !== "granted") {
                    reject();
                    return;
                }

                navigator.serviceWorker.ready
                .then(registration => {
                    registration.showNotification("Idle detected", {
                        body: "An idle activity is detected. What do you want ?",
                        icon: "./img/logo.png",
                        tag: "idle-detected",
                        actions: [
                            {action: "continue", title: "Continue the current activity"},
                            {action: "use", title: "Use the idle activity"}
                        ]
                    });
                }, reject);

                this.channel.port1.onmessage = e => {
                    resolve({action: e.data, event: idleEvent});
                };
            });
        });
    }
}

export default new EventService();

import Service from "./Service";
import socketService from "./SocketService";

class UserService extends Service {

    constructor() {
        super();
        this.user = null;
    }

    signUp(username, email, password, confirmation) {
        if (password !== confirmation) {
            return Promise.reject();
        }

        let location = this.uiLocation + "/configuration";

        return this.getApi(`/api/signup?username=${username}&email=${encodeURIComponent(email)}&passwd=${password}&location=${location}`)
        .then((user) => {
            this.user = Promise.resolve(user);
            socketService.connect();
            this.trigger("userUpdated");
        });
    }

    signIn(username, passwd) {
        return this.getApi(`/api/signin?username=${username}&passwd=${passwd}`)
        .then((user) => {
            this.user = Promise.resolve(user);
            socketService.connect();
            this.trigger("userUpdated");
        });
    }

    signOut() {
        this.user = null;
        socketService.disconnect();

        return this.getApi("/api/signout")
        .then(() => {
            this.user = Promise.resolve();
            this.trigger("userUpdated");
        });
    }

    changePassword(previousPassword, newPassword, confirmation) {
        if (newPassword !== confirmation) {
            return Promise.reject();
        }

        return this.getApi(`/api/password/change?oldPasswd=${previousPassword}&newPasswd=${newPassword}`);
    }

    resetPassword(email) {
        return this.getApi(`/api/password/reset?email=${email}`);
    }

    get() {
        if (this.user) {
            return this.user;
        }

        this.user = this.getApi("/api/user")
        .then((user) => {
            socketService.connect();
            return user;
        });

        return this.user;
    }

    getPriorities() {
        return this.getApi("/api/priorities");
    }

    setPriorities(tags) {
        return this.postApi("/api/priorities", tags);
    }
}

export default new UserService();

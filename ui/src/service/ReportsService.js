import Service from "./Service";
import Activity from "../model/Activity";
import moment from "moment";

class ReportsService extends Service {

    getReports() {
        return this.getApi("/api/reports")
        .then((data) => {
            let result = {};
            data.rows.forEach((report) => {
                result[report.id] = report;
            });
            return result;
        });
    }

    getReport(reportId) {
        return this.getApi(`/api/report?id=${reportId}`);
    }

    getDetails(id, beginDateTime, endDateTime, tag = []) {
        let tags = this.parametersToArray("tags", tag);
        let request = `/api/report/details?${tags}id=${id}&beginDateTime=${+beginDateTime}`;
        if (endDateTime !== "0") {
            request += `&endDateTime=${+endDateTime}`;
        }

        return this.getApi(request)
        .then((result) => {
            result.details = result.details.map(activity => {
                return new Activity(
                    moment(activity.beginDateTime),
                    moment(activity.endDateTime),
                    activity.tags && activity.tags.split(" ") || []
                );
            });

            return result;
        });
    }

    getWidgets() {
        return this.getApi("/api/report/widgets")
        .then((data) => {
            return data.widgets;
        });
    }

    createReport(name, tags, period, widget, beginDateTime, endDateTime) {
        let request = `/api/report/add?name=${name}&tags=${encodeURIComponent(tags)}&period=${period}&widget=${widget}`;

        if (beginDateTime) {
            let value = new Date(beginDateTime);
            request += `&selectBeginDateTime=${value.getTime()}`;
        }
        if (endDateTime) {
            let value = new Date(endDateTime);
            request += `&selectEndDateTime=${value.getTime()}`;
        }

        return this.getApi(request)
        .then((report) => {
            return report.id;
        });
    }

    saveReport(id, name, tags, period, widget, beginDateTime, endDateTime) {
        let request = `/api/report/save?id=${id}&name=${name}&tags=${encodeURIComponent(tags)}&period=${period}&widget=${widget}`;

        if (beginDateTime) {
            let value = new Date(beginDateTime);
            request += `&selectBeginDateTime=${value.getTime()}`;
        }
        if (endDateTime) {
            let value = new Date(endDateTime);
            request += `&selectEndDateTime=${value.getTime()}`;
        }

        return this.getApi(request)
        .then((report) => {
            return report.id;
        });
    }


    removeReport(id) {
        return this.getApi(`/api/report/remove?id=${id}`);
    }
}

export default new ReportsService();

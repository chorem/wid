import Service from "./Service";
import moment from "moment";
import {tagsToArray, extractNote} from "../utils/StringUtils";
import Activity from "../model/Activity";
import userService from "./UserService";
import eventService from "./EventService";
import Activities from "../model/Activities";
import socketService from "./SocketService";

let DAY_LOAD_NUMBER = 7;

class TimelineService extends Service {

    constructor() {
        super();

        this._loadTimerActivity();

        userService.on("userUpdated", this._loadTimerActivity.bind(this));
        socketService.on("eventUpdate", this._onEventUpdated.bind(this));
    }

    _loadTimerActivity() {
        this.timerActivity = Activity.createTimerActivity();
        this.activities = new Activities();
        this.activities.concat(this.timerActivity);

        return userService.get()
        .then((user) => {
            if (user && user.beginDateTime) {
                this.timerActivity.beginDateTime = moment(user.beginDateTime);
            } else {
                this.timerActivity.beginDateTime = moment();
            }
            this.timerActivity.trigger("changed");
        });
    }

    _onEventUpdated(data) {
        this.timerActivity.endDateTime = moment();

        let containsOldEvent = data.some(event => event.beginDateTime < this.timerActivity.beginDateTime);
        if (containsOldEvent) {
            this.nextActivities(0);
            return;
        }

        let events = eventService.shiftEvents(data);
        let detectIdleEvent = events.filter(event => event.type === "idle").pop();

        if (detectIdleEvent) {
            eventService.notifyIdleEventDetected(detectIdleEvent)
            .then(({action, event}) => {
                let promise = Promise.resolve();

                let firstActivity = this.activities.get()[0];
                if (action === "continue" && firstActivity) {
                    promise = this.addActivity(
                        moment(this.timerActivity.beginDateTime),
                        moment(event.beginDateTime),
                        firstActivity.tags.join(" ")
                    );
                }

                if (action === "use" || action === "continue") {
                    promise
                    .then(() => {
                        this.addActivity(
                            moment(event.beginDateTime),
                            moment(event.endDateTime),
                            eventService.getEventLabel(event));
                    });
                }
            });
        }
        this.timerActivity.trigger("changed");
    }

    nextActivities(position) {
        this.position = position !== undefined ? 0 : this.position || 0;

        let date = this.timerActivity.beginDateTime;
        let beginDateTime = date.clone().startOf("day").subtract(this.position + DAY_LOAD_NUMBER, "days");
        let endDateTime = date.clone().endOf("day").subtract(this.position, "days");

        if (this.position === 0) {
            eventService.clearEvents();
            endDateTime = moment().endOf("day");
        }

        // Get events
        eventService.getEvents(beginDateTime, endDateTime);

        // Get activities
        return this._getActivities(beginDateTime, endDateTime)
        .then((activities) => {
            this.activities.concat(activities);
            this.position += DAY_LOAD_NUMBER;
        });
    }

    _getActivities(beginDateTime, endDateTime) {
        return this.getApi(`/api/times?beginDateTime=${+beginDateTime}&endDateTime=${+endDateTime}`)
        .then(({rows}) => {

            let activities = [];
            let cache = {};

            rows.forEach((row) => {
                let key = row.beginDateTime + "-" + row.endDateTime;
                let activity = cache[key];
                if (!activity) {
                    cache[key] = activity = new Activity(moment(row.beginDateTime), moment(row.endDateTime));
                    activities.push(activity);
                }
                row.tag && activity.tags.push(row.tag);
            });
            return activities;
        });
    }

    addActivity(beginDateTime, endDateTime, value) {
        if (beginDateTime > endDateTime) {
            return Promise.reject("Invalid date times");
        }

        let tagsFromValue = tagsToArray(value);
        let notesFromValue = extractNote(value);

        let activity = new Activity(beginDateTime, endDateTime, tagsFromValue);

        let tags = this.parametersToArray("tags", tagsFromValue);

        let request;
        if (beginDateTime) {
            request = `/api/time/add?${tags}beginDateTime=${+beginDateTime}&endDateTime=${+endDateTime}`;
        } else {
            request = `/api/time/add?${tags}endDateTime=${+endDateTime}`;
        }

        if (notesFromValue) {
            let notes = this.parametersToArray("note", notesFromValue);
            request += `&${notes}`;
        }

        return this.getApi(request)
        .then(() => {
            this.timerActivity.beginDateTime = endDateTime;
            this.timerActivity.trigger("changed");

            this.activities.addActivity(activity);
        });
    }

    useActivity(value) {
        if (!this.timerActivity.beginDateTime) {
            return Promise.reject();
        }

        return this.addActivity(this.timerActivity.beginDateTime, moment(), value);
    }

    saveActivity(activity, value) {
        let {beginDateTime, endDateTime} = activity;
        if (beginDateTime > endDateTime) {
            return Promise.reject("Invalid date times");
        }

        if (this.timerActivity === activity) {
            return this.addActivity(beginDateTime, endDateTime, value);
        }

        let tagsFromValue = tagsToArray(value);
        let notesFromValue = extractNote(value);

        let tags = this.parametersToArray("tags", tagsFromValue);

        let request = `/api/time/save?${tags}beginDateTime=${+beginDateTime}&endDateTime=${+endDateTime}`;

        if (notesFromValue) {
            let notes = this.parametersToArray("note", notesFromValue);
            request += `&${notes}`;
        }

        return this.getApi(request)
        .then(() => {
            this.activities.saveActivity(activity, tagsFromValue);
        });
    }

    insertActivity(activity, beginDateTime, endDateTime, value) {
        if (beginDateTime > endDateTime) {
            return Promise.reject("Invalid date times");
        }

        if (this.timerActivity === activity) {
            if (+activity.beginDateTime !== +beginDateTime) {
                // Insert hole
                return this.addActivity(activity.beginDateTime, beginDateTime, "")
                .then(() => {
                    return this.addActivity(beginDateTime, endDateTime, value);
                });
            }
            return this.addActivity(beginDateTime, endDateTime, value);
        }

        let tagsFromValue = tagsToArray(value);
        let notesFromValue = extractNote(value);

        let tags = this.parametersToArray("tags", tagsFromValue);

        let request = `/api/time/insert?${tags}beginDateTime=${+beginDateTime}&endDateTime=${+endDateTime}`;

        if (notesFromValue) {
            let notes = this.parametersToArray("note", notesFromValue);
            request += `&note=${notes}`;
        }

        return this.getApi(request)
        .then(() => {
            this.activities.insertActivity(activity, beginDateTime, endDateTime, tagsFromValue);
        });
    }

    modifyDate(activity, newBeginDateTime, newEndDateTime) {
        if (newBeginDateTime > newEndDateTime) {
            return Promise.reject("Invalid date times");
        }

        let {beginDateTime, endDateTime} = activity;

        let request = `/api/time/modify?oldBeginDateTime=${+beginDateTime}&oldEndDateTime=${+endDateTime}` +
                        `&newBeginDateTime=${+newBeginDateTime}&newEndDateTime=${+newEndDateTime}`;
        return this.getApi(request)
        .then(() => {
            this.activities.modifyDate(activity, newBeginDateTime, newEndDateTime);
        });
    }

    modifyDates(previousActivity, currentActivity, nextActivity,
        newPreviousBeginDateTime, newPreviousEndDateTime,
        newCurrentBeginDateTime, newCurrentEndDateTime,
        newNextBeginDateTime, newNextDateTime) {

        if (+newPreviousBeginDateTime > +newPreviousEndDateTime
            || +newCurrentBeginDateTime > +newCurrentEndDateTime
            || +newNextBeginDateTime > +newNextDateTime) {
            return Promise.reject("Invalid date times");
        }

        return this.modifyDate(currentActivity, newCurrentBeginDateTime, newCurrentEndDateTime)
        .then(() => {
            return previousActivity && this.modifyDate(previousActivity, newPreviousBeginDateTime, newPreviousEndDateTime);
        })
        .then(() => {
            return nextActivity && this.modifyDate(nextActivity, newNextBeginDateTime, newNextDateTime);
        })
        .then(() => {
            if (this.timerActivity === nextActivity) {
                this.timerActivity.beginDateTime = newCurrentEndDateTime.clone();
                this.timerActivity.trigger("changed");
            }

            if (this.timerActivity === currentActivity) {
                this.timerActivity.beginDateTime = newPreviousEndDateTime.clone();
                this.timerActivity.trigger("changed");
            }

            this.activities.trigger("changed");
            this.activities.trigger("allChanged");
        });
    }

    removeEvent(activity, event) {
        return eventService.removeEvent(event.id)
        .then(() => {
            activity.trigger("changed");
        });
    }

    addNote(activity, value) {
        eventService.addNote(activity.beginDateTime, value)
        .then(() => {
            activity.trigger("changed");
        });
    }

}

export default new TimelineService();

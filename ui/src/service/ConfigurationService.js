import Service from "./Service";

class ConfigurationService extends Service {

    getIdentities() {
        return this.getApi("/api/identities")
        .then((data) => {
            return data.rows;
        });
    }

    addIdentity(identifier) {
        let location = this.uiLocation + "/configuration";
        return this.getApi(`/api/identity/add?identifier=${encodeURIComponent(identifier)}&location=${location}`);
    }

    removeIdentity(id) {
        return this.getApi(`/api/identity/remove?id=${id}`);
    }

    validIdentity(token) {
        return this.getApi(`/api/identity/validate?validation=${token}`);
    }

    getKeys() {
        return this.getApi("/api/keys")
        .then((data) => {
            return data.rows;
        });
    }

    removeKey(id) {
        return this.getApi(`/api/key/remove?id=${id}`);
    }

    generateKey(name) {
        return this.getApi(`/api/key/add?name=${encodeURIComponent(name)}`);
    }

    changeOption(name, value) {
        localStorage.setItem(name, value);
    }

    getOption(name, defaultValue) {
        let result = localStorage.getItem(name);
        return result === null ? defaultValue : result;
    }

    exportJson() {
        window.location = this.serverLocation + "/api/export";
    }

    importJtimer(file) {
        let formData = new FormData();
        formData.append("data", file);

        return this.formApi("/api/import/jtimer", formData);
    }

    importJson(file) {
        let formData = new FormData();
        formData.append("data", file);

        return this.formApi("/api/import/json?drop=1", formData);
    }

    searchTags(query, beginDateTime, endDateTime) {
        let request = `/api/time/tree/report?query=${encodeURIComponent(query)}`;

        if (beginDateTime) {
            let value = new Date(beginDateTime);
            request += `&beginDateTime=${value.getTime()}`;
        }
        if (endDateTime) {
            let value = new Date(endDateTime);
            request += `&endDateTime=${value.getTime()}`;
        }

        return this.getApi(request);
    }

    updateTags(query, beginDateTime, endDateTime, removed = [], added = []) {
        let request = `/api/time/tags/update?query=${encodeURIComponent(query)}`;

        if (beginDateTime) {
            let value = new Date(beginDateTime);
            request += `&beginDateTime=${value.getTime()}`;
        }
        if (endDateTime) {
            let value = new Date(endDateTime);
            request += `&endDateTime=${value.getTime()}`;
        }

        if (removed && removed.length) {
            request += "&" + this.parametersToArray("removed", removed);
        }
        if (added && added.length) {
            request += "&" + this.parametersToArray("added", added);
        }

        return this.getApi(request);
    }
}

export default new ConfigurationService();

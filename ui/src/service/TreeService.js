import Service from "./Service";
import configurationService from "./ConfigurationService";
import {tagsToArray} from "../utils/StringUtils";
import moment from "moment";

class TreeService extends Service {

    getRefDate() {
        let result = moment();
        let treeBack = configurationService.getOption("WID_OPTION_TREE_BACK", "1y");

        switch (treeBack) {
        case "1m":
            result.subtract("1", "month");
            break;
        case "3m":
            result.subtract("3", "month");
            break;
        case "6m":
            result.subtract("6", "month");
            break;
        case "3y":
            result.subtract("3", "year");
            break;
        case "1y":
        default:
            result.subtract("1", "year");
            break;
        }

        return result;
    }

    completion(value) {
        let refDate = +this.getRefDate();

        let tagsFromValue = tagsToArray(value);
        let tags = this.parametersToArray("tags", tagsFromValue);

        return this.getApi(`/api/time/tree/completion?${tags}beginDateTime=${refDate}`);
    }

    getFacet() {
        let refDate = +this.getRefDate();

        return this.getApi(`/api/time/tree/facet?beginDateTime=${refDate}`);
    }

    getFacetToday() {
        let refDate = +moment().startOf("day");

        return this.getApi(`/api/time/tree/facet?beginDateTime=${refDate}`);
    }

    getDecision(beginDateTime, endDateTime) {
        return this.getApi(`/api/decision?beginDateTime=${+beginDateTime}&endDateTime=${+endDateTime}`);
    }
}

export default new TreeService();

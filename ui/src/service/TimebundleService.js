import Service from "./Service";

class TimebundleService extends Service {

    getProjectUrls() {
        return this.getApi("/api/timebundle/projects")
        .then((data) => {
            return data.rows.map(project => project.projectUrl);
        });
    }

    getProjectDetails(projectUrls) {
        return Promise.all(projectUrls.map(url => this.getProject(url)));
    }

    getProject(projectUrl) {
        return fetch(projectUrl)
        .then((response) => {
            return this.processResponse(response);
        });
    }

    getTasks(projectUrl) {
        return this.getApi(`/api/timebundle/tasks?projectUrl=${encodeURIComponent(projectUrl)}`)
        .then((data) => {
            return data.rows.reduce(function(map, value) {
                map[value.taskUrl] = value;
                return map;
            }, {});
        });
    }

    sync(force) {
        return this.getApi(`/api/timebundle/sync?now=${Date.now()}&force=${+force}`);
    }

    createQuery(projectUrl, taskUrl, query) {
        if (query) {
            return this.getApi(`/api/timebundle/add?projectUrl=${encodeURIComponent(projectUrl)}&taskUrl=${encodeURIComponent(taskUrl)}&query=${encodeURIComponent(query)}`);
        }
        return Promise.resolve();
    }

    updateQuery(id, query) {
        if (query) {
            return this.getApi(`/api/timebundle/save?id=${id}&query=${encodeURIComponent(query)}`);
        }
        return this.getApi(`/api/timebundle/remove?id=${id}`);
    }
}

export default new TimebundleService();

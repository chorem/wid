import riot from "riot";

class ServiceEmitter {
    constructor() {
        riot.observable(this);
    }
}

export default new ServiceEmitter();

import riot from "riot";
import emitter from "./ServiceEmitter";

export default class Service {
    constructor() {
        riot.observable(this);

        this.serverLocation = window.WID_SERVER_URL;
        if (!this.serverLocation.startsWith("http")) {
            this.serverLocation = window.location.origin + this.serverLocation;
        }

        this.uiLocation = window.WID_UI_CONTEXT;
        if (!this.uiLocation.startsWith("http")) {
            this.uiLocation = window.location.origin + this.uiLocation;
        }
    }

    getApi(url, body) {
        return fetch(this.serverLocation + url, {
            credentials: "include",
            method: body && "POST" || "GET",
            body: JSON.stringify(body)
        })
        .then((response) => {
            //let serverVersion = response.headers.get("X-WID-Server-Version");
            //checkServerVersion(serverVersion);
            return this.processResponse(response);
        });
    }

    postApi(url, body) {
        return fetch(this.serverLocation + url, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            credentials: "include",
            method: "POST",
            body: JSON.stringify(body)
        })
        .then((response) => {
            //let serverVersion = response.headers.get("X-WID-Server-Version");
            //checkServerVersion(serverVersion);
            return this.processResponse(response);
        });
    }

    formApi(url, data) {
        return fetch(this.serverLocation + url, {
            credentials: "include",
            method: "POST",
            body: data
        })
        .then((response) => {
            //let serverVersion = response.headers.get("X-WID-Server-Version");
            //checkServerVersion(serverVersion);
            return this.processResponse(response);
        });
    }

    parametersToArray(name, values) {
        let result = values.reduce((prev, value) => {
            return prev + name + "=" + encodeURIComponent(value) + "&";
        }, "");

        return result;
    }

    processResponse(response) {
        let json = response.json();

        if (response.status === 200 || response.status === 0) {
            return json.then((result) => {
                return result;
            });

        } else if (response.status === 401) {
            return json.then((err) => {
                emitter.trigger("unauthorized");
                return Promise.reject(err);
            });
        }

        return json.then((err) => {
            return Promise.reject(err);
        });
    }
}

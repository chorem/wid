import "./ActivityDatesModifier.tag";
import "./ActivityTimesModifier.tag";
import "./Events.tag";
import emitter from "../../service/ServiceEmitter";
import timelineService from "../../service/TimelineService";
import "./Completion.tag";
import {completeTags, PATTERN_TAG_INPUT, TAB_KEY_CODE, ESCAPE_KEY_CODE} from "../../utils/StringUtils";

<Panel onkeydown="{escape}" tabindex="-1">
    <div class="panel-close" onclick="{close}"></div>
    <div class="panel-content">
        <virtual if="{isOpen}">
            <div class="section" onclick="{toggle.bind(this, 'activity')}">
                <i class="fa fa-times fa-2x button-icon" onclick="{close}"></i>
                Activity
                <i class="fa fa-caret-down button-icon" ref="activityCaret"></i>
            </div>
            <div class="section-content" ref="activityContent">
                <form onsubmit="{save}">
                    <input class="tags" type="text" ref="tagValue" pattern="{PATTERN_TAG_INPUT}"
                        title="Must contain letter, number, dot, underscore or space" value="{activity && activity.tags.join(' ')}"
                        onkeydown="{enterTag}" oninput={changeTagValue} placeholder="Enter tags"/>

                    <Completion ref="completion" tagvalue="{tagvalue}" selecttag="{selecttag}"/>
                </form>
                <div class="actions">
                    <i class="fa fa-check button-icon" onclick="{save}"></i>
                    <i class="fa fa-close button-icon" onclick="{cancel}"></i>
                </div>
            </div>

            <div class="section" onclick="{toggle.bind(this, 'times')}">
                Times
                <i class="fa fa-caret-down button-icon" ref="timesCaret"></i>
            </div>
            <div class="section-content" ref="timesContent">
                <div class="toggleModifier">
                    <i class="fa fa-clock-o button-icon" onclick="{toggleModifier}"></i>
                </div>
                <ActivityDatesModifier ref="datesModifier" activity={activity} openactivity="{openActivity}"/>
                <ActivityTimesModifier ref="timesModifier" activity={activity} class="hide"/>
            </div>

            <div class="section" onclick="{toggle.bind(this, 'events')}">
                Events
                <i class="fa fa-caret-down button-icon" ref="eventsCaret"></i>
            </div>
            <div class="section-content" ref="eventsContent">
                <Events activity={activity} eventstype={eventstype}/>
            </div>
        </virtual>
    </div>

    <script>
        this.activity = null;
        this.eventstype = null;
        this.isOpen = false;
        this.PATTERN_TAG_INPUT = PATTERN_TAG_INPUT;

        close() {
            this.isOpen = false;
            this.root.style.transform = null;
            document.documentElement.classList.remove("no-overflow");
            document.documentElement.style.paddingRight = null;
            this.update();
        };

        open(activity, eventstype) {
            this.isOpen = true;
            this.root.style.transform = "translateX(0)";
            this.update();

            var scrollbarWidth = (window.innerWidth - document.documentElement.offsetWidth);

            document.documentElement.classList.add("no-overflow");
            document.documentElement.style.paddingRight = scrollbarWidth + "px";

            this.activity = activity;
            this.eventstype = eventstype;
            this.tagvalue = this.refs.tagValue && this.refs.tagValue.value || this.activity && this.activity.tags.join(" ");

            this.update();

            if (this.eventstype) {
                this.toggle("activity");
                this.toggle("times");
                this.root.focus();
            } else {
                this.refs.tagValue.focus()
            }
        };

        this.openPanel = this.open.bind(this);
        emitter.on("openPanel", this.openPanel);

        openActivity(activity) {
            this.activity = activity;
            this.update();
        };

        toggle(name) {
            this.refs[name + "Content"] && this.refs[name + "Content"].classList.toggle("hide");
            this.refs[name + "Caret"] && this.refs[name + "Caret"].classList.toggle("fa-caret-up");
            this.preventUpdate = true;
        }

        toggleModifier() {
            this.refs.datesModifier.root.classList.toggle("hide");
            this.refs.timesModifier.root.classList.toggle("hide");
        }

        save(e) {
            e.preventDefault();
            e.stopPropagation();
            timelineService.saveActivity(this.activity, this.refs.tagValue.value)
            .then(() => {
                if (this.activity.isTimer) {
                    this.openActivity(this.activity.previous);
                }
                emitter.trigger("message-info", "Updated.");
            }, (err) => {
                emitter.trigger("message-error", "Error during updating.", err);
            });
        };

        cancel() {
            this.refs.tagValue.value = this.activity && this.activity.tags.join(" ");
        };

        enterTag(event) {
            if (event.keyCode === TAB_KEY_CODE) {
                event.stopPropagation();
                event.preventDefault();
                this.selecttag();
            }
        };

        escape(event) {
            event.stopPropagation();
            if (event.keyCode === ESCAPE_KEY_CODE) {
                this.close();
            }
        };

        selecttag(tag) {
            tag = tag || this.refs.completion.getFirstTag();
            let replace = this.refs.completion.isReplace();
            let input = this.refs.tagValue;
            let tagValue = input.value;

            input.value = completeTags(tagValue, tag, replace);

            let length = input.value.length;
            input.blur();
            input.setSelectionRange(length, length);
            input.focus();

            this.tagvalue = input.value;
        }

        changeTagValue(event) {
            this.tagvalue = this.refs.tagValue.value;
        }

        timelineService.activities.on("allChanged", () => {
            if (this.activity
                && +this.activity.beginDateTime === +this.activity.endDateTime) {

                let previous = this.activity.previous;
                let next = this.activity.next;

                if (previous && +this.activity.beginDateTime === +previous.endDateTime) {
                    this.openActivity(this.activity.next);
                } else if (next && +this.activity.beginDateTime === +next.beginDateTime) {
                    this.openActivity(this.activity.previous);
                } else {
                    this.close();
                }
            }
            this.update();
        });

        this.on("unmount", () => {
            timelineService.activities.off("allChanged", this.update);
            emitter.off("openPanel", this.openPanel);
        });
    </script>

    <style>
        panel {
            display: flex;
            justify-content: flex-end;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;

            transform: translateX(100%);
            transition: .3s transform;
        }

        .panel-close {
            position: absolute;
            top: 0;
            right: 0;
            width: 100%;
            height: 100%;
        }

        .panel-content {
            position: relative;
            overflow: auto;

            background-color: white;
            height: 100%;
            width: 490px;
            box-shadow: 0px 0px 20px 1px rgba(0, 0, 0, 0.2);
            text-align: center;
        }

        .section {
            margin: 0;
            cursor: pointer;
        }

        .section-content {
            padding-top: 10px;
        }

        activitydatesmodifier.hide, activitytimesmodifier.hide, .section-content.hide {
            display: none;
        }

        .toggleModifier {
            text-align: right;
            width: 100%;
            padding-right: 30px;
        }

        .section .fa-caret-down {
            position: absolute;
            right: 0;
            top: 10px;
        }

        .actions {
            text-align: right;
            cursor: pointer;
            margin: 0 15px;
        }

        form {
            margin: 0 20px;
        }

        .tags {
            width: 100%;
        }

        completion {
            display: flex;
            visibility: hidden;
            max-width: none;
        }

        input:focus ~ completion {
            visibility: visible;
        }

        completion:hover {
            visibility: visible;
        }

        @media screen and (max-width: 500px) {
            panel {
                width: 100%;
                min-width: 100%;
            }
        }
    </style>
</Panel>

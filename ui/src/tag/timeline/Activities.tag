import emitter from "../../service/ServiceEmitter"
import timelineService from "../../service/TimelineService";
import "./DayActivities.tag";
import "./Activity.tag";
import moment from "moment";

<Activities>

    <virtual each="{values, index in days}">
        <DayActivities index={index} date="{now.clone().startOf('day').subtract(index, 'days')}">
            <Activity each="{activity in parent.filterActivitiesByDay(opts.date, opts.index)}"
                        activity="{activity}"
                        date={parent.opts.date}/>
        </DayActivities>
    </virtual>

    <script>
        this.now = moment();
        this.days = new Array(7);
        this.activities = null;

        let refreshDays = () => {
            this.now = timelineService.timerActivity.beginDateTime || moment();
            if (!this.activities) {
                this.activities = [];
                timelineService.nextActivities(0);
            }
            this.update();
        };
        if (timelineService.timerActivity.beginDateTime) {
            refreshDays();
        }
        timelineService.timerActivity.one("changed", refreshDays);

        let refreshActivities = () => {
            this.days = new Array(timelineService.position + 7);
            this.now = timelineService.timerActivity.beginDateTime || moment();
            this.activities = timelineService.activities.get();
            this.update();

            let body = document.body;
            let scrollTop = document.documentElement.scrollTop || body.scrollTop;
            let scrollHeight = document.documentElement.scrollHeight || body.scrollHeight;

            if (body.offsetHeight === scrollHeight) {
                timelineService.nextActivities()
                .catch((err) => {
                    emitter.trigger("message-error", "Error server.", err);
                });
            } else {
                emitter.trigger("message-clear");
            }
        };
        timelineService.activities.on("allChanged", refreshActivities);

        this.on("mount", () => {
            emitter.trigger("message-loading");

            window.onscroll = () => {
                let body = document.body;
                let scrollTop = document.documentElement.scrollTop || body.scrollTop;
                let scrollHeight = document.documentElement.scrollHeight || body.scrollHeight;

                if (body.offsetHeight + scrollTop >= scrollHeight) {

                    emitter.trigger("message-loading");
                    timelineService.nextActivities();
                }
            };
        });

        this.on("unmount", () => {
            window.onscroll = null;

            timelineService.timerActivity.off("changed", refreshDays);
            timelineService.activities.off("allChanged", refreshActivities);
        });

        filterActivitiesByDay(begin, dayIndex) {
            if (!this.activities) {
                return [];
            }

            let end = begin.clone().endOf('day');

            if (dayIndex === 0 || this.beginIndex < 0) {
                this.beginIndex = 0;
            }

            let result = [];
            let length = this.activities.length;
            for (var i = this.beginIndex; i < length; i++) {
                let activity = this.activities[i];
                let {beginDateTime, endDateTime} = activity;

                if (+begin > +endDateTime) {
                    this.beginIndex = i - 1;
                    break;
                }

                if (+beginDateTime <= +end && +endDateTime >= +begin) {
                    result.push(activity);
                }
            }

            return result;
        };
    </script>

    <style media="screen">
        activities {
            display: block;
            margin: 0 20px;
            box-shadow: 0px 0px 5px 1px rgba(0, 0, 0, 0.15);
            background-color: white;
            border-top: 1px solid #e5e7e8;
            border-bottom: 1px solid #e5e7e8;
        }

        @media screen and (max-width: 900px) {
            activities {
                margin: 0;
            }
        }
    </style>

</Activities>

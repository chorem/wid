import {diffValue} from "../../utils/DateTimeUtils";
import timelineService from "../../service/TimelineService";
import emitter from "../../service/ServiceEmitter";

<ActivityTimesModifier>

    <select if="{activity}" ref="from" onchange="{onChangeFrom}">
        <option each="{item in from}" value="{item.index}">
            {diffValue(item.time - (root.selected ? timeValue : 0))} - {item.name}
        </option>
    </select>

    <div class="fa fa-arrow-down"></div>
    <div class="time">
        <i class="fa fa-bolt button-icon" onclick="{forceValue}"></i>
        <input ref="hours" type="number" value="0" min="0" max="999" oninput="{updateTime}"/>:
        <input ref="minutes" type="number" value="00" min="0" max="59" oninput="{onFixedDigit}"/>:
        <input ref="seconds" type="number" value="00" min="0" max="59" oninput="{onFixedDigit}"/>
    </div>
    <div class="fa fa-arrow-down"></div>

    <select if="{activity}" ref="to" onchange="{updateTime}">
        <option each="{item in to[refs.from.selectedIndex]}" value="{item.index}">
            {diffValue(item.time + (root.selected ? timeValue : 0))} - {item.name}
        </option>
    </select>

    <div class="actions">
        <i class="fa fa-check button-icon" onclick="{save}"></i>
    </div>

    <script>
        this.diffValue = diffValue;
        this.activity = null;
        this.from = [];
        this.to = [];
        this.labels = [];
        this.timeValue = 0;

        this.on("update", (innerUpdate) => {
            if (!this.opts.activity || innerUpdate) {
                return;
            }

            this.activity = this.opts.activity;

            this.labels = [];
            this.from = [];
            this.to = [];

            let beginDateTime = this.activity.beginDateTime.clone();
            let endDateTime = this.activity.endDateTime.clone();
            let currentTime = endDateTime.diff(beginDateTime);
            let currentLabel = {index: 1, name: this.getOptionLabel(this.activity), time: currentTime};

            let currentBeforeLabel = {index: 3, name: "Create empty activity before", time: 0};
            this.labels[3] = currentBeforeLabel;

            let currentAfterLabel = {index: 4, name: "Create empty activity after", time: 0};
            this.labels[4] = currentBeforeLabel;

            let currentTo = [currentBeforeLabel, currentAfterLabel];

            if (this.activity.previous) {
                let previousBeginDateTime = this.activity.previous.beginDateTime.clone();
                let previousEndDateTime = this.activity.previous.endDateTime.clone();
                let previousTime = previousEndDateTime.diff(previousBeginDateTime);

                let previousLabel = {index: 0, name: this.getOptionLabel(this.activity.previous), time: previousTime};
                this.labels[0] = previousLabel;
                this.from.push(previousLabel);
                this.to.push([currentLabel]);

                currentTo.unshift(previousLabel);
            }

            this.labels[1] = currentLabel;
            this.from.push(currentLabel);
            this.to.push(currentTo);

            if (this.activity.next) {
                let nextBeginDateTime = this.activity.next.beginDateTime.clone();
                let nextEndDateTime = this.activity.next.endDateTime.clone();
                let nextTime = nextEndDateTime.diff(nextBeginDateTime) || 0;

                let nextLabel = {index: 2, name: this.getOptionLabel(this.activity.next), time: nextTime};

                this.labels[2] = nextLabel;
                this.from.push(nextLabel);
                this.to.push([currentLabel]);

                currentTo.push(nextLabel);
            }

            this.update(true);
            this.initValues();
        });

        getOptionLabel(activity) {
            return activity && activity.tags.join(" ") || "<no tag>";
        }

        onFixedDigit(event) {
            let input = event.target;
            let value = parseInt(input.value, 10);

            if (value < 10) {
                input.value = "0" + value;
            } else {
                input.value = value;
            }

            this.updateTime(event);
        }

        initValues() {
            this.refs.hours.value = "0";
            this.refs.minutes.value = "00";
            this.refs.seconds.value = "00";
            this.timeValue = 0;

            let label = this.labels[this.refs.from.value];
            let [hours, minutes, seconds] = diffValue(label.time).split(":");
            this.refs.hours.max = hours;
            this.refs.minutes.max = minutes;
            this.refs.seconds.max = seconds;
        }

        onChangeFrom(e) {
            this.initValues();
            e.preventUpdate = true;
            this.update(true);
        }

        forceValue(e) {
            let timeFrom = this.labels[this.refs.from.value].time;

            let [hours, minutes, seconds] = diffValue(timeFrom).split(":");
            this.refs.hours.value = hours;
            this.refs.minutes.value = minutes;
            this.refs.seconds.value = seconds;

            this.updateTime(e);
        }

        updateTime(e) {
            let timeFrom = this.labels[this.refs.from.value];
            let hours = parseInt(this.refs.hours.value, 10);
            let minutes = parseInt(this.refs.minutes.value, 10);
            let seconds = parseInt(this.refs.seconds.value, 10);

            this.timeValue = (hours * 60 * 60 + minutes * 60 + seconds) * 1000;

            let time = this.labels[this.refs.from.value].time;
            if (diffValue(time) === `${this.refs.hours.value}:${this.refs.minutes.value}:${this.refs.seconds.value}`) {
                this.timeValue = time; // Keep the milliseconds
            }

            e.preventUpdate = true;
            this.update(true);
        }

        save() {
            let previousBeginDateTime = this.activity.previous && this.activity.previous.beginDateTime.clone();
            let previousEndDateTime = this.activity.previous && this.activity.previous.endDateTime.clone();

            let beginDateTime = this.activity.beginDateTime.clone();
            let endDateTime = this.activity.endDateTime.clone();

            let nextBeginDateTime = this.activity.next && this.activity.next.beginDateTime.clone();
            let nextEndDateTime = this.activity.next && this.activity.next.endDateTime.clone();

            if (+this.refs.from.value === 0) {
                previousEndDateTime.subtract(this.timeValue, "ms");
                beginDateTime.subtract(this.timeValue, "ms");

            } else if (+this.refs.from.value === 1) {
                if (+this.refs.to.value === 0) {
                    beginDateTime.add(this.timeValue, "ms");
                    previousEndDateTime.add(this.timeValue, "ms");

                } else if (+this.refs.to.value === 2) {
                    endDateTime.subtract(this.timeValue, "ms");
                    nextBeginDateTime.subtract(this.timeValue, "ms");

                } else if (+this.refs.to.value === 3) {
                    beginDateTime.add(this.timeValue, "ms");

                } else if (+this.refs.to.value === 4) {
                    endDateTime.subtract(this.timeValue, "ms");
                }

            } else if (+this.refs.from.value === 2) {
                nextBeginDateTime.add(this.timeValue, "ms");
                endDateTime.add(this.timeValue, "ms");
            }

            timelineService.modifyDates(
                this.activity.previous,
                this.activity,
                this.activity.next,
                previousBeginDateTime,
                previousEndDateTime,
                beginDateTime,
                endDateTime,
                nextBeginDateTime,
                nextEndDateTime
            )
            .then(() => {
                emitter.trigger("message-info", "Updated.");
            }, (err) => {
                emitter.trigger("message-error", "Error during updating.", err);
            });
        }
    </script>

    <style>
        activitytimesmodifier {
            display: block;
            margin: 0 20px;
        }

        select {
            width: 100%;
        }

        .time {
            padding-left: 18px;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        input {
            text-align: right;
            width: initial;
            border: none;
            width: 40px;
        }

        input:first-of-type {
            width: 60px;
        }

        input:focus {
            border: none;
        }
    </style>

</ActivityTimesModifier>

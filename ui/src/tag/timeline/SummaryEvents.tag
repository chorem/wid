import timelineService from "../../service/TimelineService";
import eventService from "../../service/EventService";
import emitter from "../../service/ServiceEmitter";

<SummaryEvents>

    <div if="{opts.activity}" class="events">
        <div each="{event,type in events}" if="{type != 'all'}" class="event-value" onclick="{editEvents.bind(this, event)}">
            <i if="{event.values.length}" class="fa {getEventClass(event)}"></i>
            <div if="{event.values.length}" class="agg">({event.values.length}) {parent.getEventsAggregation(event)}</div>
        </div>
    </div>

    <script>
        let refreshEvents = () => {
            this.events = eventService.events.get(this.opts.activity);
            this.update();
        }
        refreshEvents();

        eventService.events.on("changed", refreshEvents);
        this.opts.activity.on("changed", refreshEvents);
        this.on("unmount", () => {
            eventService.events.off("changed", refreshEvents);
            this.opts.activity.off("changed", refreshEvents);
        });

        getEventClass(event) {
            return eventService.getEventClass(event);
        }

        getEventsAggregation(event) {
            return eventService.getEventsAggregation(this.opts.activity, event)
        }

        editEvents(events, e) {
            emitter.trigger("openPanel", this.opts.activity, events.type);
        }
    </script>

    <style>
        .events {
            display: flex;
            flex-wrap: wrap;
            color: #1e3e4e;
            font-size: 14px;
            align-items: center;
            font-weight: normal;
            max-width: 420px;
            cursor: pointer;
        }

        .event-value {
            display: flex;
            align-items: center;
            margin-right: 10px;
        }

        .agg {
            height: 21px;
            line-height: 19px;

            max-width: 165px;
            white-space: nowrap;
            text-overflow: ellipsis;
        }

        .fa {
            margin-right: 5px;
        }

        @media screen and (max-width: 900px) {
            .events {
                max-width: 100%;
            }
        }
    </style>
</SummaryEvents>

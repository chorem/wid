import treeService from "../../service/TreeService";

<Completion>
    <div class="help" style="transform: translateX(-{position * 110}px)">
        <div if="{!opts.nocomment}" class="help-value"><strong>(</strong>comment<strong>)</strong></div>
        <div each="{value in completionValues}" class="help-value" onclick="{parent.opts.selecttag.bind(parent.parent, value)}">{value}</div>
    </div>
    <i class="fa fa-chevron-left button-icon" onclick="{onLeft}"></i>
    <i class="fa fa-chevron-right button-icon" onclick="{onRight}"></i>

    <script>
        this.completionValues = [];
        this.tagvalue = null;
        this.position = 0;

        let refresh = () => {
            if (this.opts.tagvalue === null) {
                this.opts.tagvalue = "";
                this.tagvalue = "";
            }

            this.tagvalue = this.opts.tagvalue || "";

            treeService.completion(this.tagvalue)
            .then((data) => {
                this.completionData = data;

                this.completionValues = Object.keys(data).filter((item) => {
                    return item !== "__meta__";
                })
                .sort((tag, other) => {
                    return this.completionData[other].__meta__.duration - this.completionData[tag].__meta__.duration;
                });
                this.position = 0;
                this.update(true);
            });
        };

        this.on("update", (innerUpdate) => {
            if (!innerUpdate && (this.tagvalue === null || this.tagvalue.trim() !== this.opts.tagvalue.trim())) {
                refresh();
            }
        });

        getFirstTag() {
            return this.completionValues[0] || "";
        }

        isReplace() {
            return this.completionData && this.completionData.__meta__.replace;
        }

        onLeft() {
            if (this.position) {
                this.position--;
            }
        }
        onRight() {
            this.position++;
        }
    </script>

    <style>
        completion {
            display: none;
            position: relative;
            align-items: center;
            max-width: 420px;

            color: #1e3e4e;
            font-size: 14px;
            height: 28px;
            font-weight: normal;

            height: 35px;
            margin: 0;
        }

        .fa {
            background-color: white;
            margin: 0;
        }

        .fa-chevron-left {
            position: absolute;
            top: 6px;
            left: 0px;
        }

        .fa-chevron-right {
            position: absolute;
            top: 6px;
            right: 0px;
        }

        .help {
            display: flex;
            overflow: visible;
            padding: 0 20px;
            transition: transform 0.2s ;
        }

        .help-value {
            max-width: 165px;
            white-space: nowrap;
            text-overflow: ellipsis;
            margin: 0 5px;
            cursor: pointer;
        }

        @media screen and (max-width: 900px) {
            completion {
                max-width: 100%;
            }
        }

    </style>
</Completion>

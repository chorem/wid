import moment from "moment";
import {diffTime} from "../../utils/DateTimeUtils";
import timelineService from "../../service/TimelineService";
import eventService from "../../service/EventService";
import "./SummaryEvents.tag";
import emitter from "../../service/ServiceEmitter";
import configurationService from "../../service/ConfigurationService";

<Activity class="{isFirst()} {isLast()}">
    <div class="row--tag {isHole()} {isInline()}" onclick="{holeClassName && editActivity}">

        <div class="tag--metadata">
            <div class="tag--hours">
                <div class="hours--end">{endTime}</div>
                <div if="{endTime && beginTime}" class="hours--separator">-</div>
                <div class="hours--begin">{beginTime}</div>
            </div>
            <div class="tag--time">
                <span>{duration}</span>
            </div>
        </div>

        <div class="tag-values" if="{displayValue}">
            <span class="values">
                <span class="values-tags" draggable="true" ondragstart="{startDrag}" onclick="{editActivity}">{value}</span>
                <a title="Edit" class="fa fa-pencil button-icon forceVisibility" onclick="{editActivity}"></a>
                <a title="Use tags" if="{!this.mode && !opts.inline}" class="fa fa-history button-icon forceVisibility" onclick="{useActivity}"></a>
                <a title="Play tags" if="{this.mode && !opts.inline}" class="fa fa-play button-icon forceVisibility" onclick="{playActivity}"></a>
            </span>
            <SummaryEvents if="{!displayLine || displayLine === 'tag--line-top'}" activity="{opts.activity}"/>
        </div>
    </div>

    <div if="{displayLine}" class="tag--line {displayLine}"></div>
    <div if="{displayLine !== 'tag--line-top' && displayLine !== 'tag--line-empty'}" class="row--separator"></div>

    <script>
        this.mode = +configurationService.getOption("WID_OPTION_MODE", false);

        useActivity() {
            emitter.trigger("message-loading");
            timelineService.useActivity(this.value)
            .then(() => {
                emitter.trigger("message-clear");
            })
            .catch((err) => {
                emitter.trigger("message-error", "Server errror.", err);
            });
        };

        playActivity() {
            emitter.trigger("playActivity", this.value);
        }

        startDrag(e) {
            e.dataTransfer.setData("text/plain", e.target.textContent);
        };

        editActivity(event) {
            emitter.trigger("openPanel", this.opts.activity);
        };

        isFirst() {
            return (this.opts.activity && this.opts.activity.next && this.opts.activity.next.isTimer) ? "hours--first" : "";
        }

        isLast() {
            return (this.opts.activity && !this.opts.activity.previous && !this.opts.inline) ? "hours--last" : "";
        }

        isHole() {
            return (this.opts.activity && !this.opts.activity.tags.length) ? "hole" : "";
        }

        isInline() {
            return this.opts.inline ? "inline" : "";
        }

        let refreshActivity = () => {
            let activity = this.opts.activity;

            let {beginDateTime, endDateTime, tags} = activity;

            let beginDateTimeStartOf = moment(beginDateTime).startOf("day");
            let endDateTimeStartOf = moment(endDateTime).startOf("day");

            let days = endDateTimeStartOf.diff(beginDateTimeStartOf, "days");
            let index = endDateTimeStartOf.diff(this.opts.date, "days");

            this.beginTime = null;
            this.endTime = null;
            this.value = tags.join(" ");
            this.duration = null;
            this.displayLine = "";
            this.displayValue = false;

            // End long activity
            if (!this.opts.inline && days !== 0 && index === days) {
                this.beginTime = beginDateTime.format("HH:mm");
                this.displayLine = "tag--line-bottom";

            // Begin long activity
            } else if (!this.opts.inline && days !== 0 && index === 0) {
                this.endTime = endDateTime.format("HH:mm");
                this.duration = diffTime(beginDateTime, endDateTime);
                this.displayValue = true;
                this.displayLine = "tag--line-top";

            // Continue long activity
            } else if (!this.opts.inline && days !== 0) {
                this.displayLine = "tag--line-empty";

            // Normal
            } else {
                this.beginTime = beginDateTime.format("HH:mm");
                this.endTime = endDateTime.format("HH:mm");
                this.duration = diffTime(beginDateTime, endDateTime);
                this.displayValue = true;
            }

            this.update(true);
        }
        refreshActivity();

        this.on("update", (innerUpdate) => {
            if (innerUpdate !== true) {
                refreshActivity();
            }
        });
        this.opts.activity.on("changed", refreshActivity);

        this.on("unmount", () => {
            this.opts.activity.off("changed", refreshActivity);
        });
    </script>

    <style media="screen">
        activity {
            position: relative;
            display: block;
            background-color: white;
            overflow: visible;
        }

        activity:only-child .row--tag {
            min-height: 84px;
        }

        activity.hours--first .hours--end {
            top: 0;
        }

        activity.hours--first .tag--line-top {
            top: 30px;
            height: 56px;
        }

        activity.hours--last .hours--begin {
            display: block;
            position: absolute;
            bottom: 0;
        }

        .row--tag {
            min-height: 70px;
            display: flex;
            transition: 0.3s height;
            overflow: visible;
        }

        .tag--metadata {
            padding: 0 20px;
            display: flex;
            align-items: center;
            overflow: visible;
            position: relative;
        }

        .tag--hours {
            margin-right: 20px;
            font-size: 22px;
            font-weight: bold;
            width: 65px;
            text-align: end;
        }

        .tag--time {
            color: #1e3e4e;
            font-size: 18px;
            display: flex;
            align-items: center;
        }

        .tag-values {
            font-size: 20px;
            padding: 0 20px;
            display: flex;
            flex-direction: column;
            justify-content: center;
            flex-grow: 1;
        }

        .values {
            display: flex;
            height: 30px;
        }

        .values-tags {
            text-overflow: ellipsis;
            white-space: nowrap;
            flex-grow: 1;
            cursor: pointer;
        }

        .values > .fa {
            visibility: hidden;
        }

        .row--tag:hover .values > .fa {
            visibility: visible;
        }

        .row--separator {
            border-top: 1px solid #f6f8f9;
            border-bottom: 1px solid #f6f8f9;
        }

        .tag--line {
            width: 3px;
            height: 86px;
            position: absolute;
            top: 0;
            border-right: #1ea7e6 solid 1px;
            left: 51px;

            background-color: #d1dbe1;
            border-right: 1px solid #f6f8f9;
            border-left: 1px solid #f6f8f9;
        }

        .tag--line-top {
            height: 100%;
            top: 14px;
        }

        .tag--line-empty {
        }

        .tag--line-bottom {
            height: 42px;
        }

        .hole {
            background: repeating-linear-gradient(45deg, transparent, transparent 10px, #f6f8f9 10px, #f6f8f9 20px);
            cursor: pointer;
            min-height: 56px;
        }

        .hole .tag-values {
            flex-direction: row-reverse;
            justify-content: flex-end;
            flex-grow: 1;
            align-items: center;
        }

        .hole .values {
            flex-grow: 1;
        }

        .hours--begin {
            display: none;
        }

        .inline .hours--begin {
            display: block;
        }

        .hours--separator {
            display: none;
        }

        .hours--end {
            position: absolute;
            top: -18px;
        }

        .tag--hours::before {
            content: "";
            position: absolute;
            top: -2px;
            left: 20px;
            width: 57px;
            height: 2px;
            background-color: white;
        }

        .inline .hours--end {
            position: static;
            width: auto;
        }

        @media screen and (max-width: 900px) {
            activity:only-child .row--tag {
                min-height: 70px;
            }
        }

        @media screen and (min-width: 1440px) {
            .tag-values {
                flex-grow: 0;
            }

            .values-tags {
                flex-grow: 0;
            }
        }

        @media screen and (max-width: 600px) {
            .row--tag {
                flex-direction: column;
                justify-content: center;
            }

            .tag--line-bottom {
                height: 17px;
            }

            .tag--line-top {
                height: 100%;
                top: 65px;
            }

            .hole ~ .tag--line-top {
                height: 100%;
                top: 34px;
            }

            .tag--hours {
                display: flex;
                flex-direction: row-reverse;
                width: auto;
                justify-content: flex-end;
                margin: 0;
                flex-grow: 1;
                margin-right: 5px;
            }

            .hours--end {
                position: static;
                width: auto;
            }

            .tag--hours::before {
                display: none;
            }

            .hours--separator {
                display: block;
                margin: 0 5px;
            }

            .tag--metadata {
                flex-wrap: wrap;
            }

            summaryevents {
                display: none;
            }

            .hours--begin {
                display: block;
            }
        }

        @media screen and (max-width: 351px) {
            .tag--line-top {
                height: 0px;
            }

            .hole ~ .tag--line-top {
                height: 100%;
                top: 58px;
            }
        }
    </style>
</Activity>

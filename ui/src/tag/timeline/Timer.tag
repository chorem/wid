import timelineService from "../../service/TimelineService";
import emitter from "../../service/ServiceEmitter";
import {diffTime} from "../../utils/DateTimeUtils";

<Timer>
    <span class="time" onclick="{editActivity}">{diffTime(currentActivity.beginDateTime, currentActivity.endDateTime)}</span>

    <script>
        this.diffTime = diffTime;
        this.currentActivity = timelineService.timerActivity;

        refresh() {
            this.currentActivity = timelineService.timerActivity;
            this.update();
        }

        loop() {
            // Keep refreshing the timer view each second
            this.timerLoop = setTimeout(() => {
                this.refresh();
                this.loop();
            }, 1000);
        };

        editActivity() {
            emitter.trigger("openPanel", this.currentActivity);
        };

        timelineService.timerActivity.on("changed", this.refresh);

        this.on("mount", this.loop);
        this.on("unmount", () => {
            timelineService.timerActivity.off("changed", this.refresh);
            clearTimeout(this.timerLoop);
        });
    </script>

    <style>
        .time {
            font-size: 32px;
            margin: 0 20px;
            cursor: pointer;
        }
    </style>
</Timer>

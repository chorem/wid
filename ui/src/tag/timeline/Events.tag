import eventService from "../../service/EventService";
import timelineService from "../../service/TimelineService";
import "./Event.tag";
import emitter from "../../service/ServiceEmitter";

<Events>
    <form class="form-note" onsubmit="{addNote}">
        <input ref="note" type="text" placeholder="Add a note"/>
        <i class="fa fa-2x fa-plus button-icon" onclick="{addNote}"></i>
    </form>
    <select ref="selector" if="{opts.activity}" class="select-type" onchange="{selectType}">
        <option each="{event, type in allEvents}" value="{type}" selected="{eventstype === type}">{type} ({allEvents[type].values.length})</option>
    </select>

    <Event each="{event in events}" event="{event}" activity={parent.opts.activity}
        splitfromactivity={!parent.splitFrom && parent.splitfromactivity} splittoactivity={parent.splitFrom && parent.splittoactivity}/>

    <script>
        this.events = [];
        this.allEvents = [];
        this.splitFrom = null;

        let refreshEvents = (eventstype, fromSelectType) => {
            if (!this.opts.activity) {
                return;
            }

            this.eventstype = eventstype || "all";
            let events = this.allEvents[this.eventstype];
            if (events) {
                this.events = this.allEvents[this.eventstype].values;
            } else {
                this.eventstype = "all";
                this.events = this.allEvents.all.values;
            }

            this.update(true);
        };

        let refreshActivity = () => {
            this.opts.activity.off("changed", refreshActivity);
            this.activity = this.opts.activity;
            this.activity.on("changed", refreshActivity);

            this.allEvents = eventService.events.get(this.opts.activity);
            refreshEvents(this.opts.eventstype);
        };

        this.on("update", (innerUpdate) => {
            // Activity changed ?
            if (this.opts.activity && this.activity !== this.opts.activity && !innerUpdate) {
                refreshActivity();
            }
        });

        this.on("unmount", () => {
            this.opts.activity.off("changed", refreshActivity);
        });

        selectType(e) {
            let selector = this.refs.selector;
            let type = selector.options[selector.selectedIndex].value;
            refreshEvents(type);
            e.preventUpdate = true;
        }

        addNote(e) {
            e.preventDefault();
            e.stopPropagation();

            timelineService.addNote(this.opts.activity, this.refs.note.value);
            this.refs.note.value = "";
        }

        splitfromactivity(event) {
            this.splitFrom = event;
            this.update();
        }

        splittoactivity(event) {
            let beginDateTime;
            let endDateTime;
            let value = eventService.getEventLabel(event).split(" ")[0];

            if (event === this.splitFrom && +beginDateTime !== +endDateTime) {
                beginDateTime = event.beginDateTime;
                endDateTime = event.endDateTime;

            } if (+this.splitFrom.beginDateTime < +event.endDateTime) {
                beginDateTime = this.splitFrom.beginDateTime;
                endDateTime = event.endDateTime;

            } else if (+this.splitFrom.endDateTime > +event.beginDateTime) {
                beginDateTime = event.beginDateTime;
                endDateTime = this.splitFrom.endDateTime;
            }

            if (beginDateTime && endDateTime) {
                timelineService.insertActivity(this.opts.activity, beginDateTime, endDateTime, value)
                .then(() => {
                    emitter.trigger("message-info", "Extracted.");
                }, (err) => {
                    emitter.trigger("message-error", "Error during extraction.", err);
                });
            }

            this.splitFrom = null;
            this.update();
        }
    </script>

    <style>
        events {
            display: block;
            text-align: right;
        }

        .form-note {
            display: flex;
            flex-direction: row;
            justify-content: center;
        }

        .fa-plus {
            min-width: 42px;
        }

        .select-type {
            margin-right: 20px;
        }
    </style>
</Events>

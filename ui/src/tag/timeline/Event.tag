import moment from "moment";
import {diffTime} from "../../utils/DateTimeUtils";
import timelineService from "../../service/TimelineService";
import eventService from "../../service/EventService";
import emitter from "../../service/ServiceEmitter";

<Event>
    <div class="row--event">

        <div class="event--metadata">
            <i class="fa {classType}"></i>
            <div class="event--hours">
                <div class="hours--end">{endTime}</div>
                <div if="{endTime && beginTime}" class="hours--separator">-</div>
                <div class="hours--begin">{beginTime}</div>
            </div>
            <div class="event--time">
                <span>{duration}</span>
            </div>
        </div>

        <div class="event-values">
            <a class="event-content" title="{value}">{value}</a>
            <a if="{opts.splitfromactivity}" title="Extract from" class="fa fa-chain-broken button-icon forceVisibility" onclick="{opts.splitfromactivity.bind(this, event)}"></a>
            <a if="{opts.splittoactivity}" title="Extract to" class="fa fa-level-up button-icon forceVisibility" onclick="{opts.splittoactivity.bind(this, event)}"></i>
            <a title="Remove" class="fa fa-trash-o button-icon forceVisibility" onclick="{removeEvent}"></a>
        </div>
    </div>
    <div class="row--separator"></div>

    <script>
        this.event = this.opts.event;
        this.activity = this.opts.activity;

        let {beginDateTime, endDateTime} = this.event;
        beginDateTime = moment(beginDateTime);
        endDateTime = moment(endDateTime);

        this.beginTime = beginDateTime.format("HH:mm");
        this.endTime = endDateTime.format("HH:mm");
        this.duration = diffTime(beginDateTime, endDateTime);

        this.classType = eventService.getEventClass(this.event);
        this.value = eventService.getEventLabel(this.event);

        removeEvent() {
            let confirmed = confirm("Remove event ?");
            if (confirmed) {
                timelineService.removeEvent(this.opts.activity, this.event)
                .then(() => {
                    emitter.trigger("message-info", "Deleted.");
                }, (err) => {
                    emitter.trigger("message-error", "Error during deleting.", err);
                });
            }
        }
    </script>

    <style>
        .row--event {
            min-height: 70px;
            display: flex;
            transition: 0.3s height;
        }

        .event--metadata {
            margin: 0 20px;
            display: flex;
            align-items: center;
            overflow: visible;
        }

        .event--hours {
            margin-right: 20px;
            font-size: 22px;
            font-weight: bold;
            width: 65px;
            text-align: end;
        }

        .event--time {
            color: #1e3e4e;
            font-size: 18px;
            display: flex;
            align-items: center;
            height: 28px;
        }

        .event-values {
            font-size: 20px;
            margin: 0 20px;
            display: flex;
            flex-direction: row;
            align-items: center;
            flex-grow: 1;
        }

        .event-content {
            text-align: left;
            text-overflow: ellipsis;
            white-space: nowrap;
            flex-grow: 1;
            color: #35434f;
        }

        .values {
            display: flex;
            height: 30px;
        }

        .event-values > .fa {
            visibility: hidden;
        }

        .row--event:hover .event-values > .fa {
            visibility: visible;
        }

        .fa {
            margin-right: 10px;
        }

        .row--separator {
            border-top: 1px solid #f6f8f9;
            border-bottom: 1px solid #f6f8f9;
        }

        .hours--separator {
            display: none;
        }

        @media screen and (max-width: 500px) {
            .row--event {
                flex-direction: column;
            }

            .event--hours {
                display: flex;
                flex-direction: row-reverse;
                width: auto;
                justify-content: flex-end;
                margin: 0;
                flex-grow: 1;
                margin-right: 5px;
            }

            .hours--separator {
                display: block;
                margin: 0 5px;
            }

            .event--metadata {
                flex-wrap: wrap;
            }
        }
    </style>
</Event>

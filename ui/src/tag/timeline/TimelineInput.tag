import timelineService from "../../service/TimelineService";
import treeService from "../../service/TreeService";
import "./Timer.tag";
import "./Completion.tag";
import configurationService from "../../service/ConfigurationService";
import emitter from "../../service/ServiceEmitter";
import {completeTags, PATTERN_TAG_INPUT, TAB_KEY_CODE} from "../../utils/StringUtils";

<TimelineInput>
    <form onsubmit="{saveTag}" ref="saveTagForm">
        <div class="container">
            <div class="timelineInput">
                <Timer class="timelineInput-timer" />

                <div class="timelineInput-tags">
                    <div class="timelineInput-input">
                        <input disabled="{mode && playing}" type="text" pattern="{PATTERN_TAG_INPUT}"
                            title="Must contain letter, number, dot, underscore or space" value="{tagvalue}"
                            onkeydown="{enterTag}" oninput={changeTagValue} placeholder="Enter tags" ref="tagValue"/>

                            <Completion ref="completion" tagvalue="{tagvalue}" selecttag="{selecttag}"/>
                            <SummaryEvents activity="{activity}"/>
                        </div>

                        <div class="buttons">
                            <button type="submit">
                                <i if="{!mode}" class="fa fa-plus button-icon"></i>
                                <i if="{mode}" class="fa {playing ? 'fa-stop' : 'fa-play'} button-icon"></i>
                            </button>
                            <i if="{mode}" style="visibility: {!playing ? 'visible' : 'hidden'}" class="fa fa-pause button-icon" onclick="{pause}"></i>
                            <i class="fa fa fa-magic button-icon" onclick="{getDecision}"></i>
                        </div>
                </div>
            </div>
        </div>
    </form>

    <script>
        this.PATTERN_TAG_INPUT = PATTERN_TAG_INPUT;
        this.mode = +configurationService.getOption("WID_OPTION_MODE", false);

        this.activity = timelineService.timerActivity;

        this.playing = false;
        this.tagvalue = "";
        if (this.mode) {
            this.tagvalue = configurationService.getOption("WID_OPTION_MODE_TAGS", "");
            this.playing = !!this.tagvalue;
        }

        let refresh = () => {
            this.activity = timelineService.timerActivity;
            this.update();
        };

        timelineService.timerActivity.on("changed", refresh);
        this.on("unmount", () => {
            timelineService.timerActivity.off("changed", refresh);
            emitter.off("playActivity", this.playActivity);
        });

        enterTag(event) {
            event.stopPropagation();

            if (event.keyCode === TAB_KEY_CODE) {
                event.preventDefault();
                this.selecttag();
            }
        };

        saveTag(event) {
            event.preventDefault();
            event.stopPropagation();

            if (!this.mode) {
                this.addTag();
            } else {
                this.toggle();
            }
        };

        addActivity(tags) {
            emitter.trigger("message-loading");
            return timelineService.addActivity(this.activity.beginDateTime, this.activity.endDateTime, tags)
            .then(() => {
                emitter.trigger("message-clear");
            })
            .catch((err) => {
                emitter.trigger("message-error", "Server error.", err);
            });
        }

        addTag() {
            let target = this.refs.tagValue;
            this.addActivity(target.value);
            target.value = "";
        };

        toggle() {
            this.playing = !this.playing;
            if (!this.playing) {
                this.addTag();
            }
            configurationService.changeOption("WID_OPTION_MODE_TAGS", this.tagvalue);
        }

        pause() {
            this.addActivity("");
        }

        selecttag(tag) {
            tag = tag || this.refs.completion.getFirstTag();
            let replace = this.refs.completion.isReplace();
            let input = this.refs.tagValue;
            let tagValue = input.value;

            input.value = completeTags(tagValue, tag, replace);

            let length = input.value.length;
            input.blur();
            input.setSelectionRange(length, length);
            input.focus();

            this.tagvalue = this.refs.tagValue.value;
        }

        changeTagValue() {
            this.tagvalue = this.refs.tagValue.value;
        }

        getDecision() {
            emitter.trigger("message-loading");
            treeService.getDecision(this.activity.beginDateTime, this.activity.endDateTime)
            .then(value => {
                emitter.trigger("message-clear");
                this.refs.tagValue.value = value.join(" ");
            })
            .catch((err) => {
                emitter.trigger("message-error", "Server errror.", err);
            });
        }

        playActivity(tags) {
            if (this.playing) {
                this.addTag();
            }
            this.playing = true;

            let input = this.refs.tagValue;
            input.value = tags;
            this.tagvalue = input.value;
            configurationService.changeOption("WID_OPTION_MODE_TAGS", this.tagvalue);
            this.update();
        }
        emitter.on("playActivity", this.playActivity);
    </script>

    <style>
        .container {
            position: relative;
            background-color: white;
            color: #48626f;
            font-size: 28px;
            font-weight: bold;
            margin: 0 0 20px 0;
            padding: 0 20px;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            box-shadow: 0px 0px 20px 1px rgba(0, 0, 0, 0.2);
            border-bottom: 1px solid #e1e8eb;
            min-height: 84px;
        }

        .time {
            font-size: 32px;
            margin: 0 20px;
            cursor: pointer;
        }

        .timelineInput {
            display: flex;
            align-items: center;
            width: 100%;
            justify-content: center;
        }

        .events {
            min-height: 35px;
        }

        .timelineInput-tags {
            display: flex;
            align-items: center;
        }

        .timelineInput-input {
            margin: 0 20px 5px 20px;
        }

        completion:hover ~ summaryevents {
            display: none;
        }

        input:focus ~ completion {
            display: flex;
        }

        completion:hover {
            display: flex;
        }

        input:focus ~ summaryevents {
            display: none;
        }

        .buttons {
            display: flex;
            flex-shrink: 0;
        }

        button {
            outline: none;
            border: none;
            background-color: transparent;
            font-size: 28px;
        }

        @media screen and (max-width: 900px) {
            .timelineInput {
                flex-direction: column;
            }

            .timelineInput-tags {
                width: 100%;
            }

            .timelineInput-input {
                width: 100%;
                margin: 0 20px 0 20px;
            }

            input {
                width: 100%;
            }
        }

        @media screen and (max-width: 500px) {
            .timelineInput-timer {
                display: none;
            }
        }
    </style>
</TimelineInput>

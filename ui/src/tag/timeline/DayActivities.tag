import moment from "moment";

<DayActivities>
    <div class="row">
        <div class="row--date {opts.index % 2 === 0 ? '' : 'row--date-even'}">
            <div class="row--day row--day-long">
                {opts.date.format("dddd")}
            </div>
            <div class="row--day-long">
                {opts.date.format("D MMM YYYY")}
            </div>
            <div class="row--day-short">
                {opts.date.format("ddd D")}
            </div>
            <div class="row--day row--day-short">
                {opts.date.format("MMM YY")}
            </div>
        </div>
        <div class="row--tags"><yield/></div>
    </div>

    <script>
    </script>

    <style>
        .row {
            display: flex;
            overflow: visible;
        }

        .row--date {
            color: white;
            font-weight: bold;
            font-size: 18px;
            min-width: 210px;
            background-color: #1ea7e6;
            padding: 10px;
            border: 1px solid #10a2e4;
        }

        .row--date-even {
            background-color: #0099cc;
        }

        .row--day {
            font-size: 28px;
            text-transform: capitalize;
        }

        .row--day-short {
            display: none;
        }

        .row--tags {
            flex-grow: 1;
            background: repeating-linear-gradient(45deg, transparent, transparent 10px, #f6f8f9 10px, #f6f8f9 20px);
        }

        @media screen and (max-width: 900px) {
            .row--date {
                width: 90px;
                min-width: 90px;
            }

            .row--day-long {
                display: none;
            }

            .row--day-short {
                display: block;
                font-size: 18px;
            }
        }
    </style>
</DayActivities>

import {diffValue} from "../../utils/DateTimeUtils";
import moment from "moment";
import emitter from "../../service/ServiceEmitter";
import timelineService from "../../service/TimelineService";
import "../common/Flatpickr.tag";

<ActivityDatesModifier>

    <div class="modifier">
        <div class="background-activities">
            <div class="activity activity-previous"></div>
            <div class="activity activity-current"></div>
            <div class="activity activity-next"></div>
        </div>

        <div class="sliders">
            <div class="slider slider-opaque" ref="previous"></div>
            <div class="slider slider-separator {nextTime === totalTime ? 'slider-end' : ''}" ref="previousHole">
                <i class="fa fa-caret-down fa-2x" onmousedown={startResizeActivity}></i>
                <i class="fa fa-caret-up fa-caret-up-left fa-2x" onmousedown={startResizeLeftHole}></i>
            </div>
            <div class="slider slider-opaque slider-current" ref="current"></div>
            <div class="slider slider-separator {previousTime === totalTime ? 'slider-end' : ''}" ref="nextHole">
                <i class="fa fa-caret-down fa-2x" onmousedown={startResizeActivity}></i>
                <i class="fa fa-caret-up fa-caret-up-right fa-2x" onmousedown={startResizeRightHole}></i>
            </div>
            <div class="slider slider-opaque" ref="next"></div>
        </div>

        <div class="times">
            <div class="time" onclick="{activity && activity.previous && this.opts.openactivity && this.opts.openactivity.bind(this, activity.previous)}">
                <div>{activity && activity.previous && activity.previous.tags.join(" ")}</div>
                {diffValue(previousTime)}
            </div>
            <div class="time time--current">
                <div>{activity && activity.tags.join(" ")}</div>
                <div>{diffValue(currentTime)}</div>
            </div>
            <div class="time" onclick="{activity && activity.next && this.opts.openactivity && this.opts.openactivity.bind(this, activity.next)}">
                <div>{activity && activity.next && activity.next.tags.join(" ")}</div>
                <div>{diffValue(nextTime)}</div>
            </div>
        </div>
    </div>

    <div class="dates">
        <a title="Merge left" class="fa fa-step-backward button-icon" onclick="{mergeLeft}"></a>
        <Flatpickr ref="beginDate" enabletime="true" onclose="{changeBeginDate}"
            defaultdate="{+beginDateTime}"/>
        <Flatpickr ref="endDate" enabletime="true" onclose="{changeEndDate}"
            defaultdate="{+endDateTime}"/>
        <a title="Merge right" class="fa fa-step-forward button-icon" onclick="{mergeRight}"></a>
    </div>

    <div class="actions">
        <i class="fa fa-check button-icon" onclick="{save}"></i>
        <i class="fa fa-close button-icon" onclick="{cancel}"></i>
    </div>

    <script>
        this.diffValue = diffValue;
        this.previousTime = 0;
        this.currentTime = 0;
        this.nextTime = 0;

        this.beginDateTime = null;
        this.endDateTime = null;

        let refresh = () => {
            if (!this.opts.activity) {
                return;
            }

            this.activity = this.opts.activity;

            this.beginDateTime = this.activity.beginDateTime.clone();
            this.endDateTime = this.activity.endDateTime.clone();

            this.previousBeginDateTime = this.activity.previous && this.activity.previous.beginDateTime.clone();
            this.previousEndDateTime = this.activity.previous && this.activity.previous.endDateTime.clone();

            this.nextBeginDateTime = this.activity.next && this.activity.next.beginDateTime.clone();
            this.nextEndDateTime = this.activity.next && this.activity.next.endDateTime.clone();

            this.previousTime = this.activity.previous && this.previousEndDateTime.diff(this.previousBeginDateTime) || 0;
            this.currentTime = this.endDateTime.diff(this.beginDateTime);
            this.nextTime = this.activity.next && this.nextEndDateTime.diff(this.nextBeginDateTime) || 0;

            this.totalTime = this.previousTime + this.currentTime + this.nextTime;

            this.previousStep = this.previousTime / 139;
            this.currentStep = this.currentTime / 139;
            this.nextStep = this.nextTime / 139;

            this.refs.previous.style.width = "139px";
            this.refs.previousHole.style.width = "0";
            this.refs.current.style.width = "139px";
            this.refs.nextHole.style.width = "0";
            this.refs.next.style.width = "139px";

            this.update(true);
        }

        this.on("update", (innerUpdate) => {
            // Activity changed ?
            if (this.opts.activity && !innerUpdate) {
                refresh();
            }
        });

        this.cancel = refresh;

        save() {
            let previousWidth = parseFloat(this.refs.previous.style.width);
            let previousHoleWidth = parseFloat(this.refs.previousHole.style.width);
            let currentWidth = parseFloat(this.refs.current.style.width);
            let nextWidth = parseFloat(this.refs.next.style.width);
            let nextHoleWidth = parseFloat(this.refs.nextHole.style.width);

            let previousTime = getDurations(previousWidth, [this.previousStep, this.currentStep, this.nextStep]);
            let nextTime = getDurations(nextWidth, [this.nextStep, this.currentStep, this.previousStep]);
            let previousWithHole = getDurations(previousWidth + previousHoleWidth, [this.previousStep, this.currentStep, this.nextStep]);
            let nextWithHole = getDurations(nextWidth + nextHoleWidth, [this.nextStep, this.currentStep, this.previousStep]);

            timelineService.modifyDates(
                this.activity.previous,
                this.activity,
                this.activity.next,
                this.activity.previous && this.previousBeginDateTime,
                this.activity.previous && this.previousBeginDateTime.clone().add(previousTime),
                this.activity.previous && this.previousBeginDateTime.clone().add(previousWithHole)
                        || this.beginDateTime.clone().add(previousWithHole),
                this.activity.next && this.nextEndDateTime.clone().subtract(nextWithHole)
                        || this.endDateTime.clone().subtract(nextWithHole),
                this.activity.next && this.nextEndDateTime.clone().subtract(nextTime),
                this.activity.next && this.nextEndDateTime
            )
            .then(() => {
                emitter.trigger("message-info", "Updated.");
            }, (err) => {
                emitter.trigger("message-error", "Error during updating.", err);
            });
        }

        let getDurations = (width, factors) => {
            let result = 0;
            for (let i = 0; i * 139 < width; i++) {
                let v = i * 139;
                result += factors[i] * (v + 139 < width ? 139 : width - v);
            }
            return result;
        }

        let getWidth = (date) => {
            let result = 0;
            let {beginDateTime, endDateTime} = this.activity;

            let durationBegin = date.diff(beginDateTime);
            if (durationBegin < 0) {
                result += 139 + durationBegin / this.previousStep;
                return result;
            }

            result += 139;

            let durationEnd = date.diff(endDateTime);
            if (durationEnd < 0) {
                result += durationBegin / this.currentStep;
                return result;
            }

            result += 139 + durationEnd / this.nextStep;

            return result;
        }

        changeBeginDate() {
            let beginValue = this.refs.beginDate.getValue();

            // Min/Max
            // mindate="{+previousBeginDateTime}" maxdate="{+endDateTime}"
            if (this.endDateTime && +beginValue > +this.endDateTime) {
                beginValue = this.endDateTime;
            }
            if (!this.endDateTime && +beginValue > +this.activity.endDateTime) {
                beginValue = this.activity.endDateTime;
            }
            if (this.previousBeginDateTime && +beginValue < +this.previousBeginDateTime) {
                beginValue = this.previousBeginDateTime;
            }
            if (!this.previousBeginDateTime && +beginValue < +this.activity.beginDateTime) {
                beginValue = this.activity.beginDateTime;
            }

            if (beginValue && +this.beginDateTime !== +beginValue) {
                this.beginDateTime = moment(+beginValue);
                this.previousTime = this.activity.previous && this.beginDateTime.diff(this.previousBeginDateTime) || 0;

                let widthBegin = getWidth(this.beginDateTime);
                let widthEnd = getWidth(this.endDateTime);
                let width = widthEnd - (widthBegin + parseFloat(this.refs.previousHole.style.width, 10));

                this.refs.previous.style.width = widthBegin + "px";
                this.refs.current.style.width =  width + "px";

                this.currentTime = this.endDateTime.diff(this.beginDateTime);
                this.update(true);

            } else {
                this.beginDateTime = moment(+beginValue);
                this.update(true);
            }
        }

        mergeLeft(e) {
            let beginValue = this.previousBeginDateTime;
            if (!beginValue) {
                beginValue = this.activity.beginDateTime;
            }
            this.refs.beginDate.setValue(+beginValue);
            this.changeBeginDate();
            e.preventUpdate = true;
        }

        changeEndDate() {
            let endValue = this.refs.endDate.getValue(); // + hole

            let nextHoleWidth = parseFloat(this.refs.nextHole.style.width);
            let hole = getDurations(nextHoleWidth, [this.nextStep, this.currentStep, this.previousStep]);

            // Min/Max
            // mindate="{+beginDateTime}" maxdate="{+nextEndDateTime}"/>
            if (this.nextEndDateTime && +endValue + hole > +this.nextEndDateTime) {
                endValue = +this.nextEndDateTime - hole;
            }
            if (!this.nextEndDateTime && +endValue + hole > +this.activity.endDateTime) {
                endValue = +this.activity.endDateTime - hole;
            }
            if (this.beginDateTime && +endValue < +this.beginDateTime) {
                endValue = this.beginDateTime;
            }
            if (!this.beginDateTime && +endValue < +this.activity.beginDateTime) {
                endValue = this.activity.beginDateTime;
            }

            if (endValue && +this.endDateTime !== +endValue) {
                this.endDateTime = moment(+endValue);
                this.nextTime = this.activity.next && this.nextEndDateTime.diff(this.endDateTime) || 0;

                let widthBegin = getWidth(this.beginDateTime);
                let widthEnd = getWidth(this.endDateTime);
                let width = widthEnd - widthBegin;

                this.refs.current.style.width = width + "px";
                this.refs.next.style.width = (139 * 2 - width - parseFloat(this.refs.nextHole.style.width, 10)) + "px";

                this.currentTime = this.endDateTime.diff(this.beginDateTime);
                this.update(true);

            } else {
                this.endDateTime = moment(+endValue);
                this.update(true);
            }
        }

        mergeRight(e) {
            let endValue = this.nextEndDateTime;
            if (!endValue) {
                endValue = this.activity.endDateTime;
            }
            this.refs.endDate.setValue(+endValue);
            this.changeEndDate();
            e.preventUpdate = true;
        }

        updateTimes() {
            let previousWidth = parseFloat(this.refs.previous.style.width);
            let previousHoleWidth = parseFloat(this.refs.previousHole.style.width);
            let currentWidth = parseFloat(this.refs.current.style.width);
            let nextWidth = parseFloat(this.refs.next.style.width);
            let nextHoleWidth = parseFloat(this.refs.nextHole.style.width);

            this.previousTime = getDurations(previousWidth, [this.previousStep, this.currentStep, this.nextStep]);
            this.nextTime = getDurations(nextWidth, [this.nextStep, this.currentStep, this.previousStep]);

            let previousWithHole = getDurations(previousWidth + previousHoleWidth, [this.previousStep, this.currentStep, this.nextStep]);
            let nextWithHole = getDurations(nextWidth + nextHoleWidth, [this.nextStep, this.currentStep, this.previousStep]);

            this.currentTime = this.totalTime - previousWithHole - nextWithHole;

            this.beginDateTime = this.activity.previous && this.previousBeginDateTime.clone().add(previousWithHole)
                || this.beginDateTime.clone();

            this.endDateTime = this.activity.next && this.nextEndDateTime.clone().subtract(nextWithHole)
                || this.endDateTime.clone();

            this.update(true);
        }

        let resize = (e) => {
            if (this.resizeClientX) {
                let diffX = this.resizeClientX - e.clientX;
                let previousSiblingWidth = parseFloat(this.previousSibling.style.width);
                let nextSiblingWidth = parseFloat(this.nextSibling.style.width);

                if (previousSiblingWidth - diffX < 0) {
                    diffX = previousSiblingWidth;
                }

                if (nextSiblingWidth + diffX < 0) {
                    diffX = nextSiblingWidth;
                }

                previousSiblingWidth -= diffX;
                nextSiblingWidth += diffX;

                this.previousSibling.style.width = previousSiblingWidth + "px";
                this.nextSibling.style.width = nextSiblingWidth + "px";

                this.updateTimes();
            }
            this.resizeClientX = e.clientX;
            e.preventUpdate = true;
        }

        let stopResize = (e) => {
            document.body.classList.remove("disabled-select-ew");
            document.body.classList.remove("disabled-select-e");
            document.body.classList.remove("disabled-select-w");

            window.removeEventListener("mousemove", resize, false);
            window.removeEventListener("mouseup", stopResize, false);
            e.preventUpdate = true;
        }

        startResize(e) {
            this.resizeClientX = 0;
            window.addEventListener("mousemove", resize, false);
            window.addEventListener("mouseup", stopResize, false);
            e.preventUpdate = true;
        }

        startResizeActivity(e) {
            this.previousSibling = e.target.parentElement.previousElementSibling;
            this.nextSibling = e.target.parentElement.nextElementSibling;
            document.body.classList.add("disabled-select-ew");

            this.startResize(e);
        }

        startResizeLeftHole(e) {
            this.previousSibling = e.target.parentElement;
            this.nextSibling = e.target.parentElement.nextElementSibling;
            document.body.classList.add("disabled-select-e");

            this.startResize(e);
        }

        startResizeRightHole(e) {
            this.previousSibling = e.target.parentElement.previousElementSibling;
            this.nextSibling = e.target.parentElement;
            document.body.classList.add("disabled-select-w");

            this.startResize(e);
        }
    </script>

    <style>
        .modifier {
            display: block;
            position: relative;
            padding: 20px 20px;
        }

        .background-activities {
            display: flex;
            flex-direction: row;
            left: 50%;
            margin-left: -209px;
            position: relative;
        }

        .activity {
            height: 84px;
            width: 139px;
        }

        .activity-current {
            color: white;
            background-color: #1ea7e6;
            border-top: 2px solid #d1dbe1;
            border-bottom: 2px solid #d1dbe1;
        }

        .activity-previous, .activity-next {
            background-color: #eee;
            border: 2px solid #d1dbe1;
        }

        .sliders {
            position: absolute;
            left: 50%;
            margin-left: -209px;
            top: 20px;
            display: flex;
            flex-direction: row;
            overflow: visible;
        }

        .slider {
            height: 84px;
            overflow: visible;
            position: relative;
        }

        .slider-opaque {
            background-color:#eee;
            opacity: 0.5;
        }

        .slider-current {
            background-color:#1ea7e6;
        }

        .slider-separator {
            z-index: 2;
            background: repeating-linear-gradient(45deg, transparent, transparent 10px, #f6f8f9 10px, #f6f8f9 20px);
        }

        .slider-end {
            z-index: 1;
        }

        .fa-caret-down {
            position: absolute;
            top: -19px;
            left: -9px;
            cursor: ew-resize;
        }

        .fa-caret-up-left {
            position: absolute;
            bottom: -19px;
            right: -9px;
            cursor: e-resize;
        }

        .fa-caret-up-right {
            position: absolute;
            bottom: -19px;
            left: -9px;
            cursor: w-resize;
        }

        .times {
            z-index: 3;
            position: absolute;
            top: 20px;
            left: 10px;
            display: flex;
            flex-direction: row;
            left: 50%;
            margin-left: -209px;
        }

        .time {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;

            height: 84px;
            width: 139px;
            padding: 10px;
            cursor: pointer;
        }

        .time--current {
            color: white;
        }

        .time > div {
            width: 100%;
            white-space: nowrap;
            text-overflow: ellipsis;
        }

        .dates {
            display: flex;
            justify-content: center;
        }

        .dates input {
            text-align: center;
            width: auto;
            margin: 0 5px;
        }

        .actions {
            text-align: right;
            cursor: pointer;
        }
    </style>

</ActivityDatesModifier>

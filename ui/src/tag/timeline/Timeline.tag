import "./TimelineInput.tag";
import "./Tree.tag";
import "./Activities.tag";
import "./Panel.tag";
import "../reports/Widgets.tag";
import route from "riot-route/lib/tag";
import socketService from "../../service/SocketService";

<Timeline>
    <TimelineInput/>
    <Widgets class="timeline-widgets"/>

    <div class="options">
        <a title="Tree" class="fa fa-2x fa-tags button-icon" onclick="{toggleTree}"></a>
        <a title="Update tags" class="fa fa-2x fa-gear button-icon" onclick="{toggleUpdateTags}"></a>
        <a title="Timebundle" class="fa fa-2x fa-clock-o button-icon" onclick="{toggleTimebundle}"></a>
    </div>

    <div class="split">
        <Tree if="{displayTree}"/>
        <Activities/>
    </div>
    <Panel ref="panel"/>

    <div if="{locked}" class="lock-background">
        <div class="lock-message">
            <div class="fa fa-lock"></div>
            <div>The application is already open in another place.</div>
            <div>Refresh the page to unlock the session.</div>
        </div>
    </div>

    <script>
        this.displayTree = false;
        this.locked = socketService.isLocked();

        socketService.on("lock", () => {
            this.locked = true;
            this.update();
        });

        toggleTree() {
            this.displayTree = !this.displayTree;
        }

        toggleUpdateTags() {
            route("updatetags");
        }

        toggleTimebundle() {
            route("timebundle");
        }
    </script>

    <style media="screen">
        timeline {
            position: relative;
            display: block;
        }

        .fa-tree {
            left: 8px;
            position: absolute;
            top: 140px;
        }

        .split {
            display: flex;
            clear: both;
        }

        .timeline-widgets {
            float:right;
        }

        .options {
            min-width: 130px;
            margin-left: 20px;
        }

        tree:first-child {
            margin: 0 20px 20px 40px;
            display: block;
            width: 50%;
        }

        activities {
            flex-grow: 1;
        }

        .lock-background {
            background-color: rgba(0, 0, 0, 0.5);
            width: 100%;
            height: 100%;
            position: absolute;
            left: 0;
            top: 0;
        }

        .lock-message {
            color: white;
            position: fixed;
            left: 0;
            top: 20%;
            font-size: 24px;
            text-align: center;
            padding: 0 20px;
            width: 100%;
        }

        .fa-lock {
            font-size: 200px;
        }

        @media screen and (max-width: 900px) {
            .fa-tree {
                top: 220px;
            }
        }

        @media screen and (max-width: 1280px) {
            .split {
                flex-direction: column;
            }

            tree:first-child {
                width: auto;
            }
        }

    </style>
</Timeline>

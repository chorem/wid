import treeService from "../../service/TreeService";
import timelineService from "../../service/TimelineService"
import emitter from "../../service/ServiceEmitter";
import {diffValue} from "../../utils/DateTimeUtils";
import configurationService from "../../service/ConfigurationService";

<Tree>
    <div class="node" each="{tag in Object.keys(values).sort()}" if="{tag !== '__meta__'}">
        <div class="meta">
            <div class="tag {Object.keys(parent.values[tag]).length === 1 ? 'tag--end' : 'tag--node'}" onclick="{toggle.bind(parent, tag)}">
                <virtual if="{Object.keys(parent.values[tag]).length !== 1}">
                    <i if="{parent.displaySubTree[tag]}" class="fa fa-caret-down"></i>
                    <i if="{!parent.displaySubTree[tag]}" class="fa fa-caret-right"></i>
                </virtual>
                {tag}
            </div>
            <i if="{!mode}" class="fa fa-history button-icon forceVisibility" onclick="{useActivity.bind(parent, tag)}"></i>
            <i if="{mode}" class="fa fa-play button-icon forceVisibility" onclick="{playActivity.bind(parent, tag)}"></i>
            <div if="{parent.values[tag].__meta__}" class="meta-info">
                <span class="total">{getTotalDuration(tag)}</span>
                <span class="total">{getTodayDuration(tag)}</span>
            </div>
        </div>
        <Tree values="{parent.values[tag]}" valuesToDay="{parent.valuesToDay[tag]}"
            show="{displaySubTree[tag]}" path="{(parent.opts.path ? parent.opts.path + ' ' : '') + tag}"/>
    </div>

    <script>
        this.values = [];
        this.valuesToDay = [];
        this.displaySubTree = {};
        this.mode = +configurationService.getOption("WID_OPTION_MODE", false);

        let refresh = () => {
            if (!this.opts.values) {
                emitter.trigger("message-loading");
                treeService.getFacetToday()
                .then((values) => {
                    this.valuesToDay = values;
                })
                .then(() => {
                    return treeService.getFacet();
                })
                .then((values) => {
                    this.values = values;
                    this.update(true);
                })
                .then(() => {
                    emitter.trigger("message-clear");
                })
                .catch((err) => {
                    emitter.trigger("message-error", "Server error.", err);
                })

            } else {
                this.values = this.opts.values;
                this.valuesToDay = this.opts.valuestoday || [];
            }
        }

        this.on("update", (innerUpdate) => {
            if (!innerUpdate) {
                refresh();
            }
        });
        refresh();

        timelineService.on("timerActivityChanged", refresh);

        this.on("unmount", () => {
            timelineService.off("timerActivityChanged", refresh);
        });

        toggle(tag) {
            this.displaySubTree[tag] = !this.displaySubTree[tag];
            this.update();
        }

        useActivity(tag) {
            let tags = this.opts.path ? this.opts.path + " " + tag : tag;
            timelineService.useActivity(tags)
            .catch((err) => {
                emitter.trigger("message-error", "Server errror.", err);
            });
        };

        playActivity(tag) {
            let tags = this.opts.path ? this.opts.path + " " + tag : tag;
            emitter.trigger("playActivity", tags);
        }

        getTotalDuration(value) {
            return diffValue(this.values[value].__meta__.duration);
        };

        getTodayDuration(value) {
            return this.valuesToDay[value] ? diffValue(this.valuesToDay[value].__meta__.duration) : "00:00:00";
        };
    </script>

    <style>
        .node {
            position: relative;
            display: flex;
            flex-direction: column;
            justify-content: center;
            padding: 2px 0 2px 20px;
            margin-left: 5px;
        }

        .node::after {
            content: "";
            border-top: 2px solid black;
            position: absolute;
            top: 19px;
            left: 0;
            width: 19px;
        }

        .node::before {
            content: "";
            border-left: 2px solid black;
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
        }

        .node:last-child::before {
            height: 19px;
        }

        .tag {
            background-color: #1ea7e6;
            color: white;
            border-radius: 2px 0 0 2px;
            padding: 2px;
            cursor: pointer;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        .tag--end {
            /*background-color: #74c7ed;*/
        }

        .meta {
            display: flex;
            flex-direction: row;
            align-items: center;
            background-color: #eee;
        }

        .meta-info {
            border-radius: 0 2px 2px 0;
            padding: 5px 10px 5px 10px;
            height: 26px;
            flex-grow: 1;
            display: flex;
            align-items: center;
            justify-content: flex-end;
        }

        .meta-info span:first-child {
            margin-right: 20px;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        .meta-info span:last-child {
            min-width: 64px;
        }

        .fa-history, .fa-play {
            visibility: hidden;
        }

        .fa-caret-down, .fa-caret-right {
            width: 10px;
        }

        .meta:hover .fa-history, .meta:hover .fa-play {
            visibility: visible;
        }

        @media screen and (max-width: 500px) {
            .meta-info span:first-child {
                display: none;
            }
        }
    </style>
</Tree>

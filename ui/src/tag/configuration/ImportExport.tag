import configurationService from "../../service/ConfigurationService";
import emitter from "../../service/ServiceEmitter";

<ImportExport>
    <div class="">Export to JSON:</div>
    <form class="form-inline" class="form-export">
        <i class="fa fa-download button-icon" onclick="{exportJson}"></i>
    </form>

    <div class="">Import from JSON:</div>
    <form class="form-inline" onsubmit={importJson}>
        <input ref="dataJson" required="true" type="file"/>
        <i class="fa fa-upload button-icon" onclick="{importJson}"></i>
    </form>

    <div class="">Import from Zip data folder of JTimer:</div>
    <form class="form-inline" onSubmit={importJtimer}>
        <input ref="dataJtimer" required="true" type="file"/>
        <i class="fa fa-upload button-icon" onclick="{importJtimer}"></i>
    </form>

    <script>
        exportJson() {
            configurationService.exportJson();
        }

        importJson(event) {
            event.preventDefault();

            let file = this.refs.dataJson.files[0];
            let confirmed = confirm("Be careful, you can lost data! Are you sure to import the file ?");
            if (confirmed) {
                emitter.trigger("message-loading");
                configurationService.importJson(file)
                .then(() => {
                    emitter.trigger("message-info", "Import JSON done.");
                })
                .catch((err) => {
                    emitter.trigger("message-error", "Error server.", err);
                });
            }
        }

        importJtimer(event) {
            event.preventDefault();

            let file = this.refs.dataJtimer.files[0];
            let confirmed = confirm("Be careful, you can do only once! Are you sure to import the file ?");
            if (confirmed) {
                emitter.trigger("message-loading");
                configurationService.importJtimer(file)
                .then(() => {
                    emitter.trigger("message-info", "Import JTimer done.");
                })
                .catch((err) => {
                    emitter.trigger("message-error", "Error server.", err);
                });
            }
        }
    </script>

    <style>
        form {
            text-align: center;
        }

        .form-inline {
            justify-content: center !important;
        }

        input {
            border: none;
        }
    </style>
</ImportExport>

import timebundleService from "../../service/TimebundleService";
import emitter from "../../service/ServiceEmitter";

<Timebundle>
    <div class="section">
        <a class="fa fa-2x fa-times button-icon" onclick="history.go(-1)"></a>
        Projets
    </div>
    <form class="form-inline" onsubmit="{addUrl}">
        <input ref="url" type="text" placeholder="Add url"/>
        <i class="fa fa-plus button-icon" onclick="{addUrl}"></i>
    </form>
    <form class="form-inline">
        <i class="fa fa-2x fa-retweet button-icon" title="Force sync" onclick="{sync.bind(this, true)}"></i>
        <i class="fa fa-2x fa-refresh button-icon" title="Sync" onclick="{sync.bind(this, false)}"></i>
    </form>
    <table>
        <tbody>
            <tr each="{value, index in projects}" onclick="{parent.openProject.bind(parent, index)}" class="{index === selectedIndex ? 'select': ''}">
                <td>{value.project.name}</td>
            </tr>
        </tbody>
    </table>

    <div class="section" if="{tasks}">Tasks</div>
    <table>
        <tbody>
            <tr each="{task, url in tasks}">
                <td>{task.name}</td>
                <td>{parent.taskDetails[url] && parent.taskDetails[url].query}</td>
                <td class="actions"><i class="fa fa-gear button-icon" onclick="{parent.saveQuery.bind(parent, url, task.name)}"></i></td>
            </tr>
        </tbody>
    </table>

    <script>
        this.selectedIndex = -1;
        this.projectUrls = [];
        this.projects = [];
        this.tasks = null;
        this.taskDetails = {};

        this.on("mount", () => {
            emitter.trigger("message-loading");
            timebundleService.getProjectUrls()
            .then((projectUrls) => {
                this.projectUrls = projectUrls;
                return timebundleService.getProjectDetails(projectUrls);
            })
            .then((projects) => {
                emitter.trigger("message-clear");
                this.projects = projects;
                this.update();
            })
            .catch((err) => {
                emitter.trigger("message-error", "Error server.", err);
            });
        });

        openProject(index) {
            this.selectedIndex = index;
            let project = this.projects[index];
            let projectUrl = this.projectUrls[index];

            this.tasks = project.tasks;

            emitter.trigger("message-loading");
            timebundleService.getTasks(projectUrl)
            .then((tasks) => {
                emitter.trigger("message-clear");
                this.taskDetails = tasks;
                this.update();
            })
            .catch((err) => {
                emitter.trigger("message-error", "Error server.", err);
            });
        }

        addUrl() {
            let projectUrl = this.refs.url.value;

            timebundleService.getProject(projectUrl)
            .then((project) => {
                this.projects.push(project);
                this.projectUrls.push(projectUrl);

                this.openProject(this.projects.length - 1);
                this.update();
            })
            .catch((err) => {
                emitter.trigger("message-error", "Invalid project url.", err);
            });

            this.refs.url.value = "";
        }

        saveQuery(taskUrl, taskName) {
            let task = this.taskDetails[taskUrl];

            let value = prompt(`Enter your query for the task \"${taskName}\":`, task && task.query);
            if (value !== null) {
                let promise;

                if (task) {
                    promise = timebundleService.updateQuery(task.id, value);
                } else {
                    let projectUrl = this.projectUrls[this.selectedIndex];
                    promise = timebundleService.createQuery(projectUrl, taskUrl, value)
                    .then(() => {
                        this.taskDetails[taskUrl] = {};
                    })
                    .catch((err) => {
                        emitter.trigger("message-error", "Error server.", err);
                    });
                }

                promise
                .then(() => {
                    this.taskDetails[taskUrl].query = value;
                    this.update();
                    emitter.trigger("message-info", "Query updated.");
                })
                .catch((err) => {
                    emitter.trigger("message-error", "Server error.", err);
                });
            }
        }

        sync(force) {
            emitter.trigger("message-loading");
            timebundleService.sync(force)
            .then(() => {
                emitter.trigger("message-info", "Sync done.");
            })
            .catch((err) => {
                emitter.trigger("message-error", "Server error.", err);
            });
        }
    </script>

    <style>
        timebundle {
            height: 100%;
            display: block;
            background-color: white;
        }

        .form-inline {
            display: flex;
            flex-direction: row;
            justify-content: flex-end;
            padding: 0 20px 0 20px;
        }

        table {
            width: 100%;
            padding: 0 20px;
        }

        tr.select {
            background-color: #f6f8f9;
        }

        td {
            width: 33%;
            border-bottom: 1px solid #f6f8f9;
            padding: 5px;
            cursor: pointer;
        }

        td.data {
            text-align: center;
        }

        td.actions {
            text-align: right;
        }
    </style>
</Timebundle>

import configurationService from "../../service/ConfigurationService";
import emitter from "../../service/ServiceEmitter";
import "./UpdateTagsTree.tag";
import "../common/Flatpickr.tag";
import "../timeline/Completion.tag";
import {PATTERN_QUERY_INPUT, TAB_KEY_CODE, completeQuery} from "../../utils/StringUtils";

<UpdateTags>

    <div class="split popup-container">
        <div class="popup-center query">
            <div class="popup-title">
                <a class="fa fa-2x fa-times button-icon" onclick="history.go(-1)"></a>
                Here you can modify your tags with a query.
            </div>

            <form onSubmit={searchTags} ref="form">
                <div class="row">
                    <div class="label">Query:</div>
                    <input type="text" name="query" oninput={changeTagValue} onkeydown="{enterTag}"
                        autocomplete="off" autofocus
                        placeholder="example: -notag+tag" pattern="{PATTERN_QUERY_INPUT}"/>
                </div>
                <Completion ref="completion" tagvalue="{tagvalue}" selecttag="{selecttag}" nocomment="true"/>
                <div class="row">
                    <div class="label">Begin date:</div>
                    <Flatpickr ref="beginDate" enabletime="true" defaultdate="{this.beginDate}"/>
                </div>
                <div class="row">
                    <div class="label">End date:</div>
                    <Flatpickr ref="endDate" enabletime="true" defaultdate="{this.endDate}"/>
                </div>
                <input class="button" type="submit" value="Search"/>
            </form>
        </div>

        <div if="{tagValues}" class="popup-center result">
            <div class="popup-title">Result</div>

            <form>
                <div class="row">
                    <div class="label">Replace:</div>
                    <input onkeyup={displayTagRemoved} ref="deleteTags" type="text" placeholder="delete tags"/>
                </div>
                <div class="row">
                    <div class="label">by:</div>
                    <input onkeyup={displayTagAdded} ref="addTags" type="text" placeholder="add tags"/>
                </div>
                <input class="button" onClick={updateTags} type="button" value="Replace all"/>
            </form>

            <div class="center">
                <div>Tags:</div>
                <div class="node" each="{name in addtags}">
                    <div class="meta">
                        <div class="tag tag--added">{name}</div>
                    </div>
                </div>
            </div>
            <div class="center">
                <UpdateTagsTree class="tree" data="{tagValues}" removetags="{removetags}"/>
            </div>
        </div>
    </div>

    <script>
        this.PATTERN_QUERY_INPUT = PATTERN_QUERY_INPUT;
        this.tagValues = "";
        this.removetags = new Set();
        this.addtags = [];
        this.tagvalue = "";
        this.beginDate = "";
        this.endDate = "";

        searchTags(event) {
            event.preventDefault();
            let {form, beginDate, endDate} = this.refs;
            this.beginDate = beginDate.getValue();
            this.endDate = endDate.getValue();

            emitter.trigger("message-loading");
            configurationService.searchTags(form.query.value, this.beginDate, this.endDate)
            .then((tagValues) => {
                emitter.trigger("message-clear");
                this.tagValues = tagValues;
                this.update();
            });
        }

        updateTags() {
            let confirmed = confirm("Are you sure to do replace all ?");
            if (confirmed) {
                let {form, beginDate, endDate, deleteTags, addTags} = this.refs;

                emitter.trigger("message-loading");
                configurationService.updateTags(form.query.value, beginDate.getValue(), endDate.getValue(), Array.from(this.removetags), this.addtags)
                .then(() => {
                    emitter.trigger("message-info", "Updated.");

                    deleteTags.value = "";
                    addTags.value = "";

                    this.tagValues = [];
                    this.removetags = new Set();
                    this.addtags = [];

                    this.update();
                })
                .catch((err) => {
                    emitter.trigger("message-error", "Error server.", err);
                });
            }
        }

        displayTagRemoved(event) {
            let target = event.target;
            let array = target.value.trim().split(" ");
            this.removetags = new Set(array);
        }

        displayTagAdded(event) {
            let target = event.target;
            let value = target.value.trim();
            if (value) {
                this.addtags = target.value.trim().split(" ");
            } else {
                this.addtags = [];
            }
        }

        enterTag(event) {
            event.stopPropagation();

            if (event.keyCode === TAB_KEY_CODE) {
                event.preventDefault();
                this.selecttag();
            }
        };

        changeTagValue() {
            let query = this.refs.form.query.value;
            let subqueries = query.split(" ");
            this.tagvalue = subqueries.pop().replace(/\+|-/g, " ");
        };

        selecttag(tag) {
            tag = tag || this.refs.completion.getFirstTag();
            let replace = this.refs.completion.isReplace();

            let query = this.refs.form.query;
            let tagValue = query.value;

            query.value = completeQuery(tagValue, tag, replace);

            let length = query.value.length;
            query.blur();
            query.setSelectionRange(length, length);
            query.focus();

            this.changeTagValue();
        };
    </script>

    <style>
        updatetags {
            height: 100%;
            display: block;
        }

        .split {
            display: flex;
            height: 100%;
            flex-direction: column;
            flex-wrap: wrap;
            align-items: center;
            justify-content: flex-start;
        }

        .query {
            width: 50%;
            min-width: 450px;
        }

        completion {
            display: flex;
            margin-bottom: 15px;
        }

        input[name=query] {
            margin-bottom: 0;
        }

        .result {
            width: 50%;
            min-width: 450px;
        }

        form {
            padding: 0 20px;
            text-align: center;
        }

        .center {
            display: block;
            margin: 0 20px;
        }

        .tree {
            display: block;
            padding: 0 0 5px 25px;
        }

        .row {
            display: flex;
        }
        .row:not(:first-child) {
            margin: 10px 0;
        }

        .label {
            width: 50%;
            text-align: left;
        }

        input {
            width: 100%;
        }

        flatpickr {
            width: 100%;
        }

        .tag {
            background-color: #1ea7e6;
            color: white;
            border-radius: 2px 0 0 2px;
            padding: 5px;
        }

        .tag--added {
            color: blue;
            text-decoration: underline;
        }

        .tag--removed {
            color: red;
            text-decoration: line-through;
        }

        .node {
            position: relative;
            display: flex;
            flex-direction: column;
            justify-content: center;
            padding: 5px 0 0 20px;
            margin-left: 5px;
        }

        .node::after {
            content: "";
            border-top: 2px solid #f6f8f9;
            position: absolute;
            top: 19px;
            left: 0;
            width: 19px;
        }

        .node::before {
            content: "";
            border-left: 2px solid #f6f8f9;
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
        }

        .node:last-child::before {
            height: 19px;
        }

        .meta {
            display: flex;
            flex-direction: row;
            align-items: center;
        }

        .meta-info {
            border-radius: 0 2px 2px 0;
            padding: 5px 10px 5px 10px;
            background-color: #eee;
            height: 32px;
        }
    </style>
</UpdateTags>

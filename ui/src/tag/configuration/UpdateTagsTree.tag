import {diffValue} from "../../utils/DateTimeUtils";
import {isEqualsArrays} from "../../utils/StringUtils";

<UpdateTagsTree>

    <div class="node" each="{value, name in opts.data}" if="{name !== '__meta__'}">
        <div class="meta">
            <div class="tag {parent.opts.removetags.has(name) ? 'tag--removed' : ''}">{name}</div>
            <div if="{value.__meta__}" class="meta-info">
                {getInfo(value)}
            </div>
        </div>
        <UpdateTagsTree if="{value.__meta__}" data={value} removetags="{parent.opts.removetags}"/>
    </div>

    <script>
        getInfo(value) {
            let info = [];
            info.push(this.getCountValue(value));
            info.push(this.getTimeValue(value));
            return info.join(" - ");
        }

        getCountValue(value) {
            return "(" + value.__meta__.count + ")";
        }

        getTimeValue(value) {
            return diffValue(value.__meta__.duration);
        }
    </script>
</UpdateTagsTree>

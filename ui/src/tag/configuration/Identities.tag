import configurationService from "../../service/ConfigurationService";
import emitter from "../../service/ServiceEmitter";

<Identities>
    <form class="form-inline" onsubmit="{addIdentity}">
        <input ref="identifier" type="text" placeholder="Add email"/>
        <i class="fa fa-plus button-icon" onclick="{addIdentity}"></i>
    </form>
    <table>
        <tbody>
            <tr each="{identity, index in identities}">
                <td>{identity.identifier}</td>
                <td class="data"><i class="fa {identity.valid ? 'fa-check' : 'fa-times'}"></i></td>
                <td class="actions"><i class="fa fa-trash button-icon" onclick="{parent.removeIdentity.bind(parent, index)}"></i></td>
            </tr>
        </tbody>
    </table>

    <script>
        this.identities = [];

        refresh() {
            configurationService.getIdentities()
            .then((identities) => {
                this.identities = identities;
                this.update();
            });
        };
        this.refresh();

        addIdentity(event) {
            event.preventDefault();

            configurationService.addIdentity(this.refs.identifier.value)
            .then((data) => {
                emitter.trigger("message-info", "Email added.");
                this.identities.push(data);
                this.update();
            })
            .catch((err) => {
                emitter.trigger("message-error", "Error server.", err);
            });

            this.refs.identifier.value = "";
        };

        removeIdentity(index) {
            let identity = this.identities[index];
            let confirmed = confirm(`Remove identity ${identity.identifier} ?`);
            if (confirmed) {
                configurationService.removeIdentity(identity.id)
                .then(() => {
                    emitter.trigger("message-info", "Email removed.");
                    this.identities.splice(index, 1);
                    this.update();
                })
                .catch((err) => {
                    emitter.trigger("message-error", "Error server.", err);
                });
            }
        }
    </script>
</Identities>

import configurationService from "../../service/ConfigurationService";
import emitter from "../../service/ServiceEmitter";
import "./Account.tag";
import "./Identities.tag";
import "./Applications.tag";
import "./ImportExport.tag";

<Configuration>

    <div class="section">Account</div>
    <div class="container">
        <Account/>
    </div>

    <div class="section">Identities</div>
    <div class="container">
        <Identities ref="identities"/>
    </div>

    <div class="section">Applications</div>
    <div class="container">
        <Applications/>
    </div>
    <div class="section">Import/Export</div>
    <div class="container">
        <ImportExport/>
    </div>

    <script>
        this.token = null;
        this.on("route", (token) => {
            if (token) {
                configurationService.validIdentity(token)
                .then(() => {
                    emitter.trigger("message-info", "Identity valided.");
                    this.refs.identities.refresh();
                })
                .catch((err) => {
                    emitter.trigger("message-error", "Error identity token.", err);
                });
            }
        });
    </script>

    <style>
        configuration {
            display: block;
            padding-bottom: 20px;
            background-color: white;
        }

        .container {
            padding: 0 20px;
        }

        table {
            width: 100%;
        }

        td {
            width: 33%;
            border-bottom: 1px solid #f6f8f9;
            padding: 5px;
        }

        td.data {
            text-align: center;
        }

        td.actions {
            text-align: right;
        }

        .form-inline {
            display: flex;
            flex-direction: row;
            justify-content: flex-end;
            padding-bottom: 20px;
        }
    </style>
</Configuration>

import configurationService from "../../service/ConfigurationService";
import userService from "../../service/UserService";
import emitter from "../../service/ServiceEmitter";
import "../common/Flatpickr.tag";

<Account>
    <div class="">Report default display:</div>
    <form class="form-report">
        <div class="displays">
            <input ref="displayCount" type="checkbox" onchange="{changeDisplayCount}" checked="{displayCount}"/> Count
            <input ref="displayTime" type="checkbox" onchange="{changeDisplayTime}" checked="{displayTime}"/> Time
            <input ref="displayPercentage" type="checkbox" onchange="{changeDisplayPercentage}" checked="{displayPercentage}"/> Percentage
            <input ref="displayNote" type="checkbox" onchange="{changeDisplayNote}" checked="{displayNote}"/> Note
        </div>
    </form>

    <div class="">Mode play/pause:</div>
    <form class="form-mode">
        <div class="">
            <input ref="mode" type="checkbox" onchange="{changeMode}" checked="{mode}"/>
        </div>
    </form>

    <div class="">Change the password:</div>
    <form class="form-password" onsubmit="{changePassword}">
        <div class="">
            <input name="previousPassword" type="password" placeholder="Enter your previous password" required/>
        </div>
        <div class="">
            <input name="newPassword" type="password" placeholder="Enter your new password" required/>
        </div>
        <div class="">
            <input name="confirmationPassword" type="password" placeholder="Confirm the new password" required/>
        </div>
        <div class="">
            <input class="button" type="submit" value="Save"/>
        </div>
    </form>

    <div class="">Tags priorities for tree:</div>
    <form class="form-inline" onsubmit="{addPriorityTag}">
        <input ref="tagPriority" type="text" placeholder="Enter your tag" required/>
        <i class="fa fa-plus button-icon" onclick="{addPriorityTag}"></i>
    </form>
    <ul ref="priorities" class="priorities" onmouseleave="{onMouseOutPriorities}">
        <li each="{item in priorities}"
            ondragstart="{prioritiesDragStart}" ondragenter="{prioritiesDragEnter}"
            ondrop="{prioritiesDrop}" ondragover="{prioritiesDragOver}"
            draggable="true">
            {item}
        </li>
    </ul>
    <div class="trash">
        <i class="fa fa-trash fa-2x button-icon" ondrop="{onMouseDeletePriority}" ondragover="{prioritiesDragOver}"></i>
    </div>

    <div class="">Use tags for completion and tree from:</div>
    <form class="form-tree">
        <div class="">
            <select onchange="{changeTreeBack}">
                <option value="1m" selected="{treeBack == '1m'}">-1 month</option>
                <option value="3m" selected="{treeBack == '3m'}">-3 months</option>
                <option value="6m" selected="{treeBack == '6m'}">-6 months</option>
                <option value="1y" selected="{treeBack == '1y'}">-1 year</option>
                <option value="3y" selected="{treeBack == '3y'}">-3 years</option>
            </select>
        </div>
    </form>

    <script>
        this.mode = +configurationService.getOption("WID_OPTION_MODE", false);
        this.displayCount = +configurationService.getOption("WID_OPTION_DISPLAY_COUNT", true);
        this.displayTime = +configurationService.getOption("WID_OPTION_DISPLAY_TIME", true);
        this.displayPercentage = +configurationService.getOption("WID_OPTION_DISPLAY_PERCENTAGE", true);
        this.displayNote = +configurationService.getOption("WID_OPTION_DISPLAY_NOTE", true);
        this.treeBack = configurationService.getOption("WID_OPTION_TREE_BACK", "1y");

        changePassword(event) {
            event.preventDefault();
            let form = event.target;

            emitter.trigger("message-clear");

            userService.changePassword(form.previousPassword.value, form.newPassword.value, form.confirmationPassword.value)
            .then(() => {
                emitter.trigger("message-info", "New password saved.");
            },
            (err) => {
                emitter.trigger("message-error", "Incorrect previous password or confirmation.", err);
            });
        }

        changeMode() {
            this.mode = !this.mode;
            configurationService.changeOption("WID_OPTION_MODE", +this.mode);
            configurationService.changeOption("WID_OPTION_MODE_TAGS", "");
            emitter.trigger("message-info", "Updated.");
        }

        changeDisplayCount() {
            this.displayCount = !this.displayCount;
            configurationService.changeOption("WID_OPTION_DISPLAY_COUNT", +this.displayCount);
            emitter.trigger("message-info", "Updated.");
        }

        changeDisplayTime() {
            this.displayTime = !this.displayTime;
            configurationService.changeOption("WID_OPTION_DISPLAY_TIME", +this.displayTime);
            emitter.trigger("message-info", "Updated.");
        }

        changeDisplayPercentage() {
            this.displayPercentage = !this.displayPercentage;
            configurationService.changeOption("WID_OPTION_DISPLAY_PERCENTAGE", +this.displayPercentage);
            emitter.trigger("message-info", "Updated.");
        }

        changeDisplayNote() {
            this.displayNote = !this.displayNote;
            configurationService.changeOption("WID_OPTION_DISPLAY_NOTE", +this.displayNote);
            emitter.trigger("message-info", "Updated.");
        }

        changeTreeBack(event) {
            let select = event.target;
            this.treeBack = select.options[select.selectedIndex].value;
            configurationService.changeOption("WID_OPTION_TREE_BACK", this.treeBack);
            emitter.trigger("message-info", "Updated.");
        }

        this.priorities = [];
        userService.getPriorities()
        .then((priorities) => {
            this.priorities = priorities;
            this.update();
        });

        addPriorityTag(e) {
            e.preventDefault();
            e.stopPropagation();

            let priority = this.refs.tagPriority.value;
            if (!priority) {
                return;
            }
            this.priorities.push(priority);
            this.refs.tagPriority.value = "";
            this.update();
            this.savePrioritiesTags();
        }

        savePrioritiesTags() {
            let tags = Array.from(this.refs.priorities.querySelectorAll("li"))
                        .map((li) => {
                            return li.textContent.trim();
                        });
            userService.setPriorities(tags)
            .then(() => {
                emitter.trigger("message-info", "Updated.");
                this.update();
            }, (err) => {
                emitter.trigger("message-error", "Error server.", err);
            });
        }

        prioritiesDragStart(e) {
            this.datatTarget = e.target;
            this.dataTransfer = e.target.textContent.trim();

            e.dataTransfer.effectAllowed = "all";
            e.dataTransfer.setData('Text', this.dataTransfer);
        }

        prioritiesDragEnter(e) {
            let target = e.target;
            this.datatTarget.textContent = target.textContent.trim();
            target.textContent = this.dataTransfer;
            this.datatTarget = target;
        }

        prioritiesDragOver(e) {
            e.preventDefault();
        }

        prioritiesDrop(e, target) {
            target = target || e.target;

            this.datatTarget.textContent = target.textContent.trim();
            target.textContent = this.dataTransfer;
            this.datatTarget = null;
            this.dataTransfer = null;

            this.savePrioritiesTags();

            e.preventDefault();
        }

        onMouseOutPriorities(e) {
            if (this.datatTarget) {
                this.prioritiesDrop(e, this.datatTarget);
            }
        }

        onMouseDeletePriority(e) {
            let index = this.priorities.indexOf(this.dataTransfer);
            if (index !== -1) {
                this.priorities.splice(index, 1);
            }
            this.savePrioritiesTags();

            e.stopPropagation();
            e.preventDefault();
        }
    </script>

    <style>
        .form-password > div, .form-mode > div, .form-report > div, .form-tree > div {
            text-align: center;
            padding: 5px 0;
        }

        .priorities {
            cursor: pointer;
            max-height: 200px;
            overflow-y: scroll;
            list-style-type: none;
        }

        li {
            text-align: center;
            padding: 5px;
        }

        .trash {
            text-align: right;
        }

        @media screen and (max-width: 500px) {
            .displays {
                display: flex;
                flex-direction: column;
                align-items: center;
            }

            input {
                width: 100%;
            }
        }
    </style>
</Account>

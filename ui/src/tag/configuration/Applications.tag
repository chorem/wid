import configurationService from "../../service/ConfigurationService";
import emitter from "../../service/ServiceEmitter";
import clipboard from "clipboard-js";

<Applications>
    <div class="">
        Enter the url with the key into the webhooks for GitHub or GitLab or create own application, to get event in your timeline.
    </div>
    <form class="form-inline" onsubmit="{generateKey}">
        <input ref="application" type="text" placeholder="Generate key for application name"/>
        <i class="fa fa-plus button-icon" onclick="{generateKey}"></i>
    </form>
    <table>
        <tbody>
            <tr each="{application, index in applications}">
                <td>{application.name}</td>
                <td class="data">{application.key}</td>
                <td class="actions">
                    <i class="fa fa-link button-icon" onclick="{parent.copyLink.bind(parent, application.key)}"></i>
                    <i class="fa fa-trash button-icon" onclick="{parent.removeKey.bind(parent, index)}"></i>
                </td>
            </tr>
        </tbody>
    </table>

    <script>
        this.applications = [];

        configurationService.getKeys()
        .then((applications) => {
            this.applications = applications;
            this.update();
        });

        generateKey(event) {
            event.preventDefault();

            configurationService.generateKey(this.refs.application.value)
            .then((data) => {
                emitter.trigger("message-info", "Application added.");
                this.applications.push(data);
                this.update();
            })
            .catch((err) => {
                emitter.trigger("message-error", "Error server.", err);
            });

            this.refs.application.value = "";
        };

        removeKey(index) {
            let application = this.applications[index];
            let confirmed = confirm(`Remove key ${application.name} ?`);
            if (confirmed) {
                configurationService.removeKey(application.id)
                .then(() => {
                    emitter.trigger("message-info", "Application removed.");
                    this.applications.splice(index, 1);
                    this.update();
                })
                .catch((err) => {
                    emitter.trigger("message-error", "Error server.", err);
                });
            }
        }

        copyLink(key) {
            clipboard.copy(configurationService.serverLocation + "/webhook/" + key);
            emitter.trigger("message-info", "Url copied into clipboard.");
        }
    </script>

    <style>
        .data {
            word-break: break-all;
        }
    </style>
</Applications>

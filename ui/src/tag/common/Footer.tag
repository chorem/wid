<Footer>
    <div class="more">
        More informations
    </div>
    <div class="links">
        <a href="http://gitlab.nuiton.org/chorem/wid/">GitLab</a>
        |
        <a href="http://www.codelutin.com">Code Lutin</a>
        |
        <a href="https://www.gnu.org/licenses/agpl-3.0.txt">AGPL-3.0</a>
        |
        <a href="cgu">CGU</a>
        |
        <a href="legals">Mentions Légales</a>
        |
        <a href="mailto:wid-users@list.chorem.org">Contact</a>
    </div>

    <style>
        footer {
            padding-bottom: 30px;
            text-align: center;

            font-size: 11px;
            font-family: Montserrat;
            text-transform: uppercase;
            letter-spacing: 2px;

            background-color: #35434f;
            color: #fff;
            box-shadow: 0px 0px 20px 1px rgba(0, 0, 0, 0.1);
        }

        .more {
            padding-top: 30px;
            padding-bottom: 20px;
            font-size: 22px;
        }

        .links {
            padding: 0 5px;
            line-height: 18px;
        }
    </style>
</Footer>

import emitter from "../../service/ServiceEmitter"

<Message>
    <div if={message} ref="msg" class="message {messageClass}">
        <span class="message-text">{message}</span>
        <!-- <i class="fa fa-times button-icon" onclick="{close}"></i> -->
    </div>

    <script>
        this.message = "";

        let timerClose = () => {
            let messageType = this.messageType;
            this.timer = setTimeout(() => {
                if (messageType === this.messageType) {
                    this.message = "";
                    this.update();
                }
            }, 3 * 1000);
        };

        close() {
            clearTimeout(this.timer);
            this.message = "";
            this.update();
        };

        const onMessageError = (message, err) => {
            emitter.trigger("message-clear");
            clearTimeout(this.timer);

            err && console.log(err);
            this.messageType = "error";
            this.message = message;
            this.messageClass = "message--open message--error"
            this.update();
            timerClose();
        };

        const onMessageInfo = (message) => {
            emitter.trigger("message-clear");
            clearTimeout(this.timer);

            this.messageType = "info";
            this.message = message;
            this.messageClass = "message--open message--info"
            this.update();
            timerClose();
        };

        const onMessageLoading = () => {
            clearTimeout(this.timer);
            this.messageType = "loading";
            this.message = "Loading ...";
            this.messageClass = "message--open message--loading"
            this.update();
            // timerClose();
        };

        const onMessageClear = () => {
            if (this.message && this.messageType === "loading") {
                this.close();
            }
        };

        emitter.on("message-error", onMessageError);
        emitter.on("message-info", onMessageInfo);
        emitter.on("message-loading", onMessageLoading);
        emitter.on("message-clear", onMessageClear);

        this.on("unmount", () => {
            emitter.off("message-error", onMessageError);
            emitter.off("message-info", onMessageInfo);
            emitter.off("message-loading", onMessageLoading);
            emitter.off("message-clear", onMessageClear);
        });
    </script>

    <style media="screen">
        @keyframes message--animate {
            from {opacity: 0;}
            to {opacity: .99;}
        }

        .message--open {
            animation-duration: .3s;
            animation-name: message--animate;
        }

        message {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;

            text-align: center;
        }

        .message {
            display: flex;
            align-items: center;

            width: 100%;
            height: 60px;

            font-size: 26px;
            border-style: solid;
            border-width: 1px;

            border-color: rgba(27,31,35,0.15);
        }

        .message--error {
            color: #86181d;
            background-color: #ffdce0;
        }

        /*.message--info {
            color: #1e87f0;
            background-color: #d8eafc;
        }*/

        .message--info, .message--loading {
            background-color: rgba(15, 27, 44, 0.66);
            color: white;
        }

        .message-text {
            flex-grow: 1;
            text-align: center;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        .fa-times {
            cursor: pointer;
            align-self: center;
            font-size: 18px;
        }

        .fa-times:hover {
            opacity: 0.9;
        }

        .fa-times:active {
            opacity: 0.8;
        }
    </style>
</Message>

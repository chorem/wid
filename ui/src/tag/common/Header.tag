import userService from "../../service/UserService";
import route from "riot-route/lib/tag";
import emitter from "../../service/ServiceEmitter"
import Timer from "../timeline/Timer.tag"

<Header>
    <div class="header">
        <div class="header--left">
            <a class="logo" href="/">
                <i class="fa-stack fa-lg">
                    <i class="fa fa-circle-o fa-stack-2x"></i>
                    <i class="fa fa-tag fa-stack-1x {loadingClass}"></i>
                </i>
            </a>
            <div class={header-title: true, header-title--timeline:opts.istimeline}>{opts.title}</div>
            <Timer if="{opts.istimeline}" class="header-timer" />
            <div class="header-menu fa fa-bars" onclick="{toggleMenu}"></div>
        </div>
        <div if="{!user}" ref="menu" class="header--right">
            <a each={item in opts.items} href="/{item.toLowerCase().replace(/ /g, '_')}" class="header-item">{item}</a>
        </div>
        <div if="{user}" ref="menu" class="header--right">
            <a each={item in opts.items} href="/{item.toLowerCase().replace(/ /g, '_')}" class="header-item">{item}</a>

            <div class="header-user">
                <div class="header-item-separator">|</div>
                <i class="fa fa-user-circle-o"></i>
                <a href="/timeline" class="header-username">{user.username}</a>

                <i class="fa fa-times button-icon" onclick="{signout}"></i>
            </div>
        </div>
    </div>
    <div class="header--separator"></div>

    <script>
        this.user = null;

        let refresh = () => {
            userService.get()
            .then((user) => {
                this.user = user;
                this.update();
            });
        };
        userService.on("userUpdated", refresh);
        refresh();

        signout() {
            userService.signOut()
            .then(() => {
                route("/sign_in");
            });
        }

        toggleMenu() {
            if (this.refs.menu.style.display !== "flex") {
                this.refs.menu.style.display = "flex";
            } else {
                this.refs.menu.style.display = null;
            }
        }

        emitter.on("message-loading", () => {
            this.loadingClass = "fa-spin";
            this.update();
        });

        emitter.on("message-clear", () => {
            this.loadingClass = "";
            this.update();
        });
    </script>

    <style>
        .header {
            display: flex;
            min-height: 100px;
            border-top: 1px solid #343c47;
            border-bottom: 1px solid #1c2430;
            background-color: #2b333e;

            color: white;
            padding: 0 20px 0 20px;
            font-size: 18px;
            align-items: center;
        }

        .header--left {
            font-size: 28px;
            display: flex;
            align-items: center;
        }

        .header--right {
            flex-grow: 1;
            display: flex;
            justify-content: flex-end;
            align-items: center;
        }

        .header-user {
            display: flex;
            align-items: center;
        }

        .header-username {
            padding-left: 10px;
            cursor: pointer;
        }

        .header-title {
            padding-left: 10px;
            cursor: default;
            flex-grow: 1;
        }

        .header-timer {
            flex-grow: 99;
            text-align: center;
            display: none;
        }

        .header-menu {
            display: none;
            cursor: pointer;
        }

        .header-item {
            padding: 0 15px;
        }

        .header--separator {
            border-top: 1px solid #a7abb0;
            border-bottom: 1px solid #e5e7e8;
        }

        .logo {
            font-size: 14px;
        }

        .header .fa-tag {
            top: 4px;
            left: 4px;
        }

        .fa-user-circle-o {
            font-size: 22px;
        }

        .fa-times {
            font-size: 14px;
            margin-left: 5px;
        }

        .fa-tag {
            transform-origin: 14px 14px;
        }

        .header-item-separator {
            margin: 0 20px 0 5px;
        }

        .button-icon {
            color: white;
        }

        @media screen and (max-width: 900px) {
            .header {
                display: block;
                min-height: 40px;
                padding: 5px 20px;
            }

            .header--right {
                display: none;
                flex-direction: column;
                align-items: flex-end;
            }

            .header-item-separator {
                display: none;
            }

            .header-menu {
                display: block;
            }

            .header-item {
                padding: 0;
            }
        }

        @media screen and (max-width: 500px) {
            .header-title--timeline {
                display: none;
            }
            .header-timer {
                display: block;
            }
        }
    </style>
</Header>

import flatpickr from "flatpickr";
import "flatpickr/dist/themes/material_blue.css"

<Flatpickr>
    <input ref="picker"/>

    <script>
        this.on("update", () => {
            if (this.flatpickr
                && +this.getValue() !== +this.opts.defaultdate) {

                this.setValue(this.opts.defaultdate);
            }
        });

        this.on("mount", () => {
            this.flatpickr = flatpickr(this.refs.picker, {
                mode: this.opts.mode || "single",
                enableTime: this.opts.enabletime,
                time_24hr: true,
                enableSeconds: true,
                defaultDate: this.opts.defaultdate,
                onChange: this.opts.onclose,
                minuteIncrement: 1
            });
        });

        this.on("umount", () => {
            this.flatpickr.destroy();
        });

        getValue() {
            return this.flatpickr.selectedDates && this.flatpickr.selectedDates[0];
        }

        setValue(date) {
            this.flatpickr.setDate(date);
        }
    </script>

    <style>
        flatpickr {
            display: block;
        }
    </style>
</Flatpickr>

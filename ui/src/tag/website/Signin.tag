import emitter from "../../service/ServiceEmitter"
import userService from "../../service/UserService";
import route from "riot-route";

<Signin>
    <div class="popup-container">
        <form class="popup-center" onSubmit="{onSignIn}">
            <div class="popup-title">Sign in with your account</div>
            <div class="popup-label">Username</div>
            <input type="text" name="username" required="true"/>
            <div class="popup-label">Password</div>
            <input type="password" name="password" required="true"/>
            <input class="button" type="submit" value="Sign in"/>
            <div class="reset" onclick="{resetPassword}">Reset your password</div>
        </form>
    </div>

    <script>
        this.on("route", () => {
            if (location.search === "?first") {
                emitter.trigger("message-info", "Now you can connect on your account.");
            }
        });

        onSignIn(event) {
            event.preventDefault();
            let form = event.target;

            emitter.trigger("message-clear");

            userService.signIn(form.username.value, form.password.value)
            .then(() => {
                route("timeline");
            },
            (err) => {
                emitter.trigger("message-error", "Incorrect username or password.", err);
            });
        }

        resetPassword() {
            let value = prompt("Email");
            if (value) {
                userService.resetPassword(value)
                .then(() => {
                    emitter.trigger("message-info", "An email with new password is sent.");
                })
                .catch((err) => {
                    emitter.trigger("message-error", "Incorrect email.", err);
                });
            }
        }
    </script>

    <style media="screen">
        .reset {
            text-align: right;
            cursor: pointer;
        }
    </style>

</Signin>

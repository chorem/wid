import emitter from "../../service/ServiceEmitter"
import userService from "../../service/UserService";
import route from "riot-route";

<Signup>
    <div class="popup-container">
        <form class="popup-center" onSubmit="{onSignUp}">
            <div class="popup-title">Sign up, it's free</div>
            <div class="popup-label">Username</div>
            <input type="text" name="username" required="true"/>
            <div class="popup-label">Email</div>
            <input type="text" name="email" required="true"/>
            <div class="popup-label">Password</div>
            <input type="password" name="password" required="true"/>
            <input type="password" name="confirmation" required="true" placeholder="Confirm password"/>
            <input class="button" type="submit" value="Sign up"/>
        </form>
    </div>

    <script>
        onSignUp(event) {
            event.preventDefault();
            let form = event.target;

            emitter.trigger("message-loading");

            userService.signUp(form.username.value,
                form.email.value,
                form.password.value,
                form.confirmation.value)
            .then(() => {
                route("sign_in?first");
            },
            (err) => {
                if (err) {
                    emitter.trigger("message-error", "Username or email already used.", err);
                } else {
                    emitter.trigger("message-error", "Password and confirmation is not equals.", err);
                }
            });
        }
    </script>

</Signup>

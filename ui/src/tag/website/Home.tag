<Home>
    <div class="title">
        <div class="legend">
            “&#8239;Wid is your best companion for tracking your time. You can follow your personal and professional time.
            The innovation holds in the tag system, you organize your day with tags and get your own reports.&#8239;”
        </div>
    </div>

    <div class="features">
        <div class="feature">
            <div class="feature-img" style="background-image: url(./img/home/glenn-carstens-peters-190592.jpg)"></div>
            <div class="feature-title">Timeline</div>
            <div class="feature-description">The timeline is your dashboard to handle your day.</div>
        </div>
        <div class="feature">
            <div class="feature-img" style="background-image: url(./img/home/carlos-muza-84523.jpg)"></div>
            <div class="feature-title">Report</div>
            <div class="feature-description">Create your own reports and follow the progression.</div>
        </div>
        <div class="feature">
            <div class="feature-img" style="background-image: url(./img/home/markus-spiske-232412.jpg)"></div>
            <div class="feature-title">Application</div>
            <div class="feature-description">Plug applications with Wid and share data.</div>
        </div>
    </div>

    <div class="applications">
        <div class="applications-subtitle">
            Compatible with
        </div>

        <div class="applications-logos">
            <a href="http://widand.chorem.com/WidAnd-debug.apk"><img src="./img/logo/android.png" title="Widand"></a>
            <a onclick="{downloadExtension}"><img src="./img/logo/chrome.png" title="Chrome extension"></a>
            <a href="http://github.com/"><img src="./img/logo/github.png" title="Github"></a>
            <a href="https://gitlab.com"><img src="./img/logo/gitlab.png" title="Gitlab"></a>
            <a href="https://taskwarrior.org/"><img src="./img/logo/taskwarrior.png" title="Taskwarrior"></a>
            <a href="https://wakatime.com/download"><img src="./img/logo/wakatime.png" title="Wakatime"></a>
            <a href="http://arbtt.nomeata.de/"><img src="./img/logo/haskell.png" title="Arbtt"></a>
        </div>
    </div>

    <script>
        downloadExtension() {
            window.location = "/wid-ext.crx";
        }
    </script>

    <style>
        .title {
            display: flex;
            align-items: flex-start;
            box-shadow: 0px 0px 20px 1px rgba(0, 0, 0, 0.2);
            border-bottom: 1px solid #e1e8eb;
            height: 600px;
            background-image: url(./img/home/markus-spiske-187777.jpg);
            background-size: cover;
            background-position: center;
        }

        .legend {
            width: 50%;
            background-color: rgba(255, 255, 255, 0.4);
            padding: 20px;
            margin: 4%;
            color: #2b333e;
            font-size: 26px;
            text-align: center;
            border-radius: 2px;
        }

        .features {
            margin: 6% 3% 6% 3%;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: space-around;
            overflow: visible;
        }

        .feature {
            overflow: visible;
            margin: 0 15px 30px 15px;
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        .feature-img {
            width: 350px;
            height: 350px;
            box-shadow: 0px 0px 20px 1px rgba(0, 0, 0, 0.2);
            background-size: cover;
            background-position: 33% center;
            border-radius: 175px;
            overflow: visible;
            padding-top: 350px;
        }

        .feature-title {
            text-align: center;
            margin-top: 15px;
            font-size: 22px;
            font-weight: bold;
        }

        .feature-description {
            text-align: center;
            font-size: 18px;
            width: 80%;
        }

        .applications {
            box-shadow: 0px 0px 20px 1px rgba(0, 0, 0, 0.1);
            background-color: rgba(0, 0, 0, 0.1);
            padding: 6% 0;
        }

        .applications-subtitle {
            font-size: 22px;
            line-height: 1.4;
            font-family: Montserrat;
            text-transform: uppercase;
            letter-spacing: 2px;
            color: #2D2E33;
            text-align: center;
            padding-bottom: 20px;
        }

        .applications-logos {
            text-align: center;
        }

        .applications-logos a {
            margin: 0 10px;
            cursor: pointer;
        }

        @media screen and (max-width: 900px) {
            .legend {
                width: 100%;
            }
        }

    </style>
</Home>

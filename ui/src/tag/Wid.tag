import "./common/Message.tag";
import "./common/Header.tag";
import "./common/Footer.tag";
import "./website/Home.tag";
import "./website/Signup.tag";
import "./website/Signin.tag";
import "./timeline/Timeline.tag";
import "./reports/Reports.tag";
import "./reports/Report.tag";
import "./reports/ReportForm.tag";
import "./reports/ReportsSelector.tag";
import "./pages/Legals.tag";
import "./pages/Cgu.tag";
import "./pages/Guide.tag";
import "./pages/Api.tag";
import "./configuration/Configuration.tag";
import "./configuration/UpdateTags.tag";
import "./configuration/Timebundle.tag";

import emitter from "../service/ServiceEmitter"
import route from "riot-route/lib/tag";

<Wid>
    <router>
        <!-- User -->
        <route path="sign_up">
            <Header title="Sign up" items={parent.parent.homeMenu}/>
            <Signup/>
            <Footer/>
            <Message/>
        </route>
        <route path="sign_in..">
            <Header title="Sign in" items={parent.parent.homeMenu}/>
            <Signin/>
            <Footer/>
            <Message/>
        </route>
        <route path="timeline">
            <Header title="Timeline" items={parent.parent.appMenu} isTimeline={true}/>
            <Timeline/>
            <Message/>
        </route>
        <route path="configuration">
            <Header title="Configuration" items={parent.parent.appMenu}/>
            <Configuration/>
            <Message/>
        </route>
        <route path="configuration/*">
            <Header title="Configuration" items={parent.parent.appMenu}/>
            <Configuration/>
            <Message/>
        </route>
        <route path="updatetags">
            <Header title="Update tags" items={parent.parent.appMenu}/>
            <UpdateTags/>
            <Message/>
        </route>
        <route path="timebundle">
            <Header title="Timebundle" items={parent.parent.appMenu}/>
            <Timebundle/>
            <Message/>
        </route>

        <!-- Static pages -->
        <route path="cgu">
            <Header title="CGU" items={parent.parent.homeMenu}/>
            <Cgu/>
            <Footer/>
            <Message/>
        </route>
        <route path="legals">
            <Header title="Mentions légales" items={parent.parent.homeMenu}/>
            <Legals/>
            <Footer/>
            <Message/>
        </route>
        <route path="guide">
            <Header title="Guide" items={parent.parent.homeMenu}/>
            <Guide/>
            <Footer/>
            <Message/>
        </route>
        <route path="api">
            <Header title="Api" items={parent.parent.homeMenu}/>
            <Api/>
            <Footer/>
            <Message/>
        </route>

        <!-- Reports -->
        <route path="/reports/new">
            <Header title="Reports" items={parent.parent.appMenu}/>
            <ReportForm/>
            <Message/>
        </route>
        <route path="/reports/edit/*">
            <Header title="Reports" items={parent.parent.appMenu}/>
            <ReportForm/>
            <Message/>
        </route>
        <route path="/reports/*/*/*">
            <Header title="Reports" items={parent.parent.appMenu}/>
            <ReportsSelector/>
            <Report/>
            <Message/>
        </route>
        <route path="/reports">
            <Header title="Reports" items={parent.parent.appMenu}/>
            <Reports/>
            <Message/>
        </route>

        <!-- Index -->
        <route path="/*">
            <Header title="WID" items={parent.parent.homeMenu}/>
            <Home/>
            <Footer/>
            <Message/>
        </route>
        <route path="">
            <Header title="WID" items={parent.parent.homeMenu}/>
            <Home/>
            <Footer/>
            <Message/>
        </route>
    </router>

    <script>
        this.homeMenu = ["Guide", "API", "Sign in", "Sign up"];
        this.appMenu = ["Timeline", "Reports", "Configuration"];

        emitter.on("unauthorized", () => {
            if (location.pathname !== "/"
                && location.pathname !== "/cgu"
                && location.pathname !== "/legals"
                && location.pathname !== "/guide"
                && location.pathname !== "/api"
                && location.pathname !== "/sign_up") {

                route("/sign_in");
            }
        });
    </script>

    <style>
        wid {
            display: flex;
            position: relative;
            min-height: 100%;
            margin: 0 5%;
            background-color: #f6f8f9;
            box-shadow: 0px 0px 20px 1px rgba(0, 0, 0, 0.1);
            overflow: visible;
        }

        router {
            width: 100%;
            overflow: visible;
        }

        @media screen and (max-width: 900px) {
            wid {
                margin: 0;
            }
        }
    </style>
</Wid>

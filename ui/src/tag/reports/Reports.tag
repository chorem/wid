import route from "riot-route/lib/tag";
import "./Report.tag";
import "./ReportsSelector.tag";
import "./ReportForm.tag";

<Reports>
    <div class="choose">Choose or create a report here:</div>
    <ReportsSelector multiple="{true}"/>

    <style media="screen">
        .choose {
            display: flex;
            margin-top: 10px;
            font-size: 24px;
            text-align: center;
            justify-content: center;
            color: #48626f;
        }

        .container {
            justify-content: center;
        }

        select {
            height: auto;
            overflow: auto;
        }
    </style>
</Reports>

import {diffValue} from "../../utils/DateTimeUtils";
import {isEqualsArrays} from "../../utils/StringUtils";

<ReportTree>

    <div class="node" each="{value, name in opts.data}" if="{name !== '__meta__'}">
        <div class="meta">
            <div class="tag">{name}</div>
            <div if="{value.__meta__}" class="meta-info">
                {getInfo(value)}
            </div>
        </div>
        <div if="{parent.opts.displayevent}" class="events">
            <div each="{event in getEvents(name)}" class="event">
                <i class="fa fa-pencil-square-o"></i> <span>{event}</span>
            </div>
        </div>
        <ReportTree if="{value.__meta__}" data={value} total="{parent.opts.total}"
            events={parent.opts.events} path="{parent.opts.path.concat(name)}"
            displaycount="{parent.opts.displaycount}"
            displaytime="{parent.opts.displaytime}"
            displaypercentage="{parent.opts.displaypercentage}"
            displayevent="{parent.opts.displayevent}"/>
    </div>

    <script>
        getInfo(value) {
            let info = [];
            this.opts.displaycount && info.push(this.getCountValue(value));
            this.opts.displaytime && info.push(this.getTimeValue(value));
            this.opts.displaypercentage && info.push(this.getPercentageValue(value));
            return info.join(" - ");
        }

        getCountValue(value) {
            return "(" + value.__meta__.count + ")";
        }

        getTimeValue(value) {
            return diffValue(value.__meta__.duration);
        }

        getPercentageValue(value) {
            let percentageValue = (value.__meta__.duration / this.opts.total) * 100;
            return percentageValue.toFixed(2) + "%";
        }

        getEvents(name) {
            let path = this.opts.path.concat(name);
            let events = this.opts.events.filter((event) => {
                let tags = event.tags.split(",");
                return isEqualsArrays(path, tags);
            })
            .map((event) => {
                return JSON.parse(event.value).data.content;
            });
            return events;
        }
    </script>

    <style>
        .events {
            color: #1e3e4e;
            font-size: 14px;
            font-weight: normal;
            padding-left: 20px;
        }

        .event {
            display: flex;
            align-items: center;
        }

        .fa {
            margin: 5px;
            min-width: 14px;
        }

        .event span {
            text-overflow: ellipsis;
            white-space: nowrap;
        }
    </style>
</ReportTree>

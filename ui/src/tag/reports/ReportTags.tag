import "./ReportTree.tag";
import {diffValue} from "../../utils/DateTimeUtils";
import {isEqualsArrays} from "../../utils/StringUtils";
import emitter from "../../service/ServiceEmitter";
import clipboard from "clipboard-js";
import configurationService from "../../service/ConfigurationService";

<ReportTags if="{opts.details}">
    <div class="displays">
        <input checked={displaycount} onChange={toggleCount} type="checkbox" value="true"/>
        <label onClick={toggleCount}>Count</label>

        <input checked={displaytime} onChange={toggleTime} type="checkbox" value="true"/>
        <label onClick={toggleTime}>Time</label>

        <input checked={displaypercentage} onChange={togglePercentage} type="checkbox" value="true"/>
        <label onClick={togglePercentage}>Percentage</label>

        <input checked={displayevent} onChange={toggleEvent} type="checkbox" value="true"/>
        <label onClick={toggleEvent}>Note</label>

        <div class="button" onClick={copy}>Copy into clipboard</div>
    </div>

    <div class="meta">
        <div class="tag">Tags</div>
        <div class="meta-info">{diffValue(opts.details.total)}</div>
    </div>

    <ReportTree data="{opts.details.facets}" total="{opts.details.total}"
        events={opts.details.notes} path="{[]}"
        displaycount={displaycount}
        displaytime={displaytime}
        displaypercentage={displaypercentage}
        displayevent={displayevent}/>

    <script>
        this.diffValue = diffValue;

        this.displaycount = +configurationService.getOption("WID_OPTION_DISPLAY_COUNT", true);
        this.displaytime = +configurationService.getOption("WID_OPTION_DISPLAY_TIME", true);
        this.displaypercentage = +configurationService.getOption("WID_OPTION_DISPLAY_PERCENTAGE", true);
        this.displayevent = +configurationService.getOption("WID_OPTION_DISPLAY_NOTE", true);

        toggleCount() {
            this.displaycount = !this.displaycount;
        }

        toggleTime() {
            this.displaytime = !this.displaytime;
        }

        togglePercentage() {
            this.displaypercentage = !this.displaypercentage;
        }

        toggleEvent() {
            this.displayevent = !this.displayevent;
        }

        exportTags(facets, tagsList = []) {
            let result = "";
            let keys = Object.keys(facets);

            let separator = "  ".repeat(tagsList.length);
            let items = keys.map((key) => {
                if (key !== "__meta__") {
                    let {tag, duration, count} = facets[key].__meta__;
                    let path = tagsList.concat(tag);

                    let percentage = (duration / this.opts.details.total) * 100;
                    percentage = percentage.toFixed(2) + "%";

                    let info = [];
                    this.displaycount && info.push("(" + count + ")");
                    this.displaytime && info.push(diffValue(duration));
                    this.displaypercentage && info.push(percentage);

                    result += separator + "- " + tag + " " + info.join(" - ") + "\n";
                    result += this.exportTags(facets[key], path);

                    let events = this.opts.details.notes.filter((event) => {
                        let tags = event.tags.split(",");
                        return isEqualsArrays(path, tags);
                    })
                    .map((event) => {
                        return separator + "    * " + JSON.parse(event.value).data.content + "\n";
                    });
                    result += events.join("");
                }
            });

            return result;
        }

        copy() {
            let content = this.opts.title + " - " + diffValue(this.opts.details.total)
                            + "\n" + this.exportTags(this.opts.details.facets);
            clipboard.copy(content);
            emitter.trigger("message-info", "Report copied into clipboard.");
        }
    </script>

    <style media="screen">
        reporttags {
            display: block;
            padding: 0 20px;
        }

        .tag {
            background-color: #1ea7e6;
            color: white;
            border-radius: 2px 0 0 2px;
            padding: 5px;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        .node {
            position: relative;
            display: flex;
            flex-direction: column;
            justify-content: center;
            padding: 5px 0 5px 20px;
            margin-left: 5px;
        }

        .node::after {
            content: "";
            border-top: 2px solid #f6f8f9;
            position: absolute;
            top: 19px;
            left: 0;
            width: 19px;
        }

        .node::before {
            content: "";
            border-left: 2px solid #f6f8f9;
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
        }

        .node:last-child::before {
            height: 19px;
        }

        .meta {
            display: flex;
            flex-direction: row;
            align-items: center;
        }

        .meta-info {
            border-radius: 0 2px 2px 0;
            padding: 5px 10px 5px 10px;
            background-color: #eee;
            height: 32px;
        }

        .displays {
            display: flex;
            justify-content: center;
            padding-bottom: 20px;
            align-items: center;
        }

        input {
            margin: 0 5px;
        }

        .button {
            margin-left: 20px;
        }

        @media screen and (max-width: 700px) {
            .displays {
                flex-direction: column-reverse;
            }
        }
    </style>
</ReportTags>

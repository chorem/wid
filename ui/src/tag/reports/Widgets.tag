import timelineService from "../../service/TimelineService";
import reportsService from "../../service/ReportsService";
import {getDateTimeForPeriod} from "../../utils/DateTimeUtils";
import {diffValue} from "../../utils/DateTimeUtils";
import route from "riot-route/lib/tag";

<Widgets>

    <div each="{widget in widgets}" class="content" onclick="{seeReport.bind(this, widget)}">
        <div class="value">{diffValue(widget.time)}</div>
        <div class="name">{widget.name}</div>
    </div>

    <script>
        this.diffValue = diffValue;
        this.widgets = [];

        let refresh = () => {
            reportsService.getWidgets()
            .then((widgets) => {
                this.widgets = widgets;
                this.update();
            });
        };
        refresh();

        timelineService.activities.on("allChanged", refresh);
        this.on("unmount", () => {
            timelineService.activities.off("allChanged", refresh);
        });

        seeReport(widget) {
            let {period, selectBeginDateTime, selectEndDateTime} = widget;

            if (period !== 4) {
                let {beginDateTime, endDateTime} = getDateTimeForPeriod(period);
                selectBeginDateTime = beginDateTime;
                selectEndDateTime = endDateTime;
            }

            route(`/reports/${widget.id}/${selectBeginDateTime}/${selectEndDateTime}`);
        }
    </script>

    <style media="screen">
        widgets {
            padding-left: 20px;
            color: #35434f;
            text-align: center;
            font-family: 'Lato',sans-serif;
            font-weight: 300;
            display: flex;
            justify-content: flex-end;
            align-items: center;
            flex-wrap: wrap;
        }

        .content {
            margin: 0 20px 20px 0;
            cursor: pointer;
        }

        .name {
            font-size: 24px;
        }

        .value {
            font-size: 20px;
            font-weight: bold;
        }
    </style>

</Widgets>

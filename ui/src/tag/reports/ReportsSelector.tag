import route from "riot-route/lib/tag";
import reportsService from "../../service/ReportsService";
import {getDateTimeForPeriod} from "../../utils/DateTimeUtils";
import emitter from "../../service/ServiceEmitter";

<ReportsSelector>
    <div class="container">

        <select class="selector" ref="selector" onchange="{selectReport}" if="{!opts.multiple}">
            <option value="">Choose the report</option>
            <option each="{report in reports}" value="{report.id}">{report.name}</option>
        </select>

        <select class="selector" ref="selector" onchange="{selectReport}" if="{opts.multiple}" size="10">
            <option each="{report in reports}" value="{report.id}">{report.name}</option>
        </select>

        <i class="fa fa-plus button-icon" onclick="{newReport}"></i>
    </div>

    <script>
        this.reports = [];

        this.opts.multiple && emitter.trigger("message-loading");
        this.on("mount", () => {
            this.reports = [];

            reportsService.getReports()
            .then((reports) => {
                this.opts.multiple && emitter.trigger("message-clear");
                this.reports = reports;
                this.update();
            }, (err) => {
                this.opts.multiple && emitter.trigger("message-error", "Error server.", err);
            });
        });

        selectReport() {
            let selector = this.refs.selector;
            let reportId = selector.options[selector.selectedIndex].value;
            if (reportId) {
                let {period, selectBeginDateTime, selectEndDateTime} = this.reports[reportId];

                if (period !== 4) {
                    let {beginDateTime, endDateTime} = getDateTimeForPeriod(period);
                    selectBeginDateTime = beginDateTime;
                    selectEndDateTime = endDateTime;
                }

                route(`/reports/${reportId}/${selectBeginDateTime}/${selectEndDateTime}`);
            } else {
                route(`/reports`);
            }
        }

        newReport() {
            route(`/reports/new`);
        }
    </script>

    <style>
        .container {
            padding: 20px;
            display: flex;
            justify-content: flex-end;
            font-size: 22px;
        }

        .selector {
            width: 420px;
            margin-left: 20px;
        }

        .fa-plus {
            min-width: 22px;
        }
    </style>
</ReportsSelector>

import reportsService from "../../service/ReportsService";
import route from "riot-route/lib/tag";
import {getDateTimeForPeriod, formatDatePeriod, forEachPeriods, formatChartPeriod} from "../../utils/DateTimeUtils";
import moment from "moment";
import "./ReportTags.tag";
import "./ReportHistory.tag";
import "../timeline/Activity.tag";
import "../timeline/DayActivities.tag";
import emitter from "../../service/ServiceEmitter";
import timelineService from "../../service/TimelineService";
import eventService from "../../service/EventService";
import "../timeline/Panel.tag";

<Report>
    <div class="details" if="{details}">
        <div class="report-nav">
            <div if="{details.report.period !== 4}" class="fa fa-chevron-left button-icon" onclick="{select.bind(this, -1)}"></div>
            <i class="fa fa-gear button-icon" onclick="{editReport}"></i>
            <div if="{details.report.period !== 4}" class="fa fa-chevron-right button-icon" onclick="{select.bind(this, 1)}"></div>
        </div>

        <div class="report-title">{title}</div>

        <div if="{details.report.period !== 4}" class="section">Progression</div>
        <ReportHistory if="{details.report.period !== 4}" data="{getHistoryData(details)}" onchange="{select}"
            durations="{durations}" wakatimeprojects="{wakatimeprojects}" wakatimelanguages="{wakatimelanguages}"/>
        <div class="wakatime-options">
            <input checked={durations} onChange={toggleDurations} type="checkbox" value="true"/>
            <label onClick={toggleDurations}>Durations</label>
            <input checked={wakatimeprojects} onChange={toggleWakatimeProjects} type="checkbox" value="true"/>
            <label onClick={toggleWakatimeProjects}>Wakatime projects</label>
            <input checked={wakatimelanguages} onChange={toggleWakatimeLanguages} type="checkbox" value="true"/>
            <label onClick={toggleWakatimeLanguages}>Wakatime languages</label>
        </div>

        <div class="section">Tags</div>
        <ReportTags details="{details}" title="{title}"/>

        <div class="section">Details</div>
        <div class="section-details">
            <DayActivities each="{activity in activities}" index={0} date="{activity.beginDateTime}" onclick="{parent.openPanel}">
                <Activity activity="{activity}" date="{activity.endDateTime}" inline="{true}"/>
            </DayActivities>
            <div if="{activities.length !== details.details.length}" class="report-view button" onclick="{viewAll}">View all</div>
        </div>

        <Panel ref="panel" activity="{activity}" eventstype="{eventstype}"/>
    </div>

    <script>
        this.activity = null;
        this.details = null;
        this.activities = null;
        this.durations = true;
        this.wakatimeprojects = false;
        this.wakatimelanguages = false;

        let getDetails = (reportId, selectBeginDateTime, selectEndDateTime) => {
            this.details = null;
            this.activities = null;

            emitter.trigger("message-loading");
            reportsService.getDetails(reportId, selectBeginDateTime, selectEndDateTime)
            .then((details) => {
                this.details = details;
                this.activities = details.details.slice(0, 100);
                this.reportId = reportId;
                this.selectBeginDateTime = moment(+selectBeginDateTime);
                this.selectEndDateTime = moment(+selectEndDateTime);
                this.title = this.getTitle();
                this.update();

                let lastActivity = this.activities[this.activities.length - 1];
                if (lastActivity) {
                    eventService.clearEvents();
                    eventService.getEventsShifted(selectBeginDateTime, lastActivity.endDateTime);
                }

                emitter.trigger("message-clear");
            }, (err) => {
                emitter.trigger("message-error", "Error server.", err);
            });
        };
        this.on("route", getDetails);

        let refresh = () => {
            getDetails(this.reportId, this.selectBeginDateTime, this.selectEndDateTime);
        };
        timelineService.activities.on("allChanged", refresh);

        this.on("unmount", () => {
            timelineService.activities.off("allChanged", refresh);
        });

        select(offset) {
            let {period} = this.details.report;
            let {beginDateTime, endDateTime} = getDateTimeForPeriod(period, this.selectBeginDateTime, offset);

            route(`/reports/${this.reportId}/${beginDateTime}/${endDateTime}`);
        }

        getTitle() {
            let {report} = this.details;
            let {period} = report;

            let periodFormat;
            if (period === 4) {
                periodFormat = "dates";
            } else {
                periodFormat = formatDatePeriod(period, this.selectBeginDateTime);
            }

            let selectBeginDateTime, selectEndDateTime;
            if (period === 0) {
                selectBeginDateTime = moment(this.selectBeginDateTime).format("HH:mm:ss");
                selectEndDateTime = moment(this.selectEndDateTime).format("HH:mm:ss");
            } else {
                selectBeginDateTime = moment(this.selectBeginDateTime).format("L");
                selectEndDateTime = moment(this.selectEndDateTime).format("L");
            }

            return `${report.name} - ${periodFormat} - (${selectBeginDateTime} - ${selectEndDateTime})`;
        }

        getHistoryData(details) {
            let {period} = details.report;
            let {durations, languages, projects} = details;

            let result = {
                dates: forEachPeriods(period, this.selectBeginDateTime)
            };
            result.labels = result.dates.map((v) => formatChartPeriod(period, v));

            // Durations
            let dataDurations = [];
            durations.forEach((value) => {
                let dateTime = formatChartPeriod(period, value.period);
                let index = result.labels.indexOf(dateTime);

                dataDurations[index] = value.duration;
            });
            result.durations = dataDurations;

            // Languages
            let dataLanguages = {};
            languages.forEach((value) => {
                let dateTime = formatChartPeriod(period, value.period);
                let name = value.language || "unknown";
                let index = result.labels.indexOf(dateTime);

                let project = dataLanguages[name];
                if (!project) {
                    project = dataLanguages[name] = [];
                }
                project[index] = value.duration;
            });
            result.languages = dataLanguages;

            // Projects
            let dataProjects = {};
            projects.forEach((value) => {
                let dateTime = formatChartPeriod(period, value.period);
                let name = value.project || "unknown";
                let index = result.labels.indexOf(dateTime);

                let project = dataProjects[name];
                if (!project) {
                    project = dataProjects[name] = [];
                }
                project[index] = value.duration;
            });
            result.projects = dataProjects;

            return result;
        }

        viewAll() {
            emitter.trigger("message-loading");
            setTimeout(() => {
                let lastActivity = this.activities[this.activities.length - 1];
                if (lastActivity) {
                    eventService.getEventsShifted(lastActivity.endDateTime, this.selectEndDateTime);
                }

                this.activities = this.details.details;
                this.update();

                emitter.trigger("message-clear");
            }, 0);
        }

        editReport() {
            route(`/reports/edit/${this.reportId}`);
        }

        openPanel(event) {
            let panel = this.refs.panel;

            if (event.activity) {
                this.activity = event.activity;
                this.eventstype = event.eventstype;
                this.refs.panel.open();
            }

            this.update();
        }

        toggleDurations() {
            this.durations = !this.durations;
        }

        toggleWakatimeProjects() {
            this.wakatimeprojects = !this.wakatimeprojects;
        }

        toggleWakatimeLanguages() {
            this.wakatimelanguages = !this.wakatimelanguages;
        }
    </script>

    <style>
        report {
            position: relative;
            display: block;
        }

        .details {
            position: relative;
            display: block;
            margin: 3%;
            box-shadow: 0px 0px 5px 1px rgba(0, 0, 0, 0.15);
            background-color: white;
        }

        .report-nav {
            position: absolute;
            width: 100%;
            display: flex;
            justify-content: flex-end;
        }

        .report-title {
            display: flex;
            justify-content: space-between;
            text-align: center;
            justify-content: center;
            font-size: 28px;
            color: #48626f;
            padding: 20px 20px 0 20px;
        }

        .fa-chevron-left {
            flex-grow: 1;
        }

        .fa-chevron-right {
        }

        .wakatime-options {
            display: flex;
            align-items: center;
            justify-content: flex-end;
            margin: 0 20px;
        }

        .wakatime-options input {
            margin: 0 5px 0 10px;
        }

        .section-details {
            border-top: 1px solid #e5e7e8;
            border-bottom: 1px solid #e5e7e8;
        }

        .report-view {
            margin: 20px;
        }

        @media screen and (max-width: 900px) {
            .details {
                margin: 0;
            }
        }

        @media screen and (max-width: 700px) {
            .wakatime-options {
                flex-direction: column-reverse;
            }
        }
    </style>
</Report>

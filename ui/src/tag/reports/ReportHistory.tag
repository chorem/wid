import Chart from "chart.js";
import {diffValue} from "../../utils/DateTimeUtils";
import moment from "moment";

<ReportHistory>
    <canvas ref="chart" width="0" height="300"></canvas>

    <script>
        let backgroundColors = [
            'rgba(255, 99, 132, 0.7)',
            'rgba(54, 162, 235, 0.7)',
            'rgba(75, 192, 192, 0.7)',
            'rgba(201, 203, 207, 0.7)',
            'rgba(255, 159, 64, 0.7)',
            'rgba(153, 102, 255, 0.7)',
            'rgba(255, 205, 86, 0.7)'
        ];

        let getDatasets = () => {
            let datasets =  [];

            if (this.opts.durations) {
                datasets = datasets.concat({
                    label: 'duration',
                    backgroundColor: this.opts.data.dates.map((item, index) => {
                        return backgroundColors[index % backgroundColors.length];
                    }),
                    stack: "durations",
                    data: this.opts.data.durations,
                    borderWidth: 1
                });
            }

            if (this.opts.wakatimeprojects) {
                let names = Object.keys(this.opts.data.projects);
                let projects = names.map((name, index) => {
                    return {
                        label: name,
                        backgroundColor: backgroundColors[index % backgroundColors.length],
                        stack: "projects",
                        data: this.opts.data.projects[name],
                        borderWidth: 1
                    };
                });
                datasets = datasets.concat(projects);
            }

            if (this.opts.wakatimelanguages) {
                let names = Object.keys(this.opts.data.languages);
                let languages = names.map((name, index) => {
                    return {
                        label: name,
                        backgroundColor: backgroundColors[index % backgroundColors.length],
                        stack: "languages",
                        data: this.opts.data.languages[name],
                        borderWidth: 1
                    };
                });
                datasets = datasets.concat(languages);
            }

            return datasets;
        };

        let refresh = () => {
            let ctx = this.refs.chart.getContext("2d");

            this.chart = new Chart(ctx, {
                type: "bar",
                data: {
                    labels: this.opts.data.labels,
                    datasets: getDatasets()
                },
                options: {
                    legend : {
                        display: false
                    },
                    scales: {
                        xAxes: [{
                            stacked: true,
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    },
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, data) {
                                return data.datasets[tooltipItem.datasetIndex].label + ":" + diffValue(tooltipItem.yLabel);
                            }
                        }
                    },
                    onClick: (evt) => {
                        let element = this.chart.getElementAtEvent(evt);
                        let offset = 0;

                        if (element && element[0]) {
                            offset = this.opts.data.dates.length - 1 - element[0]._index;
                        }
                        this.opts.onchange && this.opts.onchange(-offset);

                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                callback: function(value, index, values) {
                                    return diffValue(value);
                                }
                            }
                        }]
                    }
                }
            });
        };

        this.on("mount", refresh);
        this.on("update", () => {
            this.chart.data.datasets = getDatasets();
            this.chart.update();
        });

        this.on("unmount", () => {
            // Don't destroy the chart otherwise we get an error.
            // this.chart.destroy();
            this.chart = null;
        });
    </script>

    <style>
        reporthistory {
            display: block;
            padding: 0 20px;
        }

        canvas {
            width: 100%;
            max-width: 100%;
        }
    </style>
</ReportHistory>

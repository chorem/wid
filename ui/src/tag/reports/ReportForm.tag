import reportsService from "../../service/ReportsService";
import route from "riot-route/lib/tag";
import {getDateTimeForPeriod} from "../../utils/DateTimeUtils";
import "../common/Message.tag";
import emitter from "../../service/ServiceEmitter";
import moment from "moment";
import "../common/Flatpickr.tag";
import "../timeline/Completion.tag";
import {PATTERN_QUERY_INPUT, TAB_KEY_CODE, completeQuery} from "../../utils/StringUtils";

<ReportForm>
    <div class="popup-container">
        <Message/>

        <form ref="form" class="popup-center" onsubmit="{saveReport}">
            <div class="popup-title">
                <a class="fa fa-2x fa-times button-icon" onclick="history.go(-1)"></a>
                Report
            </div>

            <div class="popup-label">Name</div>
            <input type="text" name="name" required="true"/>

            <div class="popup-label">Query</div>
            <input type="text" name="query" oninput={changeTagValue} onkeydown="{enterTag}"
            autocomplete="off" autofocus
            placeholder="example: -notag+tag" pattern="{PATTERN_QUERY_INPUT}"/>
            <Completion ref="completion" tagvalue="{tagvalue}" selecttag="{selecttag}" nocomment="true"/>

            <div class="popup-label">Period</div>
            <select name="period" onchange="{selectPeriod}">
                <option value="0">Daily</option>
                <option value="1">Week</option>
                <option value="2">Month</option>
                <option value="3">Year</option>
                <option value="4">Between dates</option>
            </select>

            <virtual if="{displayDates}">
                <div class="popup-label">Begin date</div>
                <Flatpickr ref="beginDate" enabletime="true"/>

                <div class="popup-label">End date</div>
                <Flatpickr ref="endDate" enabletime="true"/>
            </virtual>

            <div class="popup-label">Display on timeline</div>
            <input type="checkbox" name="widget"/>
            <input class="button" type="submit" value="Save"/>
        </form>

        <form if="{reportId}" ref="delete" class="popup-center" onsubmit="{removeReport}">
            <div class="popup-title">Remove report ?</div>
            <div class="popup-label">Confirm</div>
            <input type="checkbox" required/>
            <input class="button" type="submit" value="Remove"/>
        </form>
    </div>

    <script>
        this.PATTERN_QUERY_INPUT = PATTERN_QUERY_INPUT;
        this.displayDates = false;
        this.tagvalue = "";

        this.on("route", (reportId) => {
            if (!reportId) {
                return;
            }

            reportsService.getReport(reportId)
            .then((report) => {
                this.reportId = reportId;

                let form = this.refs.form;
                form.name.value = report.name;
                this.tagvalue = form.query.value = report.tags;
                form.period[report.period].selected = true;
                form.widget.checked = report.widget;

                this.displayDates = report.period === 4;
                this.update();

                if (report.selectBeginDateTime) {
                    this.refs.beginDate.setValue(report.selectBeginDateTime);
                }
                if (report.selectEndDateTime) {
                    this.refs.endDate.setValue(report.selectEndDateTime);
                }
            });
        });

        selectPeriod(e) {
            let selector = e.target;
            let period = selector.options[selector.selectedIndex].value;
            this.displayDates = period === "4";
        }

        saveReport(e) {
            e.preventDefault();
            e.stopPropagation();

            let form = e.target;
            let name = form.name.value;
            let query = form.query.value;
            let period = +form.period.value;
            let widget = form.widget.checked ? 1 : 0;
            let selectBeginDateTime = this.refs.beginDate && this.refs.beginDate.getValue();
            let selectEndDateTime = this.refs.endDate && this.refs.endDate.getValue();

            let callback = (reportId) => {
                if (period !== 4) {
                    let {beginDateTime, endDateTime} = getDateTimeForPeriod(period, null, 0);
                    selectBeginDateTime = beginDateTime;
                    selectEndDateTime = endDateTime;
                } else {
                    selectBeginDateTime = selectBeginDateTime && moment(selectBeginDateTime) || 0;
                    selectEndDateTime = selectEndDateTime && moment(selectEndDateTime) || 0;
                }

                route(`/reports/${reportId}/${+selectBeginDateTime}/${+selectEndDateTime}`);
            }

            if (this.reportId) {
                reportsService.saveReport(this.reportId, name, query, period, widget, selectBeginDateTime, selectEndDateTime)
                .then(callback, (err) => {
                    emitter.trigger("message-error", "Error during updating.", err);
                });

            } else {
                reportsService.createReport(name, query, period, widget, selectBeginDateTime, selectEndDateTime)
                .then(callback, (err) => {
                    emitter.trigger("message-error", "Error during creation.", err);
                });
            }
        }

        removeReport(e) {
            e.preventDefault();
            e.stopPropagation();

            reportsService.removeReport(this.reportId)
            .then(() => {
                route("/reports")
            }, (err) => {
                emitter.trigger("message-error", "Error during deleting.", err);
            });
        }

        enterTag(event) {
            event.stopPropagation();

            if (event.keyCode === TAB_KEY_CODE) {
                event.preventDefault();
                this.selecttag();
            }
        };

        changeTagValue() {
            let query = this.refs.form.query.value;
            let subqueries = query.split(" ");
            this.tagvalue = subqueries.pop().replace(/\+|-/g, " ");
        };

        selecttag(tag) {
            tag = tag || this.refs.completion.getFirstTag();
            let replace = this.refs.completion.isReplace();

            let query = this.refs.form.query;
            let tagValue = query.value;

            query.value = completeQuery(tagValue, tag, replace);

            let length = query.value.length;
            query.blur();
            query.setSelectionRange(length, length);
            query.focus();

            this.changeTagValue();
        };
    </script>

    <style>
        completion {
            display: flex;
            margin-bottom: 15px;
        }

        input[name=query] {
            margin-bottom: 0;
        }
    </style>
</ReportForm>

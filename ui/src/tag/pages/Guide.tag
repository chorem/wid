import "./Page.tag"

<Guide>
    <Page>
        <p>Warning: The guide is very basic and need to be completed.</p>

        <h1>Get started</h1>
        <ul>
            <li>Create an account</li>
            <li>Enter your tags using a white space as a separator in order to create a new activity into your timeline</li>
            <li>Create a report for your activity</li>
        </ul>

        <h2>Tips</h2>
        <p>Add note to an activity by adding parentheses into the tags as "(comment)"</p>

        <h1>Reports</h1>

        <p>The report is used to follow the activities.</p>

        <h2>Create your first report</h2>

        <ul>
            <li>Go to reports</li>
            <li>Click on the plus icon</li>
            <li>Enter "all" as name</li>
            <li>Select Year for the period</li>
            <li>Click on Save</li>
        </ul>

        <h2>Create a custom report using a query</h2>
        <p>
            You can create a custom report by filtering your timeline with a query.
            The query has to contain the tags, separated by a "+" or a "-" or a white
            space delimiter, defining the tags you want to include or exclude from your
            report.
        </p>

        <ul>
            <li>tag1+tag2 the report will show the amounts of time allocated to tag1 and tag2 at the same time</li>
            <li>tag1-tag2 the report will show the amounts of time allocated to tag1, excluding all timeslots allocted to tag2</li>
            <li>tag1 tag2 the report will show the amounts of time allocated to tag1 or tag2</li>
        </ul>

        <h2>Widget</h2>
        <p>
            Reports can be used as widgets. It will help you keep an eye on your reports,
            directly from your timeline. The widget exposes an overview of a report.
        </p>

        <h1>Connect with GitHub/GitLab</h1>

        <p>You can see your commits on GitHub and/or GitLab in your timeline. To use this feature, you need to:</p>

        <ul>
            <li>Add the email you use with GitHub or GitLab in Configuration > identities</li>
            <li>In Configuration > Applications, enter an application name and generate an API key</li>
            <li>Copy the url associated to this api key</li>
            <li>Connect on GitHub or GitLab</li>
            <li>Paste the url into the webhook section of the given website (GitHub or GitLab)</li>
        </ul>

        <p>Now the commits you push on the projects hosted by GitLab or GitHub will be sent as an event to your timeline. You can then use those event to complete your activities.</p>

        <h1>Chrome extension</h1>

        <p>The Chrome extension allow you to detect the idle in your computer. Download the extension on the home page and drag n drop on the list of extensions in your chrome.</p>

        <ul>
            <li>Download extension</li>
            <li>In your browser go to: chrome://extensions</li>
            <li>Drag n drop extension in current page</li>
            <li>Generate an application key</li>
            <li>Click on Wid icon in navigation bar</li>
            <li>Set Server url with your generated application URL</li>
        </ul>

        <h1>Windand</h1>
        <p>Windand is the Wid agent for Android. Download it directly on wid home page. Windand send to Wid the information contained in your phone (calls, meetings, application usage, ...)</p>

        <ul>
            <li><a href="http://widand.chorem.com/WidAnd-debug.apk">Download apk</a></li>
            <li>Generate an application key</li>
            <li>Windand preference set your Device name and Wid url with your generated application URL</li>
        </ul>

        <h1>Wakatime plugin</h1>
        <p>
            Wakatime plugin is used to send what do you do in your IDE (project, branch, file, ...).
            <a href="https://wakatime.com/download">Download the plugins you need</a>. You don't need to create Wakatime account.
        </p>

        <p>
            You can use Wakatime plugin based on CLI easily. You have to modifiy
            wakatime configuration file ($HOME/.wakatime.cfg for linux) to
            change the api_url. Be carefull, the api_key must be a correct format,
            so you need to keep a fake key as the example.
        </p>

        <h2>Example</h2>

        <pre>
[settings]
api_key = DF393251-5AEF-411E-979B-E7C1EEA762A2
api_url = https://wid.chorem.com/webhook/aaaaaaaa-57ce-11e6-80d8-e9d235e45adc
offline = true
hostname = myComputer
</pre>

        <h1>Taskwarrior</h1>

        <p>This script send the modification on task from Taskwarrior.</p>

        <h2>Pre-requisite in your $PATH:</h2>
        <ul>
            <li>jq</li>
        </ul>

        <h2>Installation</h2>
        <ul>
            <li><a href="https://gitlab.nuiton.org/chorem/wid/blob/master/scripts/on-modify.wid-taskwarrior-send">Download the script</a></li>
            <li>Copy the script into ~/.task/hooks/</li>
            <li>Set script executable (chmod u+x)</li>
            <li>Generate an application key</li>
            <li>Set WID_WEBHOOK_URL with your generated application URL in your ~/.bashrc</li>
<pre>
export WID_WEBHOOK_URL=https://wid.chorem.com/webhook/&lt;application key&gt;
</pre>
            <li>Verify the script with command task diagnostic</li>
<pre>
Active: on-modify.wid-taskwarrior-send (executable)
</pre>
        </ul>

        <h1>Arbtt</h1>

        <p>This script send active window (name and title) and idle time to WID.</p>

        <h2>Pre-requisite in your $PATH:</h2>
        <ul>
            <li>arbtt-dump</li>
            <li>jq</li>
        </ul>

        <h2>Installation</h2>
        <ul>
            <li><a href="https://gitlab.nuiton.org/chorem/wid/blob/master/scripts/wid-arbtt">Download wid script</a></li>
            <li>Set script executable (chmod u+x)</li>
            <li>Add line in in your crontab (crontab -e)</li>
<pre>
 */15 * * * *       $HOME/wid-arbtt
</pre>
            <li>Generate an application key</li>
            <li>Set WID_WEBHOOK_URL with your generated application URL in your ~/.bashrc</li>
<pre>
export WID_WEBHOOK_URL=https://wid.chorem.com/webhook/&lt;application key&gt;
</pre>
        </ul>

        <h1>Wid shell</h1>
        <p>This script send all command (command line, program name, execution time, exit return, ...) to WID.</p>

        <h2>Pre-requisite in your $PATH:</h2>
        <ul>
            <li>curl</li>
        </ul>

        <h2>Installation</h2>
        <ul>
            <li><a href="https://gitlab.nuiton.org/chorem/wid/blob/master/scripts/wid-bash"></a>Download wid script</li>
            <li>Add in ~/.bashrc</li>
<pre>
source wid-bash
</pre>
            <li>Generate an application key</li>
            <li>Set WID_WEBHOOK_URL with your generated application URL in your ~/.bashrc</li>
<pre>
export WID_WEBHOOK_URL=https://wid.chorem.com/webhook/&lt;application key&gt;
</pre>
        </ul>

        <h2>Advanced Usage</h2>
        <p>You can monitor you activity on server with wid-bash. When wid-bash is installed on your computer you can do on command line:</p>
<pre>
widbash_install root@monserver.example.com
</pre>
        <p>And automaticaly all configuration needed is done.</p>

        <h1>Wid net</h1>
        <p>This script send your device ip to WID.</p>

        <h2>Pre-requisite in your $PATH:</h2>
        <ul>
            <li>ip</li>
            <li>curl</li>
            <li>jq</li>
        </ul>

        <h2>Installation</h2>
        <ul>
            <li><a href="https://gitlab.nuiton.org/chorem/wid/blob/master/scripts/wid-net">Download wid script</a></li>
            <li>Generate an application key</li>
            <li>Launch wid-net manualy when is needed or add wid-net monitor in your .xsession for example:</li>
<pre>
WID_WEBHOOK_URL=https://wid.chorem.com/webhook/&lt;application key&gt;
</pre>
        </ul>

        <p>
            If geoip data service return bad information, you can force some value. For that you can use $WID_IP_LOCATION variable.
            This variable is json Array of Object with field 'ip', and other field. Match is done on 'ip' field,
            all other field replace or are copied in event send to wid.
        </p>
<pre>
export WID_IP_LOCATION='[{"ip":"123.123.123.123", "place": "work", "latitude": 10.123, "longitude": 0.123},{"ip":"234.234.234.234",  "place": "house","latitude": 12.12, "longitude": 11.11}]'
</pre>

        <p>
            In this example, if geoip return data information for ip="123.123.123.123" or
            ip="234.234.234.234" 'latitude' and 'longitude' are replaced and 'place' field is
            added to event. Otherwise geoip data is send without modification.
        </p>
    </Page>
</Guide>

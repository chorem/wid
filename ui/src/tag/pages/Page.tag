<Page>
    <yield/>

    <script>
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    </script>

    <style>
        page {
            display: block;
            background-color: white;
            padding: 20px;
        }

        img {
            flex-grow: 1;
            height: auto;
            width: 100%;
            margin: 20px;
            background-size: cover;
            background-position: top center;
        }

        a {
            color: #8b8c8f;
        }
        a:hover {
            color: #15a4fa;
        }

        h1 {
            min-height: 51px;
            font-size: 24px;
            line-height: 1.4;
            font-family: Montserrat;
            text-transform: uppercase;
            letter-spacing: 2px;
            color: #2D2E33;
            padding-top: 20px;
            border-bottom: 1px solid #2D2E33;
            margin: 21px 20px;
        }

        h2 {
            min-height: 51px;
            font-size: 18px;
            line-height: 1.4;
            font-family: Montserrat;
            text-transform: uppercase;
            letter-spacing: 2px;
            color: #2D2E33;
            padding-top: 20px;
            border-bottom: 1px solid #2D2E33;
            margin: 19px 20px;
        }

        h3 {
            min-height: 51px;
            font-size: 14px;
            line-height: 1.4;
            font-family: Montserrat;
            text-transform: uppercase;
            letter-spacing: 2px;
            color: #2D2E33;
            padding-top: 20px;
            border-bottom: 1px solid #2D2E33;
            margin: 19px 20px;
        }

        p {
            margin: 16px 20px;
        }

        ul {
            margin: 16px 0;
            padding-left: 40px;
        }

        ol {
            margin: 16px 0;
            padding-left: 40px;
            overflow: visible;
        }

        li {
            overflow: visible;


        }

        li > p {
            overflow: visible;
            margin: 0;
        }

        pre {
            padding: 9.5px;
            margin: 0 0 10px;
            font-size: 13px;
            line-height: 1.42857143;
            color: #333;
            word-break: break-all;
            word-wrap: break-word;
            background-color: #f5f5f5;
            border: 1px solid #ccc;
            border-radius: 4px;
        }

        page > pre {
            margin: 0 20px;
        }

        code {
            background-color: #f5f5f5;
        }
    </style>
</Page>

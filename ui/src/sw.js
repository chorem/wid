self.onnotificationclick = function(event) {
    self.port.postMessage(event.action);
    event.notification.close();
};

self.addEventListener("message", event => {
    self.port = event.ports[0];
});

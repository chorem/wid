import moment from "moment";
import riot from "riot";

class Activity {
    constructor(beginDateTime = moment(), endDateTime = moment(), tags = [], isTimer = false, isHole = false) {
        this.beginDateTime = beginDateTime;
        this.endDateTime = endDateTime;
        this.tags = tags;
        this.isTimer = isTimer;
        this.isHole = isHole;
        this.events = {};

        riot.observable(this);
    }

    get beginDateTime() { return this._beginDateTime; }
    set beginDateTime(dateTime) { this._beginDateTime = dateTime; }

    get endDateTime() { return this.isTimer ? moment() : this._endDateTime; }
    set endDateTime(dateTime) { this._endDateTime = dateTime; }

    static createTimerActivity(beginDateTime, tags) {
        return new Activity(beginDateTime, null, tags, true, false);
    }

    static createHole(beginDateTime, endDateTime) {
        return new Activity(beginDateTime, endDateTime, [], false, true);
    }
}

export default Activity;

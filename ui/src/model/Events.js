import moment from "moment";
import riot from "riot";

class Events {

    constructor() {
        riot.observable(this);
        this.values = [];
    }

    clear() {
        this.values = [];
    }

    concat(data) {
        let events = data.map(this._toEvent);
        this.values = this.values.concat(events);
        this.trigger("changed");

        return events;
    }

    shift(data) {
        let events = data.map(this._toEvent);
        this.values = events.concat(this.values);
        this.trigger("changed");

        return events;
    }

    add(event) {
        event = this._toEvent(event);
        let index = this._binarySearch(event.beginDateTime, this.values, 0, this.values.length - 1);
        this.values.splice(index, 0, event);
        this.trigger("changed");
    }

    remove(id) {
        this.values = this.values.filter(e => {
            return e.id !== id;
        });
        this.trigger("changed");
    }

    /**
     * Find all the events between two dates.
     *
     * @param {Intervall object} an object containing a beginDateTime and endDateTimeProperty
     */
    get({beginDateTime, endDateTime}) {

        // A function to create a map, groupping events according to their type
        let push = (events, e) => {
            let type = e.type;

            let eventsByType = events[type];
            if (!eventsByType) {
                eventsByType = events[type] = {
                    type,
                    values: []
                };
            }
            eventsByType.values.push(e);
            events.all.values.push(e);
        };

        let eventsLength = this.values.length;

        let events = {all: {values: []}};

        // Lets loop on events to find all events between beginDateTime and endDateTime.
        // To improve performance, we search the loop starting index thanks to a binary search.
        // Then we take all events until we find an event which endDateTime is older than the given beginDateTime. (event.endDateTime < beginDateTime)
        //
        // Important : you have to consider that the array of events (this.values) is sorted by beginDateTime in a descending order.
        let endIndex = this._binarySearch(endDateTime, this.values, 0, eventsLength - 1);
        for (let position = endIndex; position < eventsLength; position++) {
            let event = this.values[position];

            if (+beginDateTime < +event.endDateTime &&
                +endDateTime > +event.beginDateTime ||
                +beginDateTime === +event.beginDateTime) {

                push(events, event);
            }

            if (+event.endDateTime < +beginDateTime) {
                return events;
            }
        }

        return events;
    }

    /**
     * This function process a binary search on the given array of events.
     * The array of events has to be sorted on the beginDataTime event property in the descending order.
     *
     * The function looks in the events array for the closest event to the given date and returns its index.
     * (closest event to the given date = the closest event which assert event.beginDateTime <= date)
     *
     * @param {Date} date
     * @param {Array of Events} events sorted by descending beginDateTime
     * @param {Integer} startIndex
     * @param {Integer} endIndex
     */
    _binarySearch(date, events, startIndex, endIndex) {
        if (endIndex < 0 || startIndex === endIndex) {
            return startIndex;
        }

        const middleIndex = startIndex + ~~((endIndex - startIndex) / 2);
        const event = events[middleIndex];
        if (event.beginDateTime <= date) {
            return this._binarySearch(date, events, startIndex, middleIndex);
        }

        return this._binarySearch(date, events, middleIndex + 1, endIndex);
    }

    _toEvent(rawEvent) {
        let result = rawEvent;
        if (rawEvent.value) {
            result = JSON.parse(rawEvent.value);
            result.id = rawEvent.id;
            result.beginDateTime = moment(+rawEvent.beginDateTime);
            result.endDateTime = moment(+rawEvent.endDateTime);
        }
        return result;
    }
}

export default Events;

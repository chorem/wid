import Activity from "./Activity";
import {isEqualsArrays} from "../utils/StringUtils";
import riot from "riot";

class Activities {

    constructor() {
        riot.observable(this);
        this.values = [];
    }

    * [Symbol.iterator]() {
        if (this.values.length === 1) {
            return;
        }

        let previousActivity = this.values[0];
        let lastBeginDateTime = this.values[1].endDateTime;

        for (let index = 1; index < this.values.length; index++) {
            let value = this.values[index];
            let {beginDateTime, endDateTime} = value;

            // Detect empty activity
            if (+beginDateTime === +endDateTime) {
                continue;
            }

            // Detect hole between two activities
            if (+lastBeginDateTime !== +endDateTime) {
                // There is a time gap between 2 activites,
                // let's create a hole and chain it with previous and next activities
                let hole = Activity.createHole(endDateTime, lastBeginDateTime);

                if (previousActivity) {
                    previousActivity.previous = hole;
                    hole.next = previousActivity;
                }
                previousActivity = hole;

                yield hole;
            }

            lastBeginDateTime = beginDateTime;

            // Chain it with previous and next activities
            if (previousActivity) {
                previousActivity.previous = value;
                value.next = previousActivity;
            } else {
                value.next = null;
            }
            previousActivity = value;

            yield value;
        }
    }

    get() {
        return Array.from(this);
    }

    concat(activities) {
        let cache = {};
        this.values = this.values.concat(activities)
        // Dectect duplicate activities
        .filter((a) => {
            let key = a.beginDateTime + "-" + a.endDateTime;
            let verify = cache[key];
            cache[key] = true;
            return !verify;
        });
        this.trigger("allChanged");
    }

    searchIndex(activity) {
        if (activity.isHole) {
            return this.searchIndex(activity.previous);
        }

        return this.values.findIndex((a) => {
            return a === activity;
        });
    }

    merge(activity) {
        let {beginDateTime, endDateTime} = activity;

        let index = this.searchIndex(activity);
        let previous = activity.isHole ? activity.previous : this.values[index + 1];
        let next = activity.isHole ? activity.next : this.values[index - 1];

        // If it was a hole but it is not anymore, we need to insert it into the values array as a plain activity
        if (activity.isHole && activity.tags.length) {
            activity.isHole = false;
            this.values.splice(index, 0, activity);
        }

        // Remove empty activity
        if (+beginDateTime === +endDateTime) {

            // Next and previous are same ?
            if (previous && next && !next.isTimer
                    && +previous.endDateTime === +next.beginDateTime
                    && isEqualsArrays(previous.tags, next.tags)) {

                if (+beginDateTime === +next.beginDateTime) {
                    previous.beginDateTime = next.beginDateTime;
                    this.values.splice(index - 1, 1);

                } else {
                    next.beginDateTime = previous.beginDateTime;
                    this.values.splice(index + 1, 1);
                }

                previous.trigger("changed");
                next.trigger("changed");
            }

            this.values.splice(index, 1);
            this.trigger("changed");
            this.trigger("allChanged");
            return;
        }

        // Merge same tags
        let removed = false;
        if (previous
                && +previous.endDateTime === +beginDateTime
                && isEqualsArrays(previous.tags, activity.tags)) {

            activity.beginDateTime = previous.beginDateTime;
            this.values.splice(index + 1, 1);
            removed = true;
        }

        if (next
                && +next.beginDateTime === +endDateTime
                && !next.isTimer
                && isEqualsArrays(next.tags, activity.tags)) {

            activity.endDateTime = next.endDateTime;
            this.values.splice(index - 1, 1);
            removed = true;
        }

        if (removed) {
            this.trigger("changed");
            this.trigger("allChanged");
        }
    }

    addActivity(activity) {
        let lastActivity = this.values[1];
        if (lastActivity
            && +activity.beginDateTime === +lastActivity.endDateTime
            && isEqualsArrays(lastActivity.tags, activity.tags)) {
            // Same activity replace last activity
            lastActivity.endDateTime = activity.endDateTime;
            lastActivity.trigger("changed");

        } else {
            // Add activity
            this.values.splice(1, 0, activity);
            this.trigger("changed");
        }

        this.trigger("allChanged");
    }

    saveActivity(activity, tags) {
        activity.tags = tags;
        activity.trigger("changed");
        this.merge(activity);

        this.trigger("allChanged");
    }

    insertActivity(activity, beginDateTime, endDateTime, tags) {
        let oldEndDateTime = activity.endDateTime;
        activity.endDateTime = beginDateTime;

        let index = this.searchIndex(activity);
        this.values.splice(index, 1,
            new Activity(endDateTime, oldEndDateTime, activity.tags),
            new Activity(beginDateTime, endDateTime, tags),
            activity
        );

        activity.trigger("changed");
        this.trigger("changed");
        this.trigger("allChanged");
    }

    modifyDate(activity, newBeginDateTime, newEndDateTime) {
        activity.beginDateTime = newBeginDateTime;
        activity.endDateTime = newEndDateTime;

        this.merge(activity);
        activity.trigger("changed");
    }
}

export default Activities;

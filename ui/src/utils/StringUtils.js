export let hashCode = function(str) {
    let hash = 0, i, chr, len;
    if (str.length === 0) {
        return hash;
    }

    for (i = 0, len = str.length; i < len; i++) {
        chr = str.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};

export let tagsToArray = function(tagsAsString = "", removeNotes = true) {
    let string = tagsAsString;
    if (removeNotes) {
        string = string.replace(/\(.*?(\)|$)/g, " ");
    }
    string = string.trim().replace(/\s+/g, " ");

    if (string) {
        return string.split(" ");
    }
    return [];
};

export let tagsToString = function(tagsAsArray) {
    return tagsAsArray.join(" ");
};

export let addTag = function(tagsAsString, tag, deleteCount = 0) {
    // Remove all space separator
    let string = tagsAsString.replace(/\s+/g, " ");

    // Add tag
    let tags = tagsToArray(string);
    if (deleteCount > 0) {
        tags.splice(-deleteCount, deleteCount, tag);
    } else {
        tags.push(tag);
    }

    // Add space after last tag
    tags.push("");

    // Return the string from the array
    return tagsToString(tags);
};

export let extractNote = function(input) {
    let match = input.match(/\(.*?(\)|$)/g);

    if (match) {
        return match.map((note) => {
            return note.replace(/\s+/g, " ")
                    .replace(/\)\(/g, " ")
                    .replace(/\)|\(/g, "");
        });
    }
    return match;
};

export let isEqualsArrays = function(first, second) {
    return JSON.stringify(first.slice(0).sort()) === JSON.stringify(second.slice(0).sort());
};

export let completeTags = function(current, complete, replace) {
    let result = current;

    if (replace) {
        result = current.replace(/([^\s]+)$/, complete);

    } else {
        let spaceBefore = !current[current.length - 1]
                            || current[current.length - 1] === " ";
        if (!spaceBefore) {
            result += " ";
        }
        result += complete;
    }

    result = result.trim();
    return result + " ";
};

export let completeQuery = function(current, complete, replace) {
    let result = current;

    if (replace) {
        result = current.replace(/([^\s-\+]+)$/, complete);

    } else {
        let seperatorBefore = !current[current.length - 1]
                                || current[current.length - 1] === "+"
                                || current[current.length - 1] === "-";
        if (!seperatorBefore) {
            result += "+";
        }
        result += complete;
    }

    result = result.trim();
    return result;
};
export let PATTERN_TAG_INPUT = "([a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ._ ]+|(\(.+\)))*";
export let PATTERN_QUERY_INPUT = "[a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ._ \+-]+";
export let ENTER_KEY_CODE = 13;
export let TAB_KEY_CODE = 9;
export let ESCAPE_KEY_CODE = 27;

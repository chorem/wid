import moment from "moment";

export let diffValue = function(diff) {
    let hours = ~~(diff / (1000 * 60 * 60));
    diff -= hours * 1000 * 60 * 60;

    let minutes = ~~(diff / (1000 * 60));
    diff -= minutes * 1000 * 60;

    let seconds = ~~(diff / 1000);
    diff -= seconds * 1000;

    return (hours < 10 ? "0" + hours : hours) + ":" +
            (minutes < 10 ? "0" + minutes : minutes) + ":" +
            (seconds < 10 ? "0" + seconds : seconds);
};

export let diffTime = function(beginDate, endDate) {
    let diff = endDate.diff(beginDate);
    return diffValue(diff);
};

export let getDateTimeForPeriod = function(period, refDateTime, offset = 0) {
    let beginDateTime = refDateTime ? moment(+refDateTime) : moment();
    let endDateTime = beginDateTime.clone();

    if (period === 0) {
        beginDateTime.add(offset, "day").startOf("day");
        endDateTime.add(offset, "day").endOf("day");

    } else if (period === 1) {
        beginDateTime.add(offset, "week").startOf("week");
        endDateTime.add(offset, "week").endOf("week");

    } else if (period === 2) {
        beginDateTime.add(offset, "month").startOf("month");
        endDateTime.add(offset, "month").endOf("month");

    } else if (period === 3) {
        beginDateTime.add(offset, "year").startOf("year");
        endDateTime.add(offset, "year").endOf("year");
    }

    return {beginDateTime: +beginDateTime, endDateTime: +endDateTime};
};

export let formatDatePeriod = function(period, dateTime) {
    let date = moment(dateTime);

    let format;
    if (period === 0) {
        date.startOf("day");
        format = "L";

    } else if (period === 1) {
        date.startOf("week");
        format = "w";

    } else if (period === 2) {
        date.startOf("month");
        format = "MMMM";

    } else if (period === 3) {
        date.startOf("year");
        format = "YYYY";

    } else if (period === 4) {
        return "null";
    }

    return date.format(format);
};

export let formatChartPeriod = function(period, dateTime) {
    let date = moment(dateTime);

    let format;
    if (period === 0) {
        date.startOf("day");
        format = "L";

    } else if (period === 1) {
        date.startOf("week");
        format = "YYYY-w";

    } else if (period === 2) {
        date.startOf("month");
        format = "MMMM YYYY";

    } else if (period === 3) {
        date.startOf("year");
        format = "YYYY";

    } else if (period === 4) {
        return "null";
    }

    return date.format(format);

};

export let forEachPeriods = function(period, refDate) {
    let number;
    let unit;
    let date = moment(refDate);

    if (period === 0) {
        number = 1;
        unit = "days";
        date.startOf("day");

    } else if (period === 1) {
        number = 7;
        unit = "days";
        date.startOf("week");

    } else if (period === 2) {
        number = 1;
        unit = "months";
        date.startOf("month");

    } else if (period === 3) {
        number = 1;
        unit = "years";
        date.startOf("year");

    } else if (period === 4) {
        return [];
    }

    let result = [];
    for (let index = 0; index < 12; index++) {
        result.push(date.clone());
        date.subtract(number, unit);
    }
    return result.reverse();
};

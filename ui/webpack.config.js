let webpack = require("webpack");
let CopyWebpackPlugin = require("copy-webpack-plugin");

const WID_SERVER_URL = process.env["npm_package_config_WID_SERVER_URL_" + process.env.npm_lifecycle_event]
                        || process.env.npm_package_config_WID_SERVER_URL;
const WID_UI_CONTEXT = process.env["npm_package_config_WID_UI_CONTEXT_" + process.env.npm_lifecycle_event]
                        || process.env.npm_package_config_WID_UI_CONTEXT;

module.exports = {
    entry: {
        bundle: "./src/index.js",
        sw: "./src/sw.js"
    },

    output: {
        path: __dirname + "/dist/",
        filename: "./[name].js"
    },

    devtool: "eval",

    module: {
        loaders: [
            {test: /\.tag$/, loader: "tag-loader"},
            {
                test: /\.css$/,
                use: [
                    {loader: "style-loader"},
                    {loader: "css-loader"}
                ]
            }
        ]
    },

    devServer: {
        historyApiFallback: {
            rewrites: [
                {from: /./, to: "/index.html"}
            ]
        }
    },

    plugins: [
        new webpack.optimize.ModuleConcatenationPlugin(),
        new CopyWebpackPlugin([
            {from: "./node_modules/font-awesome/css", to: "font-awesome/css"},
            {from: "./node_modules/font-awesome/fonts", to: "font-awesome/fonts"},
            {from: "src/assets/", transform: function(content, path) {
                if (path.endsWith("index.html")) {
                    let data = content.toString();
                    return data.replace(/%%WID_UI_CONTEXT%%/g, WID_UI_CONTEXT);
                }
                return content;
            }}
        ]),
        new webpack.DefinePlugin({
            "window.WID_SERVER_URL": JSON.stringify(WID_SERVER_URL),
            "window.WID_UI_CONTEXT": JSON.stringify(WID_UI_CONTEXT)
        })
    ]
};

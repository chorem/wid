#To create container: docker build -t wid .
#To run the container: docker run -it --rm --name wid -p 9999:80 -v /<your path>/data:/var/local/wid/data/ wid
# the -p option allows to bind the port of the host (1st port) to the port of the container (2nd port)

FROM node:latest

RUN apt-get update && apt-get -y install apache2 && apt-get clean\
    && a2enmod proxy proxy_http proxy_wstunnel rewrite

RUN echo '\
ServerName localhost\n\
\n\
<Location /server>\n\
    ProxyPass http://localhost:9090\n\
    ProxyPassReverse http://localhost:9090\n\
</Location>\n\
\n\
<Location /ws>\n\
    ProxyPass ws://localhost:9090/ws\n\
    ProxyPassReverse ws://localhost:9090/ws\n\
</Location>\n\
\n\
<Directory "/var/www/html/">\n\
    RewriteEngine On\n\
    RewriteCond /var/www/html/%{REQUEST_URI} -f\n\
    RewriteRule ^(.+) $1 [L]\n\
    RewriteRule "^[^.]*$"  "/index.html" [L]\n\
</Directory>\n\
\n\
' > /etc/apache2/conf-enabled/wid.conf

EXPOSE 80

COPY ./ui/dist /var/www/html
COPY ./extension/dist /var/www/html

RUN mkdir -p /var/local/wid/data
RUN mkdir -p /var/local/wid/tmp
COPY ./server /var/local/wid/server
COPY ./native_modules /var/local/wid/native_modules

WORKDIR /var/local/wid/server
RUN rm -rf node_modules && npm install && npm install ../native_modules/predict && npm install ../native_modules/c5.0

CMD (service apache2 start&); npm start

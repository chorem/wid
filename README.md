# WID

What I did ?

You can follow your time with tags system. For example you put "java dev" during 2 hours.

The timing never stops. Put your time after you do the task, the hobbit, ...

You can use a public instance https://wid.chorem.com.

Contact us: wid-devel@list.chorem.org

## Screens

![main_screen](http://wid.chorem.com/assets/img/timeline.png)

## Timeline
* add time, put tag and tap enter
* show events (idle, push, ...)
* edit, edit old tags and you can add note
* use, use the same tag
* split, modify the time
* merge, create one big time with two times
* note, add and remove some complement information

## Reports
* create report
* show report on the timeline
* navigate on old reports
* copy tree report

## Tags
* test query research
* mass tags modification

## Configuration
* add/remove identities for push events
* add/remove application for push events
* synchronization on Timebundle project
* import jtimer
* import/export json

## Extension Chrome
* idle detection

## Installation
* You can run your own WID instance simply by doing the following
* launch server : cd server, npm install and then sudo npm start (do not forget sudo or else wid won't have rights to create databases for users)
* launch web client : cd ui, run npm install and then npm start
* your WID instance is now running on 0.0.0.0:8080 (if you did not change the defaults)
* signup (a sqlite database will be automatically created at /var/local/wid/data/<username>.sqlite)
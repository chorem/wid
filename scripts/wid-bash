#!/bin/bash
#
# curl must be present
#
# you must source this file from $HOME/.bashrc
# $WID_WEBHOOK_URL must be set with wid web hook url
# $WID_EXCLUDE_COMMAND can be used to don't trak some command example: '^(ls|ll)(\s[^|]*$|$)|cd(\s|$)'
# $WID_EXCLUDE_PATH can be used to don't trak some action done in directory path
#
# All command and path not excluded are sent
#
# other possible configuration:
#
# LOCAL_WID_DEVICE default $HOSTNAME
# LOCAL_WID_CURL default curl: curl path
# LOCAL_WID_EVENTS_FILE default $HOME/.wid-events.json: json storage when server unavailable
#

widbash_postinstall() {
    if [ ! -e "$HOME/.bashrc" -o -z "$(grep "$BASH_SOURCE" $HOME/.bashrc)" ]; then
        echo -e "\n#automaticaly added by widbash post_install\nsource $BASH_SOURCE" >> "$HOME/.bashrc"
        echo -e "'source $BASH_SOURCE' added to your ~/.bashrc\n"
    fi
    if [ ! -e "$HOME/.ssh/config" -o -z "$(grep --no-messages 'SendEnv WID_*' $HOME/.ssh/config)" ]; then
        mkdir -p "$HOME/.ssh"
        echo -e '\n#automaticaly added by widbash post_install\nSendEnv WID_*' >> "$HOME/.ssh/config"
        echo -e "'SendEnv WID_*' added to your ~/.ssh/config\n"
    fi
    if [ "$UID" = 0 -a -e "/etc/ssh/sshd_config" ]; then
        if [ -z "$(grep --no-messages 'AcceptEnv WID_*' /etc/ssh/sshd_config)" ]; then
            echo -e '\n#automaticaly added by widbash post_install\nAcceptEnv WID_*' >> "/etc/ssh/sshd_config"
            echo -e "'AcceptEnv WID_*' added to /etc/ssh/sshd_config\n"
            # reload ssh, some distribution use sshd and other just ssh
            local SSH_SERVICE=$(type systemctl &> /dev/null && systemctl --no-legend list-units ssh*.service |cut -f1 -d' ')
            if [ -n $"$SSH_SERVICE" ]; then
                systemctl reload "$SSH_SERVICE"
            else
                echo -e "Can't reload ssh server, this system don't use systemctl or sshd not running\n"
            fi
        fi
    else
        echo -e "Only root can reconfigure ssh server. Please ask root to add\n'AcceptEnv WID_*' to '/etc/ssh/sshd_config/etc/ssh/sshd_config' file\nand reload ssh server\n"
    fi
    type curl &> /dev/null || echo -e "WARNING: curl is not installed, please install curl to use widbash"
}

if [ "$BASH_SOURCE" = "$0" ]; then
    # script is executed and not sourced
    if [ "$1" = "post_install" ]; then
        widbash_postinstall
    else
        echo "$0 must be sourced in your .bashrc"
    fi
    exit 0
fi

LOCAL_WID_SHELL="${BASH##*/}"
if [ "$LOCAL_WID_SHELL" != "bash" ]; then
    echo "widbash script only work with bash !!"
    return
fi

LAST_COMMAND_NUMBER=""

widbash_check() {
    if [ -n "WID_WEBHOOK_URL" -a "$PROMPT_COMMAND" = "pre_wid_command" ]; then
        echo "widbash correctly installed and work"
    elif [ -z "WID_WEBHOOK_URL" ]; then
        echo "widbash can't work without WID_WEBHOOK_URL configured"
    else
        echo "widbash does not seem to be installed, try to launch 'widbash_postinstall' or login again and recheck"
    fi
}

widbash_install() {
    local WIDBASH_SCRIPT="$BASH_SOURCE"
    local DEST_FILE=$(basename "$WIDBASH_SCRIPT")
    for h in "$@"; do
        echo "widbash try to install '$WIDBASH_SCRIPT' on: '$h'"
        scp "$WIDBASH_SCRIPT" "$h:$DEST_FILE"
        ssh "$h" "bash \$HOME/$DEST_FILE post_install"
        echo "widbash '$WIDBASH_SCRIPT' installed on: '$h'"
    done
}

send_json() {
    [ "$WID_DEBUG" = "true" ] && echo "--> send json"
    local md5=$(echo $WID_WEBHOOK_URL| md5sum)
    local defaultEventsFile="$HOME/.wid-events-${md5%% *}.json"
    LOCAL_WID_CURL="${LOCAL_WID_CURL:-curl}"
    LOCAL_WID_EVENTS_FILE="${LOCAL_WID_EVENTS_FILE:-$defaultEventsFile}"

    local json
    if [ -e $LOCAL_WID_EVENTS_FILE ]; then
        local tmpfile="${TMPDIR:-/tmp}/wid-events-$$.json"
        # move file to prevent other shell append information during send
        mv -f $LOCAL_WID_EVENTS_FILE $tmpfile
        json="$(cat $tmpfile)"
    fi
    # json reading from file already endind with ',''
    json="${json}${1}"
    [ "$WID_DEBUG" = "true" ] && echo "--> $LOCAL_WID_CURL send [${json}]"
    if type "$LOCAL_WID_CURL" &> /dev/null && resp=$($LOCAL_WID_CURL --data "[${json}]" --silent --output /dev/null --write-out "%{response_code}" --header "Content-Type: application/json" "$WID_WEBHOOK_URL";); then
        [ "$WID_DEBUG" = "true" ] && echo "--> $LOCAL_WID_CURL return $resp"
        if [ 200 -le $resp -a $resp -lt 300 ]; then
            # succes: remove json file
            # do nothing
            rm -f "$LOCAL_WID_EVENTS_FILE.log"
        else
            # faild: add json to stock, with endind ','
            echo "$json," >> "$LOCAL_WID_EVENTS_FILE"
            echo "$(date) $resp" > "$LOCAL_WID_EVENTS_FILE.log"
            type jq &> /dev/null && echo "[$(cat $LOCAL_WID_EVENTS_FILE)'']" |jq '.' > /dev/null 2>> "$LOCAL_WID_EVENTS_FILE.log"
        fi
    else
        # faild: add json to stock, with endind ','
        echo "$json," >> $LOCAL_WID_EVENTS_FILE
        echo "$(date) $(type $LOCAL_WID_CURL) $resp" > "$LOCAL_WID_EVENTS_FILE.log"
        type jq &> /dev/null && echo "[$(cat $LOCAL_WID_EVENTS_FILE)'']" |jq '.' > /dev/null 2>> "$LOCAL_WID_EVENTS_FILE.log"
    fi
    # always remove temp file
    rm -f $tmpfile
}

pre_wid_command() {
    # must be first
    local EXIT_STATUS=$?
    # must be second
    local WID_DURATION=$(($SECONDS - $LOCAL_WID_TIMER))
    local historyNumber=$(fc -l -0 | cut -f1)
    [ "$WID_DEBUG" = "true" ] && echo "--> $EXIT_STATUS $WID_DURATION - -n '$WID_WEBHOOK_URL' -a '$historyNumber' != '$LAST_COMMAND_NUMBER'"
    if [ -n "$WID_WEBHOOK_URL" -a -n "$LAST_COMMAND_NUMBER" -a "$historyNumber" != "$LAST_COMMAND_NUMBER" ]; then
        local command=$(echo $(fc -ln -0) | grep -vEe "${WID_EXCLUDE_COMMAND:-zzzzzzzzz}")
        local path=$(echo "$PWD" | grep -vEe "${WID_EXCLUDE_PATH:-zzzzzzzzz}")
        if [ -n "$command" -a -n "$path" ]; then
            local LOCAL_WID_DEVICE="${LOCAL_WID_DEVICE:-$HOSTNAME}"
            command=${command//\\/\\\\}
            command=${command//\"/\\\"}
            local program="${command%% *}" # remove arguments
            program="${program##*/}"       # remove path
            local NOW="$(date +%s)000"
            local json=$(cat << EOF
{
    "beginDateTime": $(($NOW - ${WID_DURATION:-0} * 1000)),
    "endDateTime": $NOW,
    "type": "shell",
    "source": "widbash",
    "data": {
        "device": "${LOCAL_WID_DEVICE//\"/\\\"}",
        "user": "$(whoami)",
        "command": "${command}",
        "program": "${program}",
        "path": "${path//\"/\\\"}",
        "shell": "${LOCAL_WID_SHELL//\"/\\\"}",
        "duration": $WID_DURATION,
        "status": $EXIT_STATUS
    }
}
EOF
)
            (send_json "$json" &)
        fi
    fi
    # All time keep LAST_COMMAND_NUMBER
    LAST_COMMAND_NUMBER=$historyNumber

    # because unset LOCAL_WID_TIMER must be last command, other PROMPT_COMMAND must be call here
    [ -n "$LOCAL_WID_OLD_PROMPT_COMMAND" ] && $LOCAL_WID_OLD_PROMPT_COMMAND

    # hack, must be unset at end, because command in pre_wid_command assign it again
    unset LOCAL_WID_TIMER
}

if [ "$PROMPT_COMMAND" != "pre_wid_command" ]; then
    LOCAL_WID_OLD_PROMPT_COMMAND="$PROMPT_COMMAND"

    # hack to execute command before each command line
    trap 'LOCAL_WID_TIMER=${LOCAL_WID_TIMER:-$SECONDS}' DEBUG
    PROMPT_COMMAND="pre_wid_command"
fi

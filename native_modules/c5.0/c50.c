#include <node_api.h>
#include <stdio.h>
#include <string.h>
#include "./src/c50.c"

napi_value Method(napi_env env, napi_callback_info info) {
  napi_value result;

  size_t argc = 2;
  napi_value args[2];
  napi_get_cb_info(env, info, &argc, args, NULL, NULL);

  // Retrieve the length of the path string
  size_t pathSize;
  napi_get_value_string_utf8(env, args[0], NULL, 0, &pathSize);

  // Retrieve the path
  String path = (char *) malloc(pathSize + 1);
  size_t pathCopied;
  napi_get_value_string_utf8(env, args[0], path, pathSize + 1, &pathCopied);

  // Retrieve the length of the path string
  size_t outputSize;
  napi_get_value_string_utf8(env, args[1], NULL, 0, &outputSize);

  // Retrieve the path
  String output = (char *) malloc(outputSize + 1);
  size_t outputCopied;
  napi_get_value_string_utf8(env, args[1], output, outputSize + 1, &outputCopied);

  // Call c5.0 with the given path and output
  char *Argv[] = {"./c50", "-f", path, "-b", "-o", output};
  int Argc =  sizeof(Argv) / sizeof(Argv[0]);
  int32_t test = call(Argc, Argv);

  // Flush file
  fflush(Of);

  napi_create_int32(env, test, &result);
  return result;
}

#define DECLARE_NAPI_METHOD(name, func)                          \
  { name, 0, func, 0, 0, 0, napi_default, 0 }

napi_value Init(napi_env env, napi_value exports) {
  napi_status status;
  napi_property_descriptor desc = DECLARE_NAPI_METHOD("c50", Method);

  status = napi_define_properties(env, exports, 1, &desc);
  assert(status == napi_ok);

  return exports;
}

NAPI_MODULE(c50, Init)

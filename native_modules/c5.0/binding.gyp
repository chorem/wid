{
  "targets": [
    {
      "target_name": "c50",
      'libraries': ['-lm'],
      'cflags': ['-g', '-Wall', '-DVerbOpt', '-O0'],
      "sources": [
            "./src/global.c",
            "./c50.c",
      	    "./src/construct.c",
      	    "./src/formtree.c",
      	    "./src/info.c",
      	    "./src/discr.c",
      	    "./src/contin.c",
      	    "./src/subset.c",
      	    "./src/prune.c",
      	    "./src/p-thresh.c",
      	    "./src/trees.c",
      	    "./src/siftrules.c",
      	    "./src/ruletree.c",
      	    "./src/rules.c",
      	    "./src/getdata.c",
      	    "./src/implicitatt.c",
      	    "./src/mcost.c",
      	    "./src/confmat.c",
      	    "./src/sort.c",
      	    "./src/update.c",
      	    "./src/attwinnow.c",
      	    "./src/classify.c",
      	    "./src/formrules.c",
      	    "./src/getnames.c",
      	    "./src/modelfiles.c",
      	    "./src/utility.c",
      	    "./src/xval.c"
        ]
    }
  ]
}

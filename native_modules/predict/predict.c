#include <node_api.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "./src/sample.c"

napi_value Method(napi_env env, napi_callback_info info) {
    napi_value result;

    size_t argc = 2;
    napi_value args[2];
    napi_get_cb_info(env, info, &argc, args, NULL, NULL);

    // Retrieve the length of the path string
    size_t pathSize;
    napi_get_value_string_utf8(env, args[0], NULL, 0, &pathSize);

    // Retrieve the path
    String path = (char *) malloc(pathSize + 1);
    size_t pathCopied;
    napi_get_value_string_utf8(env, args[0], path, pathSize + 1, &pathCopied);

    // Retrieve the length of the path string
    size_t outputSize;
    napi_get_value_string_utf8(env, args[1], NULL, 0, &outputSize);

    // Retrieve the path
    String output = (char *) malloc(outputSize + 1);
    size_t outputCopied;
    napi_get_value_string_utf8(env, args[1], output, outputSize + 1, &outputCopied);

    // Call predict with the given path and return the result
    char *Argv[] = {"./sample", "-f", path, "-o", output};
    int Argc =  sizeof(Argv) / sizeof(Argv[0]);
    String prediction = predict(Argc, Argv);
    napi_create_string_utf8(env, prediction, strlen(prediction), &result);

    // Flush file
    fflush(Of);
    return result;
}

#define DECLARE_NAPI_METHOD(name, func)                          \
  { name, 0, func, 0, 0, 0, napi_default, 0 }

napi_value Init(napi_env env, napi_value exports) {
  napi_property_descriptor desc = DECLARE_NAPI_METHOD("predict", Method);
  napi_define_properties(env, exports, 1, &desc);
  return exports;
}

NAPI_MODULE(predict, Init)

/* global chrome*/

function saveOptions() {
    let serverUrl = document.getElementById("serverUrl").value;
    let deviceName = document.getElementById("deviceName").value;
    let idleTime = document.getElementById("idleTime").value;

    chrome.storage.sync.set({
        serverUrl,
        deviceName,
        idleTime
    }, function() {
        let status = document.getElementById("status");
        status.textContent = "Options saved.";
        setTimeout(function() {
            status.textContent = "";
        }, 750);
    });
}

function restoreOptions() {
    chrome.storage.sync.get({
        serverUrl: "",
        deviceName: "",
        idleTime: 5
    }, function(items) {
        document.getElementById("serverUrl").value = items.serverUrl;
        document.getElementById("deviceName").value = items.deviceName;
        document.getElementById("idleTime").value = items.idleTime;
    });
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.getElementById("save").addEventListener("click", saveOptions);

/* global chrome*/

let serverUrl;
let idleTime;
let deviceName;
let activeTime;
let alarmTime;
const oneMinute = 60 * 1000;
const ALARM_PERIODE = 10 * 1000; // each 10s;
const ALARM_NAME = "idle-sleep-detection";

let sendIdle = function(idleDate) {
    fetch(serverUrl, {
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
        },
        method: "POST",
        body: JSON.stringify([{
            beginDateTime: idleDate,
            endDateTime: Date.now(),
            type: "idle",
            source: "chrome",
            data: {
                device: deviceName
            }
        }])
    });
};

let restartAlarm = function() {
    alarmTime = Date.now() + ALARM_PERIODE;
    chrome.alarms.create(ALARM_NAME, {
        when: alarmTime
    });
};

chrome.storage.sync.get({
    serverUrl: "",
    deviceName: "",
    idleTime: 5

}, function(items) {
    idleTime = +items.idleTime;
    serverUrl = items.serverUrl;

    chrome.alarms.onAlarm.addListener(alarm => {
        if (alarm.name === ALARM_NAME) {
            let now = Date.now();
            if (alarmTime && now > alarmTime + idleTime * oneMinute) {
                // sleep detection send idle
                sendIdle(alarmTime);
            }
            restartAlarm();
        }
    });

    restartAlarm();
    chrome.idle.setDetectionInterval(60 * idleTime); //seconds
    chrome.idle.onStateChanged.addListener(function(state) {
        if (!serverUrl) {
            return;
        }
        // Become active ?
        if (state === "active") {
            sendIdle(activeTime);
            restartAlarm();
        } else {
            // try to use alarmTime as begin inactive time, because
            // when computer waik up, inactive/active is fire at same time
            activeTime = (alarmTime || Date.now()) - idleTime * oneMinute;
            chrome.alarms.clear(ALARM_NAME);
        }
    });
});

chrome.storage.onChanged.addListener(function(changes) {
    for (let key in changes) {
        if ({}.hasOwnProperty.call(changes, key)) {

            let storageChange = changes[key];
            if (key === "serverUrl") {
                serverUrl = storageChange.newValue;

            } else if (key === "deviceName") {
                deviceName = storageChange.newValue;

            } else if (key === "idleTime") {
                idleTime = +storageChange.newValue;
                chrome.idle.setDetectionInterval(60 * idleTime); //seconds
            }
        }
    }
});

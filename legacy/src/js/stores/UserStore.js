import AppDispatcher from "../dispatcher/AppDispatcher";
import {EventEmitter} from "events";

let CHANGE_EVENT = "change";

class UserStore extends EventEmitter {

    constructor() {
        super();
        this._isAuth = false;
        this._user = null;
        this._pending = false;
    }

    getUser() {
        return this._user;
    }

    setUser({username, email}) {
        this._user = {
            username,
            email
        };
    }

    clearUser() {
        this._user = null;
    }

    getUser() {
        return this._user;
    }

    login() {
        this._isAuth = true;
    }

    logout() {
        this._isAuth = false;
    }

    isAuth() {
        return this._isAuth;
    }

    isSignOut() {
        return !this._isAuth && this._user !== null;
    }

    setPending(pending) {
        this._pending = pending;
    }

    isPending() {
        return this._pending;
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
}

let userStore = new UserStore();

AppDispatcher.register(function(action) {
    switch (action.type) {

    case "SIGNIN_DONE":
        userStore.login();
        userStore.setUser(action);
        userStore.emitChange();
        break;

    case "SIGNOUT_DONE":
        if (userStore.isAuth()) {
            userStore.logout();
            userStore.setUser({});
            userStore.emitChange();
        }
        break;

    case "SIGNUP_PENDING":
        userStore.clearUser();
        userStore.setPending(true);
        userStore.emitChange();
        break;

    case "SIGNUP_ERROR":
        userStore.setPending(false);
        userStore.emitChange();
        break;

    case "SIGNUP_DONE":
        userStore.setPending(false);
        userStore.setUser(action);
        userStore.emitChange();
        break;

    case "USER_LOGIN":
        userStore.login();
        userStore.setUser(action);
        userStore.emitChange();
        break;

    case "USER_LOGOUT":
        userStore.logout();
        userStore.emitChange();
        break;

    default:
        // Nothing to do
    }
});

export default userStore;

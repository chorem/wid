import AppDispatcher from "../dispatcher/AppDispatcher";
import {EventEmitter} from "events";

let CHANGE_EVENT = "change";

class IdentityStore extends EventEmitter {

    constructor() {
        super();
        this.init();
    }

    init() {
        this._identities = new Map();
    }

    getIdentities() {
        return Array.from(this._identities.values());
    }

    setIdentities(identities) {
        identities.forEach((identity) => {
            this.addIdentity(identity);
        });
    }

    addIdentity({id, identifier, valid}) {
        this._identities.set(id, {id, identifier, valid});
    }

    removeIdentity(id) {
        this._identities.delete(id);
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
}

let identityStore = new IdentityStore();

AppDispatcher.register(function(action) {
    switch (action.type) {

    case "GET_INDENTITIES":
        identityStore.setIdentities(action.rows);
        identityStore.emitChange();
        break;

    case "ADD_IDENTITY":
        identityStore.addIdentity(action);
        identityStore.emitChange();
        break;

    case "REMOVE_IDENTITY":
        identityStore.removeIdentity(action.id);
        identityStore.emitChange();
        break;

    case "SIGNOUT_DONE":
    case "USER_LOGOUT":
        identityStore.init();
        identityStore.emitChange();
        break;

    default:
        // Nothing to do
    }
});

export default identityStore;

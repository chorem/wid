import AppDispatcher from "../dispatcher/AppDispatcher";
import {EventEmitter} from "events";

let CHANGE_EVENT = "change";

class TimebundleStore extends EventEmitter {

    constructor() {
        super();
        this._projectUrls = [];
        this._tasks = [];
        this._queries = new Map();
        this._projectName = null;
    }

    setProjectName(name) {
        this._projectName = name;
    }

    getProjectName() {
        return this._projectName;
    }

    getProjects() {
        return this._projectUrls;
    }

    setProjects(projectUrls) {
        this._projectUrls = projectUrls;
    }

    addProject(projectUrl) {
        this._projectUrls.push({projectUrl});
    }

    setTasks(tasks) {
        this._tasks = [];
        Object.keys(tasks).forEach((url) => {
            let task = tasks[url];
            this._tasks.push({
                url,
                name: task.name
            });
        });
    }

    setQueries(tasks) {
        this._queries = new Map();
        tasks.forEach((task) => {
            this.setQuery(task);
        });
    }

    setQuery({id, taskUrl, query}) {
        this._queries.set(taskUrl, {id, query});
    }

    removeQuery(url) {
        this._queries.delete(url);
    }

    getTasks() {
        return this._tasks.map((task) => {
            return Object.assign({}, task, this._queries.get(task.url));
        });
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
}

let timebundleStore = new TimebundleStore();

AppDispatcher.register(function(action) {
    switch (action.type) {

    case "GET_PROJECTS":
        timebundleStore.setProjects(action.rows);
        timebundleStore.emitChange();
        break;

    case "GET_TASKS":
        timebundleStore.setQueries(action.rows);
        timebundleStore.emitChange();
        break;

    case "ADD_TASK":
        timebundleStore.setQuery(action);
        timebundleStore.emitChange();
        break;

    case "UPDATE_TASK":
        timebundleStore.setQuery(action);
        timebundleStore.emitChange();
        break;

    case "REMOVE_TASK":
        timebundleStore.removeQuery(action.taskUrl);
        timebundleStore.emitChange();
        break;

    case "GET_PROJECT":
        timebundleStore.setProjectName(action.project.name);
        timebundleStore.setTasks(action.tasks);
        timebundleStore.emitChange();
        break;

    case "ADD_PROJECT":
        timebundleStore.addProject(action.url);
        timebundleStore.emitChange();
        break;

    default:
        // Nothing to do
    }
});

export default timebundleStore;

import AppDispatcher from "../dispatcher/AppDispatcher";
import {EventEmitter} from "events";
let CHANGE_EVENT = "change";

class EventStore extends EventEmitter {

    constructor() {
        super();
        this._data = [];
    }

    getEvents(beginDateTime, endDateTime) {
        return this._data.filter((event) => {
            return event.beginDateTime >= beginDateTime && event.beginDateTime < endDateTime;
        });
    }

    setEvents(events, beginDateTime, endDateTime) {
        this._data = this._data.filter((event) => {
            return event.beginDateTime < beginDateTime || event.beginDateTime >= endDateTime;
        });
        this.updateEvents(events);
    }

    updateEvents(events) {
        events.forEach((newEvent) => {
            let event = this.createEvent(newEvent);
            this._data.unshift(event);
        });
    }

    createEvent(event) {
        let {id, value} = event;
        let json = JSON.parse(value);

        return Object.assign({id}, json);
    }

    removeEvent(id) {
        this._data = this._data.filter((event) => {
            return event.id !== id;
        });
    }

    emitChange(beginDateTime, endDateTime) {
        this.emit(CHANGE_EVENT, beginDateTime, endDateTime);
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
}

let eventStore = new EventStore();

AppDispatcher.register(function(action) {
    switch (action.type) {

    case "GET_EVENTS":
        eventStore.setEvents(action.rows, action.beginDateTime, action.endDateTime);
        eventStore.emitChange(action.beginDateTime, action.endDateTime);
        break;

    case "UPDATE_EVENTS":
        eventStore.updateEvents(action.events);
        eventStore.emitChange();
        break;

    case "ADD_ACTIVITY":
    case "SAVE_ACTIVITY":
    case "UPDATE_ACTIVITY":
    case "INSERT_ACTIVITY":
    case "MERGE_UP_ACTIVITY":
    case "MERGE_DOWN_ACTIVITY":
        eventStore.emitChange();
        break;

    case "REMOVE_EVENT":
        eventStore.removeEvent(action.id);
        eventStore.emitChange();
        break;

    default:
        // Nothing to do
    }
});

export default eventStore;

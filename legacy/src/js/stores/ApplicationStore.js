import AppDispatcher from "../dispatcher/AppDispatcher";
import {EventEmitter} from "events";

let CHANGE_EVENT = "change";

class ApplicationStore extends EventEmitter {

    constructor() {
        super();
        this.init();
    }

    init() {
        this._keys = new Map();
    }

    getKeys() {
        return Array.from(this._keys.values());
    }

    setKeys(keys) {
        keys.forEach((key) => {
            this.addKey(key);
        });
    }

    addKey({id, name, key}) {
        this._keys.set(id, {id, name, key});
    }

    removeKey(id) {
        this._keys.delete(id);
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
}

let applicationStore = new ApplicationStore();

AppDispatcher.register(function(action) {
    switch (action.type) {

    case "GET_KEYS":
        applicationStore.setKeys(action.rows);
        applicationStore.emitChange();
        break;

    case "ADD_KEY":
        applicationStore.addKey(action);
        applicationStore.emitChange();
        break;

    case "REMOVE_KEY":
        applicationStore.removeKey(action.id);
        applicationStore.emitChange();
        break;

    case "SIGNOUT_DONE":
    case "USER_LOGOUT":
        applicationStore.init();
        applicationStore.emitChange();
        break;

    default:
        // Nothing to do
    }
});

export default applicationStore;

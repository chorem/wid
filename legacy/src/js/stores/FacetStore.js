import AppDispatcher from "../dispatcher/AppDispatcher";
import {EventEmitter} from "events";

let CHANGE_EVENT = "change";

class FacetStore extends EventEmitter {

    constructor() {
        super();
        this.clear();
    }

    clear() {
        this._tag = null;
        this._values = [];
        this._map = new Map();
    }

    getValues() {
        return this._values;
    }

    getTag() {
        return this._tag;
    }

    getMap() {
        return this._map;
    }

    set(tag, values, replace) {
        this._tag = tag;
        this._replace = replace;
        this._values = values;
        this._map = new Map();

        values.forEach((value) => {
            this._map.set(value.tag, null);
        });
    }

    open(tag, values) {
        // Remove path from the input
        let tags = tag.slice(0);
        if (this._tag.length) {
            tags = tag.slice(this._tag.length - 1);

            if (!this._replace) {
                tags.shift();
            }
        }

        let last = this._map;
        tags.forEach((value) => {
            let next = last.get(value);
            if (!next) {
                next = new Map();
                last.set(value, next);
            }
            last = next;
        });

        values.forEach((value) => {
            last.set(value.tag, null);
        });
    }

    close(tag) {
        // Remove path from the input
        let tags = tag.slice(0);
        if (this._tag.length) {
            tags = tag.slice(this._tag.length - 1);

            if (!this._replace) {
                tags.shift();
            }
        }

        let lastTag = tags.pop();

        let last = this._map;
        tags.forEach((value) => {
            let next = last.get(value);
            if (!next) {
                next = new Map();
                last.set(value, next);
            }
            last = next;
        });

        last.set(lastTag, null);
    }

    isReplace() {
        return this._replace === true;
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
}

let facetStore = new FacetStore();

AppDispatcher.register(function(action) {
    switch (action.type) {

    case "GET_FACETS":
        facetStore.set(action.tag, action.rows, action.replace);
        facetStore.emitChange();
        break;

    case "OPEN_FACETS":
        facetStore.open(action.tag, action.rows, action.replace);
        facetStore.emitChange();
        break;

    case "CLOSE_FACETS":
        facetStore.close(action.tag);
        facetStore.emitChange();
        break;

    case "CLEAR_FACETS":
    case "SIGNOUT_DONE":
    case "USER_LOGOUT":
        facetStore.clear();
        facetStore.emitChange();
        break;

    default:
        // Nothing to do
    }
});

export default facetStore;

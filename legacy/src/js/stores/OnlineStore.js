import AppDispatcher from "../dispatcher/AppDispatcher";
import {EventEmitter} from "events";

let CHANGE_EVENT = "change";

class OnlineStore extends EventEmitter {

    constructor() {
        super();
        this._online = window.online;
    }

    setOffline() {
        this._online = false;
    }

    setOnline() {
        this._online = true;
    }

    isOffline() {
        return !this._online;
    }

    isOnline() {
        return this._online;
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
}

let onlineStore = new OnlineStore();

AppDispatcher.register(function(action) {
    switch (action.type) {

    case "ONLINE":
        onlineStore.setOnline();
        onlineStore.emitChange();
        break;

    case "OFFLINE":
        onlineStore.setOffline();
        onlineStore.emitChange();
        break;

    default:
        // Nothing to do
    }
});

export default onlineStore;

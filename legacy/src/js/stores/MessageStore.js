import AppDispatcher from "../dispatcher/AppDispatcher";
import {EventEmitter} from "events";

let CHANGE_EVENT = "change";

class MessageStore extends EventEmitter {

    get TYPE_MESSAGE() {
        return "message";
    }
    get TYPE_ERROR() {
        return "error";
    }
    get TYPE_INFO() {
        return "info";
    }
    get TYPE_PENDING() {
        return "pending";
    }
    get TYPE_PERMANENT() {
        return "permanent";
    }

    constructor() {
        super();
        this.clear();
        this._type = null;
    }

    clear() {
        if (this._type !== this.TYPE_PERMANENT) {
            this._message = "";
        }
    }

    setError(error) {
        if (this._type !== this.TYPE_PERMANENT) {
            this._message = error;
            this._type = this.TYPE_ERROR;
        }
    }

    setMessage(message) {
        if (this._type !== this.TYPE_PERMANENT) {
            this._message = message;
            this._type = this.TYPE_MESSAGE;
        }
    }

    setInfo(message) {
        if (this._type !== this.TYPE_PERMANENT) {
            this._message = message;
            this._type = this.TYPE_INFO;
        }
    }

    setPending(message) {
        if (this._type !== this.TYPE_PERMANENT) {
            this._message = message;
            this._type = this.TYPE_PENDING;
        }
    }

    setPermanent(message) {
        this._message = message;
        this._type = this.TYPE_PERMANENT;
    }

    getMessage() {
        return this._message;
    }

    getType() {
        return this._type;
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
}

let messageStore = new MessageStore();

AppDispatcher.register(function(action) {
    switch (action.type) {

    case "SIGNIN_ERROR":
        messageStore.setError("Invalid username or password.");
        messageStore.emitChange();
        break;

    case "SIGNUP_PENDING":
        messageStore.setPending("Please wait, your account is creating.");
        messageStore.emitChange();
        break;

    case "SIGNUP_DONE":
        messageStore.setMessage("Your account is created.");
        messageStore.emitChange();
        break;

    case "CHANGE_PASSWD_ERROR_CONFIRM":
    case "SIGNUP_ERROR_CONFIRM":
        messageStore.setError("The password and the confirmation is not the same.");
        messageStore.emitChange();
        break;

    case "RESET_PASSWD_DONE":
        messageStore.setMessage("An mail with a new password has been sent.");
        messageStore.emitChange();
        break;

    case "RESET_PASSWD_ERROR":
        messageStore.setError("Invalid email.");
        messageStore.emitChange();
        break;

    case "CHANGE_PASSWD_DONE":
        messageStore.setMessage("The password is modified.");
        messageStore.emitChange();
        break;

    case "CHANGE_PASSWD_ERROR":
        messageStore.setError("Old password is invalid.");
        messageStore.emitChange();
        break;

    case "SYNC_OFFLINE":
        messageStore.setInfo("Syncronization pending.");
        messageStore.emitChange();
        break;

    case "ONLINE":
        messageStore.setInfo("You are online.");
        messageStore.emitChange();
        break;

    case "OFFLINE":
        messageStore.setInfo("You are offline !");
        messageStore.emitChange();
        break;

    case "SIGNUP_ERROR":
        messageStore.setError("Username already used.");
        messageStore.emitChange();
        break;

    case "IMPORT_PENDING":
        messageStore.setPending("Import is running.");
        messageStore.emitChange();
        break;

    case "IMPORT_DONE":
        messageStore.setMessage("Import finished.");
        messageStore.emitChange();
        break;

    case "IMPORT_ERROR":
        messageStore.setMessage("Error during import.");
        messageStore.emitChange();
        break;

    case "GET_DETAILS_PENDING":
        messageStore.setPending("Loading report.");
        messageStore.emitChange();
        break;

    case "CLEAR_MESSAGE":
        messageStore.clear();
        messageStore.emitChange();
        break;

    case "SEARCH_PENDING":
        messageStore.setPending("Searching...");
        messageStore.emitChange();
        break;

    case "UPDATE_PENDING":
        messageStore.setPending("Updating...");
        messageStore.emitChange();
        break;

    case "SEARCH_ERROR":
        messageStore.setError("Error during search.");
        messageStore.emitChange();
        break;

    case "UPDATE_ERROR":
        messageStore.setError("Error during update.");
        messageStore.emitChange();
        break;

    case "UPDATE_ROWS":
        messageStore.setMessage("Update done.");
        messageStore.emitChange();
        break;

    case "TIMEBUNDLE_PENDING":
        messageStore.setPending("Synchronization running...");
        messageStore.emitChange();
        break;

    case "TIMEBUNDLE_ERROR":
        messageStore.setError("Error during synchronization with Timebundle.");
        messageStore.emitChange();
        break;

    case "TIMEBUNDLE_DONE":
        messageStore.setMessage("Synchronization done.");
        messageStore.emitChange();
        break;

    case "COPY_DONE":
        messageStore.setMessage("Copied into clipboard.");
        messageStore.emitChange();
        break;

    case "ADD_IDENTITY":
        messageStore.setMessage("An email has been sent. Please valid the new identity.");
        messageStore.emitChange();
        break;

    case "ADD_IDENTITY_ERROR":
        messageStore.setMessage("Email already used.");
        messageStore.emitChange();
        break;

    case "VALIDATE_IDENTITY":
        messageStore.setMessage("Email validated.");
        messageStore.emitChange();
        break;

    case "VALIDATE_IDENTITY_ERROR":
        messageStore.setMessage("Error during email validation.");
        messageStore.emitChange();
        break;

    case "ACTIVITY_ERROR":
        messageStore.setError("Server error.");
        messageStore.emitChange();
        break;

    case "UPDATEREADY_DONE":
        messageStore.setPermanent("A new version is available. Please reload your page? <a onclick='location.reload()'>reload</a>");
        messageStore.emitChange();
        break;

    default:
        if (messageStore.getType() === messageStore.TYPE_PENDING) {
            setTimeout(() => {
                messageStore.clear();
                messageStore.emitChange();
            }, 350); // Keep the message valid during the transition
        }
        break;
    }
});

export default messageStore;

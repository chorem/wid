/**
 * Store how the UI should be displaying.
 */

let DISPLAY_CONFIG_KEY = "wid.config.display";

export let getDisplayConfig = () => {
    let config = localStorage.getItem(DISPLAY_CONFIG_KEY);
    return JSON.parse(config) || {};
};

export let setDisplayConfig = (name, value) => {
    let config = getDisplayConfig();
    config[name] = value;
    localStorage.setItem(DISPLAY_CONFIG_KEY, JSON.stringify(config));
};

export let hashCode = function(str) {
    let hash = 0, i, chr, len;
    if (str.length === 0) {
        return hash;
    }

    for (i = 0, len = str.length; i < len; i++) {
        chr = str.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};

export let tagsToArray = function(tagsAsString, removeNotes) {
    let string = tagsAsString;
    if (removeNotes) {
        string = string.replace(/\(.*?(\)|$)/g, " ");
    }
    string = string.trim().replace(/\s+/g, " ");

    if (string) {
        return string.split(" ");
    }
    return [];
};

export let tagsToString = function(tagsAsArray) {
    return tagsAsArray.join(" ");
};

export let addTag = function(tagsAsString, tag, deleteCount = 0) {
    // Remove all space separator
    let string = tagsAsString.replace(/\s+/g, " ");

    // Add tag
    let tags = tagsToArray(string);
    if (deleteCount > 0) {
        tags.splice(-deleteCount, deleteCount, tag);
    } else {
        tags.push(tag);
    }

    // Add space after last tag
    tags.push("");

    // Return the string from the array
    return tagsToString(tags);
};

export let extractNote = function(input) {
    let match = input.match(/\(.*?(\)|$)/g);

    if (match) {
        return match.map((note) => {
            return note.replace(/\s+/g, " ")
                    .replace(/\)\(/g, " ")
                    .replace(/\)|\(/g, "");
        });
    }
    return match;
};

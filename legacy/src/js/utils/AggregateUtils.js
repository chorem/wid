/* eslint eqeqeq: 0 */
let uuid = require("uuid");
import moment from "moment";
import React from "react";
import Markdown from "react-remarkable";
import {diffValue} from "../utils/DateTimeUtils";

/**
 * Aggration Object is:
 * {
 *   name: "String",              // "allNote"
 *   selection: "String",         // ex: "NOTE"
 *   transforms: [Array of transform] // ex: [{regexp: true, substr: ".*project:([^ ]).*", newSubStr: "$1"]
 *   aggregateFunction: "String", // "concat"
 *   aggregateParams: [Array]     // ["\n"]
 * }
 */
class AggregateHelper {

    constructor(events) {
        this.functions = {
            json: function(s) {
                return JSON.parse(s);
            },
            markdown: function(s) {
                return (<Markdown source={s || ""} />);
            },
            formatDuration(duration) {
                return diffValue(parseFloat(duration));
            },
            parseDate(s, {format}) {
                let m = moment(s, format);
                return m.toDate();
            },
            formatDate(date, {format = "YYYY-MM-DD HH:mm"}) {
                let m = moment(date);
                return m.format(format);
            }
        };
        this.aggregateFunction = {
            count: function(subEvents) {
                return subEvents.length;
            },
            duration: function(subEvents) {
                let duration = 0;
                subEvents.forEach(e => {
                    duration += e.endDateTime - e.beginDateTime;
                });
                return duration;
            },
            concat: function(subEvents, dataSource, {prefix = "", join = "", suffix = ""}) {
                let values = new Set();
                subEvents.forEach(e => {
                    values.add(prefix + this._eval(dataSource, e) + suffix);
                });
                let result = [...values].join(join);

                return result;
            },
            sum: function(subEvents, dataSource) {
                let result = 0;
                subEvents.forEach(e => {
                    result += parseFloat(this._eval(dataSource, e) || 0);
                });

                return result;
            },
            avg: function(subEvents, dataSource) {
                let result = 0;
                subEvents.forEach(e => {
                    result += parseFloat(this._eval(dataSource, e) || 0);
                });

                return (result / subEvents.length).toFixed(2);
            },
            city: function(subEvents, dataSource = `{   "place": "\${data.place}",
                                                        "latitude": \${data.latitude},
                                                        "longitude": \${data.longitude}
                                                    }`) {
                let id = uuid.v1();

                let latlon = subEvents.map(e => {
                    return this._eval(dataSource, e);
                });
                let data;
                try {
                    data = JSON.parse(latlon[0]) || [];
                } catch (eee) {
                    data = [];
                }
                let html = `
                <html>
                <span id=${id}>
                    <a href="http://www.openstreetmap.org/?mlat=${data.latitude}&mlon=${data.longitude}#map=16/${data.latitude}/${data.longitude}">${data.place || "map"}</a>
                </span>
                <script type="text/javascript">
                function putCity(id, latlonList) {
                    let cities = new Map();

                    let update = function(citiesMap) {
                        let span = document.getElementById(id);
                        if (span) {
                            span.innerHTML = [...citiesMap.values()].map(data => {
                                return '<a href="http://www.openstreetmap.org/?mlat='
                                + data.lat + '&mlon=' + data.lng + '#map=16/'
                                + data.lat +'/' + data.lng + '">' + data.name + '</a>'
                            }).join(", ");
                        }
                    };

                    latlonList.forEach(({place, latitude, longitude}) => {
                        if (place) {
                            cities.set(place, {name: place, lat: latitude, lng: longitude});
                        } else {
                            fetch("//geonames.chorem.com/findNearbyPlaceNameJSON?style=SHORT&cities=cities1000&lat="
                            + latitude.toFixed(3) + "&lng=" + longitude.toFixed(3)).catch(() => {
                                return null;
                            }).then(response => {
                                if(response && response.ok) {
                                    return response.json().catch(() => null);
                                } else {
                                    return null;
                                }
                            }).then(json => {
                                if (json) {
                                    cities.set(json.geonames[0].name, {name: json.geonames[0].name, lat: latitude, lng: longitude});
                                    update(cities);
                                }
                            });
                        }
                    });
                    cities.size && update(cities);
                }
                putCity("${id}", [${latlon.join(",")}]);
                </script>
                </html>
                `;
                return html;
            },
            map: function(subEvents, dataSource = "", args) {
                if (subEvents.length < (args.thresholdPath || 5)) {
                    return this.aggregateFunction.mapPoint.call(this, subEvents, dataSource, args);
                }
                return this.aggregateFunction.mapPath.call(this, subEvents, dataSource, args);
            },
            mapPoint: function(subEvents, dataSource = "", {details = "", latitude = "${data.latitude}", longitude = "${data.longitude}"}) {
                let points = subEvents.map(e => {
                    let lat = this._eval(latitude, e);
                    let lon = this._eval(longitude, e);
                    let text = this._details(details, e);
                    return `{text:"${text.replace(/"/g, "\\\"").replace(/\n/g, "\\n")}", lat:${lat}, lon:${lon}}`;
                });
                let id = uuid.v1();
                let html = `
                <html>
                <div id="${id}" style="width: 800px; height: 600px">
                    <div id="${id}-intercalaire" style="text-align: center; line-height: 600px; font-weight: bold; border: 1px dotted grey; background-color: #eee;">
                        Map currently unavailable.
                    </div>
                </div>
                <script type="text/javascript">
                function createMapPoint(id, data) {
                    let map = L.map(id);

                    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    }).addTo(map);

                    // create a red polyline from an array of LatLng points
                    var polyline = L.polyline(data);
                    // zoom the map to the polyline
                    map.fitBounds(polyline.getBounds());

                    // add each points
                    data.forEach(p => {
                        let marker = L.marker([p.lat, p.lon]);
                        marker.addTo(map).bindPopup("<span>" + p.text + "</span>");
                    });
                }
                createMapPoint("${id}", [${points.join(",")}]);
                </script>
                </html>
                `;
                return html;
            },
            mapPath: function(subEvents, dataSource = "", {details = "", latitude = "${data.latitude}", longitude = "${data.longitude}"}) {
                let points = subEvents.map(e => {
                    let lat = this._eval(latitude, e);
                    let lon = this._eval(longitude, e);
                    let text = this._details(details, e);
                    return `{text:"${text.replace(/"/g, "\\\"").replace(/\n/g, "\\n")}", lat:${lat}, lon:${lon}}`;
                });

                let id = uuid.v1();
                let html = `
                <html>
                <div id="${id}" style="width: 800px; height: 600px">
                    <div id="${id}-intercalaire" style="text-align: center; line-height: 600px; font-weight: bold; border: 1px dotted grey; background-color: #eee;">
                        Map currently unavailable.
                    </div>
                </div>
                <script type="text/javascript">
                function createMapPath(id, data) {
                    let map = L.map(id);

                    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    }).addTo(map);

                    // create a red polyline from an array of LatLng points
                    var polyline = L.polyline(data, {color: 'red'}).addTo(map);

                    // zoom the map to the polyline
                    map.fitBounds(polyline.getBounds());

                    var popup = L.popup();

                    var onMapClick = function(e) {
                        // get closest Point
                        let closest = data.reduce((old, point, index) => {
                            let cur = {index, point, dist: e.latlng.distanceTo(point)};
                            if (!old || cur.dist < old.dist) {
                                return cur;
                            } else {
                                return old;
                            }
                        }, null);

                        var p = closest.point;
                        popup.setLatLng(p).setContent("<span>" + p.text + "</span>").openOn(map);
                    }

                    polyline.on('click', onMapClick);

                }
                createMapPath("${id}", [${points.join(",")}]);
                </script>
                </html>
                `;

                return html;
            }
        };

        this.events = events;
        // contains id of used event by one or more aggregate
        this.usedEvents = new Set();
    }

    _toKey(event) {
        return event.id || JSON.stringify(event);
    }

    computeDisplay(display) {
        let events = this._selection(this.events, display.selection);
        let result = [];
        if (events.length) {
            events.forEach(e => {
                this.usedEvents.add(this._toKey(e));
                let aggResult = {};
                Object.keys(display.aggregates || {}).forEach(k => {
                    let value = this._aggregate(display.aggregates[k], [e]);
                    aggResult[k] = value;
                });
                let val = Object.assign({}, e, aggResult);
                if (typeof display.display === "function") {
                    e.display = display.display.call(this, val);
                } else {
                    e.display = this._eval(display.display || "", val);
                }
                e.tag = this._eval(display.tag || "", val);
                if (typeof display.details === "function") {
                    e.details = display.details.call(this, val);
                } else {
                    e.details = this._details(display.details || "", val);
                }
                result.push(e);
            });
        }
        return result;
    }

    /**
     * Create new events from
     *
     * @param {Aggregate} aggregate aggregate object with
     * - groupByValue if true aggregate must be done per distinct value
     * - aggregateFunction function name
     * - aggregateParams array of parameter for aggregateFunction
     */
    computeAggregate(aggregate) {
        let events = this._selection(this.events, aggregate.selection);
        let result = [];
        if (events.length) {
            events.forEach(e => {
                this.usedEvents.add(this._toKey(e));
            });

            let groups = [];
            if (!aggregate.groupBy) {
                groups.push(events);
            } else {
                let mapValue = new Map();
                events.forEach(e => {
                    let aggValue = {};
                    let key = Object.keys(aggregate.groupBy).map(k => {
                        let g = aggregate.groupBy[k];
                        let val = this._eval(g, e);
                        aggValue[k] = val;
                        return val;
                    });
                    let keyString = this._toKey(key);
                    let list = mapValue.get(keyString);
                    if (!list) {
                        list = [];
                        list.aggValue = aggValue;
                        mapValue.set(keyString, list);
                    }
                    list.push(e);
                });
                groups.push.apply(groups, [...mapValue.values()]);
            }

            result = groups.map(list => {
                let aggResult = list.aggValue || {};
                Object.keys(aggregate.aggregates || {}).forEach(k => {
                    let value = this._aggregate(aggregate.aggregates[k], list);
                    aggResult[k] = value;
                });
                let e = this._getDates(list);
                e.type = aggregate.name;
                e.source = "aggregation";
                e.events = list;
                e.aggregations = aggregate.aggregations || [];

                let val = Object.assign({}, e, aggResult);
                if (typeof aggregate.display === "function") {
                    e.display = aggregate.display.call(this, val);
                } else {
                    e.display = this._eval(aggregate.display || "", val);
                }
                e.tag = this._eval(aggregate.tag || "", val);
                if (typeof aggregate.details === "function") {
                    e.details = aggregate.details.call(this, val);
                } else {
                    e.details = this._details(aggregate.details || "", val);
                }
                return e;
            });
        }
        return result;
    }

    _eval(exp, event) {
        let result = exp.replace(/\$\{([^}]+)\}/g, (match, path) => {
            let functions = path.split("|");
            path = functions.shift();
            let val = this._getFieldValue(path, event);
            if (path[0] === "|" && functions.length) {
                val = val || event;
            }
            if (functions.length) {
                functions.forEach(fargs => {
                    let f, args;
                    [f, args] = fargs.split(/\((.*)\)/); // split and keep args
                    // transform in real JSON
                    args = args && args
                        .replace(/(['"])?([a-zA-Z0-9_]+)(['"])?\s*:/g, "\"$2\": ")
                        .replace(/:\s*'([^']*?)'/g, ": \"$1\"");
                    args = args && JSON.parse("{" + args + "}") || {};
                    let fn = this.functions[f];
                    if (fn) {
                        val = fn.call(this, val, args);
                    } else {
                        fn = this.aggregateFunction[f];
                        val = fn && fn.call(this, [val], undefined, args);
                    }
                });
            }
            return val;
        });
        return result;
    }

    _details(exp, event) {
        let result = "";
        if (exp) {
            result = this._eval(exp, event);
        } else if (event.data) {
            // details is not specified, create default representation
            Object.keys(event.data).forEach(field => {
                result += `* ${field} = ${event.data[field]}\n`;
            });
        }
        return result;
    }

    _getFieldValue(path, o) {
        let result = o[path];
        if (!result) {
            result = o;
            path.split(".").forEach(f => {
                result = result && result[f];
            });
        }
        return result || "";
    }

    /**
     * After multiple call to computeAggregate you can is this method to
     * retrieve all not used events
     */
    getUnusedEvent() {
        let beginDateTime;
        let endDateTime;

        let result = [];
        this.events.forEach(e => {
            if (!this.usedEvents.has(this._toKey(e))) {
                result.push(e);
                if (!beginDateTime || beginDateTime > e.beginDateTime) {
                    beginDateTime = e.beginDateTime;
                }
                if (!endDateTime || endDateTime < e.endDateTime) {
                    endDateTime = e.endDateTime;
                }
            }
        });

        return result;
    }

    _getDates(events) {
        let beginDateTime;
        let endDateTime;

        events.forEach(e => {
            if (!beginDateTime || beginDateTime > e.beginDateTime) {
                beginDateTime = e.beginDateTime;
            }
            if (!endDateTime || endDateTime < e.endDateTime) {
                endDateTime = e.endDateTime;
            }
        });

        return {beginDateTime, endDateTime};
    }

    _aggregate(agg, events) {
        let fn = this.aggregateFunction[agg.type] || function() {};
        let dataSource = agg.dataSource;
        let params = agg.params || {};

        let result = fn.call(this, events, dataSource, params);
        return result;
    }

    _selection(events, selection) {
        if (!selection || !selection.length) {
            return events;
        }

        let result = new Set();
        selection.forEach(and => {
            let sub = [...events];
            and.forEach(exp => {
                sub = this._evalCond(exp, sub);
            });
            sub.forEach(e => {
                result.add(e);
            });
        });
        return [...result];
    }

    _evalCond(exp, events) {
        let field = exp.replace(/-?(.*?)=(.*)/, "$1");
        let expectedValue = exp.replace(/-?(.*?)=(.*)/, "$2");
        let isNot = exp[0] === "-";

        let result = events.filter(e => {
            let value = this._getFieldValue(field, e);
            // don't use !== and ===, because we want conversion type
            return isNot && value != expectedValue || value == expectedValue;
        });

        return result;
    }
}

let defaultAggregateEvents = [
    {id: "a3447653-e4f0-4187-b5cd-eeabc5b58545",
        name: "application",
        selection: [["type=application"]],
        aggregates: {
            duration: {type: "duration"}
        },
        display: "${duration|formatDuration}",
        tag: "applications",
        aggregations: [
            {id: "298a25ec-0a25-4426-99d3-e8662be13982",
                name: "app duration",
                selection: [["type=application"]],
                groupBy: {
                    name: "${data.name}"
                },
                aggregates: {
                    duration: {type: "duration"}
                },
                display: "${name} ${data.title} (${duration|formatDuration})",
                tag: "${name}"
            }
        ]
    },
    {id: "901bb408-d467-4d14-be03-9f473b7fdb6d",
        name: "step sum",
        selection: [["type=step"]],
        aggregates: {
            sum: {type: "sum", dataSource: "${data.number}"}
        },
        display: "steps ${sum}",
        tag: "step"
    },
    {id: "3e62064c-4dd9-4b45-85eb-ef52e82adaf5",
        name: "notes",
        selection: [["type=note"]],
        display: "notes",
        tag: "notes"
    },
    {id: "9a4d0eb8-2b33-44bd-844b-fc638e9c9847",
        name: "idle",
        selection: [["type=idle"]],
        aggregates: {
            duration: {type: "duration"}
        },
        display: "${duration|formatDuration}",
        tag: "idle"
    },
    {id: "b323806c-a2cc-40ae-aebe-100cb8f9de85",
        name: "code",
        selection: [["type=code"]],
        groupBy: {
            project: "${data.project}"
        },
        display: "${project}",
        tag: "${project}",
        aggregations: [{
            id: "53875fa6-7fe7-4890-92f1-9a1539bcb141",
            name: "code",
            groupBy: {
                project: "${data.project}",
                branch: "${data.branch}"
            },
            display: "* ${branch}",
            tag: "${project} ${branch}"
        }]
    },
    {id: "ad9c447d-95e4-43a8-bb70-d0213dae27e0",
        name: "scm",
        selection: [["type=scm"], ["data.type=commit"]],
        groupBy: {
            project: "${data.project}"
        },
        display: "${project}",
        tag: "${project}"
    },
    {id: "caba5202-fa3d-42b0-b60c-d27b34886333",
        name: "shell",
        selection: [["type=shell"]],
        aggregates: {
            duration: {type: "duration"}
        },
        display: "${duration|formatDuration}",
        tag: "shell",
        aggregations: [
            {id: "9bf3d0d0-ba23-4d52-a91e-2a9e1bd4fda7",
                name: "shell",
                selection: [["type=shell"]],
                groupBy: {
                    device: "${data.device}"
                },
                aggregates: {
                    duration: {type: "duration"}
                },
                display: "${device} ${duration|formatDuration}",
                tag: "${device}"
            }
        ]
    },
    {id: "54e38ce3-809c-4ee5-bb75-b43f1f373489",
        name: "task",
        selection: [["type=task"]],
        aggregates: {
            duration: {type: "duration"}
        },
        display: "${duration|formatDuration}",
        tag: "tasks"
    },
    {id: "b991cb92-453c-4862-8724-4cf0922c6975",
        name: "call",
        selection: [["type=call"]],
        aggregates: {
            duration: {type: "duration"}
        },
        display: "${duration|formatDuration}",
        tag: "calls"
    },
    {id: "250476cb-d839-4693-ba5b-770449765781",
        name: "calendar",
        selection: [["type=calendar"]],
        aggregates: {
            duration: {type: "duration"}
        },
        display: "${duration|formatDuration}",
        tag: "calendars"
    },
    {id: "250476cb-d839-4693-ba5b-770449765781",
        name: "heartbeat",
        selection: [["type=heartbeat"]],
        aggregates: {
            avg: {type: "avg", dataSource: "${data.pulse}"}
        },
        display: "${avg}",
        tag: "heartbeats"
    },
    {id: "c8d09835-7c76-4d0b-aa0e-07ee9e8d776d",
        name: "location",
        selection: [["type=location"]],
        aggregates: {
            cities: {type: "city"},
            map: {type: "map"}
        },
        display: "${cities}",
        tag: "locations",
        details: "${map}"
    }
];

let defaultDisplayEvents = [
    {id: "ea91684d-0209-453e-b78b-3b5b5e38e81c",
        name: "scm",
        selection: [["type=scm"]],
        display: "* ${data.message}",
        tag: "${data.project} (${data.message})"
    },
    {id: "725664e8-69c6-422d-b598-5d8c86ba44de",
        name: "code",
        selection: [["type=code"]],
        display: "* ${data.file}",
        tag: "${data.project}"
    },
    {id: "b7a1884b-7a69-4958-8e06-9a214589e553",
        name: "idle",
        selection: [["type=idle"]],
        display: "* idle",
        tag: "idle"
    },
    {id: "ff75bde1-70fa-4272-9501-17723c651d30",
        name: "application",
        selection: [["type=application"]],
        display: "* ${data.name}",
        tag: "${data.name}"
    },
    {id: "b329568d-aa78-4d35-832b-a883a59629b5",
        name: "note",
        selection: [["type=note"]],
        display: "* ${data.content}",
        tag: "note"
    },
    {id: "394eda12-c128-48ab-a98a-a8a638a359c9",
        name: "shell",
        selection: [["type=shell"]],
        display: "* ${data.program}",
        tag: "${data.device}"
    },
    {id: "f21d56a7-24fa-4872-8044-acdcbfb4b6bd",
        name: "task",
        selection: [["type=task"]],
        display: "* [${data.status}] ${data.description}",
        tag: "${data.description}"
    },
    {id: "57afb029-5aed-4d79-9b1e-89faf9ec808c",
        name: "call",
        selection: [["type=call"]],
        display: "* [${data.type}] ${data.name} ${data.number}",
        tag: "${data.name} ${data.type}"
    },
    {id: "3ce5cf74-dc63-4b9e-b201-89194cab5348",
        name: "calendar",
        selection: [["type=calendar"]],
        display: "* ${data.title} ${data.description}",
        tag: "${data.title}"
    },
    {id: "62f3bacc-926f-4f71-8023-64a1381691e8",
        name: "step",
        selection: [["type=step"]],
        display: "* ${data.number}",
        tag: "step"
    },
    {id: "0b6951aa-c529-44f4-a727-004ea7b15453",
        name: "heartbeat",
        selection: [["type=heartbeat"]],
        display: "* ${data.pulse}",
        tag: "heartbeat"
    },
    {id: "76ab7c68-b924-4606-b09e-bf489b41b013",
        name: "location",
        selection: [["type=location"]],
        display: function(val) {
            return this.aggregateFunction.city.call(this, [val], undefined, {});
        },
        tag: "location",
        details: function(val) {
            return this.aggregateFunction.mapPoint.call(this, [val], undefined, {});
        }
    }
];

/**
 * @param {Array} events list of event to aggregate or generate display
 * @param {Array} aggregations list of aggregation to use, must be empty (not null) if we want to use displays
 * @param {Array} displays list of diplays to use, only used if aggregations is empty ([])
 * @return {Map} key is aggregation or display id, with exception for used events, key is 'unused'.
 *               value is Array of events generate by aggregation or display
 */
export let aggregateEvents = function(events, aggregations = defaultAggregateEvents, displays = defaultDisplayEvents) {
    let result = new Map();
    if (events && events.length > 0) {
        let ah = new AggregateHelper(events);
        let list;
        let method;
        if (aggregations.length > 0) {
            list = aggregations;
            method = "computeAggregate";
        } else {
            list = displays;
            method = "computeDisplay";
        }
        list.forEach(a => {
            let newEvents = ah[method](a);
            if (newEvents.length) {
                result.set(a.id, newEvents);
            }
        });
        let unused = ah.getUnusedEvent();
        if (unused.length) {
            result.set("unused", unused);
        }
    }
    return result;
};


// Usage sample:
//
//let events = [
//{ "beginDateTime": 1469295324322, "endDateTime": 1469295328605, "type": "application", "source": "widand", "data": {"device":"pouny", "application":"Accueil Xperia™"}},
//{ "beginDateTime": 1469295350530, "endDateTime": 1469295382203, "type": "application", "source": "widand", "data": {"device":"pouny", "application":"Réglages"}},
//{ "beginDateTime": 1469812103895, "endDateTime": 1469812104906, "type": "application", "source": "widand", "data": {"device":"pouny", "application":"Accueil Xperia™"}},
//{ "beginDateTime": 1469822219010, "endDateTime": 1469822220494, "type": "application", "source": "widand", "data": {"device":"pouny", "application":"Fennec F-Droid"}},
//{ "beginDateTime": 1469824100203, "endDateTime": 1469824100203, "type": "note", "source": "wid", "data": {"content":"une note"}},
//{ "beginDateTime": 1469824153919, "endDateTime": 1469824153919, "type": "note", "source": "wid", "data": {"content":"ma joli petite note :)"}},
//{ "beginDateTime": 1469836589202, "endDateTime": 1469836589862, "type": "truc", "source": "widand", "data": {"device":"pouny", "application":"IU système"}},
//{ "beginDateTime": 1469875917806, "endDateTime": 1469875920361, "type": "application", "source": "widand", "data": {"device":"pouny", "application":"Accueil Xperia™"}},

//    {"id":"ef7ce4f8-5369-11e6-8dd7-d56cd1eb7fac","source":"widand pouny step","value":"27","beginDateTime":1469559002041,"endDateTime":1469560305504},
//    {"id":"51cba5e0-5362-11e6-8dd7-d56cd1eb7fac","source":"EXTENSION","value":"idle","beginDateTime":1469558942557,"endDateTime":1469559247456},
//    {"id":"ef7ce4f7-5369-11e6-8dd7-d56cd1eb7fac","source":"widand pouny step","value":"81","beginDateTime":1469558393009,"endDateTime":1469559002041},
//    {"id":"9eadb990-5360-11e6-8dd7-d56cd1eb7fac","source":"EXTENSION","value":"idle","beginDateTime":1469558212125,"endDateTime":1469558556992},
//    {"id":"ef7ce4f6-5369-11e6-8dd7-d56cd1eb7fac","source":"widand pouny step","value":"72","beginDateTime":1469558073362,"endDateTime":1469558393009},
//    {"id":"35b3eb75-53f7-11e6-8dd7-d56cd1eb7fac","source":"widand pouny application","value":"Accueil_Xperia™","beginDateTime":1469623165833,"endDateTime":1469623175547},
//    {"id":"35b3eb74-53f7-11e6-8dd7-d56cd1eb7fac","source":"widand pouny application","value":"Agenda","beginDateTime":1469623143409,"endDateTime":1469623165833},
//    {"id":"35b3eb73-53f7-11e6-8dd7-d56cd1eb7fac","source":"widand pouny application","value":"Accueil_Xperia™","beginDateTime":1469623141212,"endDateTime":1469623143409},
//    {"id":"35b3eb72-53f7-11e6-8dd7-d56cd1eb7fac","source":"widand pouny application","value":"Messagerie","beginDateTime":1469623086801,"endDateTime":1469623141212},
//    {"id":"35b3eb71-53f7-11e6-8dd7-d56cd1eb7fac","source":"widand pouny application","value":"IU_système","beginDateTime":1469623085418,"endDateTime":1469623086801},
//    {"id":"35b3eb70-53f7-11e6-8dd7-d56cd1eb7fac","source":"widand pouny application","value":"Accueil_Xperia™","beginDateTime":1469623082313,"endDateTime":1469623085418},
//    {"id":"35b3c46b-53f7-11e6-8dd7-d56cd1eb7fac","source":"widand pouny application","value":"Agenda","beginDateTime":1469623060125,"endDateTime":1469623082313},
//    {"id":"35b3c46a-53f7-11e6-8dd7-d56cd1eb7fac","source":"widand pouny application","value":"Accueil_Xperia™","beginDateTime":1469623057345,"endDateTime":1469623060125},
//    {"id":"35b3c469-53f7-11e6-8dd7-d56cd1eb7fac","source":"widand pouny application","value":"Messagerie","beginDateTime":1469623022069,"endDateTime":1469623057345},
//    {"id":"35b3c468-53f7-11e6-8dd7-d56cd1eb7fac","source":"widand pouny application","value":"Accueil_Xperia™","beginDateTime":1469623018389,"endDateTime":1469623022069}
//];
//
// // if selection tag don't have specifier, default is 'source='
//let aggs=[
//    {name:"step sum", selection:"source=widand+step", aggregateFunction:"sum"},
//    {name:"NOTE", selection:"NOTE", aggregateFunction:"concat", aggregateParams:["\n"]},
//    {name:"idle", selection:"EXTENSION+value=idle", aggregateFunction:"duration"}
//];
//
//let aggEvents = aggregateEvents(events, aggs);

import {updateReady} from "../actions/MessageActions";

export let listenUpdateReady = () => {
    window.addEventListener("load", function() {

        window.applicationCache.addEventListener("updateready", function() {
            if (window.applicationCache.status === window.applicationCache.UPDATEREADY) {
                updateReady();
            }}, false);

    }, false);
};

window.currentServerVersion = null;
export let checkServerVersion = (serverVersion) => {
    if (!window.currentServerVersion) {
        window.currentServerVersion = serverVersion;
        return;
    }

    if (serverVersion !== window.currentServerVersion) {
        window.applicationCache.update();
    }
};

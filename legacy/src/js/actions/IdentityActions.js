import {callApi, processResponse} from "./ActionUtils";

export let addIdentity = function(identifier) {
    let location = window.location.origin + window.serverContext + "/config/identities";

    callApi(`/api/identity/add?identifier=${encodeURIComponent(identifier)}&location=${location}`)
    .then(processResponse.bind(null, "ADD_IDENTITY", "ADD_IDENTITY_ERROR"));
};

export let validateIdentity = function(validation) {
    callApi(`/api/identity/validate?validation=${validation}`)
    .then(processResponse.bind(null, "VALIDATE_IDENTITY", "VALIDATE_IDENTITY_ERROR"));
};

export let removeIdentity = function(id) {
    callApi(`/api/identity/remove?id=${id}`)
    .then(processResponse.bind(null, {type: "REMOVE_IDENTITY", id}, "IDENTITY_ERROR"));
};

export let getIdentities = function() {
    callApi("/api/identities")
    .then(processResponse.bind(null, "GET_INDENTITIES", "IDENTITY_ERROR"));
};

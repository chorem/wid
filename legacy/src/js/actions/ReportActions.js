import {callApi, processResponse, parametersToArray} from "./ActionUtils";
import AppDispatcher from "../dispatcher/AppDispatcher";

export let addReport = function(name, tags, period, widget, beginDateTime, endDateTime) {
    let request = `/api/report/add?name=${name}&tags=${encodeURIComponent(tags)}&period=${period}&widget=${widget}`;

    if (beginDateTime) {
        let value = new Date(beginDateTime);
        request += `&selectBeginDateTime=${value.getTime()}`;
    }
    if (endDateTime) {
        let value = new Date(endDateTime);
        request += `&selectEndDateTime=${value.getTime()}`;
    }

    callApi(request).then(processResponse.bind(null, {type: "ADD_REPORT", name, tags, period, widget, beginDateTime, endDateTime}, "REPORT_ERROR"));
};

export let removeReport = function(id) {
    callApi(`/api/report/remove?id=${id}`)
    .then(processResponse.bind(null, {type: "REMOVE_REPORT", id}, "REPORT_ERROR"));
};

export let getReports = function() {
    callApi("/api/reports")
    .then(processResponse.bind(null, "GET_REPORTS", "REPORT_ERROR"));
};

export let getWidgets = function(beginDateTime, endDateTime) {
    callApi(`/api/report/widgets?beginDateTime=${beginDateTime}&endDateTime=${endDateTime}`)
    .then(processResponse.bind(null, "GET_WIDGETS", "REPORT_ERROR"));
};

export let getDetails = function(id, beginDateTime, endDateTime, tag = []) {
    AppDispatcher.dispatch({type: "GET_DETAILS_PENDING"});

    let tags = parametersToArray("tags", tag);
    let request = `/api/report/details?${tags}id=${id}&beginDateTime=${beginDateTime}`;
    if (endDateTime) {
        request += `&endDateTime=${endDateTime}`;
    }

    callApi(request).then(processResponse.bind(null, {type: "GET_DETAILS", id, date: beginDateTime}, "REPORT_ERROR"));
};

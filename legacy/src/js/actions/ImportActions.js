import AppDispatcher from "../dispatcher/AppDispatcher";

export let importJtimer = function(file) {
    AppDispatcher.dispatch({type: "IMPORT_PENDING"});

    let formData = new FormData();
    formData.append("data", file);

    fetch(window.serverContext + "/api/import/jtimer", {
        credentials: "same-origin",
        method: "POST",
        body: formData
    })
    .then(() => {
        AppDispatcher.dispatch({type: "IMPORT_DONE"});
    })
    .catch(() => {
        AppDispatcher.dispatch({type: "IMPORT_ERROR"});
    });
};

export let importJson = function(file) {
    AppDispatcher.dispatch({type: "IMPORT_PENDING"});

    let formData = new FormData();
    formData.append("data", file);

    fetch(window.serverContext + "/api/import/json?drop=1", {
        credentials: "same-origin",
        method: "POST",
        body: formData
    })
    .then(() => {
        AppDispatcher.dispatch({type: "IMPORT_DONE"});
    })
    .catch(() => {
        AppDispatcher.dispatch({type: "IMPORT_ERROR"});
    });
};

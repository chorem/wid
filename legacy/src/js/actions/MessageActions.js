import AppDispatcher from "../dispatcher/AppDispatcher";

export let clearMessage = () => {
    if (!AppDispatcher.isDispatching()) {
        AppDispatcher.dispatch({
            type: "CLEAR_MESSAGE"
        });
    }
};

export let copyMessage = () => {
    AppDispatcher.dispatch({
        type: "COPY_DONE"
    });
};

export let updateReady = () => {
    AppDispatcher.dispatch({
        type: "UPDATEREADY_DONE"
    });
};

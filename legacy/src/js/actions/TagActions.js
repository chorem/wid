import {callApi, processResponse, parametersToArray} from "./ActionUtils";
import AppDispatcher from "../dispatcher/AppDispatcher";

export let search = function(query, beginDateTime, endDateTime) {
    AppDispatcher.dispatch({type: "SEARCH_PENDING"});
    let request = `/api/time/tags?query=${encodeURIComponent(query)}`;

    if (beginDateTime) {
        let value = new Date(beginDateTime);
        request += `&beginDateTime=${value.getTime()}`;
    }
    if (endDateTime) {
        let value = new Date(endDateTime);
        request += `&endDateTime=${value.getTime()}`;
    }

    callApi(request)
    .then(processResponse.bind(null, "GET_ROWS", "SEARCH_ERROR"));
};

export let update = function(query, beginDateTime, endDateTime, removed = [], added = []) {
    AppDispatcher.dispatch({type: "UPDATE_PENDING"});
    let request = `/api/time/tags/update?query=${encodeURIComponent(query)}`;

    if (beginDateTime) {
        let value = new Date(beginDateTime);
        request += `&beginDateTime=${value.getTime()}`;
    }
    if (endDateTime) {
        let value = new Date(endDateTime);
        request += `&endDateTime=${value.getTime()}`;
    }

    if (removed && removed.length) {
        request += "&" + parametersToArray("removed", removed);
    }
    if (added && added.length) {
        request += "&" + parametersToArray("added", added);
    }

    callApi(request)
    .then(processResponse.bind(null, "UPDATE_ROWS", "UPDATE_ERROR"));
};

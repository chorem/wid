import AppDispatcher from "../dispatcher/AppDispatcher";
import {callApi, processResponse} from "./ActionUtils";

export let getProjects = function() {
    return callApi("/api/timebundle/projects")
            .then(processResponse.bind(null, "GET_PROJECTS", "TIMEBUNDLE_ERROR"));
};

export let getTasks = function(projectUrl) {
    if (!projectUrl) {
        AppDispatcher.dispatch({
            type: "GET_TASKS",
            rows: []
        });
        return Promise.resolve();
    }

    return callApi(`/api/timebundle/tasks?projectUrl=${encodeURIComponent(projectUrl)}`)
            .then(processResponse.bind(null, "GET_TASKS", "TIMEBUNDLE_ERROR"));
};

export let addTask = function(projectUrl, taskUrl, query) {
    return callApi(`/api/timebundle/add?projectUrl=${encodeURIComponent(projectUrl)}&taskUrl=${encodeURIComponent(taskUrl)}&query=${encodeURIComponent(query)}`)
            .then(processResponse.bind(null, "ADD_TASK", "TIMEBUNDLE_ERROR"));
};

export let updateTask = function(id, taskUrl, query) {
    return callApi(`/api/timebundle/save?id=${id}&query=${encodeURIComponent(query)}`)
            .then(processResponse.bind(null, {type: "UPDATE_TASK", id, taskUrl, query}, "TIMEBUNDLE_ERROR"));
};

export let removeTask = function(id, taskUrl) {
    return callApi(`/api/timebundle/remove?id=${id}`)
            .then(processResponse.bind(null, {type: "REMOVE_TASK", taskUrl}, "TIMEBUNDLE_ERROR"));
};

export let sync = function(force) {
    AppDispatcher.dispatch({type: "TIMEBUNDLE_PENDING"});
    return callApi(`/api/timebundle/sync?now=${Date.now()}&force=${force}`)
            .then(processResponse.bind(null, "TIMEBUNDLE_DONE", "TIMEBUNDLE_ERROR"));
};

export let addProject = (url) => {
    AppDispatcher.dispatch({
        type: "ADD_PROJECT",
        url
    });
};

export let getProject = (url) => {
    if (!url) {
        AppDispatcher.dispatch({
            type: "GET_PROJECT",
            project: {},
            tasks: []
        });
        return;
    }

    fetch(url)
    .then(processResponse.bind(null, "GET_PROJECT", "TIMEBUNDLE_ERROR"));
};

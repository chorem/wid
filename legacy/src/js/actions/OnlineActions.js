import AppDispatcher from "../dispatcher/AppDispatcher";
import {getActivities} from "./TimelineActions";
import {checkUser} from "./UserActions";
import {syncOfflineData} from "../utils/OfflineService";
import {connect, disconnect} from "../utils/SocketUtils";

export let syncOffline = () => {
    AppDispatcher.dispatch({
        type: "SYNC_OFFLINE"
    });
};

export let setOnline = () => {
    checkUser()
    .then(syncOfflineData)
    .then(() => {
        AppDispatcher.dispatch({
            type: "ONLINE"
        });
    })
    .then(getActivities)
    .then(connect);
};

export let setOffline = () => {
    AppDispatcher.dispatch({
        type: "OFFLINE"
    });
    getActivities();
    disconnect();
};

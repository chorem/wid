import React from "react";
import UserStore from "../stores/UserStore";
import {signIn} from "../actions/UserActions";
import Piwik from "./Piwik";

class Signin extends React.Component {

    constructor() {
        super();
        this._onChange = this._onChange.bind(this);
        this._onSignIn = this._onSignIn.bind(this);
        this._onSignUp = this._onSignUp.bind(this);
        this._onResetPassword = this._onResetPassword.bind(this);
    }

    componentDidMount() {
        UserStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        UserStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        if (UserStore.isAuth()) {
            let {location} = this.props;

            if (location.state && location.state.nextPathname) {
                this.context.router.replace(location.state.nextPathname);
            } else {
                this.context.router.replace("/timeline");
            }
        }
    }

    _onSignIn(event) {
        event.preventDefault();
        signIn(this.refs.username.value, this.refs.passwd.value);
    }

    _onSignUp() {
        this.context.router.push("/site/signup");
    }

    _onResetPassword() {
        this.context.router.push("/site/passwd/reset");
    }

    render() {
        return (
            <div className="signin">
                <Piwik />
                <div className="logo">
                    <div className="fa-stack fa-lg">
                      <i className="fa fa-circle-o fa-stack-2x"></i>
                      <i className="fa fa-tag fa-stack-1x"></i>
                    </div>
                </div>
                <div className="signin-title">Sign in to WID</div>
                <div className="signin-box">
                    <form onSubmit={this._onSignIn}>
                        <p>Username</p>
                        <p><input placeholder="Username" ref="username" required="true" type="text"/></p>
                        <p>Password</p>
                        <p><input placeholder="Password" ref="passwd" required="true" type="password"/></p>
                        <input className="action" type="submit" value="Sign in"/>
                    </form>
                    <div className="action" onClick={this._onResetPassword}>
                        Forgot password ?
                    </div>
                </div>
                <div className="action" onClick={this._onSignUp}>
                    New to WID? Create an account.
                </div>
            </div>
        );
    }
}


Signin.propTypes = {
    location: React.PropTypes.object
};

Signin.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default Signin;

import React from "react";
import MessageStore from "../stores/MessageStore";
import {clearMessage} from "../actions/MessageActions";

function getStateFromStores() {
    return {
        message: MessageStore.getMessage(),
        type: MessageStore.getType()
    };
}

class Message extends React.Component {

    constructor() {
        super();

        this.state = getStateFromStores();
        this._onMessage = this._onMessage.bind(this);
        this._onClose = this._onClose.bind(this);
    }

    componentDidMount() {
        MessageStore.addChangeListener(this._onMessage);
    }

    componentWillUnmount() {
        MessageStore.removeChangeListener(this._onMessage);
    }

    _onMessage() {
        this.setState(getStateFromStores());
    }

    _onClose() {
        clearTimeout(this.timer);
        this.timer = null;

        clearMessage();
    }

    rawContent() {
        return {__html: this.state.message};
    }

    render() {
        let content = this.state.message;

        let className = "show";
        let hasTimer = true;
        let classIcon = "";

        if (!content) {
            className = " hide";
        }

        switch (this.state.type) {
        case MessageStore.TYPE_INFO:
            classIcon = "fa fa-info";
            className += " info";
            break;
        case MessageStore.TYPE_PENDING:
            classIcon = "fa fa-spinner fa-pulse";
            className += " pending";
            hasTimer = false;
            break;
        case MessageStore.TYPE_ERROR:
            classIcon = "fa fa-exclamation";
            className += " error";
            break;
        case MessageStore.TYPE_MESSAGE:
            classIcon = "fa fa-info";
            className += " message";
            break;
        case MessageStore.TYPE_PERMANENT:
            classIcon = "fa fa-info";
            className += " permanent";
            break;
        default:
        }

        if (this.state.type !== MessageStore.TYPE_PERMANENT) {
            if (content && hasTimer) {
                this.timer = setTimeout(this._onClose, 3 * 1000);

            } else if (!content) {
                classIcon = "";
                clearTimeout(this.timer);
                this.timer = null;
            }
        }

        return (
            <div className={"message " + className}>
                <i className={classIcon}></i>
                <span dangerouslySetInnerHTML={this.rawContent()}></span>
                <span className="message-close" onClick={this._onClose}>x</span>
            </div>
        );
    }
}

Message.propTypes = {
};

Message.contextTypes = {
};

export default Message;

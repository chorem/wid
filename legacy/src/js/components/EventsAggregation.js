/*eslint no-alert: 0*/
import React from "react";

import {aggregateEvents} from "../utils/AggregateUtils";
import EventAggregation from "./EventAggregation";

class EventsAggregation extends React.Component {

    constructor() {
        super();
        this.state = {
            marked: null
        };
        this._onMark = this._onMark.bind(this);
    }

    _onMark(date) {
        if (date && !this.state.marked) {
            this.setState({
                marked: date
            });
        } else {
            this.setState({
                marked: null
            });
        }
    }

    render() {
        let events = [];
        let aggregationMap = this.props.events;

        let index = 0;
        aggregationMap && aggregationMap.forEach((aggregation) => {
            aggregation.forEach((event) => {
                if (event.source !== "aggregation") {
                    aggregateEvents([event], []);
                }
                events.push(<EventAggregation event={event}
                                              key={index++}
                                              marked={this.state.marked}
                                              noActions={this.props.noActions}
                                              onMark={this._onMark}
                                              onSetTag={this.props.onSetTag}
                                              />);
            });
        });

        return (
            <div>
                {events}
            </div>
        );
    }
}

EventsAggregation.propTypes = {
    events: React.PropTypes.object,
    noActions: React.PropTypes.bool,
    onSetTag: React.PropTypes.func
};

export default EventsAggregation;

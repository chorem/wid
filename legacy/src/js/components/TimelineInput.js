import React from "react";
import moment from "moment";

import TimelineStore from "../stores/TimelineStore";
import Item from "./Item";

function getStateFromStores() {
    return {
        beginDateTime: TimelineStore.getBeginDateTime(),
        fromNow: +moment(),
        pending: TimelineStore.isPending()
    };
}

class TimelineInput extends React.Component {

    constructor() {
        super();
        this.state = getStateFromStores();
        this._onChange = this._onChange.bind(this);
    }

    componentDidMount() {
        TimelineStore.addChangeListener(this._onChange);
        this._fromNowTimeout();
    }

    componentWillUnmount() {
        TimelineStore.removeChangeListener(this._onChange);
        clearTimeout(this._timer);
    }

    _fromNowTimeout() {
        this.setState(Object.assign(this.state, {
            fromNow: +moment()
        }));

        this._timer = setTimeout(() => {
            this._fromNowTimeout();
        }, 1000);
    }

    _onChange() {
        this.setState(getStateFromStores());
    }

    render() {
        let beginDateTime = this.state.beginDateTime;
        return (<Item activity={{beginDateTime, endDateTime: this.state.fromNow}}
                    pending={this.state.pending}
                    ref="input"
                    subClass="first"/>);
    }
}

export default TimelineInput;

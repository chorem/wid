/*eslint no-alert: 0*/
import React from "react";
import ApplicationStore from "../stores/ApplicationStore";
import {generateKey, removeKey, getKeys} from "../actions/ApplicationActions";
import clipboard from "clipboard-js";
import {copyMessage} from "../actions/MessageActions";
import Piwik from "./Piwik";

function getStateFromStores() {
    return {
        keys: ApplicationStore.getKeys()
    };
}
class Identities extends React.Component {

    constructor() {
        super();

        this.state = getStateFromStores();

        this._onChange = this._onChange.bind(this);
        this._generateKey = this._generateKey.bind(this);
        this._removeKey = this._removeKey.bind(this);
    }

    componentDidMount() {
        getKeys();
        ApplicationStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        ApplicationStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(getStateFromStores());
    }

    _generateKey(event) {
        event.preventDefault();

        let name = this.refs.name.value;
        generateKey(name);
        this.refs.name.value = "";
    }

    _removeKey(key) {
        let confirmed = confirm(`Are you sure to remove key for ${key.name} ?`);
        confirmed && removeKey(key.id);
    }

    _copyUrl(key) {
        clipboard.copy(window.location.origin + window.serverContext + "/webhook/" + key.key);
        copyMessage();
    }

    render() {
        let keys = this.state.keys.map((key) => {
            return (
                <tr className="main-item" key={key.id}>
                    <td>{key.name}</td>
                    <td><a className="link" href={window.location.origin + window.serverContext + "/webhook/" + key.key}>{key.key}</a></td>
                    <td>
                        <span className="action" onClick={this._copyUrl.bind(this, key)}>Get url</span>
                    </td>
                    <td>
                        <span className="action" onClick={this._removeKey.bind(this, key)}>Delete</span>
                    </td>
                </tr>
            );
        });

        return (
            <div className="applications">
                <Piwik />
                <div className="main-separator">Generate key</div>
                <div className="main-form">
                    <form onSubmit={this._generateKey}>
                        <div>
                            <label>Application name:</label>
                            <input ref="name" required="true" type="text"/>
                        </div>
                        <input className="action" type="submit" value="Generate key"/>
                    </form>
                </div>

                <div className="main-separator">Keys</div>
                <p className="main-description">Enter the url with the key into the webhooks for GitHub or GitLab or create own application, to get event in your timeline.</p>
                <table className="main-body">
                    <tbody>
                        {keys}
                    </tbody>
                </table>
            </div>
        );
    }
}

Identities.propTypes = {
};

export default Identities;

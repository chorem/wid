/*eslint no-alert: 0*/
import React from "react";
import {Link} from "react-router";
import ReportStore from "../stores/ReportStore";
import {formatPeriod} from "../utils/DateTimeUtils";
import {getReports, addReport, removeReport} from "../actions/ReportActions";
import Piwik from "./Piwik";

function getStateFromStores(period) {
    return {
        reports: ReportStore.getReports(),
        period
    };
}

class Reports extends React.Component {

    constructor() {
        super();

        this.state = getStateFromStores(0);

        this._onChange = this._onChange.bind(this);
        this._addReport = this._addReport.bind(this);
        this._removeReport = this._removeReport.bind(this);
    }

    componentDidMount() {
        getReports();
        ReportStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        ReportStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(getStateFromStores());
    }

    _addReport(event) {
        event.preventDefault();

        let name = this.refs.name.value;
        let query = this.refs.query.value;
        let period = this.refs.period.value;
        let widget = this.refs.widget.checked ? 1 : 0;
        let beginDate = this.refs.beginDate && this.refs.beginDate.value;
        let endDate = this.refs.endDate && this.refs.endDate.value;

        addReport(name, query, period, widget, beginDate, endDate);
    }

    _onChangePeriod() {
        let period = this.refs.period.value;
        this.setState(getStateFromStores(period));
    }

    _removeReport(report) {
        let confirmed = confirm(`Are you sure to remove report ${report.name} ?`);
        confirmed && removeReport(report.id);
    }

    render() {
        let reports = this.state.reports.map((report) => {
            return (
                <tr className="main-item" key={report.id}>
                    <td>{report.name}</td>
                    <td>{report.tags}</td>
                    <td>{formatPeriod(report.period)}</td>
                    <td>{report.widget === 1 ? "true" : "false"}</td>
                    <td>
                        <span className="action" onClick={this._removeReport.bind(this, report)}>Delete</span>
                    </td>
                    <td>
                        <Link className="action" to={`/report/${report.id}`}>
                            See
                        </Link>
                    </td>
                </tr>
            );
        });

        let dates;
        if (this.state.period === "4") {
            dates = (
                <div>
                    <div>
                        <label>Begin date:</label>
                        <input ref="beginDate" type="datetime-local"/>
                    </div>
                    <div>
                        <label>End date:</label>
                        <input ref="endDate" type="datetime-local"/>
                    </div>
                </div>
            );
        }

        return (
            <div className="reports main">
                <Piwik />
                <div className="main-table">
                    <div className="main-separator">Create report</div>
                    <div className="main-form">
                        <form onSubmit={this._addReport}>
                            <div>
                                <label>Name:</label>
                                <input ref="name" required="true" type="text"/>
                            </div>
                            <div>
                                <label>Query:</label>
                                <input ref="query" type="text"/>
                            </div>
                            <div>
                                <label>Period:</label>
                                <select onChange={this._onChangePeriod.bind(this)} ref="period">
                                    <option value="0">Today</option>
                                    <option value="1">Week</option>
                                    <option value="2">Month</option>
                                    <option value="3">Year</option>
                                    <option value="4">Between dates</option>
                                </select>
                            </div>
                            {dates}
                            <div>
                                <label>Widget:</label>
                                <input ref="widget" type="checkbox" value="true"/>
                            </div>
                            <input className="action" type="submit" value="Add new report"/>
                        </form>
                    </div>

                    <div className="main-separator">Reports</div>
                    <table className="main-body">
                        <tbody>
                            {reports}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

Reports.propTypes = {
};

Reports.contextTypes = {
};

export default Reports;

import React from "react";
import {Link} from "react-router";

class Configuration extends React.Component {

    render() {
        return (
            <div className="configuration main">
                <div className="main-table">
                    <div className="filter">
                        <div className="filter-type">
                            <Link activeClassName="selected" to="/config/account">
                                Account
                            </Link>
                            <Link activeClassName="selected" to="/config/identities">
                                Identities
                            </Link>
                            <Link activeClassName="selected" to="/config/applications">
                                Applications
                            </Link>
                            <Link activeClassName="selected" to="/config/timebundle">
                                Timebundle
                            </Link>
                            <Link activeClassName="selected" to="/config/import">
                                Import
                            </Link>
                            <Link activeClassName="selected" to="/config/export">
                                Export
                            </Link>
                        </div>
                    </div>
                    {this.props.children}
                </div>
            </div>
        );
    }
}

Configuration.propTypes = {
    children: React.PropTypes.object
};

export default Configuration;

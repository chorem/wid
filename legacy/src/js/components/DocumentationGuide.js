/* eslint max-len:0*/
import React from "react";
import Markdown from "react-remarkable";
import Piwik from "./Piwik";

class Documentation extends React.Component {

    componentDidMount() {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    }

    render() {
        let content = `
# Guide

## Index
* [Timeline](#timeline)
* [Reports](#reports)
* [Connect with GitHub/GitLab](#connect)
* [Chrome extension](#chrome)
* [Windand](#windand)
* [Wakatime plugin](#wakatime)
* [Taskwarrior](#taskwarrior)
* [Arbtt](#arbtt)
* [Wid shell](#shell)
* [Wid net](#net)

## <a class="anchor" name="timeline">Timeline</a>
![timeline](../../assets/img/timeline.png)

### Get started
1. Create an account
2. Enter your tags using a white space as a separator in order to create a new activity into your timeline

### Modify the time of an activity
You have multiple choices to modify the time of an activity in your timeline:
* **edit**, allow you to modify the tags in the activity
* **split**, allow you to modify the interval of time assigned to the activity
* **merge**, allow you to group two activities into one

### Add a note to an activity
As you enter the tags of an activity, you can also create a note and associate it with the activity. To do so, just write your note in brackets.
For example : **(This is a comment)**.

### Reuse tags
You can reuse the tags of a previous activity :
* click on the USE action of the previous activity in the timeline
* or just copy and paste the tags of this previous activity

## <a class="anchor" name="reports">Reports</a>
![report](../../assets/img/report.png)

### Create your first report
1. Enter **all** as name
2. Select **YEAR** for the period
3. Click on ADD NEW REPORT
4. Click on SEE to display your report

### Create a custom report using a query
You can create a custom report by filtering your timeline with a query.
The query has to contain the tags, separated by a "+" or a "-" or a white space delimiter, defining the tags you want to include or exclude from your report.

* **tag1+tag2** the report will show the amounts of time allocated to tag1 and tag2 at the same time
* **tag1-tag2** the report will show the amounts of time allocated to tag1, excluding all timeslots allocted to tag2
* **tag1 tag2** the report will show the amounts of time allocated to tag1 or tag2

### Widget
Reports can be used as widgets. It will help you keep an eye on your reports, directly from your timeline.
The widget exposes an overview of a report. However the widgets will react and update their view thanks to
the period of time you select in the timeline (day, week, month,..).

## <a class="anchor" name="connect">Connect with GitHub/GitLab</a>
![application](../../assets/img/application.png)

You can see your commits on GitHub and/or GitLab in your timeline. To use this
feature, you need to:
1. Add the email you use with GitHub or GitLab in Configuration > identities.
2. In Configuration > Applications, enter an application name and generate an API key
3. Copy the url associated to this api key
4. Connect on GitHub or GitLab
5. Paste the url into the webhook section of the given website (GitHub or GitLab)

Now the commits you push on the projects hosted by GitLab or GitHub will be sent as an event to your timeline.
You can then use those event to complete your activities.

## <a class="anchor" name="chrome">Chrome extension</a>

The Chrome extension allow you to detect the idle in your computer. Download the extension
on the home page and drag n drop on the list of extensions in your chrome.

1. [Download extension.](http://wid.chorem.com/wid-ext.crx)
2. In your browser go to: chrome://extensions
3. Drag n drop extension in current page
4. Generate an [application key](https://wid.chorem.com/config/applications)
5. Click on Wid icon in navigation bar
6. Set **Server url** with your generated application URL

## <a class="anchor" name="windand">Windand</a>

Windand is the Wid agent for Android. Download it directly on wid home page. Windand
send to Wid the information contained in your phone (calls, meetings, application usage, ...)

1. [Download apk.](http://widand.chorem.com/WidAnd-debug.apk)
2. Generate an [application key](https://wid.chorem.com/config/applications)
3. In Windand preference
   1. Set your **Device name**
   2. Set **Wid url** with your generated application URL

## <a class="anchor" name="wakatime">Wakatime plugin</a>

Wakatime plugin is used to send what do you do in your IDE (project, branch, file, ...)

[Download the plugins you need.](https://wakatime.com/download)
You don't need to create Wakatime account.

You can use Wakatime plugin based on CLI easily. You have to
modifiy wakatime configuration file ($HOME/.wakatime.cfg sous linux) to change the api_url. Example:

    [settings]
    api_key = aaaaaaaa-57c9-11e6-95ea-13f4829e4830
    api_url = https://wid.chorem.com/webhook/aaaaaaaa-57ce-11e6-80d8-e9d235e45adc
    offline = true
    hostname = myComputer

## <a class="anchor" name="taskwarrior" href="https://taskwarrior.org/">Taskwarrior</a>

This script send the modification on task from Taskwarrior.

Pre-requisite in your $PATH:
* jq

Installation:
1. [Download the script](https://gitlab.nuiton.org/chorem/wid/blob/master/scripts/on-modify.wid-taskwarrior-send)
2. Copy the script into ~/.task/hooks/
3. Set script executable (chmod u+x)
4. Generate an [application key](https://wid.chorem.com/config/applications)
5. Set WID_WEBHOOK_URL with your generated application URL in your ~/.bashrc

        export WID_WEBHOOK_URL=https://wid.chorem.com/webhook/aaaaaaaa-57ce-11e6-80d8-e9d235e45adc

6. Verify the script with command task diagnostic

        ...
        Active: on-modify.wid-taskwarrior-send (executable)
        ...

## <a class="anchor" name="arbtt" href="http://arbtt.nomeata.de/">Arbtt</a>

This script send active window (name and title) and idle time to WID.

Pre-requisite in your $PATH:
* arbtt-dump
* jq

Installation:
1. [Download wid script.](https://gitlab.nuiton.org/chorem/wid/blob/master/scripts/wid-arbtt)
2. Set script executable (chmod u+x)
3. Add line in in your crontab (crontab -e)

        */15 * * * *       $HOME/wid-arbtt

4. Generate an [application key](https://wid.chorem.com/config/applications)
5. Set WID_WEBHOOK_URL with your generated application URL in your ~/.bashrc

        export WID_WEBHOOK_URL=https://wid.chorem.com/webhook/aaaaaaaa-57ce-11e6-80d8-e9d235e45adc

## <a class="anchor" name="shell">Wid shell</a>

This script send all command (command line, program name, execution time, exit return, ...)
to wid

Pre-requisite in your $PATH:
* curl

Installation:
1. [Download wid script.](https://gitlab.nuiton.org/chorem/wid/blob/master/scripts/wid-bash)
2. Add in ~/.bashrc

        source wid-bash

4. Generate an [application key](https://wid.chorem.com/config/applications)
5. Set WID_WEBHOOK_URL with your generated application URL in your ~/.bashrc

        export WID_WEBHOOK_URL=https://wid.chorem.com/webhook/aaaaaaaa-57ce-11e6-80d8-e9d235e45adc

### Advanced Usage

You can monitor you activity on server with wid-bash. When wid-bash is installed
on your computer you can do on command line:

        widbash_install root@monserver.example.com

And automaticaly all configuration needed is done.

## <a class="anchor" name="net">Wid net</a>

This script send your device ip to wid

Pre-requisite in your $PATH:
* ip
* curl
* jq

Installation:
1. [Download wid script.](https://gitlab.nuiton.org/chorem/wid/blob/master/scripts/wid-net)
2. Generate an [application key](https://wid.chorem.com/config/applications)
3. Launch wid-net manualy when is needed or add wid-net monitor in your .xsession for example:

        WID_WEBHOOK_URL=https://wid.chorem.com/webhook/aaaaaaaa-57ce-11e6-80d8-e9d235e45adc wid-net monitor

If geoip data service return bad information, you can force some value. For that
you can use $WID_IP_LOCATION variable. This variable is json Array of Object
with field 'ip', and other field. Match is done on 'ip' field, all other field
replace or are copied in event send to wid.

        export WID_IP_LOCATION='[{"ip":"123.123.123.123", "place": "work", "latitude": 10.123, "longitude": 0.123},{"ip":"234.234.234.234",  "place": "house","latitude": 12.12, "longitude": 11.11}]'

In this example, if geoip return data information for ip="123.123.123.123" or ip="234.234.234.234"
'latitude' and 'longitude' are replaced and 'place' field is added to event. Otherwise
geoip data is send without modification.
`;

        return (
            <div className="documentation">
                <Piwik />
                <Markdown options={{html: true}} source={content} />
            </div>
        );
    }
}

export default Documentation;

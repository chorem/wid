/*eslint no-alert: 0*/
import React from "react";
import {validateIdentity} from "../actions/IdentityActions";

class IdentityValidation extends React.Component {

    constructor({params}) {
        super();

        let {validation} = params;
        if (validation) {
            validateIdentity(validation);
        }
    }

    componentDidMount() {
        this.context.router.replace("/config/identities");
    }

    render() {
        return null;
    }
}

IdentityValidation.propTypes = {
};

IdentityValidation.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default IdentityValidation;

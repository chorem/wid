/*eslint no-alert: 0*/
import React from "react";

import OnlineStore from "../stores/OnlineStore";

function getStateFromStores() {
    return {
        online: OnlineStore.isOnline()
    };
}

class Online extends React.Component {

    constructor() {
        super();
        this.state = getStateFromStores();
        this._onChange = this._onChange.bind(this);
    }

    componentDidMount() {
        OnlineStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        OnlineStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(getStateFromStores());
    }

    render() {
        let icon = this.state.online !== false ? "fa-chain" : "fa-chain-broken";
        return (
            <div className={"online fa " + icon}></div>
        );
    }
}

Online.propTypes = {
};

Online.contextTypes = {
};

export default Online;

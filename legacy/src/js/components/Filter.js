import React from "react";

import TimelineStore from "../stores/TimelineStore";
import {changeFilterDate} from "../actions/TimelineActions";

function getStateFromStores() {
    let filter = TimelineStore.getFilterDate();
    return {
        value: filter.value,
        offset: filter.offset
    };
}

class Filter extends React.Component {

    constructor() {
        super();
        this.state = getStateFromStores();
        this._onChange = this._onChange.bind(this);
        this._onReset = this._onReset.bind(this);
    }

    componentDidMount() {
        TimelineStore.addChangeListener(this._onChange);
        TimelineStore.addResetListener(this._onReset);
    }

    componentWillUnmount() {
        TimelineStore.removeChangeListener(this._onChange);
        TimelineStore.removeResetListener(this._onReset);
    }

    _onChange() {
        this.setState(getStateFromStores());
    }

    _onReset() {
        this.setState({
            value: this.state.value,
            offset: 0
        }, () => {
            changeFilterDate(this.state);
        });
    }

    _onSelect(value) {
        this.setState({
            value,
            offset: 0
        }, () => {
            changeFilterDate(this.state);
        });
    }

    _onOffset(offset) {
        if (!this.state.offset && offset > 0) {
            return;
        }

        this.setState({
            value: this.state.value,
            offset: this.state.offset + offset
        }, () => {
            changeFilterDate(this.state);
        });
    }

    render() {
        return (
            <div className="filter">
                <div className="filter-type">
                    <div className="fa fa-chevron-left" onClick={this._onOffset.bind(this, -1)}></div>
                    <div className={this.state.value === 0 ? "selected" : ""} onClick={this._onSelect.bind(this, 0)}>
                        Today {this.state.value === 0 && this.state.offset ? this.state.offset : ""}
                    </div>
                    <div className={this.state.value === 1 ? "selected" : ""} onClick={this._onSelect.bind(this, 1)}>
                        Week {this.state.value === 1 && this.state.offset ? this.state.offset : ""}
                    </div>
                    <div className={this.state.value === 2 ? "selected" : ""} onClick={this._onSelect.bind(this, 2)}>
                        Month {this.state.value === 2 && this.state.offset ? this.state.offset : ""}
                    </div>
                    <div className={this.state.value === 3 ? "selected" : ""} onClick={this._onSelect.bind(this, 3)}>
                        Year {this.state.value === 3 && this.state.offset ? this.state.offset : ""}
                    </div>
                    <div className="fa fa-chevron-right" onClick={this._onOffset.bind(this, 1)}></div>
                </div>
            </div>
        );
    }
}

Filter.propTypes = {
};

Filter.contextTypes = {
};

export default Filter;

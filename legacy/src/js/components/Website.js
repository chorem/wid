import React from "react";
import {Link} from "react-router";
import UserStore from "../stores/UserStore";
import Message from "./Message";

function getStateFromStores() {
    let user = UserStore.getUser();
    return {
        pending: UserStore.isPending(),
        isSignOut: UserStore.isSignOut(),
        isAuth: UserStore.isAuth(),
        username: user && user.username
    };
}

class Website extends React.Component {
    constructor() {
        super();
        this.state = getStateFromStores();
        this._onChange = this._onChange.bind(this);
    }

    componentDidMount() {
        UserStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        UserStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(getStateFromStores(), () => {
            if (this.state.isSignOut && !this.state.pending) {
                this.context.router.push("/site/signin");
            }
        });
    }

    render() {
        let actions;
        if (this.state.isAuth) {
            actions = (
                <div className="site-actions">
                    <div className="site-action">
                        <Link to="/timeline">
                            <i className="fa fa-user"></i>
                            {this.state.username}
                        </Link>
                    </div>
                </div>
            );
        } else {
            actions = (
                <div className="site-actions">
                    <div className="site-action">
                        <Link to="/site/signin">
                            <i className="fa fa-sign-in"></i>
                            Sign in
                        </Link>
                    </div>
                    <div className="site-action">
                        <Link to="/site/signup">
                            <i className="fa fa-user-plus"></i>
                            Sign up
                        </Link>
                    </div>
                </div>
            );
        }
        return (
            <div className="site">
                <div className="site-header">
                    <Link className="site-logo" to="/site">
                        <div className="logo">
                            <div className="fa-stack fa-lg">
                              <i className="fa fa-circle-o fa-stack-2x"></i>
                              <i className="fa fa-tag fa-stack-1x"></i>
                            </div>
                        </div>
                        <div className="site-title">WID</div>
                    </Link>
                    {actions}
                </div>
                <Message />

                {this.props.children}

                <div className="site-footer">
                    <a href="http://gitlab.nuiton.org/chorem/wid/">GitLab</a>
                    |<a href="http://www.codelutin.com">Code Lutin</a>
                    |<a href="https://www.gnu.org/licenses/agpl-3.0.txt">AGPL-3.0</a>
                    |<Link to="/site/cgu">CGU</Link>
                    |<Link to="/site/legals">Mentions Légales</Link>
                    |<a href="mailto:wid-users@list.chorem.org">Contact</a>
                </div>
            </div>
        );
    }
}

Website.propTypes = {
    children: React.PropTypes.object
};

Website.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default Website;

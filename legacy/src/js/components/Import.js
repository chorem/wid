/*eslint no-alert: 0*/
import React from "react";
import {importJson, importJtimer} from "../actions/ImportActions";
import Piwik from "./Piwik";

class Import extends React.Component {

    constructor() {
        super();
        this._importJson = this._importJson.bind(this);
        this._importJtimer = this._importJtimer.bind(this);
    }

    _importJson(event) {
        event.preventDefault();

        let file = this.refs.dataJson.files[0];
        let confirmed = confirm("Be careful, you can lost data! Are you sure to import the file ?");
        confirmed && importJson(file);
    }

    _importJtimer(event) {
        event.preventDefault();

        let file = this.refs.dataJtimer.files[0];
        let confirmed = confirm("Be careful, you can do only once! Are you sure to import the file ?");
        confirmed && importJtimer(file);
    }

    render() {
        return (
            <div className="import">
                <Piwik />
                <div className="main-separator">Import Db</div>
                <div className="main-form">
                    <div>
                        <form onSubmit={this._importJson}>
                            <label>Json file:</label>
                            <input name="data" ref="dataJson" required="true" type="file"/>
                            <input className="action" type="submit" value="Import Json"/>
                        </form>
                    </div>
                </div>

                <div className="main-separator">Import Jtimer</div>
                <div className="main-form">
                    <div>
                        <form onSubmit={this._importJtimer}>
                            <label>Zip of data folder:</label>
                            <input name="data" ref="dataJtimer" required="true" type="file"/>
                            <input className="action" type="submit" value="Import Jtimer"/>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Import;

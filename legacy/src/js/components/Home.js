import React from "react";
import {Link} from "react-router";
import Piwik from "./Piwik";

class Home extends React.Component {

    render() {
        return (
            <div className="home">
                <Piwik />
                <div className="site-summary">
                    <p>&ldquo; Wid is your best companion for tracking your time. You can follow your personal and professional time.</p>
                    <p>The innovation holds in the tag system, you organize your day with tags and get your own reports. &rdquo;</p>
                </div>
                <div className="site-documentation">
                    <Link to="/site/documentation/guide">See guide</Link>
                    <Link to="/site/documentation/api">See Api</Link>
                </div>

                <div className="site-presentation">
                    <div className="site-img timeline"></div>
                    <div className="site-legend">
                        <span className="">Timeline</span>
                        <p>The timeline is your dashboard to handle your day.</p></div>
                </div>

                <div className="site-presentation">
                    <div className="site-legend">
                        <span className="">Report</span>
                        <p>Create your own reports and follow the progression.</p></div>
                    <div className="site-img report"></div>
                </div>

                <div className="site-presentation">
                    <div className="site-img application"></div>
                    <div className="site-legend">
                        <span className="">Application</span>
                        <p>
                            Plug applications with Wid and share data from:
                        </p>
                        <div className="site-app-logo">
                            <a href="http://widand.chorem.com/WidAnd-debug.apk"><img src={window.serverContext + "/assets/img/applications/android.png"} title="Widand"/></a>
                            <a href={window.serverContext + "/wid-ext.crx"}><img src={window.serverContext + "/assets/img/applications/chrome.png"} title="Chrome extension"/></a>
                            <a href="http://github.com/"><img src={window.serverContext + "/assets/img/applications/github.png"} title="Github"/></a>
                            <a href="https://gitlab.com"><img src={window.serverContext + "/assets/img/applications/gitlab.png"} title="Gitlab"/></a>
                            <a href="https://taskwarrior.org/"><img src={window.serverContext + "/assets/img/applications/taskwarrior.png"} title="Taskwarrior"/></a>
                            <a href="https://wakatime.com/download"><img src={window.serverContext + "/assets/img/applications/wakatime.png"} title="Wakatime"/></a>
                            <a href="http://arbtt.nomeata.de/"><img id="site-app-logo-arbtt" src={window.serverContext + "/assets/img/applications/haskell.png"} title="Arbtt"/></a>
                        </div>
                    </div>
                </div>

                <div className="site-documentation last">
                    <Link to="/site/documentation/guide">Read the guide for more information</Link>
                </div>
            </div>
        );
    }
}

export default Home;

/* eslint no-console: 0 */
import React from "react";
import ReactDOM from "react-dom";

class RawHtml extends React.Component {

    componentDidMount() {
        // inject script is not evaluated, force evaluation
        let script = ReactDOM.findDOMNode(this).querySelectorAll("script[type=''], script[type='javascript'], script[type='text/javascript']");
        for (let n = 0; n < script.length; n++) {
            try {
                eval(script[n].innerHTML); // eslint-disable-line no-eval
            } catch (eee) {
                console.error("Can't evaluate page script", name, script[n].innerHTML);
            }
        }
    }

    shouldComponentUpdate() {
        return false;
    }

    render() {
        let html = {__html: this.props.source};
        return <div dangerouslySetInnerHTML={html}></div>;
    }
}

RawHtml.propTypes = {
    source: React.PropTypes.string.isRequired
};

RawHtml.contextTypes = {
};

export default RawHtml;

import React from "react";
import {diffValue, getReferenceTime, forEachPeriods} from "../utils/DateTimeUtils";

class ReportHistory extends React.Component {

    render() {
        let period = this.props.period;
        let values = this.props.values;

        let refTime = getReferenceTime(period);

        let index = 0;
        let bars = [];
        values && forEachPeriods(period, this.props.date, (date, format, formatShort, formatAggregation) => {
            let value = values.find((duration) => {
                return duration.period === formatAggregation;
            });

            let progress = value && value.time || 0;
            let height = ~~(progress / refTime * 100);

            let title = diffValue(progress) + " for " + format;
            let selectedClass = index++ === 0 ? " selected" : "";

            bars.push(
                <div className="report-bar-for" key={"bar_" + index}>
                    <a className="report-bar" onClick={this.props.onClick.bind(null, +date)} title={title}>
                        <div className={"report-bar-value" + selectedClass} style={{height: height + "%"}}></div>
                    </a>
                    <div className="report-bar-title">
                        {formatShort}
                    </div>
                </div>
            );

        });

        return (
            <div className="report-history">
                {bars}
            </div>
        );
    }
}

ReportHistory.propTypes = {
    date: React.PropTypes.number,
    onClick: React.PropTypes.func.isRequired,
    period: React.PropTypes.number.isRequired,
    values: React.PropTypes.array
};

ReportHistory.contextTypes = {
};

export default ReportHistory;

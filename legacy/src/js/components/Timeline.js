import React from "react";
import moment from "moment";

import TimelineStore from "../stores/TimelineStore";
import Item from "./Item";
import Filter from "./Filter";
import ReportWidgets from "./ReportWidgets";
import TimelineInput from "./TimelineInput";
import Piwik from "./Piwik";

function getStateFromStores() {
    return {
        activities: TimelineStore.getActivities(),
        beginDateTime: TimelineStore.getBeginDateTimeWithFilter(),
        filter: TimelineStore.getFilterDate()
    };
}

class Timeline extends React.Component {

    constructor() {
        super();
        this.state = getStateFromStores();
        this._onChange = this._onChange.bind(this);
    }

    componentDidMount() {
        TimelineStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        TimelineStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        if (!TimelineStore.isPending()) {
            this.setState(getStateFromStores());
        }
    }

    render() {
        let previousDate = null;

        let items = [];
        this.state.activities.forEach((activity, index) => {

            let beginDateTime = moment(activity.beginDateTime);

            let nextDate = beginDateTime.format("dddd D MMMM YYYY");
            if (nextDate !== previousDate) {
                previousDate = nextDate;
                items.push(<div className="date-separator" key={"date_" + index}>{nextDate}</div>);
            }

            let subClass = !activity.proposition ? "" : "proposition";

            items.push(<Item activity={activity}
                            key={"item_" + index}
                            subClass={subClass}/>);
        });

        if (!items.length) {
            items.push(<div className="date-separator" key={"date_0"}>{moment(this.state.beginDateTime).format("dddd D MMMM YYYY")}</div>);
        }

        return (
            <div className="timeline">
                <Piwik />
                {this.props.children}
                <div className="items">
                    <TimelineInput />
                    <div className="item--charts">
                        <ReportWidgets period={this.state.filter}/>
                        <Filter />
                    </div>
                    {items}
                </div>
            </div>
        );
    }
}

Timeline.propTypes = {
    children: React.PropTypes.object
};

export default Timeline;

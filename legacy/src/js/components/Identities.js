/*eslint no-alert: 0*/
import React from "react";
import IndentityStore from "../stores/IdentityStore";
import {addIdentity, removeIdentity, getIdentities, validateIdentity} from "../actions/IdentityActions";
import Piwik from "./Piwik";

function getStateFromStores() {
    return {
        identities: IndentityStore.getIdentities()
    };
}
class Identities extends React.Component {

    constructor({params}) {
        super();

        this.state = getStateFromStores();

        let {validation} = params;
        if (validation) {
            validateIdentity(validation);
            this.context.router.replace("/config/identities");
        }

        this._onChange = this._onChange.bind(this);
        this._addIdentity = this._addIdentity.bind(this);
        this._removeIdentity = this._removeIdentity.bind(this);
    }

    componentDidMount() {
        getIdentities();
        IndentityStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        IndentityStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(getStateFromStores());
    }

    _addIdentity(event) {
        event.preventDefault();

        let email = this.refs.email.value;
        addIdentity(email);
        this.refs.email.value = "";
    }

    _removeIdentity(identity) {
        let confirmed = confirm(`Are you sure to remove identity ${identity.name} ?`);
        confirmed && removeIdentity(identity.id);
    }

    render() {
        let itentities = this.state.identities.map((identity) => {
            return (
                <tr className="main-item" key={identity.id}>
                    <td>{identity.identifier}</td>
                    <td>{identity.valid ? "active" : "inative"}</td>
                    <td>
                        <span className="action" onClick={this._removeIdentity.bind(this, identity)}>Delete</span>
                    </td>
                </tr>
            );
        });

        return (
            <div className="identities">
                <Piwik />
                <div className="main-separator">Add identity</div>
                <div className="main-form">
                    <form onSubmit={this._addIdentity}>
                        <div>
                            <label>Email:</label>
                            <input ref="email" required="true" type="email"/>
                        </div>
                        <input className="action" type="submit" value="Add identity"/>
                    </form>
                </div>

                <div className="main-separator">Identities</div>
                <table className="main-body">
                    <tbody>
                        {itentities}
                    </tbody>
                </table>
            </div>
        );
    }
}

Identities.propTypes = {
};

Identities.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default Identities;

import React from "react";
import Piwik from "./Piwik";

class Ggu extends React.Component {

    componentDidMount() {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    }

    render() {
        return (
            <div className="cgu">
                <Piwik />
                <h1>Conditions Générales d’Utilisation</h1>

                <h2>Préambule</h2>
                <p>
                    En utilisant ce service, vous acceptez d’être lié par les conditions suivantes.<br/>
                    L’équipe WID se réserve le droit de mettre à jour et modifier ces conditions de temps à autre.
                </p>
                <h2>Version courte</h2>
                <p>Pour en faciliter la lecture, sans le blabla juridique, nous vous en proposons ci-dessous une version compréhensible par un être humain normal.</p>
                <p>On est sympathiques :</p>
                <ul>
                    <li>Votre contenu vous appartient ;</li>
                    <li>L’équipe WID n’exploitera pas vos données personnelles, sauf à fin de statistiques internes (anonymisées) ou pour vous prévenir
                    d’un changement important sur le service ;</li>
                    <li>L’équipe WID ne transmettra ni ne revendra vos données personnelles (votre vie privée nous tient - vraiment - à cœur) ;</li>
                </ul>
                <p>Mais il ne faut pas nous prendre pour autant pour des chatons :</p>
                <ul>
                    <li>Clause <em>« La loi est la loi, et on ne veut pas finir en taule »</em> : vous devez respecter la loi (que celle-ci soit bien
                        faite ou idiote), sinon votre compte sera supprimé ;</li>
                    <li>Clause <em>« Merci d’être poli et respectueux avec ses voisins de service »</em> : vous devez respecter les autres utilisateurs
                    en faisant preuve de civisme et de politesse, sinon votre contenu, voire votre compte, pourront être supprimés, sans négociation ;</li>
                    <li>Clause <em>« Si ça casse, on n’est pas obligé de réparer »</em> : L’équipe WID propose ce service gratuitement et librement.
                    Si vous perdez des données, par votre faute ou par la nôtre, désolé, mais ça arrive. Nous ferons ce que nous pouvons pour les récupérer,
                    mais nous ne nous assignons aucune obligation de résultat. En clair, évitez de mettre des données sensibles ou importantes, car en cas de
                    perte, nous ne garantissons pas leur récupération ;</li>
                    <li>Clause <em>« Si vous n’êtes pas contents, vous êtes invités à aller voir ailleurs »</em> : si le service ne vous convient pas, libre à
                    vous d’en trouver un équivalent (ou meilleur) ailleurs, ou de monter le vôtre ;</li>
                    <li>Clause <em>« Tout abus sera puni »</em> : si un utilisateur abuse du service, par exemple en monopolisant des ressources machines partagées,
                    ou en publiant des contenus considérés comme non pertinents, son contenu ou son compte pourra être supprimé sans avertissement ni négociation.
                    L’équipe WID reste seul juge de cet notion « d’abus » dans le but de fournir le meilleur service possible à l’ensemble de ses utilisateurs.
                    Si cela vous parait anti-démocratique, anti-libriste, anti-liberté-d’expression, merci de vous référer à la clause précédente ;</li>
                    <li>Clause <em>« Rien n’est éternel »</em> : les service peuvent fermer (faute de fonds pour les maintenir, par exemple), ils peuvent être
                    victimes d’intrusion (le « 100 % sécurisé » n’existe pas). Nous vous encourageons donc à conserver une copie des données qui vous importent, car
                    l’équipe WID ne saurait être tenu pour responsable de leur hébergement sans limite de temps.</li>
                </ul>

                <h2>Version complète</h2>
                <h3>Conditions du service</h3>

                <ol>
                    <li>L’utilisation du service se fait à vos propres risques. Le service est fourni tel quel.</li>
                    <li>Vous ne devez pas modifier un autre site afin de signifier faussement qu’il est associé avec ce service.</li>
                    <li>Les comptes ne peuvent être créés et utilisés que par des humains. Les comptes créés par les robots ou autres méthodes automatisées pourront
                    être supprimés sans avertissement.</li>
                    <li>Vous êtes responsable de la sécurité de votre compte et de votre mot de passe. <br/>
                    L’équipe WID ne peut pas et ne sera pas responsable de toutes pertes ou dommages résultant de votre non-respect de cette obligation de sécurité.</li>
                    <li>Vous êtes responsable de tout contenu affiché et de l’activité qui se produit sous votre compte.</li>
                    <li>Vous ne pouvez pas utiliser le service à des fins illégales ou non autorisées.<br/>
                    Vous ne devez pas transgresser les lois de votre pays.</li>
                    <li>Vous ne pouvez pas vendre, échanger, revendre, ou exploiter dans un but commercial non autorisé un compte du service utilisé.</li>
                </ol>
                <p>La violation de l’un de ces accords entraînera la résiliation de votre compte.<br/>
                Vous comprenez et acceptez que l’équipe WID ne puisse être tenue responsable pour les contenus publiés sur ce service.</p>

                <ol>
                    <li>Vous comprenez que la mise en ligne du service ainsi que de votre contenu implique
                    une transmission (en clair ou chiffrée, suivant les services) sur divers réseaux.
                    </li>
                    <li>Vous ne devez pas transmettre des vers, des virus ou tout autre code de nature malveillante.</li>
                    <li>L’équipe WID ne garantit pas que
                        <ul>
                            <li>le service répondra à vos besoins spécifiques ;</li>
                            <li>le service sera ininterrompu ou exempte de bugs ;</li>
                            <li>que les erreurs dans le service seront corrigés.</li>
                        </ul>
                    </li>
                    <li> Vous comprenez et acceptez que l’équipe WID ne puisse être tenue responsable de tout dommages directs, indirects, ou fortuits,
                    comprenant les dommages pour perte de profits, de clientèle, d’accès, de données ou d’autres pertes intangibles
                    (même si l’équipe WID est informée de la possibilité de tels dommages) et qui résulteraient de :
                        <ol>
                            <li>l’utilisation ou de l’impossibilité d’utiliser le service ; </li>
                            <li>l’accès non autorisé ou altéré de la transmission des données ; </li>
                            <li>les déclarations ou les agissements d’un tiers sur le service ;</li>
                            <li>la résiliation de votre compte ;</li>
                            <li>toute autre question relative au service.</li>
                        </ol>
                    </li>
                    <li>L’échec de l’équipe WID à exercer ou à appliquer tout droit ou disposition des Conditions Générale d’Utilisation
                     ne constitue pas une renonciation à ce droit ou à cette disposition.
                     Les Conditions d’utilisation constituent l’intégralité de l’accord entre vous et l’équipe WID et régissent votre utilisation du service,
                     remplaçant tous les accords antérieurs entre vous et l’équipe WID (y compris les versions précédentes des Conditions Générales d’Utilisation).
                    </li>
                    <li>Les questions sur les conditions de service doivent être envoyées via l’<a href="mailto:webadmin@codelutin.com">e-mail</a>.</li>
                </ol>

                <h2>Modifications du service</h2>
                <ol>
                    <li>L’équipe WID se réserve le droit, à tout moment de modifier ou d’interrompre, temporairement ou définitivement, le service avec ou sans préavis.</li>
                    <li>L’équipe WID ne sera pas responsable envers vous ou tout tiers pour toute modification, suspension ou interruption du service.</li>
                </ol>

                <h3>Droit d’auteur sur le contenu</h3>

                <ol>
                    <li>Vous ne pouvez pas envoyer, télécharger, publier sur un blog, distribuer, diffuser tout contenu illégal,
                     diffamatoire, harcelant, abusif, frauduleux, contrefait, obscène ou autrement répréhensible.</li>
                    <li>Nous revendiquons aucun droit sur votre données : textes, images, son, vidéo,
                     ou tout autre élément, que vous téléchargez ou transmettez depuis votre compte.</li>
                    <li>Nous n’utiliserons pas votre contenu pour un autre usage que de vous fournir le service.</li>
                    <li>Vous ne devez pas télécharger ou rendre disponible tout contenu qui porte atteinte aux droits de quelqu’un d’autre.</li>
                    <li>Nous nous réservons le droit de supprimer tout contenu nous paraissant non pertinent pour l’usage du service, selon notre seul jugement.</li>
                    <li>Nous pouvons, si nécessaire, supprimer ou empêcher la diffusion de tout contenu sur le service qui ne respecterait pas les présentes conditions.</li>
                </ol>

                <h3>Édition et partage de données</h3>
                <ul>
                    <li>Les fichiers que vous créez avec le service peuvent être - si vous le souhaitez - lus, copiés, utilisés et redistribués par des gens que
                    vous connaissez ou non.</li>
                    <li>En rendant publiques vos données, vous reconnaissez et acceptez que toute personne utilisant ce site web puisse les consulter sans restrictions.</li>
                    <li>Mais le service peut également vous proposer la possibilité d’autoriser l’accés et le travail collaboratif sur ses documents de manière restreinte
                    à un ou plusieurs autres utilisateurs.</li>
                    <li>L’équipe WID ne peut être tenue responsable de tout problème résultant du partage ou de la publication de données entre utilisateurs.</li>
                </ul>

                <h3>Résiliation</h3>
                <p>
                    L’équipe WID, à sa seule discrétion, a le droit de suspendre ou de résilier votre compte et de refuser toute utilisation actuelle ou future du service.
                    Cette résiliation du service entraînera la désactivation de l’accès à votre compte,
                    et la restitution de tout le contenu.<br/>
                    L’équipe WID se réserve le droit de refuser le service à n’importe qui pour n’importe quelle raison à tout moment.
                </p>
                <p>L’équipe WID se réserve également le droit de résilier votre compte si vous ne vous connectez pas à votre compte pour une période supérieure à 6 mois.</p>


                <h3>Données personnelles</h3>

                    <p>Conformément à l’article 34 de la loi « Informatique et Libertés », l’équipe WID garantit à l’utilisateur un droit d’opposition, d’accès et de
                    rectification sur les données nominatives le concernant.
                    L’utilisateur a la possibilité d’exercer ce droit en utilisant l’ <a href="mailto:webadmin@codelutin.com">e-mail</a> mis à sa disposition.</p>

                <ul>
                    <li>Pour utiliser le service, vous devez créer un compte. L’équipe WID demande certaines informations personnelles :
                    une adresse e-mail valide et un mot de passe qui est utilisé pour protéger votre compte contre tout accès non autorisé.
                    Les champs « Nom » et « Prénom » peuvent être requis pour le bon fonctionnement du logiciel mais il n’est pas nécéssaire qu’ils révélent votre véritable
                    identité.</li>
                    <li>Tout comme d’autres services en ligne, l’équipe WID enregistre automatiquement certaines informations concernant votre utilisation du service
                    telles que l’activité du compte (exemple : espace de stockage occupé, nombre d’entrées, mesures prises),
                    les données affichées ou cliquées (exemple : liens, éléments de l’interface utilisateur),
                    et d’autres informations pour vous identifier (exemple : type de navigateur, adresse IP, date et heure de l’accès, URL de référence).
                    </li>
                    <li>Nous utilisons ces informations en interne pour vous améliorer l’interface utilisateur du service et maintenir une expérience utilisateur
                    cohérente et fiable.</li>
                    <li>Ces données ne sont ni vendues, ni transmises à des tiers.</li>
                </ul>

                <h1>Licence</h1>
                <p>
                    Les Conditions Générales d’Utilisation du service sont placés sous
                    <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr"> Creative Commons By-SA</a>. Elles ont été inspirées par les CGU de Framasoft.
                </p>
            </div>
        );
    }
}

export default Ggu;

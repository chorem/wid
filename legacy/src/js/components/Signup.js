import React from "react";
import {signUp} from "../actions/UserActions";
import UserStore from "../stores/UserStore";
import Piwik from "./Piwik";

function getStateFromStores() {
    return {
        pending: UserStore.isPending()
    };
}

class Signup extends React.Component {

    constructor() {
        super();
        this.state = getStateFromStores();

        this._onChange = this._onChange.bind(this);
        this._onSignUp = this._onSignUp.bind(this);
    }

    componentDidMount() {
        UserStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        UserStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(getStateFromStores());
    }

    _onSignUp(event) {
        event.preventDefault();
        if (!this.state.pending) {
            signUp(this.refs.username.value, this.refs.email.value, this.refs.passwd.value, this.refs.confirm.value);
        }
    }

    render() {
        return (
            <div className="signin">
                <Piwik />
                <div className="logo">
                    <div className="fa-stack fa-lg">
                      <i className="fa fa-circle-o fa-stack-2x"></i>
                      <i className="fa fa-tag fa-stack-1x"></i>
                    </div>
                </div>
                <div className="signin-title">Sign up to WID</div>
                <div className="signin-box">
                    <form onSubmit={this._onSignUp}>
                        <p>Username*</p>
                        <p><input placeholder="Your username" ref="username" required="true" type="text"/></p>
                        <p>Email*</p>
                        <p><input placeholder="Your email" ref="email" required="true" type="email"/></p>
                        <p>Password*</p>
                        <p><input placeholder="Your password" ref="passwd" required="true" type="password"/></p>
                        <p>Confirm the password*</p>
                        <p><input placeholder="Confirm the password" ref="confirm" required="true" type="password"/></p>
                        <input className="action" type="submit" value="Sign up"/>
                    </form>
                </div>
            </div>
        );
    }
}


Signup.propTypes = {
    location: React.PropTypes.object
};

Signup.contextTypes = {
};

export default Signup;

import React from "react";
import {Link} from "react-router";
import Online from "./Online";
import Message from "./Message";
import UserStore from "../stores/UserStore";
import {signOut} from "../actions/UserActions";

function getStateFromStores() {
    let user = UserStore.getUser();
    return {
        isAuth: UserStore.isAuth(),
        isSignOut: UserStore.isSignOut(),
        username: user && user.username
    };
}

class App extends React.Component {

    constructor() {
        super();
        this.state = getStateFromStores();
        this._onChange = this._onChange.bind(this);
        this._onSignOut = this._onSignOut.bind(this);
    }

    componentDidMount() {
        UserStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        UserStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(getStateFromStores());

        if (this.state.isSignOut) {
            this.context.router.replace("/site/signin");
        }
    }

    _onSignOut() {
        signOut();
    }

    render() {
        let menu = <div className="menu"></div>;
        if (this.state.isAuth) {
            menu = (
                <div className="menu">
                    <div className="user">
                        <div className="fa-stack fa-lg">
                          <i className="fa fa-square-o fa-stack-2x"></i>
                          <i className="fa fa-user fa-stack-1x"></i>
                        </div>
                        <div>{this.state.username}</div>
                        <div className="menu-shortcuts">
                            <Link activeClassName="selected" title="Go home" to="/site">
                                <i className="fa fa-home"></i>
                            </Link>
                            <a className="fa fa-sign-out" onClick={this._onSignOut} title="Sign out"></a>
                            <Online />
                        </div>
                    </div>

                    <div className="menu-separator"/>
                    <div className="menu-item">
                        <Link activeClassName="selected" to="/timeline">
                            <i className="fa fa-clock-o"></i>
                            Timeline
                        </Link>
                    </div>
                    <div className="menu-item">
                        <Link activeClassName="selected" to="/reports">
                            <i className="fa fa-bar-chart"></i>
                            Reports
                        </Link>
                    </div>
                    <div className="menu-item">
                        <Link activeClassName="selected" to="/tags">
                            <i className="fa fa-tags"></i>
                            Tags
                        </Link>
                    </div>
                    <div className="menu-item">
                        <Link activeClassName="selected" to="/config">
                            <i className="fa fa-cog"></i>
                            Configuration
                        </Link>
                    </div>
                </div>
            );
        }

        return (
            <div className="app">
                <div className="content">
                    {menu}
                    <div className="content-body">
                        <Message />
                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    }
}

App.propTypes = {
    children: React.PropTypes.object,
    location: React.PropTypes.object
};

App.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default App;

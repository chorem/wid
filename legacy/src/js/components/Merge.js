import React from "react";

import {mergeUpActivity, mergeDownActivity} from "../actions/TimelineActions";

class Merge extends React.Component {

    constructor(params) {
        super();

        this.state = {
            beginDate: +params.oldBegin,
            endDate: +params.oldEnd
        };

        this._onCancel = this._onCancel.bind(this);
        this._onMergeUp = this._onMergeUp.bind(this);
        this._onMergeDown = this._onMergeDown.bind(this);
    }

    _onCancel() {
        this.props.close();
    }

    _onMergeUp() {
        let oldBegin = +this.props.oldBegin;
        let oldEnd = +this.props.oldEnd;
        let overwrite = this.refs.overwrite.checked ? 1 : 0;

        mergeUpActivity(oldBegin, oldEnd, overwrite);
        this.props.close();
    }

    _onMergeDown() {
        let oldBegin = +this.props.oldBegin;
        let oldEnd = +this.props.oldEnd;
        let overwrite = this.refs.overwrite.checked ? 1 : 0;

        mergeDownActivity(oldBegin, oldEnd, overwrite);
        this.props.close();
    }

    render() {
        return (
            <div className="popup">
                <div>
                    <label>Overwrite:</label>
                    <input ref="overwrite" type="checkbox" value="true"/>
                </div>
                <div className="actions">
                    <div className="action fa fa-remove" onClick={this._onCancel}>Cancel</div>
                    <div className="action fa fa-arrow-up" onClick={this._onMergeUp}>Merge Up</div>
                    <div className="action fa fa-arrow-down" onClick={this._onMergeDown}>Merge Down</div>
                </div>
            </div>
        );
    }
}


Merge.propTypes = {
    close: React.PropTypes.func,
    oldBegin: React.PropTypes.number,
    oldEnd: React.PropTypes.number
};

Merge.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default Merge;

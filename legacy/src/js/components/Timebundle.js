/*eslint no-alert: 0*/
import React from "react";
import {getProjects, addProject, getProject, getTasks, addTask, removeTask, updateTask, sync} from "../actions/TimebundleActions";
import TimebundleStore from "../stores/TimebundleStore";
import Piwik from "./Piwik";

function getStateFromStores() {
    return {
        projects: TimebundleStore.getProjects(),
        projectName: TimebundleStore.getProjectName(),
        tasks: TimebundleStore.getTasks()
    };
}

class Timebundle extends React.Component {

    constructor() {
        super();

        this.state = getStateFromStores();

        this._onChange = this._onChange.bind(this);
        this._addProject = this._addProject.bind(this);
        this._getProject = this._getProject.bind(this);
        this._sync = this._sync.bind(this);
    }

    componentDidMount() {
        getProjects();
        TimebundleStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        TimebundleStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(getStateFromStores());
    }

    _addProject(event) {
        event.preventDefault();

        let url = this.refs.url.value;
        addProject(url);
    }

    _getProject() {
        let url = this.refs.projects.value;
        getProject(url);
        getTasks(url);
    }

    _addTask(task) {
        let projectUrl = this.refs.projects.value;
        let {id, url, name, query} = task;

        let value = prompt("Enter your query for the task " + name + ":", query);
        if (value !== null) {

            if (id) {
                updateTask(id, url, value);
            } else {
                addTask(projectUrl, url, value);
            }
        }
    }

    _removeTask(task) {
        let confirmed = confirm("Remove sync " + task.name + " ?");
        if (confirmed) {
            removeTask(task.id, task.url);
        }
    }

    _sync() {
        let force = this.refs.force.checked ? 1 : 0;
        sync(force);
    }

    projectDisplayName(url) {
        let result = url;
        let i = url.indexOf("#");
        if (i >= 0) {
            result = `${url.substr(i + 1)} (${url})`;
        }
        return result;
    }

    render() {
        let projects = this.state.projects.map((project, index) => {
            return <option key={index} value={project.projectUrl}>{this.projectDisplayName(project.projectUrl)}</option>;
        });
        projects.unshift(<option key={-1} value="">Select an url</option>);

        let tasks = this.state.tasks.map((task, index) => {
            return (
                <tr className="main-item" key={index}>
                    <td>{task.name}</td>
                    <td>{task.query || "No sync"}</td>
                    <td>
                        <span className="action" onClick={this._removeTask.bind(this, task)}>Remove query</span>
                    </td>
                    <td>
                        <span className="action" onClick={this._addTask.bind(this, task)}>Modify query</span>
                    </td>
                </tr>
            );
        });

        return (
            <div className="timebundle">
                <Piwik />
                <div className="main-separator">Projects</div>
                <div className="main-form">
                    <form onSubmit={this._addProject}>
                        <div>
                            <label>Url:</label>
                            <input ref="url" required="true" type="text"/>
                        </div>
                        <input className="action" type="submit" value="Add sync url"/>
                    </form>
                </div>

                <div className="main-separator">Tasks</div>
                <div className="main-form">
                    <div className="main-row">
                        <input id="force" ref="force" type="checkbox" value="true"/>
                        <label htmlFor="force">Force</label>
                        <input className="action" onClick={this._sync} type="button" value="Sync all projects"/>
                    </div>
                    <label>Available urls:</label>
                    <select onChange={this._getProject} ref="projects">
                        {projects}
                    </select>
                </div>

                <table className="main-body">
                    <thead className="main-thead">
                        <tr>
                            <td colSpan="4">{this.state.projectName || "No url selected"}</td>
                        </tr>
                    </thead>
                    <tbody>
                        {tasks}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Timebundle;

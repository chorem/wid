import React from "react";
import ReactDOM from "react-dom";
import Merge from "./Merge";
import Split from "./Split";
import moment from "moment";

import {addActivity, saveActivity, clearCompletion} from "../actions/TimelineActions";
import {diffTime} from "../utils/DateTimeUtils";
import {hashCode, tagsToArray, extractNote} from "../utils/StringUtils";

import Events from "./Events";
import Proposition from "./Proposition";

let ENTER_KEY_CODE = 13;
let TAB_KEY_CODE = 9;

class Item extends React.Component {

    constructor() {
        super();

        this.state = {
            edit: false,
            split: false,
            merge: false
        };

        this._onSplit = this._onSplit.bind(this);
        this._onMerge = this._onMerge.bind(this);
        this._onEdit = this._onEdit.bind(this);
        this._onBlur = this._onBlur.bind(this);
        this._onKeyDown = this._onKeyDown.bind(this);
        this._doProposition = this._doProposition.bind(this);
        this._onCopyPaste = this._onCopyPaste.bind(this);
        this._onFocus = this._onFocus.bind(this);
        this._onSetTag = this._onSetTag.bind(this);
        this._onSetTag = this._onSetTag.bind(this);
        this._addctivity = this._addctivity.bind(this);
        this._toggleEvents = this._toggleEvents.bind(this);
    }

    _onEdit() {
        this.setState({
            edit: true
        }, () => {
            this._focusInput();
        });
    }

    _focusInput() {
        let input = this.refs.input;
        let length = input.value.length;

        input.focus();
        input.setSelectionRange(length, length);
    }

    _onMerge(beginDateTime) {
        this.setState({
            edit: false,
            split: null,
            merge: beginDateTime
        });
    }

    _onSplit(beginDateTime) {
        this.setState({
            edit: false,
            split: beginDateTime,
            merge: null
        });
    }

    _onFocus() {
        this.setState({
            edit: true,
            split: null,
            merge: null
        });
    }

    _toggleEvents() {
        this.setState({
            events: !this.state.events
        });
    }

    _onBlur(e) {
        if (e.relatedTarget !== ReactDOM.findDOMNode(this.refs.proposition)) {
            this.setState({
                edit: false,
                tags: null
            });
            clearCompletion();
        }
    }

    _onKeyDown(event) {
        if (event.keyCode === ENTER_KEY_CODE) {
            event.preventDefault();
            this._addctivity();

        } else if (event.keyCode === TAB_KEY_CODE) {
            event.preventDefault();
            this.refs.proposition.setTagTab();
        }
    }

    _addctivity() {
        if (!this.props.pending) {
            let value = this.refs.input.value;
            if (value) {
                let tags = tagsToArray(value, true);
                let notes = extractNote(value);
                this.refs.input.value = "";

                let activity = this.props.activity;
                if (this.props.subClass === "first") {
                    addActivity(tags, activity.beginDateTime, undefined, notes);
                    clearCompletion();

                } else {
                    saveActivity(tags, activity.beginDateTime, activity.endDateTime, notes);
                    this.setState({
                        edit: false
                    });
                    clearCompletion();
                }
            }
        }
    }

    _onCopyPaste() {
        setTimeout(() => {
            this._doProposition();
        }, 0);
    }

    _onSetTag(tags) {
        this.refs.input.value = tags;
        this._doProposition();
    }

    _doProposition() {
        this.setState({
            edit: true,
            tags: {
                value: this.refs.input.value,
                index: this.refs.input.selectionStart
            }
        }, () => {
            this.refs.input.focus();
        });
    }

    _useActivity(activity) {
        addActivity(activity.tag);
        this.setState({
            edit: false,
            split: null,
            merge: null
        });
    }

    renderAction(activity) {
        let editAction = <span className="action" key={"editAction"} onClick={this._onEdit}>edit</span>;
        let mergeAction = (
            <span className="ext-action" key="mergeAction">
                <div className="action" onClick={this._onMerge.bind(this, activity.beginDateTime)}>
                    merge
                </div>
            </span>
        );
        if (activity.beginDateTime && activity.beginDateTime === this.state.merge) {
            mergeAction = (
                <span className="ext-action" key="mergeAction">
                    <div className="action" onClick={this._onMerge}>
                        merge
                    </div>
                    <Merge close={this._onMerge} oldBegin={activity.beginDateTime} oldEnd={activity.endDateTime}/>
                </span>
            );
        }

        let splitAction = (
            <span className="ext-action" key="splitAction">
                <div className="action" onClick={this._onSplit.bind(this, activity.beginDateTime)}>
                    split
                </div>
            </span>
        );
        if (activity.beginDateTime && activity.beginDateTime === this.state.split) {
            splitAction = (
                <span className="ext-action" key="splitAction">
                    <div className="action" onClick={this._onSplit}>
                        split
                    </div>
                    <Split begin={activity.beginDateTime} close={this._onSplit} end={activity.endDateTime}/>
                </span>
            );
        }

        let useAction = (
            <div className="action" key="useAction">
                <a className="action" onClick={this._useActivity.bind(this, activity)}>
                    use
                </a>
            </div>
        );
        let eventsAction = this.state.events ? (
            <div className="fa fa-angle-up action--events" key="eventsAction" onClick={this._toggleEvents}></div>
        ) : (
            <div className="fa fa-angle-down action--events" key="eventsAction" onClick={this._toggleEvents}></div>
        );
        let addAction = (
            <div className="action" key="addAction">
                <a className="action" onClick={this._addctivity.bind(this, activity)}>
                    add activity
                </a>
            </div>
        );

        if (this.props.subClass === "first") {
            return [addAction];
        } else if (activity.proposition) {
            return [editAction, eventsAction];
        }
        return [editAction, mergeAction, splitAction, useAction, eventsAction];
    }

    renderTagValue(activity) {
        let tagValue;

        if (this.state.edit || this.props.subClass === "first") {
            tagValue = (<textarea defaultValue={activity.tag && activity.tag.join(" ")}
                            onChange={this._doProposition}
                            onKeyDown={this._onKeyDown}
                            onPaste={this._onCopyPaste}
                            placeholder="Enter your activity tags then press enter."
                            ref="input"/>);

        } else {
            tagValue = <span>{activity.tag && activity.tag.join(" ")}</span>;
        }

        return tagValue;
    }

    render() {
        let {activity} = this.props;

        let tagsClass = activity.proposition ? "fa-thumb-tack" : "fa-tags";

        let hash = hashCode(activity.tag ? activity.tag.join("") : "");
        let color = Math.abs(hash % 4);

        let proposition;
        if (this.state.edit) {
            proposition = (<Proposition onSetTag={this._onSetTag}
                                        ref="proposition"
                                        tags={this.state.tags}/>);
        }

        let events;
        if (this.props.subClass === "first" || this.state.events) {
            events = (<Events beginDateTime={activity.beginDateTime}
                             endDateTime={activity.endDateTime || Number.MAX_VALUE}
                             noActions={this.props.subClass !== "first"}
                             onSetTag={this._onSetTag}/>);
        }

        let beginDateTime = moment(activity.beginDateTime);
        let endDateTime = moment(activity.endDateTime);

        let duration = diffTime(beginDateTime, endDateTime);

        let time = [<a key="beginDateTime" title={beginDateTime.format("L")}>{beginDateTime.format("LTS")}</a>];
        if (this.props.subClass !== "first") {
            time.push(" - ");
            time.push(<a key="endDateTime" title={endDateTime.format("L")}>{endDateTime.format("LTS")}</a>);
        }

        return (
            <div className="item-container">
                <div className={"item item--" + this.props.subClass}>
                    <div className={"icon fa fa-clock-o fa-2x" + " tags--color_" + color}></div>
                    <div className={"datetime datetime--" + this.props.subClass}>
                        <div>{duration}</div>
                        <div>{time}</div>
                    </div>

                    <div className={"icon fa fa-2x " + tagsClass}></div>
                    <div className={"tags tags--" + this.props.subClass} onBlur={this._onBlur} onFocus={this._onFocus}>
                        <div className={"tag tag--" + this.props.subClass}>
                            {this.renderTagValue(activity)}
                        </div>
                        {proposition}
                    </div>

                    <div className="icon fa fa-wrench fa-2x"></div>
                    <div className={"actions actions--" + this.props.subClass}>
                        {this.renderAction(activity)}
                    </div>
                </div>
                {events}
            </div>
        );
    }
}

Item.propTypes = {
    activity: React.PropTypes.object,
    pending: React.PropTypes.bool,
    subClass: React.PropTypes.string.isRequired
};

export default Item;

import React from "react";
import FacetStore from "../stores/FacetStore";
import {completion, openCompletion, closeCompletion} from "../actions/TimelineActions";
import {tagsToArray} from "../utils/StringUtils";

function getStateFromStores() {
    return {
        facets: FacetStore.getValues(),
        replace: FacetStore.isReplace(),
        map: FacetStore.getMap()
    };
}

class Proposition extends React.Component {

    constructor() {
        super();
        this.state = getStateFromStores();
        this._onChange = this._onChange.bind(this);
    }

    componentDidMount() {
        FacetStore.addChangeListener(this._onChange);
    }

    componentWillReceiveProps(props) {
        let tags = props.tags;
        if (props.tags && props.tags !== this.propstags) {

            let value = tags.value;
            let index = tags.index;
            let start = value.substring(0, index);
            let end = value.substring(index);

            let startArray = tagsToArray(start);
            let endArray = tagsToArray(end);

            // In comment ?
            if (start.startsWith("(")) {
                return;
            }

            // In midle of a words
            if (!start.endsWith(" ") && !end.startsWith(" ")) {
                endArray.shift();
            }

            completion(endArray.concat(startArray));
            this.propstags = props.tags;
            this.tags = {
                value: this.propstags.value.slice(),
                index: this.propstags.index
            };
        }
    }

    componentWillUnmount() {
        FacetStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(getStateFromStores());
    }

    _setTag(tag, path) {
        let tags = this.tags;
        let value = tags.value;
        let index = tags.index;
        let start = value.substring(0, index);
        let end = value.substring(index);

        let startArray = tagsToArray(start);
        let endArray = tagsToArray(end);

        if (!start.endsWith(" ") && this.state.replace) {
            startArray.pop();
        }
        if (!end.startsWith(" ")) {
            endArray.shift();
        }

        let all = startArray.concat(path).concat(endArray);
        this.props.onSetTag(all.join(" ") + " ");
    }

    _openTree(tag, path) {
        let tags = this.tags;
        let value = tags.value;
        let index = tags.index;
        let start = value.substring(0, index);
        let end = value.substring(index);

        let startArray = tagsToArray(start);
        let endArray = tagsToArray(end);

        if (!start.endsWith(" ") && this.state.replace) {
            startArray.pop();
        }
        if (!end.startsWith(" ")) {
            endArray.shift();
        }

        let all = startArray.concat(path).concat(endArray);
        openCompletion(all);
    }

    _closeTree(tag, path) {
        closeCompletion(path);
    }

    setTagTab() {
        let firstProposition = this.refs.proposition0;
        if (firstProposition) {
            let tag = firstProposition.firstChild.lastChild.textContent;
            this._setTag(tag, [tag]);
        }
    }

    renderMap(map, path) {
        let propositions = [];
        map && map.forEach((value, key) => {

            let elementPath = path.concat(key);
            let element;
            if (!value) {
                element = <div className="fa fa-plus-square-o" onClick={this._openTree.bind(this, key, elementPath)}></div>;
            } else {
                element = <div className="fa fa-minus-square-o" onClick={this._closeTree.bind(this, key, elementPath)}></div>;
            }

            propositions.push(
                <div className="action tag" key={"proposition_" + key} ref={!path.length ? "proposition" + propositions.length : ""}>
                    <div className="title">
                        {element}
                        <div onClick={this._setTag.bind(this, key, elementPath)}>{key}</div>
                    </div>

                    <div>
                        {this.renderMap(value, elementPath)}
                    </div>
                </div>
            );
        });
        return propositions;
    }

    render() {
        return (
            <div className="propositions" tabIndex="1">
                {this.renderMap(this.state.map, [])}
            </div>
        );
    }
}

Proposition.propTypes = {
    onSetTag: React.PropTypes.func,
    tags: React.PropTypes.object
};

export default Proposition;

/*eslint no-alert: 0*/
import React from "react";

import {aggregateEvents} from "../utils/AggregateUtils";
import {getEvents} from "../actions/EventActions";
import EventStore from "../stores/EventStore";

import EventsAggregation from "./EventsAggregation";

function getStateFromStores(beginDateTime, endDateTime) {
    let events = EventStore.getEvents(beginDateTime, endDateTime);
    return {
        events,
        aggregations: aggregateEvents(events),
        beginDateTime, endDateTime
    };
}

class Events extends React.Component {

    constructor({beginDateTime, endDateTime}) {
        super();

        this.state = {events: [], aggregations: new Map(), beginDateTime, endDateTime};
        getEvents(beginDateTime, endDateTime);

        this._onChange = this._onChange.bind(this);
    }

    componentDidMount() {
        EventStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        EventStore.removeChangeListener(this._onChange);
    }

    _onChange(beginDateTime, endDateTime) {
        if (!beginDateTime && !endDateTime
            || this.state.beginDateTime === beginDateTime && this.state.endDateTime === endDateTime) {

            this.setState(getStateFromStores(this.props.beginDateTime, this.props.endDateTime));
        }
    }

    render() {
        return (
            <div className="events">
                <EventsAggregation events={this.state.aggregations} noActions={this.props.noActions} onSetTag={this.props.onSetTag}/>
            </div>
        );
    }
}

Events.propTypes = {
    beginDateTime: React.PropTypes.number,
    endDateTime: React.PropTypes.number,
    noActions: React.PropTypes.bool,
    onSetTag: React.PropTypes.func
};

export default Events;

/*eslint no-alert: 0*/
import React from "react";
import moment from "moment";
import Markdown from "react-remarkable";

import RawHtml from "./RawHtml";
import EventsAggregation from "./EventsAggregation";

import {aggregateEvents} from "../utils/AggregateUtils";
import {removeEvent} from "../actions/EventActions";

import {addActivity, insertActivity} from "../actions/TimelineActions";
import {diffValue} from "../utils/DateTimeUtils";
import {extractNote, tagsToArray} from "../utils/StringUtils";

class EventAggregation extends React.Component {

    _toggleDetails(event, refDetailsArrow) {
        let detailsArrow = this.refs[refDetailsArrow];
        detailsArrow.classList.toggle("fa-angle-down");
        detailsArrow.classList.toggle("fa-angle-up");

        if (event.subAggregation) {
            event.subAggregation = null;
        } else {
            event.subAggregation = aggregateEvents(event.events, event.aggregations, event.displays);
        }
        this.forceUpdate();
    }

    _toggleShowBig(event, refShowBigSign) {
        let showBigSign = this.refs[refShowBigSign];
        showBigSign.classList.toggle("fa-expand");
        showBigSign.classList.toggle("fa-compress");

        if (event.showBig) {
            event.showBig = null;
        } else {
            event.showBig = true;
        }
        this.forceUpdate();
    }

    _removeEvent(event) {
        let tags = event.tag || "unrecognized event";
        let confirmed = confirm(`Are you sure to remove event \"${tags}\" ?`);
        confirmed && removeEvent(event.id);
    }

    _markUse(event) {
        if (event && this.props.marked) {
            let tagsValue = event.tag || "unrecognized event";
            let note = extractNote(tagsValue);
            let tags = tagsToArray(tagsValue, true);

            let beginDateTime = Math.min(this.props.marked, event.endDateTime);
            let endDateTime = Math.max(this.props.marked, event.endDateTime);

            addActivity(tags, beginDateTime, endDateTime, note);
        } else {
            this.props.onMark(event && event.beginDateTime);
        }
    }

    _markSplit(event) {
        if (event && this.props.marked) {
            let tagsValue = event.tag || "unrecognized event";
            let note = extractNote(tagsValue);
            let tags = tagsToArray(tagsValue, true);

            let beginDateTime = Math.min(this.props.marked, event.endDateTime);
            let endDateTime = Math.max(this.props.marked, event.endDateTime);

            insertActivity(tags, beginDateTime, endDateTime, note);
        } else {
            this.props.onMark(event && event.beginDateTime);
        }
    }

    _setTag(tag) {
        this.props.onSetTag(tag, false);
    }

    transformToJSX(content) {
        let result;
        if (typeof content === "string") {
            result = [];
            let last = 0;
            content.replace(/(?:<html>((?:.|\n|\r)*?)<\/html>)/g, function(match, html, offset, all) {
                offset - last && result.push((<Markdown key="head" source={all.substring(last, offset)} />));
                last = offset + match.length;
                html && result.push((<RawHtml key="body" source={html} />));
                return "";
            });
            content.length - last && result.push((<Markdown key="footer" source={content.substr(last)} />));
        } else {
            // content is Object then jsx
            result = content;
        }
        return result;
    }

    render() {
        let event = this.props.event;

        let dates;
        let beginDateTime = +event.beginDateTime;
        let endDateTime = +event.endDateTime;

        if (beginDateTime !== endDateTime) {
            dates = (
                <div className="event-dates">
                    <span className="event-date">
                        <a title={moment(beginDateTime).format("L")}>{moment(beginDateTime).format("LTS")}</a> -
                        <a title={moment(beginDateTime).format("L")}>{moment(endDateTime).format("LTS")}</a>
                    </span>
                    <span className="event-duration">{diffValue(endDateTime - beginDateTime)}</span>
                </div>
            );
        } else {
            dates = (
                <div className="event-date">
                    <span className="event-date">
                        <a title={moment(beginDateTime).format("L")}>{moment(beginDateTime).format("LTS")}</a>
                    </span>
                </div>
            );
        }

        let actions = [];
        let firstItem = !this.props.noActions;
        let marked = this.props.marked;
        let markedFrom = this.props.marked === event.beginDateTime;
        let withDuration = event.endDateTime !== event.beginDateTime;
        let isAggregation = !!event.events;

        let tagsInfo = event.tag || JSON.stringify(event, null, " ");
        let tagsValue = event.tag || "unrecognized event";
        let note = extractNote(tagsValue);
        let tags = tagsToArray(tagsValue, true);

        withDuration && firstItem &&
            actions.push(<a className="action" key="0" onClick={addActivity.bind(null, tags, event.beginDateTime, event.endDateTime, note)} title={tagsInfo}>Use</a>);

        withDuration && !firstItem &&
            actions.push(<a className="action" key="1" onClick={insertActivity.bind(null, tags, event.beginDateTime, event.endDateTime, note)} title={tagsInfo}>Split</a>);

        !firstItem && !isAggregation && !marked &&
            actions.push(<a className="action" key="2" onClick={this._markSplit.bind(this, event)} title={tagsInfo}>Split from</a>);

        !firstItem && !isAggregation && marked && !markedFrom &&
            actions.push(<a className="action" key="3" onClick={this._markSplit.bind(this, event)} title={tagsInfo}>Split to</a>);

        !firstItem && !isAggregation && marked && markedFrom &&
            actions.push(<a className="action" key="4" onClick={this._markSplit.bind(this, null)}>Cancel split</a>);

        firstItem && !isAggregation && !marked &&
            actions.push(<a className="action" key="5" onClick={this._markUse.bind(this, event)} title={tagsInfo}>Use from</a>);

        firstItem && !isAggregation && marked && !markedFrom &&
            actions.push(<a className="action" key="6" onClick={this._markUse.bind(this, event)} title={tagsInfo}>Use to</a>);

        firstItem && !isAggregation && marked && markedFrom &&
            actions.push(<a className="action" key="7" onClick={this._markUse.bind(this, null)}>Cancel use</a>);

        firstItem &&
            actions.push(<a className="action" key="8" onClick={this._setTag.bind(this, tagsValue)} title={tagsInfo}>Add tag</a>);

        !isAggregation &&
            actions.push(<a className="action" key="9" onClick={this._removeEvent.bind(this, event)}>Remove</a>);

        isAggregation &&
            actions.push(
                <a className="action" key="10" onClick={this._toggleDetails.bind(this, event, "detailsArrow")}>
                    ({event.events.length})
                    <span className="fa fa-angle-down" ref="detailsArrow"/>
                </a>
            );

        let showBigClass = event.showBig ? "fa fa-compress" : "fa fa-expand";
        event.details &&
            actions.push(
                <a className="action" key="11" onClick={this._toggleShowBig.bind(this, event, "showBigSign")}>
                    <span className={showBigClass} ref="showBigSign"/>
                </a>
            );

        let icons = {
            scm: "fa-code-fork",
            code: "fa-code",
            idle: "fa-pause",
            application: "fa-cog",
            note: "fa-sticky-note-o",
            shell: "fa-terminal",
            task: "fa-tasks",
            call: "fa-phone",
            calendar: "fa-calendar",
            step: "fa-male",
            heartbeat: "fa-heart",
            location: "fa-compass",

            "app duration": "fa-cog",
            "step sum": "fa-male",
            notes: "fa-sticky-note-o"
        };

        let display = this.transformToJSX(event.display || "unrecognized event");
        let big = event.showBig && this.transformToJSX(event.details) || "";
        return (
            <div className="event">
                <div className="event-data">
                    <div className="event-time">
                        <a className={"event-type fa " + (icons[event.type] || "fa-asterisk")} title={event.type + " " + event.source}></a>
                        {dates}
                    </div>
                    <div className="event-value">
                        {display}
                    </div>
                    <div className="event-actions">
                        {actions}
                    </div>
                </div>
                <div className="subEvents">
                    <EventsAggregation
                        events={event.subAggregation}
                        marked={this.props.marked}
                        noActions={this.props.noActions}
                        onMark={this.props.onMark}
                        onSetTag={this.props.onSetTag}/>
                </div>
                <div className="event-big">
                    {big}
                </div>
            </div>
        );
    }
}

EventAggregation.propTypes = {
    event: React.PropTypes.object,
    marked: React.PropTypes.number,
    noActions: React.PropTypes.bool,
    onMark: React.PropTypes.func,
    onSetTag: React.PropTypes.func
};

export default EventAggregation;

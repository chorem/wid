/*eslint no-alert: 0*/
import React from "react";
import FacetStore from "../stores/FacetStore";
import {facetFilter} from "../actions/TimelineActions";
import {diffValue} from "../utils/DateTimeUtils";

function getStateFromStores(params) {
    let tags = params.tags || params && params.splat && params.splat.split("/") || [];
    return {
        tags,
        facets: FacetStore.getValues()
    };
}

class Facets extends React.Component {

    constructor({params}) {
        super();

        this.state = getStateFromStores(params);
        this._onChange = this._onChange.bind(this);
    }

    componentDidMount() {
        facetFilter(this.state.tags);
        FacetStore.addChangeListener(this._onChange);
    }

    componentWillReceiveProps({params}) {
        this.setState(getStateFromStores(params), () => {
            facetFilter(this.state.tags);
        });
    }

    componentWillUnmount() {
        FacetStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(getStateFromStores(this.state));
    }

    _addTag(tag) {
        this.context.router.push(this.props.location.pathname + "/" + tag);
    }

    render() {
        let facets = this.state.facets.map((facet) => {
            return (
                <tr className="main-item" key={facet.tag}>
                    <td><a className="action" onClick={this._addTag.bind(this, facet.tag)}>{facet.tag}</a></td>
                    <td>{facet.count}</td>
                    <td>{diffValue(facet.duration)}</td>
                </tr>
            );
        });

        return (
            <div className="facets main">
                <div className="main-table">
                    <div className="main-separator">Facets</div>
                    <table className="main-body">
                        <tbody>
                            <tr className="main-item">
                                <td>{this.state.tags && this.state.tags.join(" ")}</td>
                            </tr>
                            {facets}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

Facets.propTypes = {
    location: React.PropTypes.object
};

Facets.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default Facets;

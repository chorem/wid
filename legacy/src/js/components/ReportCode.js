import React from "react";
import {getReferenceTime, forEachPeriods} from "../utils/DateTimeUtils";
import {Pie, Line} from "react-chartjs";

class ReportCode extends React.Component {

    render() {
        let languages = this.props.languages;
        let period = this.props.period;
        let projects = this.props.projects;
        let refTime = getReferenceTime(period);

        // For languages
        let count = languages && languages.reduce((previous, current) => {
            return previous + current.total;
        }, 0);

        let data = languages && languages.map((value, index) => {
            return {
                label: value.language,
                value: (value.total / count * 100).toFixed(2),
                color: "rgba(21, 164, " + (250 - index * 40) + ", 1)"
            };
        });

        // For projects
        let projectNames = new Set(projects && projects.map(item => {return item.project;}));
        let labels = new Set();

        let datasets = [];
        Array.from(projectNames).forEach((projectName, index) => {
            let dataset = {
                label: projectName,
                fillColor: "transparent",
                strokeColor: "rgba(21, 164, " + (250 - index * 80) + ", 1)",
                pointColor: "rgba(21, 164, " + (250 - index * 80) + ", 1)",
                pointStrokeColor: "white",
                pointHighlightFill: "#AE69AF",
                data: []
            };
            datasets.push(dataset);

            forEachPeriods(period, this.props.date, (date, format, formatShort, formatAggregation) => {
                let value = projects.find((item) => {
                    return item.period === formatAggregation && item.project === projectName;
                });

                dataset.data.push(value && value.duration || 0);
                labels.add(format);
            });
        });

        return (
            <div className="report-code">
                <Pie data={data}
                height="250px"
                options={{
                    animation: false,
                    tooltipTemplate: "<%=label%>: <%=value%>%"
                }}
                redraw
                width="250px"/>
                <Line data={{
                    labels: Array.from(labels),
                    datasets: datasets
                }}
                height="300px"
                options={{
                    animation: false,
                    scaleOverride: true,
                    scaleSteps: 5,
                    scaleStepWidth: refTime / 5,
                    scaleStartValue: 0,
                    scaleLabel: "<%=diffValue(value)%>",
                    tooltipTemplate: "<%=diffValue(value)%>",
                    multiTooltipTemplate: "<%=datasetLabel%>: <%=diffValue(value)%>"
                }}
                redraw
                width="400px"/>
            </div>
        );
    }
}

ReportCode.propTypes = {
    date: React.PropTypes.number,
    languages: React.PropTypes.array,
    period: React.PropTypes.number.isRequired,
    projects: React.PropTypes.array
};

ReportCode.contextTypes = {
};

export default ReportCode;

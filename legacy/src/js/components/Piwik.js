import React from "react";

class Piwik extends React.Component {

    componentDidMount() {
        let _paq = window._paq || [];
        _paq.push(["setCustomUrl", location.toString()]);
        _paq.push(["trackPageView", location.pathname]);
    }

    render() {
        return null;
    }
}

Piwik.propTypes = {
};

Piwik.contextTypes = {
};

export default Piwik;

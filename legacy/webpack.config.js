/* eslint strict: 0 */
/* eslint no-process-env: 0 */

"use strict";

let path = require("path");
let ExtractTextPlugin = require("extract-text-webpack-plugin");
let HtmlWebpackPlugin = require("html-webpack-plugin");
let CrxWebpackPlugin = require("crx-webpack-plugin");
let AppCachePlugin = require("appcache-webpack-plugin");
let serverContext = process.env.WID_SERVER_CONTEXT || "";
let glob = require("glob");

let assetsImages = glob.sync("assets/img/**", {
    cwd: "./src",
    nodir: true
});

module.exports = {
    entry: [
        "./src/js/index",
        "./src/scss/main.scss"
    ],
    output: {
        path: path.join(__dirname, "/dist"),
        filename: "bundle.js"
    },
    debug: true,
    devtool: "source-map",
    module: {
        preLoaders: [
            {
                test: /\.js?$/,
                loader: "eslint",
                exclude: /node_modules/
            }
        ],
        loaders: [
            {
                test: /\.js$/,
                include: path.join(__dirname, "src"),
                loader: "babel-loader",
                query: {
                    presets: ["es2015", "react"]
                }
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract(["css", "sass"])
            }
        ]
    },
    eslint: {
        failOnWarning: false,
        failOnError: true
    },
    sassLoader: {
        data: "$serverContext: '" + serverContext + "';"
    },
    plugins: [
        new AppCachePlugin({
            cache: [
                "index.html",
                "assets/font-awesome-4.6.3/css/font-awesome.min.css",
                "assets/font-awesome-4.6.3/fonts/fontawesome-webfont.eot?v=4.6.3",
                "assets/font-awesome-4.6.3/fonts/fontawesome-webfont.woff2?v=4.6.3",
                "assets/font-awesome-4.6.3/fonts/fontawesome-webfont.woff?v=4.6.3",
                "assets/font-awesome-4.6.3/fonts/fontawesome-webfont.ttf?v=4.6.3",
                "assets/OpenSans/OpenSans-Regular.ttf"
            ]
            .concat(assetsImages)
            .map((fileName) => {
                return serverContext + "/" + fileName;
            }),
            fallback: [serverContext + "/assets/online.js " + serverContext + "/assets/offline.js"],
            network: ["*"]
        }),
        new ExtractTextPlugin("styles.css"),
        new HtmlWebpackPlugin({
            title: "WID",
            template: "index.ejs",
            inject: false,
            serverContext,
            manifest: "/manifest.appcache"
        }),
        new CrxWebpackPlugin({
            keyFile: "key.pem",
            contentPath: "src/extension",
            outputPath: "dist",
            name: "wid-ext"
        })
    ]
};
